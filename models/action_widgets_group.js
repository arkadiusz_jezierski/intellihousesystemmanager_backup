const mongoose = require('mongoose');
const config = require('../config/database');
const ActionWidget = require('../models/action_widget');


// ActionWidgetsGroup Schema
const ActionWidgetsGroupSchema = mongoose.Schema({
    key: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    }
});

const ActionWidgetsGroup = module.exports = mongoose.model('ActionWidgetsGroup', ActionWidgetsGroupSchema);

module.exports.addUpdateActionWidgetsGroup = function(newGroup, callback){
    //console.log('>>> newGroup: ', newGroup);
    //console.log('>>> newGroup key: ', newGroup.key);
    const query = {key: newGroup.key};
    const update = { name: newGroup.name, key: newGroup.key };
    const options  = { upsert: true };
    console.log('>>> query: ', query);
    console.log('>>> update: ', update);
    ActionWidgetsGroup.findOneAndUpdate(query, update, options, function(error, result) {
        callback(error);
    });
}

module.exports.insertActionWidgetsGroups = function(groups, callback){
    ActionWidgetsGroup.collection.insert(groups, callback);
}

module.exports.getActionWidgetsGroups = function(callback){
    const query = {}
    ActionWidgetsGroup.find(query, callback);
}

module.exports.getActionWidgetsGroup = function(groupKey, callback){
    const query = {key: groupKey}
    ActionWidgetsGroup.find(query, callback);
}

module.exports.deleteActionWidgetsGroup = function(groupKey, callback){
    const query = { key : groupKey };
    ActionWidget.deleteActionWidgets(groupKey, function(error, result) {
        if (!error) {
            ActionWidgetsGroup.remove(query, callback);
        } else {
            callback(error);
        }
        
    });
    
}

// module.exports.getActionWidgetsGroupByName = function(groupName, callback){
//     const query = {name: groupName}
//     ActionWidgetsGroup.findOne(query, callback);
// }

// module.exports.getDevicesByKey = function(keyNames, callback){
//     Device.find({key: { $in: keyNames }}).exec(callback);
// }

// module.exports.removeDevices = function(callback){
//     Device.remove({}, callback);
// }