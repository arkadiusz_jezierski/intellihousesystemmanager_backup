const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

var passwd = require('passwd-linux');

// // User Schema
// const UserSchema = mongoose.Schema({
//     name: {
//         type: String
//     },
//     email: {
//         type: String,
//         required: true
//     },
//     username: {
//         type: String,
//         required: true
//     },
//     password: {
//         type: String,
//         required: true
//     }
// });
//
// const User = module.exports = mongoose.model('User', UserSchema);
//
// module.exports.getUserById = function(id, callback){
//     User.findById(id, callback);
// }
//
// module.exports.getUserByUsername = function(username, callback){
//     const query = {username: username}
//     User.findOne(query, callback);
// }
//
// module.exports.addUser = function(newUser, callback){
//     bcrypt.genSalt(10, (err, salt) => {
//         bcrypt.hash(newUser.password, salt, (err, hash) => {
//         if(err) throw err;
//     newUser.password = hash;
//     newUser.save(callback);
// });
// });
// }
//
// module.exports.comparePassword = function(candidatePasssword, hash, callback) {
//     bcrypt.compare(candidatePasssword, hash, (err, isMatch) => {
//         if(err) throw err;
//         callback(null, isMatch);
//     });
// }

module.exports.validatePassword = function(username, password, callback) {
     passwd.checkPass(username, password,  function (error, response) {
         "use strict";
         console.log('>>>> validatePassword response: ', response);
         if (error) {
             console.log('err: ' , error);
             callback(error, false, response);
         } else {
             if (response == 'passwordCorrect') {
                 callback(null, true, response);
             } else {
                 callback(null, false, response);
             }
         }
     });
}