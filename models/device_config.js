const mongoose = require('mongoose');
const config = require('../config/database');


// DeviceConfig Schema
const DeviceConfigSchema = mongoose.Schema({
    key: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    parameter: {
        type: String,
        required: true
    },
    value: {
        type: Number,
        required: true
    }
});

const DeviceConfig = module.exports = mongoose.model('DeviceConfig', DeviceConfigSchema);

module.exports.addConfig = function(config, callback){
    const query = {key: config.key,
                    address: config.address,
                    category: config.category,
                    parameter: config.parameter
    };
    const update = {
                    value: config.value
    };

    const options  = { upsert: true };
    DeviceConfig.findOneAndUpdate(query, update, options, function(error, result) {
        callback(error);
    });
}

module.exports.getConfigByKey = function(keyName, callback){
    const query = {key: keyName}
    DeviceConfig.find(query, callback);
}

module.exports.removeConfigs = function(callback){
    DeviceConfig.remove({}, callback);
}

module.exports.getAllDeviceConfigs = function(callback){
    DeviceConfig.find({}, callback);
}

module.exports.insertAllDeviceConfigs = function(configs, callback){
    DeviceConfig.collection.insert(configs, callback);
}