const mongoose = require('mongoose');
const config = require('../config/database');
var ObjectId = require('mongoose').Types.ObjectId;

// BasicConfig Schema
const ClientLogSchema = mongoose.Schema({
    log: {
        type: String,
        required: true
    }
});

const ClientLog = module.exports = mongoose.model('ClientLog', ClientLogSchema);

module.exports.insertLog = function(data, callback){
    const log = { log: data.log };
    ClientLog.create(log, function(error, result) {});

}

module.exports.getLatestLogs = function(lastLogId, filter, callback){

    if (lastLogId == '') {
        if (filter == '') {
            ClientLog.find({}).sort('-_id').limit(50).exec(callback);
        } else {
            query = { log: { $regex: '.*' + filter + '.*' } };
            ClientLog.find(query).sort('-_id').limit(50).exec(callback);
        }
    } else {
        var query;
        if (filter == '') {
            query = { _id: { $gt: new ObjectId(lastLogId) }};
        } else {
            query = { _id: { $gt: new ObjectId(lastLogId) }, log: { $regex: '.*' + filter + '.*' } };
        }

        ClientLog.find(query).sort('-_id').exec(callback);
    }
    
}

module.exports.getOlderLogs = function(oldestLogId, filter, callback){

    var query;

    if (filter == '') {
        query = { _id: { $lt: new ObjectId(oldestLogId) }};
    } else {
        query = { _id: { $lt: new ObjectId(oldestLogId) } , log: { $regex: '.*' + filter + '.*' } };
    }

    ClientLog.find(query).sort('-_id').limit(50).exec(callback);
    
    
}

module.exports.getLogsQnty = function(callback){

    ClientLog.count().exec(callback);
    
}

module.exports.dropOldLogs = function(callback){

    var timestamp = new Date();
    timestamp.setDate(timestamp.getDate()-7);

    var hexSeconds = Math.floor(timestamp/1000).toString(16);
    var constructedObjectId = ObjectId(hexSeconds + "0000000000000000");

    const query = { _id: { $lt: constructedObjectId }};
    ClientLog.find(query).sort('-_id').remove().exec(callback);
    
}