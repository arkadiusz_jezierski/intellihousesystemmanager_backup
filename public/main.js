(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<app-navbar></app-navbar>-->\n<!--<div class=\"container\">-->\n  <!--<flash-messages></flash-messages>-->\n  <!--<router-outlet></router-outlet>-->\n<!--</div>-->\n\n<view></view>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app works!';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_event_manager_event_manager_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/event-manager/event-manager.component */ "./src/app/components/event-manager/event-manager.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/navbar/navbar.component */ "./src/app/components/navbar/navbar.component.ts");
/* harmony import */ var _components_view_view_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/view/view.component */ "./src/app/components/view/view.component.ts");
/* harmony import */ var _components_device_manager_device_manager_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/device-manager/device-manager.component */ "./src/app/components/device-manager/device-manager.component.ts");
/* harmony import */ var _components_operation_operation_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/operation/operation.component */ "./src/app/components/operation/operation.component.ts");
/* harmony import */ var _components_condition_condition_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/condition/condition.component */ "./src/app/components/condition/condition.component.ts");
/* harmony import */ var _components_action_action_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/action/action.component */ "./src/app/components/action/action.component.ts");
/* harmony import */ var _components_time_picker_time_picker_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/time-picker/time-picker.component */ "./src/app/components/time-picker/time-picker.component.ts");
/* harmony import */ var _components_living_room_living_room_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/living-room/living-room.component */ "./src/app/components/living-room/living-room.component.ts");
/* harmony import */ var _components_living_room_lights_living_room_lights_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/living-room-lights/living-room-lights.component */ "./src/app/components/living-room-lights/living-room-lights.component.ts");
/* harmony import */ var _components_living_room_wall_effect_living_room_wall_effect_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/living-room-wall-effect/living-room-wall-effect.component */ "./src/app/components/living-room-wall-effect/living-room-wall-effect.component.ts");
/* harmony import */ var _components_color_picker_color_picker_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/color-picker/color-picker.component */ "./src/app/components/color-picker/color-picker.component.ts");
/* harmony import */ var _components_living_room_ceiling_effect_living_room_ceiling_effect_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/living-room-ceiling-effect/living-room-ceiling-effect.component */ "./src/app/components/living-room-ceiling-effect/living-room-ceiling-effect.component.ts");
/* harmony import */ var _components_sockets_sockets_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/sockets/sockets.component */ "./src/app/components/sockets/sockets.component.ts");
/* harmony import */ var _components_alarm_panel_alarm_panel_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/alarm-panel/alarm-panel.component */ "./src/app/components/alarm-panel/alarm-panel.component.ts");
/* harmony import */ var _components_device_advance_control_pwm_device_advance_control_pwm_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/device-advance-control-pwm/device-advance-control-pwm.component */ "./src/app/components/device-advance-control-pwm/device-advance-control-pwm.component.ts");
/* harmony import */ var _components_device_advance_control_rgb_device_advance_control_rgb_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/device-advance-control-rgb/device-advance-control-rgb.component */ "./src/app/components/device-advance-control-rgb/device-advance-control-rgb.component.ts");
/* harmony import */ var _components_radial_color_picker_radial_color_picker_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/radial-color-picker/radial-color-picker.component */ "./src/app/components/radial-color-picker/radial-color-picker.component.ts");
/* harmony import */ var _components_actions_widgets_list_actions_widgets_list_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./components/actions-widgets-list/actions-widgets-list.component */ "./src/app/components/actions-widgets-list/actions-widgets-list.component.ts");
/* harmony import */ var _components_actions_widgets_actions_widgets_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./components/actions-widgets/actions-widgets.component */ "./src/app/components/actions-widgets/actions-widgets.component.ts");
/* harmony import */ var _components_widgets_action_widget_slider_action_widget_slider_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./components/widgets/action-widget-slider/action-widget-slider.component */ "./src/app/components/widgets/action-widget-slider/action-widget-slider.component.ts");
/* harmony import */ var _components_widgets_action_widget_color_picker_action_widget_color_picker_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./components/widgets/action-widget-color-picker/action-widget-color-picker.component */ "./src/app/components/widgets/action-widget-color-picker/action-widget-color-picker.component.ts");
/* harmony import */ var _components_widgets_action_widget_buttons_action_widget_buttons_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./components/widgets/action-widget-buttons/action-widget-buttons.component */ "./src/app/components/widgets/action-widget-buttons/action-widget-buttons.component.ts");
/* harmony import */ var _components_widgets_action_widget_rgb_slider_action_widget_rgb_slider_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./components/widgets/action-widget-rgb-slider/action-widget-rgb-slider.component */ "./src/app/components/widgets/action-widget-rgb-slider/action-widget-rgb-slider.component.ts");
/* harmony import */ var _components_widget_action_select_widget_action_select_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./components/widget-action-select/widget-action-select.component */ "./src/app/components/widget-action-select/widget-action-select.component.ts");
/* harmony import */ var _components_widget_actions_list_widget_actions_list_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./components/widget-actions-list/widget-actions-list.component */ "./src/app/components/widget-actions-list/widget-actions-list.component.ts");
/* harmony import */ var _utils_longPress__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./utils/longPress */ "./src/app/utils/longPress.ts");
/* harmony import */ var _utils_touchButton__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./utils/touchButton */ "./src/app/utils/touchButton.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_36___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_36__);
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var angular_weather_widget__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! angular-weather-widget */ "./node_modules/angular-weather-widget/index.js");
/* harmony import */ var ngx_ui_switch__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ngx-ui-switch */ "./node_modules/ngx-ui-switch/ui-switch.es5.js");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");
/* harmony import */ var _services_validate_service__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./services/validate.service */ "./src/app/services/validate.service.ts");
/* harmony import */ var _services_actions_widgets_service__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./services/actions-widgets.service */ "./src/app/services/actions-widgets.service.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var _services_tools_service__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./services/tools.service */ "./src/app/services/tools.service.ts");
/* harmony import */ var _services_config_service__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./services/config.service */ "./src/app/services/config.service.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _services_operation_manager_service__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./services/operation-manager.service */ "./src/app/services/operation-manager.service.ts");
/* harmony import */ var _services_builder_service__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./services/builder.service */ "./src/app/services/builder.service.ts");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./guards/auth.guard */ "./src/app/guards/auth.guard.ts");
/* harmony import */ var _pipes_hex_transform_pipe__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./pipes/hex-transform.pipe */ "./src/app/pipes/hex-transform.pipe.ts");
/* harmony import */ var _pipes_name_filter_pipe__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./pipes/name-filter.pipe */ "./src/app/pipes/name-filter.pipe.ts");
/* harmony import */ var _pipes_safe_pipe__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./pipes/safe.pipe */ "./src/app/pipes/safe.pipe.ts");
/* harmony import */ var ngx_order_pipe__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ngx-order-pipe */ "./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js");
/* harmony import */ var _pipes_logs_filter_pipe__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./pipes/logs-filter.pipe */ "./src/app/pipes/logs-filter.pipe.ts");
/* harmony import */ var _components_backup_restore_backup_restore_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./components/backup-restore/backup-restore.component */ "./src/app/components/backup-restore/backup-restore.component.ts");































































var appRoutes = [
    { path: '', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"] },
    { path: 'devicemanager', component: _components_device_manager_device_manager_component__WEBPACK_IMPORTED_MODULE_11__["DeviceManagerComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'eventmanager', component: _components_event_manager_event_manager_component__WEBPACK_IMPORTED_MODULE_7__["EventManagerComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'operation/:id/:src', component: _components_operation_operation_component__WEBPACK_IMPORTED_MODULE_12__["OperationComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'condition/:id/:clone', component: _components_condition_condition_component__WEBPACK_IMPORTED_MODULE_13__["ConditionComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'action/:id', component: _components_action_action_component__WEBPACK_IMPORTED_MODULE_14__["ActionComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'livingroom', component: _components_living_room_living_room_component__WEBPACK_IMPORTED_MODULE_16__["LivingRoomComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'livingroomlights/:id', component: _components_living_room_lights_living_room_lights_component__WEBPACK_IMPORTED_MODULE_17__["LivingRoomLightsComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'livingroomwalleffect', component: _components_living_room_wall_effect_living_room_wall_effect_component__WEBPACK_IMPORTED_MODULE_18__["LivingRoomWallEffectComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'livingroomceilingeffect', component: _components_living_room_ceiling_effect_living_room_ceiling_effect_component__WEBPACK_IMPORTED_MODULE_20__["LivingRoomCeilingEffectComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'sockets', component: _components_sockets_sockets_component__WEBPACK_IMPORTED_MODULE_21__["SocketsComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'colorpicker/:cat/:adr/:r/:g/:b/:ret', component: _components_color_picker_color_picker_component__WEBPACK_IMPORTED_MODULE_19__["ColorPickerComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'alarmpanel', component: _components_alarm_panel_alarm_panel_component__WEBPACK_IMPORTED_MODULE_22__["AlarmPanelComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'widgetsgroupslist', component: _components_actions_widgets_list_actions_widgets_list_component__WEBPACK_IMPORTED_MODULE_26__["ActionsWidgetsListComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'widgets/:edit/:key', component: _components_actions_widgets_actions_widgets_component__WEBPACK_IMPORTED_MODULE_27__["ActionsWidgetsComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'widgetactionslist', component: _components_widget_actions_list_widget_actions_list_component__WEBPACK_IMPORTED_MODULE_33__["WidgetActionsListComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] },
    { path: 'backuprestore', component: _components_backup_restore_backup_restore_component__WEBPACK_IMPORTED_MODULE_56__["BackupRestoreComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"]] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_9__["NavbarComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"],
                _components_event_manager_event_manager_component__WEBPACK_IMPORTED_MODULE_7__["EventManagerComponent"],
                _components_view_view_component__WEBPACK_IMPORTED_MODULE_10__["ViewComponent"],
                _components_device_manager_device_manager_component__WEBPACK_IMPORTED_MODULE_11__["DeviceManagerComponent"],
                _components_operation_operation_component__WEBPACK_IMPORTED_MODULE_12__["OperationComponent"],
                _components_condition_condition_component__WEBPACK_IMPORTED_MODULE_13__["ConditionComponent"],
                _components_action_action_component__WEBPACK_IMPORTED_MODULE_14__["ActionComponent"],
                _components_time_picker_time_picker_component__WEBPACK_IMPORTED_MODULE_15__["TimePickerComponent"],
                _components_living_room_living_room_component__WEBPACK_IMPORTED_MODULE_16__["LivingRoomComponent"],
                _components_living_room_lights_living_room_lights_component__WEBPACK_IMPORTED_MODULE_17__["LivingRoomLightsComponent"],
                _components_living_room_lights_living_room_lights_component__WEBPACK_IMPORTED_MODULE_17__["LightAdjustComponent"],
                _components_alarm_panel_alarm_panel_component__WEBPACK_IMPORTED_MODULE_22__["AlarmPanelComponent"],
                _components_device_manager_device_manager_component__WEBPACK_IMPORTED_MODULE_11__["DeviceAdvanceCommandComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__["LogsSettingsComponent"],
                _utils_longPress__WEBPACK_IMPORTED_MODULE_34__["LongPress"],
                _utils_touchButton__WEBPACK_IMPORTED_MODULE_35__["TouchButton"],
                _components_living_room_wall_effect_living_room_wall_effect_component__WEBPACK_IMPORTED_MODULE_18__["LivingRoomWallEffectComponent"],
                _components_color_picker_color_picker_component__WEBPACK_IMPORTED_MODULE_19__["ColorPickerComponent"],
                _components_living_room_ceiling_effect_living_room_ceiling_effect_component__WEBPACK_IMPORTED_MODULE_20__["LivingRoomCeilingEffectComponent"],
                _components_sockets_sockets_component__WEBPACK_IMPORTED_MODULE_21__["SocketsComponent"],
                _pipes_hex_transform_pipe__WEBPACK_IMPORTED_MODULE_51__["HexTransformPipe"],
                _pipes_name_filter_pipe__WEBPACK_IMPORTED_MODULE_52__["NameFilterPipe"],
                _pipes_safe_pipe__WEBPACK_IMPORTED_MODULE_53__["SafePipe"],
                _pipes_logs_filter_pipe__WEBPACK_IMPORTED_MODULE_55__["LogsFilterPipe"],
                _components_device_advance_control_pwm_device_advance_control_pwm_component__WEBPACK_IMPORTED_MODULE_23__["DeviceAdvanceControlPwmComponent"],
                _components_device_advance_control_rgb_device_advance_control_rgb_component__WEBPACK_IMPORTED_MODULE_24__["DeviceAdvanceControlRgbComponent"],
                _components_radial_color_picker_radial_color_picker_component__WEBPACK_IMPORTED_MODULE_25__["RadialColorPickerComponent"],
                _components_actions_widgets_list_actions_widgets_list_component__WEBPACK_IMPORTED_MODULE_26__["ActionsWidgetsListComponent"],
                _components_actions_widgets_list_actions_widgets_list_component__WEBPACK_IMPORTED_MODULE_26__["ActionGroupDeletePopupComponent"],
                _components_actions_widgets_actions_widgets_component__WEBPACK_IMPORTED_MODULE_27__["ActionsWidgetsComponent"],
                _components_actions_widgets_actions_widgets_component__WEBPACK_IMPORTED_MODULE_27__["WidgetPopupComponent"],
                _components_widget_actions_list_widget_actions_list_component__WEBPACK_IMPORTED_MODULE_33__["WidgetActionPopupComponent"],
                _components_widgets_action_widget_slider_action_widget_slider_component__WEBPACK_IMPORTED_MODULE_28__["ActionWidgetSliderComponent"],
                _components_widgets_action_widget_color_picker_action_widget_color_picker_component__WEBPACK_IMPORTED_MODULE_29__["ActionWidgetColorPickerComponent"],
                _components_widgets_action_widget_buttons_action_widget_buttons_component__WEBPACK_IMPORTED_MODULE_30__["ActionWidgetButtonsComponent"],
                _components_widgets_action_widget_rgb_slider_action_widget_rgb_slider_component__WEBPACK_IMPORTED_MODULE_31__["ActionWidgetRgbSliderComponent"],
                _components_widget_action_select_widget_action_select_component__WEBPACK_IMPORTED_MODULE_32__["WidgetActionSelectComponent"],
                _components_widget_actions_list_widget_actions_list_component__WEBPACK_IMPORTED_MODULE_33__["WidgetActionsListComponent"],
                _components_backup_restore_backup_restore_component__WEBPACK_IMPORTED_MODULE_56__["BackupRestoreComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot(appRoutes),
                angular2_flash_messages__WEBPACK_IMPORTED_MODULE_36__["FlashMessagesModule"].forRoot(),
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_37__["ModalModule"].forRoot(),
                ngx_order_pipe__WEBPACK_IMPORTED_MODULE_54__["OrderModule"],
                ngx_ui_switch__WEBPACK_IMPORTED_MODULE_39__["UiSwitchModule"],
                ng5_slider__WEBPACK_IMPORTED_MODULE_40__["Ng5SliderModule"],
                angular_weather_widget__WEBPACK_IMPORTED_MODULE_38__["AngularWeatherWidgetModule"].forRoot({
                    key: '4d95e1f8c20f89c62ed6f9095b10c60c',
                    name: angular_weather_widget__WEBPACK_IMPORTED_MODULE_38__["WeatherApiName"].OPEN_WEATHER_MAP,
                    baseUrl: 'http://api.openweathermap.org/data/2.5'
                })
            ],
            providers: [
                _services_validate_service__WEBPACK_IMPORTED_MODULE_41__["ValidateService"],
                _services_auth_service__WEBPACK_IMPORTED_MODULE_43__["AuthService"],
                _guards_auth_guard__WEBPACK_IMPORTED_MODULE_50__["AuthGuard"],
                _services_rest_service__WEBPACK_IMPORTED_MODULE_44__["RestService"],
                _services_tools_service__WEBPACK_IMPORTED_MODULE_45__["ToolsService"],
                _services_actions_widgets_service__WEBPACK_IMPORTED_MODULE_42__["ActionsWidgetsService"],
                _services_config_service__WEBPACK_IMPORTED_MODULE_46__["ConfigService"],
                _services_device_manager_service__WEBPACK_IMPORTED_MODULE_47__["DeviceManagerService"],
                _services_operation_manager_service__WEBPACK_IMPORTED_MODULE_48__["OperationManagerService"],
                _services_builder_service__WEBPACK_IMPORTED_MODULE_49__["BuilderService"]
            ],
            bootstrap: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]
            ],
            entryComponents: [
                _components_living_room_lights_living_room_lights_component__WEBPACK_IMPORTED_MODULE_17__["LightAdjustComponent"],
                _components_device_manager_device_manager_component__WEBPACK_IMPORTED_MODULE_11__["DeviceAdvanceCommandComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__["LogsSettingsComponent"],
                _components_actions_widgets_list_actions_widgets_list_component__WEBPACK_IMPORTED_MODULE_26__["ActionGroupDeletePopupComponent"],
                _components_actions_widgets_actions_widgets_component__WEBPACK_IMPORTED_MODULE_27__["WidgetPopupComponent"],
                _components_widget_actions_list_widget_actions_list_component__WEBPACK_IMPORTED_MODULE_33__["WidgetActionPopupComponent"],
                _components_widget_action_select_widget_action_select_component__WEBPACK_IMPORTED_MODULE_32__["WidgetActionSelectComponent"]
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/action/action.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/action/action.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flash-message-container {\r\n  position: fixed;\r\n  z-index: 10;\r\n  width: 100%;\r\n}\r\n\r\n.flash-message {\r\n  display: block;\r\n  margin-right: 10%;\r\n  margin-left: 10%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hY3Rpb24vYWN0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFlO0VBQ2YsV0FBVztFQUNYLFdBQVc7QUFDYjs7QUFFQTtFQUNFLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZ0JBQWdCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hY3Rpb24vYWN0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZmxhc2gtbWVzc2FnZS1jb250YWluZXIge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB6LWluZGV4OiAxMDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmZsYXNoLW1lc3NhZ2Uge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbi1yaWdodDogMTAlO1xyXG4gIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/action/action.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/action/action.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"flash-message-container\">\n  <div class=\"flash-message\">\n    <flash-messages></flash-messages>\n  </div>\n</div>\n<div class=\"container\">\n  <div class=\"panel panel-default\">\n    <div class=\"panel-body\">\n\n      <div class=\"form-group\">\n        <div style=\"margin-bottom: 5px;\">\n          Select action type:\n        </div>\n\n        <div  style=\"margin-bottom: 15px;\">\n          <select class=\"form-control\" id=\"selectType\" [(ngModel)]=\"action.type\" style=\"width: 50%\">\n            <option *ngFor=\"let type of types\" [ngValue]=\"type\">\n              {{type}}\n            </option>\n          </select>\n        </div>\n\n      </div>\n\n      <div *ngIf=\"action.type == 'device'\">\n        <div class=\"form-group\">\n          <div style=\"margin-bottom: 5px;\">\n            Select category:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <select class=\"form-control\" id=\"selectCond\" [(ngModel)]=\"action.category\" style=\"width: 50%\">\n              <option *ngFor=\"let cat of deviceManager.categoriesList\" [ngValue]=\"cat.id\">\n                {{cat.name}}\n              </option>\n            </select>\n          </div>\n\n        </div>\n\n\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Select device:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <select class=\"form-control\" id=\"selectDev\" [(ngModel)]=\"action.address\" style=\"width: 50%\">\n              <option *ngFor=\"let device of getDevicesForSelectedCategory() | orderBy: 'name'\" [ngValue]=\"device.address\">\n                {{device.name}}\n              </option>\n            </select>\n          </div>\n\n        </div>\n\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Select parameter:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <select class=\"form-control\" id=\"selectParam\" [(ngModel)]=\"action.param\" style=\"width: 50%\" >\n              <option *ngFor=\"let param of getParamsForSelectedCategory()\" [ngValue]=\"param\">\n                {{param}}\n              </option>\n            </select>\n          </div>\n\n        </div>\n\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Input value:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <input type=\"number\" [(ngModel)]=\"action.value\" class=\"form-control\" id=\"inputName\" style=\"width: 50%; margin-bottom: 5px;\">\n          </div>\n\n        </div>\n      </div>\n\n\n      <div *ngIf=\"action.type == 'gsmModem'\">\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Input number (+48XXXXXXXXX):\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <input type=\"text\" [(ngModel)]=\"action.number\" class=\"form-control\" id=\"inputNumber\" style=\"width: 50%; margin-bottom: 5px;\">\n          </div>\n\n          <div style=\"margin-bottom: 5px;\">\n            Input message:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <input type=\"text\" [(ngModel)]=\"action.message\" class=\"form-control\" id=\"inputMessage\" style=\"width: 50%; margin-bottom: 5px;\">\n          </div>\n\n        </div>\n\n      </div>\n\n      <div *ngIf=\"action.type == 'rest'\">\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Input endpoint:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <input type=\"text\" [(ngModel)]=\"action.endpoint\" class=\"form-control\" id=\"inputRestEndpoint\" style=\"width: 50%; margin-bottom: 5px;\">\n          </div>\n\n          <div style=\"margin-bottom: 5px;\">\n            Input message:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <input type=\"text\" [(ngModel)]=\"action.message\" class=\"form-control\" id=\"inputRestMessage\" style=\"width: 50%; margin-bottom: 5px;\">\n          </div>\n\n        </div>\n\n      </div>\n\n\n      <button class=\"btn btn-success mr-2\" (click)=\"onActionSave()\">Save Action</button>\n      <button class=\"btn btn-default\" (click)=\"onCancel()\">Cancel</button>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/action/action.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/action/action.component.ts ***!
  \*******************************************************/
/*! exports provided: ActionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionComponent", function() { return ActionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_operation_manager_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/operation-manager.service */ "./src/app/services/operation-manager.service.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _services_builder_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/builder.service */ "./src/app/services/builder.service.ts");
/* harmony import */ var _services_validate_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/validate.service */ "./src/app/services/validate.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../models/consts */ "./src/app/models/consts.ts");










var ActionComponent = /** @class */ (function () {
    function ActionComponent(route, operationManager, router, deviceManager, builder, validator, flashMessage) {
        this.route = route;
        this.operationManager = operationManager;
        this.router = router;
        this.deviceManager = deviceManager;
        this.builder = builder;
        this.validator = validator;
        this.flashMessage = flashMessage;
    }
    ActionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadActionTypes();
        this.sub = this.route.params.subscribe(function (params) {
            _this.actionId = params['id'];
            _this.operation = _this.operationManager.tmpOperation;
            if (_this.actionId != 0) {
                var tmpAction = _this.operationManager.getOperationActionById(_this.operation, _this.actionId);
                _this.action = tmpAction.toJson();
            }
            else {
                _this.action = new Object();
                _this.action['number'] = '+48';
            }
        });
    };
    ActionComponent.prototype.loadActionTypes = function () {
        this.types = new Array();
        this.types.push('device');
        this.types.push('gsmModem');
        this.types.push('rest');
    };
    ActionComponent.prototype.getDevicesForSelectedCategory = function () {
        var _this = this;
        if (this.action['category']) {
            var devs = this.deviceManager.devicesList.filter(function (item) { return item.category == _this.action['category']; });
            console.log('>>>> devices: ', devs);
            return devs;
        }
        return new Array();
    };
    ActionComponent.prototype.getParamsForSelectedCategory = function () {
        var params = new Array();
        if (this.action['category']) {
            return this.deviceManager.getCategoryById(this.action['category']).commands;
        }
        return params;
    };
    ActionComponent.prototype.onActionSave = function () {
        var res = new Object();
        if (this.validator.validateAction(this.action, res)) {
            if (this.actionId != 0) {
                this.operationManager.setOperationActionById(this.operation, this.actionId, this.builder.createAction(this.action, this.deviceManager));
            }
            else {
                var tmpAction = this.builder.createAction(this.action, this.deviceManager);
                this.operation.actions.push(tmpAction);
            }
            this.router.navigate(['/operation', this.operation.id, _models_consts__WEBPACK_IMPORTED_MODULE_8__["SRC_PAGE_ACT_EDIT"]]);
        }
        else {
            this.flashMessage.show(res['error'], {
                cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                timeout: 5000
            });
        }
    };
    ActionComponent.prototype.onCancel = function () {
        this.router.navigate(['/operation', this.operation.id, _models_consts__WEBPACK_IMPORTED_MODULE_8__["SRC_PAGE_ACT_EDIT"]]);
    };
    ActionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-action',
            template: __webpack_require__(/*! ./action.component.html */ "./src/app/components/action/action.component.html"),
            styles: [__webpack_require__(/*! ./action.component.css */ "./src/app/components/action/action.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_operation_manager_service__WEBPACK_IMPORTED_MODULE_3__["OperationManagerService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__["DeviceManagerService"],
            _services_builder_service__WEBPACK_IMPORTED_MODULE_5__["BuilderService"],
            _services_validate_service__WEBPACK_IMPORTED_MODULE_6__["ValidateService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_7__["FlashMessagesService"]])
    ], ActionComponent);
    return ActionComponent;
}());



/***/ }),

/***/ "./src/app/components/actions-widgets-list/actions-widgets-list.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/components/actions-widgets-list/actions-widgets-list.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".panel-body {\r\n  padding-top: 10px;\r\n  padding-right: 15px;\r\n  padding-bottom: 15px;\r\n  padding-left: 15px;\r\n}\r\n\r\n.action-group-name {\r\n   white-space: nowrap;\r\n   overflow: hidden;\r\n   text-overflow: ellipsis;\r\n   /*width: 100px; */\r\n}\r\n\r\n.action-group-buttons {\r\n  white-space: nowrap;\r\n}\r\n\r\n.wide-button{\r\n  width: 50%;\r\n}\r\n\r\n.modal-header-custom{\r\n  width: 100%;\r\n  text-align: center;\r\n}\r\n\r\n.modal-content-center{\r\n  text-align: center;\r\n}\r\n\r\n.flash-message-container {\r\n  position: fixed;\r\n  z-index: 10;\r\n  width: 100%;\r\n}\r\n\r\n.flash-message {\r\n  display: block;\r\n  margin-right: 10%;\r\n  margin-left: 10%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hY3Rpb25zLXdpZGdldHMtbGlzdC9hY3Rpb25zLXdpZGdldHMtbGlzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsa0JBQWtCO0FBQ3BCOztBQUVBO0dBQ0csbUJBQW1CO0dBQ25CLGdCQUFnQjtHQUNoQix1QkFBdUI7R0FDdkIsaUJBQWlCO0FBQ3BCOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsVUFBVTtBQUNaOztBQUVBO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLGVBQWU7RUFDZixXQUFXO0VBQ1gsV0FBVztBQUNiOztBQUVBO0VBQ0UsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixnQkFBZ0I7QUFDbEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2FjdGlvbnMtd2lkZ2V0cy1saXN0L2FjdGlvbnMtd2lkZ2V0cy1saXN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGFuZWwtYm9keSB7XHJcbiAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgcGFkZGluZy1yaWdodDogMTVweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcclxuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbn1cclxuXHJcbi5hY3Rpb24tZ3JvdXAtbmFtZSB7XHJcbiAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAvKndpZHRoOiAxMDBweDsgKi9cclxufVxyXG5cclxuLmFjdGlvbi1ncm91cC1idXR0b25zIHtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG59XHJcblxyXG4ud2lkZS1idXR0b257XHJcbiAgd2lkdGg6IDUwJTtcclxufVxyXG5cclxuLm1vZGFsLWhlYWRlci1jdXN0b217XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4ubW9kYWwtY29udGVudC1jZW50ZXJ7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uZmxhc2gtbWVzc2FnZS1jb250YWluZXIge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB6LWluZGV4OiAxMDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmZsYXNoLW1lc3NhZ2Uge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbi1yaWdodDogMTAlO1xyXG4gIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/actions-widgets-list/actions-widgets-list.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/components/actions-widgets-list/actions-widgets-list.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"flash-message-container\">\n  <div class=\"flash-message\">\n    <flash-messages></flash-messages>\n  </div>\n</div>\n<div class=\"container mt-2\">\n\n  <div class=\"panel\">\n    <div class=\"panel-body\">\n      <div class=\"form-group\">\n        <div>\n          <button type=\"button\" (click)=\"createActionGroup()\" class=\"btn btn-primary mb-2 mr-2\">Add new actions group</button>\n        </div>\n\n        <div class=\"mt-2 mb-2\">\n          Action widgets groups list:\n        </div>\n\n        <div class=\"bs-component\">\n\n          <div class=\"list-group\" id=\"widgetsList\">\n           \n             <button *ngFor=\"let actionGroup of actionWidgetsService.actionGroups\" (click)=\"goToActionsGroupPage(actionGroup.key)\"\n                    class=\"list-group-item list-group-item-action d-flex justify-content-between align-items-center\">\n              <div class=\"action-group-name\">\n                {{actionGroup.name}}\n              </div>\n              <div class=\"action-group-buttons\">\n                <button type=\"button\" class=\"btn btn-primary mr-2\" (click)=\"goToEditActionsGroupPage(actionGroup.key)\">Edit</button>\n                <button type=\"button\" class=\"btn btn-danger\" (click)=\"onDeleteGroupPage(actionGroup, $event)\">Delete</button>\n              </div>\n            </button>\n          </div>\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>\n\n"

/***/ }),

/***/ "./src/app/components/actions-widgets-list/actions-widgets-list.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/actions-widgets-list/actions-widgets-list.component.ts ***!
  \***********************************************************************************/
/*! exports provided: ActionsWidgetsListComponent, ActionGroupDeletePopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsWidgetsListComponent", function() { return ActionsWidgetsListComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionGroupDeletePopupComponent", function() { return ActionGroupDeletePopupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_actions_widgets_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/actions-widgets.service */ "./src/app/services/actions-widgets.service.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/index.js");
/* harmony import */ var ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/modal/bs-modal-ref.service */ "./node_modules/ngx-bootstrap/modal/bs-modal-ref.service.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");









var ActionsWidgetsListComponent = /** @class */ (function () {
    function ActionsWidgetsListComponent(actionWidgetsService, modalService, router, route, flashMessage, location) {
        this.actionWidgetsService = actionWidgetsService;
        this.modalService = modalService;
        this.router = router;
        this.route = route;
        this.flashMessage = flashMessage;
        this.location = location;
    }
    ActionsWidgetsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.flashMessageCss = params["cssClass"];
            _this.flashMessageTxt = params["message"];
            if (_this.flashMessageCss && _this.flashMessageTxt) {
                _this.flashMessage.show(_this.flashMessageTxt, {
                    cssClass: _this.flashMessageCss,
                    timeout: 5000
                });
            }
            _this.location.replaceState(_this.location.path().split('?')[0], '');
        });
        this.actionWidgetsService.loadActionGroups();
    };
    ActionsWidgetsListComponent.prototype.goToActionsGroupPage = function (key) {
        this.router.navigate(['/widgets', 0, key]);
    };
    ActionsWidgetsListComponent.prototype.goToEditActionsGroupPage = function (key) {
        this.router.navigate(['/widgets', 1, key]);
    };
    ActionsWidgetsListComponent.prototype.onDeleteGroupPage = function (actionGroup, event) {
        this.showPopup(actionGroup.key);
        event.preventDefault();
        event.stopPropagation();
    };
    ActionsWidgetsListComponent.prototype.showPopup = function (key) {
        // this.popupRequest = request;
        this.bsModalRef = this.modalService.show(ActionGroupDeletePopupComponent);
        this.bsModalRef.content.closeBtnName = 'Close';
        this.bsModalRef.content.parentController = this;
        this.bsModalRef.content.actionGroupKey = key;
    };
    ActionsWidgetsListComponent.prototype.deleteActionWidgetsGroup = function (key) {
        var _this = this;
        this.actionWidgetsService.deleteActionWidgetsGroup(key, (function (callback) {
            _this.actionWidgetsService.loadActionGroups();
        }));
    };
    ActionsWidgetsListComponent.prototype.createActionGroup = function () {
        this.actionWidgetsService.createNewActionGroup('New Actions Group');
        this.actionWidgetsService.loadActionGroups();
    };
    ActionsWidgetsListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-actions-widgets-list',
            template: __webpack_require__(/*! ./actions-widgets-list.component.html */ "./src/app/components/actions-widgets-list/actions-widgets-list.component.html"),
            styles: [__webpack_require__(/*! ./actions-widgets-list.component.css */ "./src/app/components/actions-widgets-list/actions-widgets-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_actions_widgets_service__WEBPACK_IMPORTED_MODULE_2__["ActionsWidgetsService"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["BsModalService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__["FlashMessagesService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"]])
    ], ActionsWidgetsListComponent);
    return ActionsWidgetsListComponent;
}());

var ActionGroupDeletePopupComponent = /** @class */ (function () {
    function ActionGroupDeletePopupComponent(bsModalRef) {
        this.bsModalRef = bsModalRef;
    }
    ActionGroupDeletePopupComponent.prototype.ngOnInit = function () { };
    ActionGroupDeletePopupComponent.prototype.onPopupConfirm = function () {
        this.parentController.deleteActionWidgetsGroup(this.actionGroupKey);
        this.bsModalRef.hide();
    };
    ActionGroupDeletePopupComponent.prototype.onPopupCancel = function () {
        this.bsModalRef.hide();
    };
    ActionGroupDeletePopupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'modal-content',
            template: "\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title pull-left modal-header-custom\">WARNING</h4>\n    </div>\n    <div class=\"modal-body modal-body-custom\">\n    <div class=\"modal-content-center\">\n        Do you really want to delete actions group?\n    </div>\n    <div modal-input-section class=\"modal-content-center mt-3\">\n        <button type=\"button\" class=\"btn btn-danger wide-button mr-2\" onclick=\"this.blur();\" (click)=\"onPopupCancel()\">No</button>\n        <button type=\"button\" class=\"btn btn-success\" onclick=\"this.blur();\"  (click)=\"onPopupConfirm()\">Yes</button>\n    </div>\n    </div>\n  ",
            styles: [__webpack_require__(/*! ./actions-widgets-list.component.css */ "./src/app/components/actions-widgets-list/actions-widgets-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_4__["BsModalRef"]])
    ], ActionGroupDeletePopupComponent);
    return ActionGroupDeletePopupComponent;
}());



/***/ }),

/***/ "./src/app/components/actions-widgets/actions-widgets.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/actions-widgets/actions-widgets.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".widget-container-style {\r\n  display: inline-block;\r\n  align-items: center;\r\n  justify-content: center;\r\n  width: 160px;\r\n  vertical-align: top;\r\n}\r\n\r\n.custom-container {\r\n  width: 100%;\r\n  margin-right: auto;\r\n  margin-left: auto;\r\n}\r\n\r\n.widget-style {\r\n  background-color: #353535;\r\n  border-radius: 10px;\r\n  padding: 10px;\r\n}\r\n\r\n.widget-buttons {\r\n  text-align: center;\r\n  display: grid;\r\n}\r\n\r\n.wide-button{\r\n  width: 50%;\r\n}\r\n\r\n.modal-content-center {\r\n  text-align: center;\r\n}\r\n\r\n.widgets-list {\r\n  text-align: center;\r\n}\r\n\r\n.half-size {\r\n  width: 50%;\r\n}\r\n\r\n.modal-body {\r\n  text-align: -webkit-center;\r\n}\r\n\r\n::ng-deep .switch .form-control {\r\n  display: inline;\r\n}\r\n\r\n.button-name {\r\n  width: 70%;\r\n}\r\n\r\n.button-settings {\r\n  width: 50%;\r\n  text-align: left;\r\n}\r\n\r\n.flash-message-container {\r\n  position: fixed;\r\n  z-index: 10;\r\n  width: 100%;\r\n}\r\n\r\n.flash-message {\r\n  display: block;\r\n  margin-right: 10%;\r\n  margin-left: 10%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hY3Rpb25zLXdpZGdldHMvYWN0aW9ucy13aWRnZXRzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1osbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSx5QkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLGFBQWE7QUFDZjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxVQUFVO0FBQ1o7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxVQUFVO0FBQ1o7O0FBRUE7RUFDRSwwQkFBMEI7QUFDNUI7O0FBRUE7RUFDRSxlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsVUFBVTtBQUNaOztBQUVBO0VBQ0UsVUFBVTtFQUNWLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLGVBQWU7RUFDZixXQUFXO0VBQ1gsV0FBVztBQUNiOztBQUVBO0VBQ0UsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixnQkFBZ0I7QUFDbEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2FjdGlvbnMtd2lkZ2V0cy9hY3Rpb25zLXdpZGdldHMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi53aWRnZXQtY29udGFpbmVyLXN0eWxlIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB3aWR0aDogMTYwcHg7XHJcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcclxufVxyXG5cclxuLmN1c3RvbS1jb250YWluZXIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBtYXJnaW4tbGVmdDogYXV0bztcclxufVxyXG5cclxuLndpZGdldC1zdHlsZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM1MzUzNTtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi53aWRnZXQtYnV0dG9ucyB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGRpc3BsYXk6IGdyaWQ7XHJcbn1cclxuXHJcbi53aWRlLWJ1dHRvbntcclxuICB3aWR0aDogNTAlO1xyXG59XHJcblxyXG4ubW9kYWwtY29udGVudC1jZW50ZXIge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLndpZGdldHMtbGlzdCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uaGFsZi1zaXplIHtcclxuICB3aWR0aDogNTAlO1xyXG59XHJcblxyXG4ubW9kYWwtYm9keSB7XHJcbiAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAuc3dpdGNoIC5mb3JtLWNvbnRyb2wge1xyXG4gIGRpc3BsYXk6IGlubGluZTtcclxufVxyXG5cclxuLmJ1dHRvbi1uYW1lIHtcclxuICB3aWR0aDogNzAlO1xyXG59XHJcblxyXG4uYnV0dG9uLXNldHRpbmdzIHtcclxuICB3aWR0aDogNTAlO1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbn1cclxuXHJcbi5mbGFzaC1tZXNzYWdlLWNvbnRhaW5lciB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHotaW5kZXg6IDEwO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZmxhc2gtbWVzc2FnZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMCU7XHJcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/actions-widgets/actions-widgets.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/actions-widgets/actions-widgets.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"custom-container mt-2\">\n  <div *ngIf=\"editMode\" class=\"flash-message-container\">\n    <div class=\"flash-message\">\n      <flash-messages></flash-messages>\n    </div>\n  </div>\n  <div *ngIf=\"actionGroup != undefined\">\n    <div *ngIf=\"editMode\" class=\"p-3\">\n      <button type=\"button\" class=\"btn btn-default mb-2\" (click)=\"onCancel()\">Back</button>\n      <button type=\"button\" class=\"btn btn-primary ml-2 mb-2\" (click)=\"addWidget()\">Add widget</button>\n    </div>\n    <div *ngIf=\"editMode\" class=\"p-3 col-sm-5 col-xs-12 col-lg-4\">\n\t\t  <label class=\"form-control-label\" for=\"inputName\">Group name:</label>\n      <input type=\"text\" [(ngModel)]=\"groupName\" class=\"form-control\" id=\"inputName\">\n      <button type=\"button\" *ngIf=\"groupName != actionGroup.name\" class=\"btn btn-success mb-2 mr-2 mt-2\" (click)=\"onSave()\">Save</button>\n\t  </div>\n\n    <div class=\"widgets-list\">\n      <div *ngFor=\"let widget of actionGroup.widgets\" class=\"mr-2 ml-2 mt-3 widget-container-style\">\n        <div class=\"widget-style\" *ngIf=\"showWidget(widget)\">\n          <app-action-widget-color-picker (nameClicked)=\"onNameClicked($event)\" (onActionSetup)=\"onActionSetup($event)\" (onAction)=\"onAction($event)\" [widget]=\"widget\" [editMode]=\"editMode\" *ngIf=\"widget.type == 'rgb-picker'\"></app-action-widget-color-picker>\n          <app-action-widget-rgb-slider (nameClicked)=\"onNameClicked($event)\" (onActionSetup)=\"onActionSetup($event)\" (onAction)=\"onAction($event)\" [widget]=\"widget\" [editMode]=\"editMode\" *ngIf=\"widget.type == 'rgb-slider'\"></app-action-widget-rgb-slider>\n          <app-action-widget-slider (nameClicked)=\"onNameClicked($event)\" (onActionSetup)=\"onActionSetup($event)\" (onAction)=\"onAction($event)\" [widget]=\"widget\" [editMode]=\"editMode\" *ngIf=\"widget.type == 'slider'\"></app-action-widget-slider>\n          <app-action-widget-buttons (nameClicked)=\"onNameClicked($event)\" (onActionSetup)=\"onActionSetup($event)\" (onAction)=\"onAction($event)\" [widget]=\"widget\" [editMode]=\"editMode\" *ngIf=\"widget.type == '1-button' || widget.type == '2-buttons' || widget.type == '3-buttons' || widget.type == '4-buttons'\"></app-action-widget-buttons>\n          <div id=\"widget-butons\" class=\"widget-buttons mt-2\" *ngIf=\"editMode\">\n            <button type=\"button\" *ngIf=\"widget.active == true\" class=\"btn btn-success mb-2\" (click)=\"onInactivate(widget)\">Active</button>\n            <button type=\"button\" *ngIf=\"widget.active == false\" class=\"btn btn-warning mb-2\" (click)=\"onActivate(widget)\">Inactive</button>\n            <button type=\"button\" class=\"btn btn-primary mb-2\" (click)=\"showEditWidgetPopup(widget)\">Edit</button>\n            <button type=\"button\" class=\"btn btn-danger\" (click)=\"showDeleteWidgetPopup(widget)\">Delete</button>\n          </div>\n        </div>\n\n      </div>\n    </div>\n\n    <div *ngIf=\"editMode\" class=\"pl-3 pt-5\">\n      <button type=\"button\" class=\"btn btn-default\" (click)=\"onCancel()\">Back</button>\n      <button type=\"button\" class=\"btn btn-primary ml-2\" (click)=\"addWidget()\">Add widget</button>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/actions-widgets/actions-widgets.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/actions-widgets/actions-widgets.component.ts ***!
  \*************************************************************************/
/*! exports provided: ActionsWidgetsComponent, WidgetPopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsWidgetsComponent", function() { return ActionsWidgetsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetPopupComponent", function() { return WidgetPopupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_actions_widgets_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/actions-widgets.service */ "./src/app/services/actions-widgets.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../models/consts */ "./src/app/models/consts.ts");
/* harmony import */ var ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal/bs-modal-ref.service */ "./node_modules/ngx-bootstrap/modal/bs-modal-ref.service.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/index.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../utils/utils */ "./src/app/utils/utils.ts");












var ActionsWidgetsComponent = /** @class */ (function () {
    function ActionsWidgetsComponent(router, route, actionWidgetsService, flashMessage, modalService, restService, deviceManager) {
        this.router = router;
        this.route = route;
        this.actionWidgetsService = actionWidgetsService;
        this.flashMessage = flashMessage;
        this.modalService = modalService;
        this.restService = restService;
        this.deviceManager = deviceManager;
    }
    ActionsWidgetsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.deviceManager.loadDevices();
        this.route.params.subscribe(function (params) {
            _this.groupKey = params[_models_consts__WEBPACK_IMPORTED_MODULE_5__["URL_PARAM_KEY"]];
            _this.editMode = (params[_models_consts__WEBPACK_IMPORTED_MODULE_5__["URL_PARAM_EDIT"]] == 1);
            console.log('>>>> this groupKey: ', _this.groupKey);
            console.log('>>>> this editMode: ', _this.editMode);
            _this.loadWidgets();
        });
    };
    ActionsWidgetsComponent.prototype.loadWidgets = function () {
        var _this = this;
        this.actionWidgetsService.getActionWidgetsGroup(this.groupKey, (function (val) {
            if (!val) {
                var navigationExtras = {
                    queryParams: {
                        "cssClass": "alert-danger",
                        "message": "Loading action widget group failed"
                    }
                };
                _this.router.navigate(['/widgetsgroupslist'], navigationExtras);
            }
            _this.actionGroup = val;
            _this.groupName = val.name;
        }));
    };
    ActionsWidgetsComponent.prototype.onSave = function () {
        var _this = this;
        this.actionGroup.name = this.groupName;
        this.actionWidgetsService.updateActionGroup(this.actionGroup, (function (callback) {
            if (callback['success'] === false) {
                _this.flashMessage.show('Creating action widget group failed: ' + callback['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else if (callback['success'] === true) {
                _this.flashMessage.show('Action widget group saved', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        }));
    };
    ActionsWidgetsComponent.prototype.onCancel = function () {
        this.router.navigate(['/widgetsgroupslist']);
    };
    ActionsWidgetsComponent.prototype.onActivate = function (widget) {
        var _this = this;
        widget.active = true;
        this.actionWidgetsService.updateWidget(widget, (function (callback) {
            if (callback['success'] === false) {
                _this.flashMessage.show('Updating action widget failed: ' + callback['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
                widget.active = false;
            }
            else if (callback['success'] === true) {
                _this.flashMessage.show('Action widget updated', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        }));
    };
    ActionsWidgetsComponent.prototype.onInactivate = function (widget) {
        var _this = this;
        widget.active = false;
        this.actionWidgetsService.updateWidget(widget, (function (callback) {
            if (callback['success'] === false) {
                _this.flashMessage.show('Updating action widget failed: ' + callback['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
                widget.active = true;
            }
            else if (callback['success'] === true) {
                _this.flashMessage.show('Action widget updated', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        }));
    };
    ActionsWidgetsComponent.prototype.onDelete = function (widget) {
        var _this = this;
        this.actionWidgetsService.deleteActionsWidget(widget.key, (function (callback) {
            if (callback['success'] === false) {
                _this.flashMessage.show('Delete widget failed: ' + callback['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else if (callback['success'] === true) {
                _this.flashMessage.show('Widget deleted', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
                _this.loadWidgets();
            }
        }));
    };
    ActionsWidgetsComponent.prototype.showWidget = function (widget) {
        return this.editMode || widget.active;
    };
    ActionsWidgetsComponent.prototype.addWidget = function () {
        this.showNewWidgetPopup();
    };
    ActionsWidgetsComponent.prototype.onNameClicked = function (event) {
        this.showRenameWidgetPopup(event);
    };
    ActionsWidgetsComponent.prototype.onActionSetup = function (event) {
        // this.showWidgetActionPopup(event);
        var navigationExtras = {
            queryParams: {
                "actionsetup": JSON.stringify(event),
                "groupkey": this.groupKey
            }
        };
        this.router.navigate(['/widgetactionslist'], navigationExtras);
    };
    ActionsWidgetsComponent.prototype.onAction = function (event) {
        console.log('>>> event: ', event);
        var device = this.deviceManager.getDevice(event.devCategory, event.devAddress);
        this.restService.setDeviceParameter(device, event.devCommand, event.devCommandValue).subscribe(function (data) {
            if (data.errorCode) {
                console.log('>> error: ', data.result);
            }
            else {
                console.log('>> success');
                if (!device.parameters) {
                    device.parameters = {};
                }
                device.parameters[event.devCommand] = event.devCommandValue;
            }
        });
    };
    ActionsWidgetsComponent.prototype.showNewWidgetPopup = function () {
        // this.popupRequest = request;
        this.bsModalRef = this.modalService.show(WidgetPopupComponent);
        this.bsModalRef.content.closeBtnName = 'Close';
        this.bsModalRef.content.parentController = this;
        this.bsModalRef.content.popupType = 'new_widget';
    };
    ActionsWidgetsComponent.prototype.showEditWidgetPopup = function (widget) {
        this.bsModalRef = this.modalService.show(WidgetPopupComponent);
        this.bsModalRef.content.closeBtnName = 'Close';
        this.bsModalRef.content.parentController = this;
        this.bsModalRef.content.popupType = 'edit_widget';
        this.bsModalRef.content.widget = widget;
        this.bsModalRef.content.newWidgetName = widget.name;
        this.bsModalRef.content.newWidgetType = widget.type;
        if (widget.details) {
            if (widget.type == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_SLIDER"]) {
                var details = JSON.parse(widget.details);
                this.bsModalRef.content.maxSliderValue = details[_models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_DETAILS_SLIDER_MAX"]];
                this.bsModalRef.content.showSliderGraphic = details[_models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC"]];
                this.bsModalRef.content.sliderGraphicColor = details[_models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR"]];
            }
            if (widget.type == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_1"] || widget.type == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_2"]
                || widget.type == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_3"] || widget.type == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_4"]) {
                var buttonSettings = JSON.parse(widget.details);
                this.bsModalRef.content.buttonSettings = buttonSettings[_models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_DETAILS_BUTTON_SETTINGS"]];
            }
        }
    };
    ActionsWidgetsComponent.prototype.showDeleteWidgetPopup = function (widget) {
        // this.popupRequest = request;
        this.bsModalRef = this.modalService.show(WidgetPopupComponent);
        this.bsModalRef.content.closeBtnName = 'Close';
        this.bsModalRef.content.parentController = this;
        this.bsModalRef.content.popupType = 'delete_widget';
        this.bsModalRef.content.widget = widget;
    };
    ActionsWidgetsComponent.prototype.showRenameWidgetPopup = function (widget) {
        // this.popupRequest = request;
        this.bsModalRef = this.modalService.show(WidgetPopupComponent);
        this.bsModalRef.content.closeBtnName = 'Close';
        this.bsModalRef.content.parentController = this;
        this.bsModalRef.content.popupType = 'rename_widget';
        this.bsModalRef.content.widget = widget;
        this.bsModalRef.content.newWidgetName = widget.name;
    };
    ActionsWidgetsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-actions-widget',
            template: __webpack_require__(/*! ./actions-widgets.component.html */ "./src/app/components/actions-widgets/actions-widgets.component.html"),
            styles: [__webpack_require__(/*! ./actions-widgets.component.css */ "./src/app/components/actions-widgets/actions-widgets.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_actions_widgets_service__WEBPACK_IMPORTED_MODULE_3__["ActionsWidgetsService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__["FlashMessagesService"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_7__["BsModalService"],
            _services_rest_service__WEBPACK_IMPORTED_MODULE_8__["RestService"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_9__["DeviceManagerService"]])
    ], ActionsWidgetsComponent);
    return ActionsWidgetsComponent;
}());

var WidgetPopupComponent = /** @class */ (function () {
    function WidgetPopupComponent(bsModalRef) {
        this.bsModalRef = bsModalRef;
    }
    WidgetPopupComponent.prototype.ngOnInit = function () {
        this.widgetTypes = _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPES"];
        if (this.buttonSettings == undefined) {
            this.buttonSettings = [];
            for (var i = 0; i < 4; i++) {
                var obj = {};
                obj[_models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_DETAILS_BUTTON_NAME"]] = '' + (i + 1);
                obj[_models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_DETAILS_BUTTON_COLOR"]] = false;
                this.buttonSettings.push(obj);
            }
        }
    };
    WidgetPopupComponent.prototype.getButtonIds = function () {
        var tab = [];
        if (this.buttonSettings && this.buttonSettings.length > 0) {
            var buttonsQnty = 0;
            if (this.newWidgetType == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_1"]) {
                buttonsQnty = 1;
            }
            else if (this.newWidgetType == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_2"]) {
                buttonsQnty = 2;
            }
            else if (this.newWidgetType == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_3"]) {
                buttonsQnty = 3;
            }
            else if (this.newWidgetType == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_4"]) {
                buttonsQnty = 4;
            }
            else {
                buttonsQnty = 0;
            }
            for (var i = 0; i < buttonsQnty; i++) {
                tab.push(this.buttonSettings[i]);
            }
        }
        return tab;
    };
    WidgetPopupComponent.prototype.onSaveNewWidget = function () {
        var _this = this;
        // this.parentController.deleteActionWidgetsGroup(this.actionGroupKey);
        var details = this.generateWidgetDetails();
        this.parentController.actionWidgetsService.createNewWidget(this.parentController.groupKey, this.newWidgetType, true, this.newWidgetName, details, (function (callback) {
            if (callback['success'] === false) {
                _this.parentController.flashMessage.show('Creating widget failed: ' + callback['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else if (callback['success'] === true) {
                _this.parentController.flashMessage.show('New widget created', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
                _this.parentController.loadWidgets();
            }
        }));
        this.bsModalRef.hide();
    };
    WidgetPopupComponent.prototype.onSaveWidget = function () {
        var _this = this;
        this.widget.name = this.newWidgetName;
        if (this.popupType == 'edit_widget') {
            this.widget.type = this.newWidgetType;
            this.widget.details = this.generateWidgetDetails();
        }
        // this.parentController.deleteActionWidgetsGroup(this.actionGroupKey);
        this.parentController.actionWidgetsService.updateWidget(this.widget, (function (callback) {
            if (callback['success'] === false) {
                _this.parentController.flashMessage.show('Updating widget failed: ' + callback['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else if (callback['success'] === true) {
                _this.parentController.flashMessage.show('Widget updated', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
                _this.parentController.loadWidgets();
            }
        }));
        this.bsModalRef.hide();
    };
    WidgetPopupComponent.prototype.onDeleteWidget = function () {
        this.parentController.onDelete(this.widget);
        this.bsModalRef.hide();
    };
    WidgetPopupComponent.prototype.onPopupCancel = function () {
        this.bsModalRef.hide();
    };
    WidgetPopupComponent.prototype.getBackgroundColor = function () {
        if (this.sliderGraphicColor && this.sliderGraphicColor.length == 6) {
            return new _utils_utils__WEBPACK_IMPORTED_MODULE_10__["Utils"]().hexColorToRgbCss(this.sliderGraphicColor);
        }
        else {
            return new _utils_utils__WEBPACK_IMPORTED_MODULE_10__["Utils"]().hexColorToRgbCss('000000');
        }
    };
    WidgetPopupComponent.prototype.generateWidgetDetails = function () {
        var detailsObj = {};
        if (this.maxSliderValue != undefined) {
            detailsObj[_models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_DETAILS_SLIDER_MAX"]] = this.maxSliderValue;
        }
        if (this.showSliderGraphic != undefined) {
            detailsObj[_models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC"]] = this.showSliderGraphic;
        }
        else if (this.newWidgetType == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_SLIDER"]) {
            detailsObj[_models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC"]] = false;
        }
        if (this.sliderGraphicColor != undefined) {
            detailsObj[_models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR"]] = this.sliderGraphicColor;
        }
        if (this.newWidgetType == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_1"] || this.newWidgetType == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_2"]
            || this.newWidgetType == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_3"] || this.newWidgetType == _models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_TYPE_BUTTONS_4"]) {
            detailsObj[_models_consts__WEBPACK_IMPORTED_MODULE_5__["WIDGET_DETAILS_BUTTON_SETTINGS"]] = this.buttonSettings;
        }
        if (Object.keys(detailsObj).length > 0) {
            return JSON.stringify(detailsObj);
        }
        else {
            return '';
        }
    };
    WidgetPopupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'modal-content',
            template: "\n      <div *ngIf=\"popupType == 'new_widget' || popupType == 'edit_widget'\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title pull-left modal-header-custom\">New widget</h4>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"\">\n              Select widget type:\n          </div>\n     \n          <div>\n            <select class=\"form-control half-size\" id=\"widgetType\" [(ngModel)]=\"newWidgetType\">\n              <option *ngFor=\"let type of widgetTypes\" [ngValue]=\"type\">\n                {{type}}\n              </option>\n            </select>\n          </div>\n          <div class=\"mt-3\">\n            Widget name:\n          </div>\n          <div>\n            <input type=\"text\" [(ngModel)]=\"newWidgetName\" class=\"form-control half-size\" id=\"widgetName\">\n          </div>\n          \n          <div *ngIf=\"newWidgetType == 'slider'\">\n            <div class=\"mt-3\">\n              Max slider value:\n            </div>\n            <div>\n              <input type=\"number\" [(ngModel)]=\"maxSliderValue\" class=\"form-control half-size\" id=\"sliderMaxValue\">\n            </div>\n            <div class=\"mt-3\">\n              Show slider graphic:\n            </div>\n            <div>\n              <ui-switch\n                size=\"small\"\n                switchColor=\"#fcfcfc\"\n                color=\"rgb(35,255,150);\"\n                defaultBgColor=\"rgb(255,80,80);\"\n                defaultBoColor=\"black\"\n                [(ngModel)]=\"showSliderGraphic\"></ui-switch>\n            </div>\n            <div class=\"mt-3\">\n              Set graphic color (RRGGBB):\n            </div>\n            <div>\n              <input type=\"text\" [(ngModel)]=\"sliderGraphicColor\" class=\"form-control half-size\" [style.background]=\"getBackgroundColor()\" id=\"sliderGraphicColor\">\n            </div>\n          </div>\n          \n          <div *ngIf=\"newWidgetType == '1-button' || newWidgetType == '2-buttons' || newWidgetType == '3-buttons' || newWidgetType == '4-buttons'\">\n            <div *ngFor=\"let buttonSettings of getButtonIds(); let i = index;\" >\n              <div class=\"mt-3\">\n                Button {{i}} settings:\n              </div>\n              <div class=\"switch button-settings\">\n             \n                <input type=\"text\" placeholder=\"Button name\" [(ngModel)]=\"buttonSettings.buttonName\" class=\"form-control button-name mr-2\">\n             \n                <ui-switch\n                size=\"small\"\n                switchColor=\"#fcfcfc\"\n                color=\"red\"\n                defaultBgColor=\"#555555\"\n                defaultBoColor=\"black\"\n                [(ngModel)]=\"buttonSettings.buttonColor\"></ui-switch>\n       \n              </div>\n            </div>\n              \n          </div>\n          \n          <div modal-input-section class=\"modal-content-center mt-5\">\n              <button type=\"button\" class=\"btn btn-default mr-2\" onclick=\"this.blur();\" (click)=\"onPopupCancel()\">Cancel</button>\n              <button type=\"button\" *ngIf=\"popupType == 'new_widget'\" class=\"btn btn-success\" onclick=\"this.blur();\"  (click)=\"onSaveNewWidget()\">Save</button>\n              <button type=\"button\" *ngIf=\"popupType == 'edit_widget'\" class=\"btn btn-success\" onclick=\"this.blur();\"  (click)=\"onSaveWidget()\">Save</button>\n          </div>\n        </div>\n      </div>\n      \n      <div *ngIf=\"popupType == 'delete_widget'\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title pull-left modal-header-custom\">WARNING</h4>\n        </div>\n        <div class=\"modal-body modal-content-center\">\n            <div>\n                Do you really want to remove the widget?\n            </div>\n            <div modal-input-section class=\"modal-content-center mt-3\">\n                <button type=\"button\" class=\"btn btn-danger wide-button mr-2\" onclick=\"this.blur();\" (click)=\"onPopupCancel()\">No</button>\n                <button type=\"button\" class=\"btn btn-success\" onclick=\"this.blur();\"  (click)=\"onDeleteWidget()\">Yes</button>\n            </div>\n        </div>\n      </div>\n\n      <div *ngIf=\"popupType == 'rename_widget'\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title pull-left modal-header-custom\">Set widget name</h4>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"mt-3\">\n            Widget name:\n          </div>\n          <div>\n            <input type=\"text\" [(ngModel)]=\"newWidgetName\" class=\"form-control half-size\" id=\"widgetName\">\n          </div>\n\n          <div modal-input-section class=\"modal-content-center mt-5\">\n            <button type=\"button\" class=\"btn btn-default mr-2\" onclick=\"this.blur();\" (click)=\"onPopupCancel()\">Cancel</button>\n            <button type=\"button\" class=\"btn btn-success\" onclick=\"this.blur();\"  (click)=\"onSaveWidget()\">Save</button>\n          </div>\n        </div>\n      </div>\n    \n  ",
            styles: [__webpack_require__(/*! ./actions-widgets.component.css */ "./src/app/components/actions-widgets/actions-widgets.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_6__["BsModalRef"]])
    ], WidgetPopupComponent);
    return WidgetPopupComponent;
}());



/***/ }),

/***/ "./src/app/components/alarm-panel/alarm-panel.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/alarm-panel/alarm-panel.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-panel {\r\n  max-width: 770px;\r\n  margin: auto;\r\n  padding-top: 10px;\r\n}\r\n\r\n.text-input {\r\n  text-align: center;\r\n  background-color: #387e00;\r\n  font-size: 1.3rem;\r\n  padding: 0.8rem 0.5rem;\r\n  border-width: 3px;\r\n  border-style: inset;\r\n  border-color: initial;\r\n  -o-border-image: initial;\r\n     border-image: initial;\r\n  color: #cccccc;\r\n}\r\n\r\n.btn-alarm {\r\n  padding: 0.5rem 1.5rem;\r\n  font-size: 1.8rem;\r\n  border-radius: 1rem;\r\n}\r\n\r\n.alarm-buttons {\r\n  text-align: center;\r\n}\r\n\r\n.img-lock {\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n\r\n.btn-lock-img {\r\n  width: 4.14rem;\r\n  padding: 0.5rem 0.8rem;\r\n}\r\n\r\n.btn-function {\r\n  width: 50%;\r\n  border-radius: 1rem;\r\n}\r\n\r\n.wheater-widget {\r\n  line-height: 1.4;\r\n}\r\n\r\nweather-widget {\r\n  margin: auto;\r\n}\r\n\r\n.function-buttons {\r\n  text-align: center;\r\n}\r\n\r\n.img-func-btn {\r\n  width: 75%;\r\n  margin-top: 1rem;\r\n  margin-bottom: 1rem;\r\n}\r\n\r\n.clock-panel {\r\n  text-align: center;\r\n  font-size: 60px;\r\n  color: #adc9ff;\r\n}\r\n\r\n.custom-btn-primary:hover{\r\n  color: #ffffff;\r\n  background-color: #2a9fd6;\r\n  border-color: #2a9fd6;\r\n}\r\n\r\n.custom-btn-primary:active{\r\n  color: #ffffff;\r\n  background-color: #2180ac;\r\n  border-color: #1f78a1;\r\n}\r\n\r\n.custom-btn-secondary:hover{\r\n  color: #ffffff;\r\n  background-color: #555;\r\n  border-color: #555;\r\n}\r\n\r\n.custom-btn-secondary:active{\r\n  color: #ffffff;\r\n  background-color: #3c3b3b;\r\n  border-color: #353535;\r\n}\r\n\r\n.custom-btn-danger:hover{\r\n  color: #ffffff;\r\n  background-color: #cc0000;\r\n  border-color: #cc0000;\r\n}\r\n\r\n.custom-btn-danger:active{\r\n  color: #ffffff;\r\n  background-color: #990000;\r\n  border-color: #8c0000;\r\n}\r\n\r\n.custom-btn-success:hover{\r\n  color: #ffffff;\r\n  background-color: #77B300;\r\n  border-color: #77B300;\r\n}\r\n\r\n.custom-btn-success:active{\r\n  color: #ffffff;\r\n  background-color: #558000;\r\n  border-color: #4d7300;\r\n}\r\n\r\n.slide-show {\r\n  border-color: darkgray;\r\n  width: 100%;\r\n  height: 339px;\r\n  -o-object-fit: scale-down;\r\n     object-fit: scale-down;\r\n  border-style: inset;\r\n  border-width: 4px;\r\n  border-radius: 7px;\r\n  background-size: cover;\r\n  /*-webkit-transition: opacity 1s ease-in-out;*/\r\n  /*-moz-transition: opacity 1s ease-in-out;*/\r\n  /*-o-transition: opacity 1s ease-in-out;*/\r\n  /*transition: opacity 1s ease-in-out;*/\r\n}\r\n\r\n.transition {\r\n   opacity: 0;\r\n}\r\n\r\n.noselect {\r\n  -webkit-touch-callout: none; /* iOS Safari */\r\n  -webkit-user-select: none; /* Safari */ /* Konqueror HTML */\r\n  -moz-user-select: none; /* Firefox */\r\n  -ms-user-select: none; /* Internet Explorer/Edge */\r\n  user-select: none; /* Non-prefixed version, currently\r\n                                  supported by Chrome and Opera */\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hbGFybS1wYW5lbC9hbGFybS1wYW5lbC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIseUJBQXlCO0VBQ3pCLGlCQUFpQjtFQUNqQixzQkFBc0I7RUFDdEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixxQkFBcUI7RUFDckIsd0JBQXFCO0tBQXJCLHFCQUFxQjtFQUNyQixjQUFjO0FBQ2hCOztBQUVBO0VBQ0Usc0JBQXNCO0VBQ3RCLGlCQUFpQjtFQUNqQixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsWUFBWTtBQUNkOztBQUVBO0VBQ0UsY0FBYztFQUNkLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLFVBQVU7RUFDVixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztBQUNoQjs7QUFFQTtFQUNFLGNBQWM7RUFDZCx5QkFBeUI7RUFDekIscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxjQUFjO0VBQ2Qsc0JBQXNCO0VBQ3RCLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLGNBQWM7RUFDZCx5QkFBeUI7RUFDekIscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLHFCQUFxQjtBQUN2Qjs7QUFFQTtFQUNFLGNBQWM7RUFDZCx5QkFBeUI7RUFDekIscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxzQkFBc0I7RUFDdEIsV0FBVztFQUNYLGFBQWE7RUFDYix5QkFBc0I7S0FBdEIsc0JBQXNCO0VBQ3RCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHNCQUFzQjtFQUN0Qiw4Q0FBOEM7RUFDOUMsMkNBQTJDO0VBQzNDLHlDQUF5QztFQUN6QyxzQ0FBc0M7QUFDeEM7O0FBRUE7R0FDRyxVQUFVO0FBQ2I7O0FBRUE7RUFDRSwyQkFBMkIsRUFBRSxlQUFlO0VBQzVDLHlCQUF5QixFQUFFLFdBQVcsRUFDWixtQkFBbUI7RUFDN0Msc0JBQXNCLEVBQUUsWUFBWTtFQUNwQyxxQkFBcUIsRUFBRSwyQkFBMkI7RUFDbEQsaUJBQWlCLEVBQUU7aUVBQzRDO0FBQ2pFIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hbGFybS1wYW5lbC9hbGFybS1wYW5lbC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW4tcGFuZWwge1xyXG4gIG1heC13aWR0aDogNzcwcHg7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG59XHJcblxyXG4udGV4dC1pbnB1dCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMzODdlMDA7XHJcbiAgZm9udC1zaXplOiAxLjNyZW07XHJcbiAgcGFkZGluZzogMC44cmVtIDAuNXJlbTtcclxuICBib3JkZXItd2lkdGg6IDNweDtcclxuICBib3JkZXItc3R5bGU6IGluc2V0O1xyXG4gIGJvcmRlci1jb2xvcjogaW5pdGlhbDtcclxuICBib3JkZXItaW1hZ2U6IGluaXRpYWw7XHJcbiAgY29sb3I6ICNjY2NjY2M7XHJcbn1cclxuXHJcbi5idG4tYWxhcm0ge1xyXG4gIHBhZGRpbmc6IDAuNXJlbSAxLjVyZW07XHJcbiAgZm9udC1zaXplOiAxLjhyZW07XHJcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcclxufVxyXG5cclxuLmFsYXJtLWJ1dHRvbnMge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLmltZy1sb2NrIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbi5idG4tbG9jay1pbWcge1xyXG4gIHdpZHRoOiA0LjE0cmVtO1xyXG4gIHBhZGRpbmc6IDAuNXJlbSAwLjhyZW07XHJcbn1cclxuXHJcbi5idG4tZnVuY3Rpb24ge1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcclxufVxyXG5cclxuLndoZWF0ZXItd2lkZ2V0IHtcclxuICBsaW5lLWhlaWdodDogMS40O1xyXG59XHJcblxyXG53ZWF0aGVyLXdpZGdldCB7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG59XHJcblxyXG4uZnVuY3Rpb24tYnV0dG9ucyB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uaW1nLWZ1bmMtYnRuIHtcclxuICB3aWR0aDogNzUlO1xyXG4gIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxufVxyXG5cclxuLmNsb2NrLXBhbmVsIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiA2MHB4O1xyXG4gIGNvbG9yOiAjYWRjOWZmO1xyXG59XHJcblxyXG4uY3VzdG9tLWJ0bi1wcmltYXJ5OmhvdmVye1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyYTlmZDY7XHJcbiAgYm9yZGVyLWNvbG9yOiAjMmE5ZmQ2O1xyXG59XHJcblxyXG4uY3VzdG9tLWJ0bi1wcmltYXJ5OmFjdGl2ZXtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjE4MGFjO1xyXG4gIGJvcmRlci1jb2xvcjogIzFmNzhhMTtcclxufVxyXG5cclxuLmN1c3RvbS1idG4tc2Vjb25kYXJ5OmhvdmVye1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgYm9yZGVyLWNvbG9yOiAjNTU1O1xyXG59XHJcblxyXG4uY3VzdG9tLWJ0bi1zZWNvbmRhcnk6YWN0aXZle1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMzYzNiM2I7XHJcbiAgYm9yZGVyLWNvbG9yOiAjMzUzNTM1O1xyXG59XHJcblxyXG4uY3VzdG9tLWJ0bi1kYW5nZXI6aG92ZXJ7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2NjMDAwMDtcclxuICBib3JkZXItY29sb3I6ICNjYzAwMDA7XHJcbn1cclxuXHJcbi5jdXN0b20tYnRuLWRhbmdlcjphY3RpdmV7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzk5MDAwMDtcclxuICBib3JkZXItY29sb3I6ICM4YzAwMDA7XHJcbn1cclxuXHJcbi5jdXN0b20tYnRuLXN1Y2Nlc3M6aG92ZXJ7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzc3QjMwMDtcclxuICBib3JkZXItY29sb3I6ICM3N0IzMDA7XHJcbn1cclxuXHJcbi5jdXN0b20tYnRuLXN1Y2Nlc3M6YWN0aXZle1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTgwMDA7XHJcbiAgYm9yZGVyLWNvbG9yOiAjNGQ3MzAwO1xyXG59XHJcblxyXG4uc2xpZGUtc2hvdyB7XHJcbiAgYm9yZGVyLWNvbG9yOiBkYXJrZ3JheTtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDMzOXB4O1xyXG4gIG9iamVjdC1maXQ6IHNjYWxlLWRvd247XHJcbiAgYm9yZGVyLXN0eWxlOiBpbnNldDtcclxuICBib3JkZXItd2lkdGg6IDRweDtcclxuICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAvKi13ZWJraXQtdHJhbnNpdGlvbjogb3BhY2l0eSAxcyBlYXNlLWluLW91dDsqL1xyXG4gIC8qLW1vei10cmFuc2l0aW9uOiBvcGFjaXR5IDFzIGVhc2UtaW4tb3V0OyovXHJcbiAgLyotby10cmFuc2l0aW9uOiBvcGFjaXR5IDFzIGVhc2UtaW4tb3V0OyovXHJcbiAgLyp0cmFuc2l0aW9uOiBvcGFjaXR5IDFzIGVhc2UtaW4tb3V0OyovXHJcbn1cclxuXHJcbi50cmFuc2l0aW9uIHtcclxuICAgb3BhY2l0eTogMDtcclxufVxyXG5cclxuLm5vc2VsZWN0IHtcclxuICAtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7IC8qIGlPUyBTYWZhcmkgKi9cclxuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lOyAvKiBTYWZhcmkgKi9cclxuICAta2h0bWwtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIEtvbnF1ZXJvciBIVE1MICovXHJcbiAgLW1vei11c2VyLXNlbGVjdDogbm9uZTsgLyogRmlyZWZveCAqL1xyXG4gIC1tcy11c2VyLXNlbGVjdDogbm9uZTsgLyogSW50ZXJuZXQgRXhwbG9yZXIvRWRnZSAqL1xyXG4gIHVzZXItc2VsZWN0OiBub25lOyAvKiBOb24tcHJlZml4ZWQgdmVyc2lvbiwgY3VycmVudGx5XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdXBwb3J0ZWQgYnkgQ2hyb21lIGFuZCBPcGVyYSAqL1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/alarm-panel/alarm-panel.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/alarm-panel/alarm-panel.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row main-panel\">\n  <div *ngIf=\"alarmArmed === false\" class=\"col-lg-4 col-xs-4 col-md-4 wheater-widget\">\n    <weather-widget [settings]=\"settings\"></weather-widget>\n  </div>\n  <div *ngIf=\"alarmArmed === true\" class=\"col-lg-4 col-xs-4 col-md-4 \">\n  </div>\n  <div class=\"col-lg-4 col-xs-4 col-md-4 mt-4\">\n    <div *ngIf=\"slideShowOn == false\">\n      <div class=\"form-group noselect form-control form-control-lg text-input mt-4\" [style.background-color]=\"getAlarmTextBackground()\" id=\"inputLarge\" (click)=\"enableSlideshow()\">{{displayValue}}</div>\n      <div class=\"alarm-buttons\">\n      <div class=\"mb-2\">\n      <button class=\"btn btn-secondary custom-btn-secondary btn-alarm mr-2\" onclick=\"this.blur();\" touch-button (onTouch)=\"typePassword($event)\">1</button>\n      <button class=\"btn btn-secondary custom-btn-secondary btn-alarm\" onclick=\"this.blur();\" touch-button (onTouch)=\"typePassword($event)\">2</button>\n      <button class=\"btn btn-secondary custom-btn-secondary btn-alarm ml-2\" onclick=\"this.blur();\" touch-button (onTouch)=\"typePassword($event)\">3</button>\n      </div>\n      <div class=\"mb-2\">\n      <button class=\"btn btn-secondary custom-btn-secondary btn-alarm mr-2\" onclick=\"this.blur();\" touch-button (onTouch)=\"typePassword($event)\">4</button>\n      <button class=\"btn btn-secondary custom-btn-secondary btn-alarm\" onclick=\"this.blur();\" touch-button (onTouch)=\"typePassword($event)\">5</button>\n      <button class=\"btn btn-secondary custom-btn-secondary btn-alarm ml-2\" onclick=\"this.blur();\" touch-button (onTouch)=\"typePassword($event)\">6</button>\n      </div>\n      <div class=\"mb-2\">\n      <button class=\"btn btn-secondary custom-btn-secondary btn-alarm mr-2\" onclick=\"this.blur();\" touch-button (onTouch)=\"typePassword($event)\">7</button>\n      <button class=\"btn btn-secondary custom-btn-secondary btn-alarm\" onclick=\"this.blur();\" touch-button (onTouch)=\"typePassword($event)\">8</button>\n      <button class=\"btn btn-secondary custom-btn-secondary btn-alarm ml-2\" onclick=\"this.blur();\" touch-button (onTouch)=\"typePassword($event)\">9</button>\n      </div>\n      <div class=\"mb-2\">\n      <button *ngIf=\"password == ''\" class=\"btn btn-danger custom-btn-danger btn-alarm btn-lock-img mr-2\" onclick=\"this.blur();\"  (click)=\"armAlarm()\">\n      <img src=\"assets/img/lock.png\" class=\"img-lock\"/>\n      </button>\n      <button *ngIf=\"password != ''\" class=\"btn btn-secondary custom-btn-secondary btn-alarm btn-lock-img mr-2\" onclick=\"this.blur();\" (click)=\"clearPassword()\">C</button>\n      <button class=\"btn btn-secondary custom-btn-secondary btn-alarm\" onclick=\"this.blur();\" touch-button (onTouch)=\"typePassword($event)\">0</button>\n      <button class=\"btn btn-success custom-btn-success btn-alarm btn-lock-img ml-2\" onclick=\"this.blur();\" touch-button (onTouch)=\"disarmAlarm()\">\n      <img src=\"assets/img/unlock.png\" class=\"img-lock\"/>\n      </button>\n      </div>\n      </div>\n    </div>\n\n    <div id=\"slideshow\" *ngIf=\"slideShowOn\" [style.background-image]=\"'url(' + image + ')' | safe: 'style'\" class=\"slide-show mt-4\" long-press  (click)=\"getImage()\" (onLongPress)=\"disableSlideShow()\" >\n    </div>\n\n  </div>\n  <div *ngIf=\"alarmArmed === false\" class=\"col-lg-4 col-xs-4 col-md-4 pt-4\">\n    <div class=\"function-buttons\">\n      <div>\n        <button class=\"btn btn-primary custom-btn-primary btn-function mt-4\" onclick=\"this.blur();\" touch-button (onTouch)=\"onSendSms()\">\n          <img src=\"assets/img/sms_icon2.png\" class=\"img-func-btn\"/>\n        </button>\n      </div>\n      <div>\n        <button class=\"btn btn-primary custom-btn-primary btn-function mt-5 mb-3\" onclick=\"this.blur();\" touch-button (onTouch)=\"turnOffLights()\">\n          <img src=\"assets/img/bulb_icon.png\" class=\"img-func-btn\"/>\n        </button>\n      </div>\n      <div class=\"clock-panel mb-4\">\n        {{hours}}:{{minutes}}\n      </div>\n    </div>\n  </div>\n  <div *ngIf=\"alarmArmed === true\" class=\"col-lg-4 col-xs-4 col-md-4 pt-4\">\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/components/alarm-panel/alarm-panel.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/alarm-panel/alarm-panel.component.ts ***!
  \*****************************************************************/
/*! exports provided: AlarmPanelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlarmPanelComponent", function() { return AlarmPanelComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var _services_tools_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/tools.service */ "./src/app/services/tools.service.ts");
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../models/consts */ "./src/app/models/consts.ts");
/* harmony import */ var md5_typescript__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! md5-typescript */ "./node_modules/md5-typescript/dist/index.js");
/* harmony import */ var rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/observable/TimerObservable */ "./node_modules/rxjs-compat/_esm5/observable/TimerObservable.js");
/* harmony import */ var angular_weather_widget__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-weather-widget */ "./node_modules/angular-weather-widget/index.js");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../utils/deviceConsts */ "./src/app/utils/deviceConsts.ts");










var AlarmPanelComponent = /** @class */ (function () {
    function AlarmPanelComponent(restService, toolsService, deviceManager) {
        this.restService = restService;
        this.toolsService = toolsService;
        this.deviceManager = deviceManager;
        this.imageType = 'data:image/JPG;base64,';
        this.settings = {
            location: {
                cityName: 'Lublin'
            },
            backgroundColor: '#000000',
            color: '#adc9ff',
            width: '250px',
            height: 'auto',
            showWind: true,
            scale: angular_weather_widget__WEBPACK_IMPORTED_MODULE_7__["TemperatureScale"].CELCIUS,
            forecastMode: angular_weather_widget__WEBPACK_IMPORTED_MODULE_7__["ForecastMode"].GRID,
            showDetails: true,
            showForecast: true,
            layout: angular_weather_widget__WEBPACK_IMPORTED_MODULE_7__["WeatherLayout"].NARROW,
            language: 'pl'
        };
        this.failImage = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAOEAlgDASIAAhEBAxEB/8QAGwABAQADAQEBAAAAAAAAAAAAAAcDBQYEAgn/xAAxEAEAAQMBBwIGAwACAwEAAAAAAQIDBAUGERIhMUFRI3EUIlKBscEzYtETYQcVQqH/xAAaAQEAAwEBAQAAAAAAAAAAAAAABAYHAwIF/8QAMhEBAAECAggDCQACAwAAAAAAAAECAwYxBAUREiEiQVETYXEUFTJCYoGhssEjsQeR8P/aAAwDAQACEQMRAD8A/KoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABvti9IwtZ1evH1C3VXat2KrvBFU08U76Y5zHP/638vENCy4mXk4OTby8S9VavWp4qaqe3++zxXE1UzFM7JTtW6RZ0XTLd7SKN+imYmae8ffhPpPCXbbTbCWaMb4zQLFVNVqPUx+KauOPNO+Znf8A9d+3Prwir7M7TY20GNw1cNrMtR6trf1/tT5j8dJ7TOo2x2O+L49X0m16/Oq9Zpj+TzVTH1eY7+/WHZv1UVeHdaHiLC+jay0aNb6iiJpmNs00x+aY6THWn/rjnPwE9lwAAAAAAAAAAAAAA2WhaFma/mRjY0cNundN27MfLbp/cz2jv7b5hoWhZmv5kY2NHDbp3TduzHy26f3M9o7+2+Yqmn6fp+hafGNjRTasWomuuuuYjfy511T9uv4iEXSNIi1G7TmuuE8J166r9p0nlsU5zlvbOkeXeftHHKebR7F39Bw6c63mfFW+PgubrU0zRv6T1nl27c5jy5x0e1+1X/vLsYeJG7Cs18VMzHzXKt0xxf8AUc53R/3vnxHOOtnf3P8AJm+LiGNW06fXTqqP8UbIzmYmesxM7Z2f+jhsAHV8QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABlxMvJwcm3l4l6q1etTxU1U9v99lS2Z2mxtoMbhq4bWZaj1bW/r/AGp8x+Ok9pmUMuJl5ODk28vEvVWr1qeKmqnt/vs4X7EXo81mw1iW/h+/tjmtVfFT/Y7TH5ynpMd3tjsd8Xx6vpNr1+dV6zTH8nmqmPq8x39+s/VfZnabG2gxuGrhtZlqPVtb+v8AanzH46T2mdRtjsd8Xx6vpNr1+dV6zTH8nmqmPq8x39+sexfm3PhXVtxLhqxrax761LzRVxqpjr3mI6VR81PX1zn4CeywAAAAAAAAAAbLQtCzNfzIxsaOG3Tum7dmPlt0/uZ7R39t8w0LQszX8yMbGjht07pu3Zj5bdP7me0d/bfMVTT9P0/QtPjGxoptWLUTXXXXMRv5c66p+3X8RCLpGkRajdpzXXCeE69dV+06Ty2Kc5y3tnSPLvP2jjk0/T9P0LT4xsaKbVi1E1111zEb+XOuqft1/EQn21u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiG1u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiOaedH0eYnxLmafizFlF+j3Vqrls08JmOG9s6R9P7emYBMZ2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAy4mXk4OTby8S9VavWp4qaqe3++ypbM7TY20GNw1cNrMtR6trf1/tT5j8dJ7TMoZcTLycHJt5eJeqtXrU8VNVPb/AH2cL9iL0eazYaxLfw/f2xzWqvip/sdpj85T0mO72x2O+L49X0m16/Oq9Zpj+TzVTH1eY7+/WfqvsztNjbQY3DVw2sy1Hq2t/X+1PmPx0ntM6jbHY74vj1fSbXr86r1mmP5PNVMfV5jv79Y9i/NufCurbiXDVjW1j31qXmirjVTHXvMR0qj5qevrnPwE9lgAAAAAA2WhaFma/mRjY0cNundN27MfLbp/cz2jv7b5hoWhZmv5kY2NHDbp3TduzHy26f3M9o7+2+Yqmn6fp+hafGNjRTasWomuuuuYjfy511T9uv4iEXSNIi1G7TmuuE8J166r9p0nlsU5zlvbOkeXeftHHJp+n6foWnxjY0U2rFqJrrrrmI38uddU/br+IhPtrdrbms3JwcGqqjBonnPSb0x3nxHiPvPPdENrdrbms3JwcGqqjBonnPSb0x3nxHiPvPPdEc086Po8xPiXM0/FmLKL9HurVXLZp4TMcN7Z0j6f29MwCYzsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABlxMvJwcm3l4l6q1etTxU1U9v99lS2Z2mxtoMbhq4bWZaj1bW/r/anzH46T2mZQy4mXk4OTby8S9VavWp4qaqe3++zhfsRejzWbDWJb+H7+2Oa1V8VP8AY7TH5ynpMd3tjsd8Xx6vpNr1+dV6zTH8nmqmPq8x39+s/VfZnabG2gxuGrhtZlqPVtb+v9qfMfjpPaZ1G2Ox3xfHq+k2vX51XrNMfyeaqY+rzHf36x7F+bc+FdW3EuGrGtrHvrUvNFXGqmOveYjpVHzU9fXOfgJ7LAABstC0LM1/MjGxo4bdO6bt2Y+W3T+5ntHf23zDQtCzNfzIxsaOG3Tum7dmPlt0/uZ7R39t8xVNP0/T9C0+MbGim1YtRNdddcxG/lzrqn7dfxEIukaRFqN2nNdcJ4Tr11X7TpPLYpznLe2dI8u8/aOOTT9P0/QtPjGxoptWLUTXXXXMRv5c66p+3X8RCfbW7W3NZuTg4NVVGDRPOek3pjvPiPEfeee6IbW7W3NZuTg4NVVGDRPOek3pjvPiPEfeee6I5p50fR5ifEuZp+LMWUX6PdWquWzTwmY4b2zpH0/t6ZgExnYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADLiZeTg5NvLxL1Vq9anipqp7f77KlsztNjbQY3DVw2sy1Hq2t/X+1PmPx0ntMyhlxMvJwcm3l4l6q1etTxU1U9v99nC/Yi9Hms2GsS38P39sc1qr4qf7HaY/OU9Jju9sdjvi+PV9JtevzqvWaY/k81Ux9XmO/v1n6r7M7TY20GNw1cNrMtR6trf1/tT5j8dJ7TOo2x2O+L49X0m16/Oq9Zpj+TzVTH1eY7+/WPYvzbnwrq24lw1Y1tY99al5oq41Ux17zEdKo+anr65z9stC0LM1/MjGxo4bdO6bt2Y+W3T+5ntHf23zDQtCzNfzIxsaOG3Tum7dmPlt0/uZ7R39t8xVNP0/T9C0+MbGim1YtRNdddcxG/lzrqn7dfxEOukaRFqN2nN8HCeE69dV+06Ty2Kc5y3tnSPLvP2jjk0/T9P0LT4xsaKbVi1E1111zEb+XOuqft1/EQn21u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiG1u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiOaedH0eYnxLmafizFlF+j3Vqrls08JmOG9s6R9P7emYBMZ2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAy4mXk4OTby8S9VavWp4qaqe3++yqbMbR2tocOqubf/AB5NjdF6iInh3zv3TTPid08usbvaZnGhaFma/mRjY0cNundN27MfLbp/cz2jv7b5iqafp+n6Fp8Y2NFNqxaia6665iN/LnXVP26/iIQNNqomIp+Zqf8Ax1o2saK69Iid3Rp27Yn5p709tnWcunGcvTasWLHH/wAFm3b/AOSublfBTEcVU9ap3dZnymu1u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiO60faTS9cuXrWDdq47Mzvprjhmqn64jvH/7HeI3w0G2Ox3xfHq+k2vX51XrNMfyeaqY+rzHf368NH3bdz/LHFY8V06VrTVG/qauKrfHeinOqO0THadu2M5/Ez8B9ZhIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA2WhaFma/mRjY0cNundN27MfLbp/cz2jv7b5hoWhZmv5kY2NHDbp3TduzHy26f3M9o7+2+Yqmn6fp+hafGNjRTasWomuuuuYjfy511T9uv4iEXSNIi1G7TmuuE8J166r9p0nlsU5zlvbOkeXeftHHJp+n6foWnxjY0U2rFqJrrrrmI38uddU/br+IhPtrdrbms3JwcGqqjBonnPSb0x3nxHiPvPPdENrdrbms3JwcGqqjBonnPSb0x3nxHiPvPPdEc086Po8xPiXM0/FmLKL9HurVXLZp4TMcN7Z0j6f29M8uJl5ODk28vEvVWr1qeKmqnt/vsqWzO02NtBjcNXDazLUera39f7U+Y/HSe0zKGXEy8nBybeXiXqrV61PFTVT2/wB9nW/Yi9Hm+DhrEt/D9/bHNaq+Kn+x2mPzlPSY7vbHY74vj1fSbXr86r1mmP5PNVMfV5jv79Z+q+zO02NtBjcNXDazLUera39f7U+Y/HSe0zqNsdjvi+PV9JtevzqvWaY/k81Ux9XmO/v1j2L8258K6tuJcNWNbWPfWpeaKuNVMde8xHSqPmp6+uc/AT2WAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADZaFoWZr+ZGNjRw26d03bsx8tun9zPaO/tvmGhaFma/mRjY0cNundN27MfLbp/cz2jv7b5iqafp+n6Fp8Y2NFNqxaia6665iN/LnXVP26/iIRdI0iLUbtOa64TwnXrqv2nSeWxTnOW9s6R5d5+0ccmn6fp+hafGNjRTasWomuuuuYjfy511T9uv4iE+2t2tuazcnBwaqqMGiec9JvTHefEeI+8890Q2t2tuazcnBwaqqMGiec9JvTHefEeI+8890RzTzo+jzE+JczT8WYsov0e6tVctmnhMxw3tnSPp/b0zAJjOwAGXEy8nBybeXiXqrV61PFTVT2/32VLZnabG2gxuGrhtZlqPVtb+v9qfMfjpPaZlDLiZeTg5NvLxL1Vq9anipqp7f77OF+xF6PNZsNYlv4fv7Y5rVXxU/2O0x+cp6THd7Y7HfF8er6Ta9fnVes0x/J5qpj6vMd/frP1X2Z2mxtoMbhq4bWZaj1bW/r/anzH46T2mdRtjsd8Xx6vpNr1+dV6zTH8nmqmPq8x39+sexfm3PhXVtxLhqxrax761LzRVxqpjr3mI6VR81PX1zn4CeywAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAbLQtCzNfzIxsaOG3Tum7dmPlt0/uZ7R39t8w0LQszX8yMbGjht07pu3Zj5bdP7me0d/bfMVTT9P0/QtPjGxoptWLUTXXXXMRv5c66p+3X8RCLpGkRajdpzXXCeE69dV+06Ty2Kc5y3tnSPLvP2jjk0/T9P0LT4xsaKbVi1E1111zEb+XOuqft1/EQn21u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiG1u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiOaedH0eYnxLmafizFlF+j3Vqrls08JmOG9s6R9P7emYBMZ2AAAAAAy4mXk4OTby8S9VavWp4qaqe3++ypbM7TY20GNw1cNrMtR6trf1/tT5j8dJ7TMoZcTLycHJt5eJeqtXrU8VNVPb/fZwv2IvR5rNhrEt/D9/bHNaq+Kn+x2mPzlPSY7vbHY74vj1fSbXr86r1mmP5PNVMfV5jv79Z+q+zO02NtBjcNXDazLUera39f7U+Y/HSe0zqNsdjvi+PV9JtevzqvWaY/k81Ux9XmO/v1j2L8258K6tuJcNWNbWPfWpeaKuNVMde8xHSqPmp6+uc/AT2WAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADZaFoWZr+ZGNjRw26d03bsx8tun9zPaO/tvmGhaFma/mRjY0cNundN27MfLbp/cz2jv7b5iqafp+n6Fp8Y2NFNqxaia6665iN/LnXVP26/iIRdI0iLUbtOa64TwnXrqv2nSeWxTnOW9s6R5d5+0ccmn6fp+hafGNjRTasWomuuuuYjfy511T9uv4iE+2t2tuazcnBwaqqMGiec9JvTHefEeI+8890Q2t2tuazcnBwaqqMGiec9JvTHefEeI+8890RzTzo+jzE+JczT8WYsov0e6tVctmnhMxw3tnSPp/b0zAJjOwAAAAAAAAAGXEy8nBybeXiXqrV61PFTVT2/wB9lS2Z2mxtoMbhq4bWZaj1bW/r/anzH46T2mZQy4mXk4OTby8S9VavWp4qaqe3++zhfsRejzWbDWJb+H7+2Oa1V8VP9jtMfnKekx3e2Ox3xfHq+k2vX51XrNMfyeaqY+rzHf36z9V9mdpsbaDG4auG1mWo9W1v6/2p8x+Ok9pnUbY7HfF8er6Ta9fnVes0x/J5qpj6vMd/frHsX5tz4V1bcS4asa2se+tS80VcaqY695iOlUfNT19c5+AnssAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWbT9P0/QtPjGxoptWLUTXXXXMRv5c66p+3X8RCfbW7W3NZuTg4NVVGDRPOek3pjvPiPEfeee6I0lzVtTu4dOn3c+/VjU9LU1zw7uW6PaOGN0dI7PKiWdG3Kt+udsr1r7GdWsdEp0DQLfhWtmyY7/AExsyp/312RwkAlqKAAAAAAAAAAAAAAy4mXk4OTby8S9VavWp4qaqe3++ypbM7TY20GNw1cNrMtR6trf1/tT5j8dJ7TMofdi/fxbtN/GvXLVynfw10VTTVG+N3KYcL9iL0eay4bxLpGHr+2nmtVfFT/Y7T/vKekx1v8A5E0nCw8mxqGNFNu7lzX/AMtuN/zTG7545bo68+fPfE7usuPZ83OzNRvzk52TcvXJ/wDqud+6N8zujxHOeUcmB7tUTRRFNU7Xz9d6dY1lp9zStHt7lNU7dn24zPTbM8Z2d/uAOj5QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/9k=';
    }
    AlarmPanelComponent.prototype.ngOnInit = function () {
        this.password = '';
        this.displayValue = '';
        this.armingSystem = false;
        this.armingCounter = 0;
        this.smsCounter = 0;
        this.slideShowOn = false;
        this.alarmArmed = false;
        this.startArmStatusSubscription();
        this.startClockSubscription();
        this.deviceManager.loadDevices();
    };
    AlarmPanelComponent.prototype.ngOnDestroy = function () {
        this.unsubscribeArmStatusSubscription();
        this.unsubscribePasswordTypingSubscription();
        this.unsubscribeArmingSystemSubscription();
        this.unsubscribeClockSubscription();
        this.unsubscribeSmsSubscription();
        this.unsubscribeImageSubscription();
    };
    AlarmPanelComponent.prototype.typePassword = function (event) {
        if (!this.armingSystem) {
            var btnLabel = event['target'].textContent;
            this.password += btnLabel;
            if (this.password.length == 1) {
                this.displayValue = '#';
            }
            else if (this.password.length < 10) {
                this.displayValue += '#';
            }
            this.stopTyping();
            this.startTyping();
        }
    };
    AlarmPanelComponent.prototype.stopTyping = function () {
        this.unsubscribePasswordTypingSubscription();
    };
    AlarmPanelComponent.prototype.startTyping = function () {
        var _this = this;
        var timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_6__["TimerObservable"].create(5000, 5000);
        this.passwordTypingSubscription = timer.subscribe(function (t) {
            _this.clearPassword();
        });
    };
    AlarmPanelComponent.prototype.clearPassword = function () {
        this.stopTyping();
        this.password = '';
        this.showAlarmArmedText();
    };
    AlarmPanelComponent.prototype.armAlarm = function () {
        var _this = this;
        if (!this.armingSystem) {
            this.clearPassword();
            this.restService.armSystem(1, '').subscribe(function (data) {
                if (data.errorCode) {
                }
                else {
                    _this.armingSystem = true;
                    _this.displayValue = _models_consts__WEBPACK_IMPORTED_MODULE_4__["ALARM_ARMING"];
                    _this.unsubscribeArmStatusSubscription();
                    var timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_6__["TimerObservable"].create(2000, 2000);
                    _this.armingSystemSubscription = timer.subscribe(function (t) {
                        _this.checkAlarmStatus();
                    });
                }
            });
        }
    };
    AlarmPanelComponent.prototype.disarmAlarm = function () {
        var _this = this;
        if (this.alarmArmed && !this.armingSystem) {
            this.restService.armSystem(0, this.getHash(this.password)).subscribe(function (data) {
                _this.unsubscribeArmStatusSubscription();
                _this.stopTyping();
                _this.password = '';
                var timer;
                if (data.errorCode) {
                    _this.displayValue = data.result.toUpperCase();
                    timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_6__["TimerObservable"].create(5000, 5000);
                }
                else {
                    timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_6__["TimerObservable"].create(0, 5000);
                }
                _this.armStatusSubscription = timer.subscribe(function (t) {
                    _this.checkAlarmStatus();
                });
            });
        }
        this.stopTyping();
        this.password = '';
    };
    AlarmPanelComponent.prototype.checkAlarmStatus = function () {
        var _this = this;
        this.restService.getArmSystemStatus().subscribe(function (data) {
            console.log('data: ', data);
            if (data.errorCode) {
            }
            else {
                _this.alarmArmed = (data.value == 1);
                if (_this.armingSystem) {
                    if (_this.alarmArmed) {
                        _this.armingSystem = false;
                        _this.unsubscribeArmingSystemSubscription();
                        _this.startArmStatusSubscription();
                        _this.showAlarmArmedText();
                    }
                    _this.armingCounter++;
                    if (_this.armingCounter > 10) {
                        _this.armingCounter = 0;
                        _this.armingSystem = false;
                        _this.unsubscribeArmingSystemSubscription();
                        _this.startArmStatusSubscription();
                        _this.showAlarmArmedText();
                    }
                }
                else {
                    _this.showAlarmArmedText();
                }
            }
        });
    };
    AlarmPanelComponent.prototype.getAlarmTextBackground = function () {
        if (this.armingSystem) {
            return "#bb8000";
        }
        else {
            if (this.alarmArmed) {
                return "#C00000";
            }
            else {
                return "#387e00";
            }
        }
    };
    AlarmPanelComponent.prototype.showAlarmArmedText = function () {
        if (this.password == '') {
            if (this.alarmArmed) {
                this.displayValue = _models_consts__WEBPACK_IMPORTED_MODULE_4__["ALARM_ARMED"];
            }
            else {
                this.displayValue = _models_consts__WEBPACK_IMPORTED_MODULE_4__["ALARM_DISARMED"];
            }
        }
    };
    AlarmPanelComponent.prototype.getHash = function (userPasswd) {
        var serverPasswd = '@larmP@ssword';
        var millis = Math.floor((Date.now() / 1000)).toString();
        return md5_typescript__WEBPACK_IMPORTED_MODULE_5__["Md5"].init(serverPasswd + millis + userPasswd);
    };
    AlarmPanelComponent.prototype.unsubscribeArmStatusSubscription = function () {
        if (this.armStatusSubscription) {
            this.armStatusSubscription.unsubscribe();
        }
    };
    AlarmPanelComponent.prototype.unsubscribePasswordTypingSubscription = function () {
        if (this.passwordTypingSubscription) {
            this.passwordTypingSubscription.unsubscribe();
        }
    };
    AlarmPanelComponent.prototype.unsubscribeArmingSystemSubscription = function () {
        if (this.armingSystemSubscription) {
            this.armingSystemSubscription.unsubscribe();
        }
    };
    AlarmPanelComponent.prototype.unsubscribeClockSubscription = function () {
        if (this.clockSubscription) {
            this.clockSubscription.unsubscribe();
        }
    };
    AlarmPanelComponent.prototype.unsubscribeSmsSubscription = function () {
        if (this.smsSubscription) {
            this.smsSubscription.unsubscribe();
        }
    };
    AlarmPanelComponent.prototype.startArmStatusSubscription = function () {
        var _this = this;
        var timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_6__["TimerObservable"].create(0, 5000);
        this.armStatusSubscription = timer.subscribe(function (t) {
            _this.checkAlarmStatus();
        });
    };
    AlarmPanelComponent.prototype.startClockSubscription = function () {
        var _this = this;
        var timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_6__["TimerObservable"].create(0, 1000);
        this.clockSubscription = timer.subscribe(function (t) {
            _this.setClock();
        });
    };
    AlarmPanelComponent.prototype.startSmsSubscription = function () {
        var _this = this;
        var timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_6__["TimerObservable"].create(2000, 2000);
        this.smsSubscription = timer.subscribe(function (t) {
            console.log('>>>> reset sms counter');
            _this.smsCounter = 0;
            _this.unsubscribeSmsSubscription();
        });
    };
    AlarmPanelComponent.prototype.startImageSubscription = function () {
        var _this = this;
        var timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_6__["TimerObservable"].create(30000, 30000);
        this.imageSubscription = timer.subscribe(function (t) {
            _this.getImage();
        });
    };
    AlarmPanelComponent.prototype.unsubscribeImageSubscription = function () {
        if (this.imageSubscription) {
            this.imageSubscription.unsubscribe();
        }
    };
    AlarmPanelComponent.prototype.setClock = function () {
        var date = new Date();
        this.hours = date.getHours().toString();
        var mins = date.getMinutes();
        if (mins < 10) {
            this.minutes = '0' + mins.toString();
        }
        else {
            this.minutes = mins.toString();
        }
    };
    AlarmPanelComponent.prototype.turnOffLights = function () {
        this.setDeviceParameter(this.deviceManager.getDevice(3, 11), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(3, 15), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(3, 17), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(3, 25), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(3, 26), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(3, 30), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(3, 31), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(3, 35), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(3, 36), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(7, 1), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_MODE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(7, 10), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_MODE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(7, 12), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_MODE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(7, 3), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_MODE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(7, 6), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_MODE, 0);
        this.setDeviceParameter(this.deviceManager.getDevice(5, 0), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_9__["DeviceConsts"].PARAM_DUTY_CYCLE_ALL, 0);
    };
    AlarmPanelComponent.prototype.setDeviceParameter = function (dev, param, val) {
        this.restService.setDeviceParameter(dev, param, val).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Setting device parameter failed: ' + data.result);
            }
            else {
                console.log('Device parameter set successfully!');
            }
        });
    };
    AlarmPanelComponent.prototype.sendSms = function (number, message) {
        this.restService.sendMessage(number, message).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Sending SMS failed: ' + data.result);
            }
            else {
                console.log('SMS sent successfully!');
            }
        });
    };
    AlarmPanelComponent.prototype.getImage = function () {
        var _this = this;
        this.toolsService.getImage().subscribe(function (data) {
            console.log('>>>> DATA: ', data);
            _this.unsubscribeImageSubscription();
            _this.startImageSubscription();
            if (data['success'] && _this.validateImage(data['img'])) {
                _this.image = _this.imageType + data['img'];
            }
            else {
                _this.image = _this.imageType + _this.failImage;
            }
        });
    };
    AlarmPanelComponent.prototype.enableSlideshow = function () {
        if (!this.image) {
            this.getImage();
        }
        this.startImageSubscription();
        this.slideShowOn = true;
    };
    AlarmPanelComponent.prototype.disableSlideShow = function () {
        this.slideShowOn = false;
        this.unsubscribeImageSubscription();
    };
    AlarmPanelComponent.prototype.onSendSms = function () {
        this.smsCounter++;
        console.log('>>> sms counter: ' + this.smsCounter);
        if (this.smsCounter == 3) {
            this.sendSms('+48788158458', 'TEST');
        }
        this.unsubscribeSmsSubscription();
        this.startSmsSubscription();
    };
    AlarmPanelComponent.prototype.validateImage = function (img) {
        return (img.substring(0, 4) == '/9j/');
    };
    AlarmPanelComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-alarm-panel',
            template: __webpack_require__(/*! ./alarm-panel.component.html */ "./src/app/components/alarm-panel/alarm-panel.component.html"),
            styles: [__webpack_require__(/*! ./alarm-panel.component.css */ "./src/app/components/alarm-panel/alarm-panel.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            _services_tools_service__WEBPACK_IMPORTED_MODULE_3__["ToolsService"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_8__["DeviceManagerService"]])
    ], AlarmPanelComponent);
    return AlarmPanelComponent;
}());



/***/ }),

/***/ "./src/app/components/backup-restore/backup-restore.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/backup-restore/backup-restore.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-width {\r\n\twidth: 100%;\r\n}\r\n\r\n.flash-message-container {\r\n  position: fixed;\r\n  z-index: 10;\r\n  width: 100%;\r\n}\r\n\r\n.flash-message {\r\n  display: block;\r\n  margin-right: 10%;\r\n  margin-left: 10%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9iYWNrdXAtcmVzdG9yZS9iYWNrdXAtcmVzdG9yZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0NBQ0MsV0FBVztBQUNaOztBQUVBO0VBQ0UsZUFBZTtFQUNmLFdBQVc7RUFDWCxXQUFXO0FBQ2I7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGdCQUFnQjtBQUNsQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFja3VwLXJlc3RvcmUvYmFja3VwLXJlc3RvcmUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idG4td2lkdGgge1xyXG5cdHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZmxhc2gtbWVzc2FnZS1jb250YWluZXIge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB6LWluZGV4OiAxMDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmZsYXNoLW1lc3NhZ2Uge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbi1yaWdodDogMTAlO1xyXG4gIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/backup-restore/backup-restore.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/backup-restore/backup-restore.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"flash-message-container\">\n  <div class=\"flash-message\">\n    <flash-messages></flash-messages>\n  </div>\n</div>\n<div class=\"container mt-2 \">\n\t<div class=\"col-lg-4 col-sm-12 col-xs-12 mt-3\">\n\t  <button type=\"button\" *ngFor=\"let dataName of dataSets\" (click)=\"saveData(dataName)\" class=\"btn btn-primary mb-2 mr-2 btn-width\">Download {{dataName}} data</button>\n\t  \n\t</div>\n\n\t<div>\n\t\t<div class=\"input-group mb-3 mt-3 col-lg-6 col-sm-12 col-xs-12\">\n        <div class=\"custom-file\">\n          <input type=\"file\" class=\"custom-file-input\" id=\"inputFile\" (change)=\"onFileSelect($event.target.files)\">\n          <label class=\"custom-file-label\" for=\"inputGroupFile02\">{{selectedFileName}}</label>\n        </div>\n        <div class=\"input-group-append\">\n          <span class=\"btn btn-secondary\" id=\"loadDataBtn\" (click)=\"loadData()\">Upload Data</span>\n        </div>\n      </div>  \n\t</div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/components/backup-restore/backup-restore.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/backup-restore/backup-restore.component.ts ***!
  \***********************************************************************/
/*! exports provided: BackupRestoreComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BackupRestoreComponent", function() { return BackupRestoreComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_backup_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/backup.service */ "./src/app/services/backup.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__);




var BackupRestoreComponent = /** @class */ (function () {
    function BackupRestoreComponent(backupService, flashMessage) {
        this.backupService = backupService;
        this.flashMessage = flashMessage;
        this.fileToUpload = null;
        this.uploadJson = null;
    }
    BackupRestoreComponent.prototype.ngOnInit = function () {
        this.selectedFileName = 'Choose file';
        this.dataSets = [
            'actionWidgetsGroups',
            'actionWidgets',
            'widgetActions',
            'devices',
            'basicConfigs',
            'deviceConfigs',
        ];
    };
    BackupRestoreComponent.prototype.onFileSelect = function (files) {
        var _this = this;
        this.fileToUpload = files.item(0);
        if (this.fileToUpload) {
            this.selectedFileName = this.fileToUpload.name;
        }
        var fileReader = new FileReader();
        fileReader.onload = function (e) {
            console.log(fileReader.result);
            _this.uploadJson = fileReader.result;
        };
        fileReader.readAsText(this.fileToUpload);
    };
    BackupRestoreComponent.prototype.loadData = function () {
        var _this = this;
        if (this.uploadJson) {
            var obj;
            try {
                obj = JSON.parse(this.uploadJson);
            }
            catch (ex) {
                this.flashMessage.show('Uploading data failed: ' + ex, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            var dataName = Object.keys(obj)[0];
            this.backupService.saveData(dataName, obj).subscribe(function (data) {
                console.log('data: ', data);
                //console.log('>>> err code: ', data.errorCode);
                if (data['success'] === false) {
                    _this.flashMessage.show('Uploading data failed: ' + data['result'], {
                        cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                        timeout: 5000
                    });
                }
                else if (data['success'] === true) {
                    _this.flashMessage.show('\'' + dataName + '\' data loaded successfully', {
                        cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                        timeout: 5000
                    });
                }
            }, function (error) {
                console.log('>>> error: ', error.message);
                _this.flashMessage.show('Uploading data failed: ' + error.message, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            });
        }
        else {
            this.flashMessage.show('No file selected', {
                cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                timeout: 5000
            });
        }
    };
    BackupRestoreComponent.prototype.saveData = function (name) {
        var _this = this;
        this.backupService.getData(name).subscribe(function (data) {
            console.log('data: ', data);
            //console.log('>>> err code: ', data.errorCode);
            if (data['success'] === false) {
                _this.flashMessage.show('Downloading data failed: ' + data['result'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else if (data['success'] === true) {
                if (data[name]) {
                    var backupData = {};
                    backupData[name] = data[name];
                    var stream = JSON.stringify(backupData);
                    var blob = new Blob([stream], { type: 'application/octet-stream' });
                    var url = window.URL.createObjectURL(blob);
                    var a = document.createElement('a');
                    document.body.appendChild(a);
                    a.setAttribute('style', 'display: none');
                    a.href = url;
                    a.download = name + '.json';
                    a.click();
                    window.URL.revokeObjectURL(url);
                    a.remove();
                }
            }
        });
    };
    BackupRestoreComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-backup-restore',
            template: __webpack_require__(/*! ./backup-restore.component.html */ "./src/app/components/backup-restore/backup-restore.component.html"),
            styles: [__webpack_require__(/*! ./backup-restore.component.css */ "./src/app/components/backup-restore/backup-restore.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_backup_service__WEBPACK_IMPORTED_MODULE_2__["BackupService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__["FlashMessagesService"]])
    ], BackupRestoreComponent);
    return BackupRestoreComponent;
}());



/***/ }),

/***/ "./src/app/components/color-picker/color-picker.component.css":
/*!********************************************************************!*\
  !*** ./src/app/components/color-picker/color-picker.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".panel-style {\r\n  padding-right: 60px;\r\n  padding-left: 60px;\r\n  padding-top: 50px;\r\n}\r\n\r\n.custom-btn-primary:hover{\r\n  color: #ffffff;\r\n  background-color: #2a9fd6;\r\n  border-color: #2a9fd6;\r\n}\r\n\r\n.custom-btn-primary:active{\r\n  color: #ffffff;\r\n  background-color: #1b698e;\r\n  border-color: #15506c;\r\n}\r\n\r\n.rgb-sliders {\r\n  width: 60%;\r\n}\r\n\r\n.horizontal-center {\r\n  justify-content: flex-end;\r\n  display: flex;\r\n  width: 40%;\r\n}\r\n\r\n.btn-back {\r\n  position: absolute;\r\n  bottom: 0;\r\n  height: 50px;\r\n  left: 20px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .red-slider .ng5-slider .ng5-slider-pointer {\r\n  background: rgb(255, 0, 0) !important;\r\n}\r\n\r\n::ng-deep .rgb-sliders .green-slider .ng5-slider .ng5-slider-pointer {\r\n  background: rgb(0, 255, 0) !important;\r\n}\r\n\r\n::ng-deep .rgb-sliders .blue-slider .ng5-slider .ng5-slider-pointer {\r\n  background: rgb(0, 0, 255) !important;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb2xvci1waWNrZXIvY29sb3ItcGlja2VyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLGNBQWM7RUFDZCx5QkFBeUI7RUFDekIscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxVQUFVO0FBQ1o7O0FBRUE7RUFDRSx5QkFBeUI7RUFDekIsYUFBYTtFQUNiLFVBQVU7QUFDWjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsWUFBWTtFQUNaLFVBQVU7QUFDWjs7QUFFQTtFQUNFLHFDQUFxQztBQUN2Qzs7QUFFQTtFQUNFLHFDQUFxQztBQUN2Qzs7QUFFQTtFQUNFLHFDQUFxQztBQUN2QyIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29sb3ItcGlja2VyL2NvbG9yLXBpY2tlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBhbmVsLXN0eWxlIHtcclxuICBwYWRkaW5nLXJpZ2h0OiA2MHB4O1xyXG4gIHBhZGRpbmctbGVmdDogNjBweDtcclxuICBwYWRkaW5nLXRvcDogNTBweDtcclxufVxyXG5cclxuLmN1c3RvbS1idG4tcHJpbWFyeTpob3ZlcntcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmE5ZmQ2O1xyXG4gIGJvcmRlci1jb2xvcjogIzJhOWZkNjtcclxufVxyXG5cclxuLmN1c3RvbS1idG4tcHJpbWFyeTphY3RpdmV7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFiNjk4ZTtcclxuICBib3JkZXItY29sb3I6ICMxNTUwNmM7XHJcbn1cclxuXHJcbi5yZ2Itc2xpZGVycyB7XHJcbiAgd2lkdGg6IDYwJTtcclxufVxyXG5cclxuLmhvcml6b250YWwtY2VudGVyIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgd2lkdGg6IDQwJTtcclxufVxyXG5cclxuLmJ0bi1iYWNrIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGhlaWdodDogNTBweDtcclxuICBsZWZ0OiAyMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnJnYi1zbGlkZXJzIC5yZWQtc2xpZGVyIC5uZzUtc2xpZGVyIC5uZzUtc2xpZGVyLXBvaW50ZXIge1xyXG4gIGJhY2tncm91bmQ6IHJnYigyNTUsIDAsIDApICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAucmdiLXNsaWRlcnMgLmdyZWVuLXNsaWRlciAubmc1LXNsaWRlciAubmc1LXNsaWRlci1wb2ludGVyIHtcclxuICBiYWNrZ3JvdW5kOiByZ2IoMCwgMjU1LCAwKSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnJnYi1zbGlkZXJzIC5ibHVlLXNsaWRlciAubmc1LXNsaWRlciAubmc1LXNsaWRlci1wb2ludGVyIHtcclxuICBiYWNrZ3JvdW5kOiByZ2IoMCwgMCwgMjU1KSAhaW1wb3J0YW50O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/components/color-picker/color-picker.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/color-picker/color-picker.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"panel-style\">\n\n  <div class=\"row\"> \n    <div class=\"rgb-sliders\">\n      <div class=\"red-slider mb-2 mt-4\">\n      <ng5-slider id=\"red\" [(value)]=\"redValue\" [options]=\"redSliderOptions\" (userChange)=\"onUserChange('red', $event)\"></ng5-slider>\n      </div>\n      <div class=\"green-slider mb-2\">\n      <ng5-slider id=\"green\" [(value)]=\"greenValue\" [options]=\"greenSliderOptions\" (userChange)=\"onUserChange('green', $event)\"></ng5-slider>\n      </div>\n      <div class=\"blue-slider mb-2\">\n      <ng5-slider id=\"blue\" [(value)]=\"blueValue\" [options]=\"blueSliderOptions\" (userChange)=\"onUserChange('blue', $event)\"></ng5-slider>\n      </div>\n    </div>\n    <div class=\"horizontal-center\">\n      <app-radial-color-picker\n      [size]=\"240\"\n      [centerRadius]=\"30\"\n      [redValue]=\"redValue\"\n      [greenValue]=\"greenValue\"\n      [blueValue]=\"blueValue\"\n      (color)=\"onColorPickerChange($event)\"\n      ></app-radial-color-picker>\n    </div>\n  </div>\n  <div class=\"btn-back\">\n      <button type=\"button\" class=\"btn btn-primary custom-btn-primary \" onclick=\"this.blur();\"  (click)=\"goBack()\" >WRÓĆ</button>\n  </div>\n\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/components/color-picker/color-picker.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/color-picker/color-picker.component.ts ***!
  \*******************************************************************/
/*! exports provided: ColorPickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ColorPickerComponent", function() { return ColorPickerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utils/deviceConsts */ "./src/app/utils/deviceConsts.ts");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");








var ColorPickerComponent = /** @class */ (function () {
    function ColorPickerComponent(route, router, deviceManager, restService) {
        this.route = route;
        this.router = router;
        this.deviceManager = deviceManager;
        this.restService = restService;
    }
    ColorPickerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.devices = new Array();
        this.redSliderOptions = new ng5_slider__WEBPACK_IMPORTED_MODULE_2__["Options"]();
        this.redSliderOptions.floor = 0;
        this.redSliderOptions.ceil = 0xfff;
        this.greenSliderOptions = new ng5_slider__WEBPACK_IMPORTED_MODULE_2__["Options"]();
        this.greenSliderOptions.floor = 0;
        this.greenSliderOptions.ceil = 0xfff;
        this.blueSliderOptions = new ng5_slider__WEBPACK_IMPORTED_MODULE_2__["Options"]();
        this.blueSliderOptions.floor = 0;
        this.blueSliderOptions.ceil = 0xfff;
        this.route.params.subscribe(function (params) {
            _this.retPage = params['ret'];
            console.log('>>>> params: ', params);
            _this.redValue = params['r'];
            _this.greenValue = params['g'];
            _this.blueValue = params['b'];
            var addresses = params['adr'].split(',');
            for (var _i = 0, addresses_1 = addresses; _i < addresses_1.length; _i++) {
                var adr = addresses_1[_i];
                _this.devices.push(_this.deviceManager.getDevice(params['cat'], adr));
            }
            console.log('>>> devs: ', _this.devices);
            // this.device = this.deviceManager.getDevice(params['cat'], params['adr']);
        });
    };
    ColorPickerComponent.prototype.goBack = function () {
        this.router.navigate(['/' + this.retPage]);
    };
    ColorPickerComponent.prototype.setDeviceParameter = function (dev, param, val) {
        this.restService.setDeviceParameter(dev, param, val).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Setting device parameter failed: ' + data.result);
            }
            else {
                console.log('Device parameter set successfully!');
            }
        });
    };
    ColorPickerComponent.prototype.onUserChange = function (param, event) {
        for (var _i = 0, _a = this.devices; _i < _a.length; _i++) {
            var dev = _a[_i];
            this.setDeviceParameter(dev, param, event.value);
        }
    };
    ColorPickerComponent.prototype.onColorPickerChange = function (event) {
        var color = JSON.parse(event);
        this.redValue = color.red * 0xf;
        this.greenValue = color.green * 0xf;
        this.blueValue = color.blue * 0xf;
        var rgb = (this.redValue * 0x1000000) + (this.greenValue * 0x1000) + this.blueValue;
        for (var _i = 0, _a = this.devices; _i < _a.length; _i++) {
            var dev = _a[_i];
            this.setDeviceParameter(dev, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, rgb);
        }
    };
    ColorPickerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-color-picker',
            template: __webpack_require__(/*! ./color-picker.component.html */ "./src/app/components/color-picker/color-picker.component.html"),
            styles: [__webpack_require__(/*! ./color-picker.component.css */ "./src/app/components/color-picker/color-picker.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__["DeviceManagerService"],
            _services_rest_service__WEBPACK_IMPORTED_MODULE_6__["RestService"]])
    ], ColorPickerComponent);
    return ColorPickerComponent;
}());



/***/ }),

/***/ "./src/app/components/condition/condition.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/condition/condition.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".custom-checkbox {\r\n  -webkit-transform: scale(1.5);\r\n          transform: scale(1.5);\r\n  display: inline;\r\n  width: auto;\r\n  margin-left: 20px;\r\n}\r\n\r\n.flash-message-container {\r\n  position: fixed;\r\n  z-index: 10;\r\n  width: 100%;\r\n}\r\n\r\n.flash-message {\r\n  display: block;\r\n  margin-right: 10%;\r\n  margin-left: 10%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb25kaXRpb24vY29uZGl0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSw2QkFBcUI7VUFBckIscUJBQXFCO0VBQ3JCLGVBQWU7RUFDZixXQUFXO0VBQ1gsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UsZUFBZTtFQUNmLFdBQVc7RUFDWCxXQUFXO0FBQ2I7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGdCQUFnQjtBQUNsQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29uZGl0aW9uL2NvbmRpdGlvbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmN1c3RvbS1jaGVja2JveCB7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgxLjUpO1xyXG4gIGRpc3BsYXk6IGlubGluZTtcclxuICB3aWR0aDogYXV0bztcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG5cclxuLmZsYXNoLW1lc3NhZ2UtY29udGFpbmVyIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgei1pbmRleDogMTA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5mbGFzaC1tZXNzYWdlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW4tcmlnaHQ6IDEwJTtcclxuICBtYXJnaW4tbGVmdDogMTAlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/condition/condition.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/condition/condition.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"flash-message-container\">\n  <div class=\"flash-message\">\n    <flash-messages></flash-messages>\n  </div>\n</div>\n<div class=\"container  mt-2\">\n  <div class=\"panel panel-default\">\n    <div class=\"panel-body\">\n\n      <div class=\"form-group\">\n        <div style=\"margin-bottom: 5px;\">\n          Select condition type:\n        </div>\n\n        <div  style=\"margin-bottom: 15px;\">\n          <select class=\"form-control\" id=\"selectType\" [(ngModel)]=\"condition.type\" style=\"width: 50%\">\n            <option *ngFor=\"let type of types\" [ngValue]=\"type\">\n              {{type}}\n            </option>\n          </select>\n        </div>\n\n      </div>\n\n      <div *ngIf=\"condition.type == 'device'\">\n        <div class=\"form-group\">\n          <div style=\"margin-bottom: 5px;\">\n            Select category:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <select class=\"form-control\" id=\"selectCond\" [(ngModel)]=\"condition.category\" style=\"width: 50%\">\n              <option *ngFor=\"let cat of deviceManager.categoriesList\" [ngValue]=\"cat.id\">\n                {{cat.name}}\n              </option>\n            </select>\n          </div>\n\n        </div>\n\n\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Select device:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <select class=\"form-control\" id=\"selectDev\" [(ngModel)]=\"condition.address\" style=\"width: 50%\" >\n              <option *ngFor=\"let device of getDevicesForSelectedCategory() | orderBy: 'name'\" [ngValue]=\"device.address\">\n              {{device.name}}\n              </option>\n            </select>\n          </div>\n\n        </div>\n\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Select parameter:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <select class=\"form-control\" id=\"selectParam\" [(ngModel)]=\"condition.param\" style=\"width: 50%\">\n              <option *ngFor=\"let param of getParamsForSelectedCategory()\" [ngValue]=\"param\">\n                {{param}}\n              </option>\n            </select>\n          </div>\n\n        </div>\n\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Select condition:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <select class=\"form-control\" id=\"selectLogicCond\" [(ngModel)]=\"condition.condition\" style=\"width: 50%\" >\n              <option *ngFor=\"let cond of logicalConditions\" [ngValue]=\"cond\">\n                {{cond}}\n              </option>\n            </select>\n          </div>\n\n        </div>\n\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Input value:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <input type=\"number\" [(ngModel)]=\"condition.value\" class=\"form-control\" id=\"inputName\" style=\"width: 50%; margin-bottom: 5px;\">\n          </div>\n\n        </div>\n      </div>\n\n\n      <div *ngIf=\"condition.type == 'time'\">\n\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Select condition:\n          </div>\n\n          <div  style=\"margin-bottom: 15px;\">\n            <select class=\"form-control\" id=\"selectTimeLogicCond\" [(ngModel)]=\"condition.condition\" style=\"width: 50%\">\n              <option *ngFor=\"let cond of logicalConditions\" [ngValue]=\"cond\">\n                {{cond}}\n              </option>\n            </select>\n          </div>\n\n        </div>\n\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Input value:\n          </div>\n\n          <div class=\"col-lg-8\">\n            <app-time-picker [(secondsFromMidnight)]=\"condition.value\"></app-time-picker>\n          </div>\n        </div>\n      </div>\n\n      <div *ngIf=\"condition.type == 'alarm'\">\n\n        <div class=\"form-group\" >\n          <div style=\"margin-bottom: 5px;\">\n            Alarm active:\n            <input type=\"checkbox\" [(ngModel)]=\"condition.value\" class=\"form-control custom-checkbox\" id=\"inputActive\">\n          </div>\n\n        </div>\n\n      </div>\n\n      <button class=\"btn btn-success mr-2\" (click)=\"onConditionSave()\">Save Condition</button>\n      <button class=\"btn btn-default\" (click)=\"onCancel()\">Cancel</button>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/condition/condition.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/condition/condition.component.ts ***!
  \*************************************************************/
/*! exports provided: ConditionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConditionComponent", function() { return ConditionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_operation_manager_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/operation-manager.service */ "./src/app/services/operation-manager.service.ts");
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../models/consts */ "./src/app/models/consts.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _services_builder_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/builder.service */ "./src/app/services/builder.service.ts");
/* harmony import */ var _services_validate_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/validate.service */ "./src/app/services/validate.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../utils/utils */ "./src/app/utils/utils.ts");











var ConditionComponent = /** @class */ (function () {
    function ConditionComponent(route, operationManager, router, deviceManager, builder, validator, flashMessage) {
        this.route = route;
        this.operationManager = operationManager;
        this.router = router;
        this.deviceManager = deviceManager;
        this.builder = builder;
        this.validator = validator;
        this.flashMessage = flashMessage;
    }
    ConditionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadConditionTypes();
        this.loadLogicalConditions();
        this.sub = this.route.params.subscribe(function (params) {
            _this.conditionId = params['id'];
            _this.operation = _this.operationManager.tmpOperation;
            var cloneOption = params['clone'];
            if (_this.conditionId > 10) {
                var tmpCond = _this.operationManager.getOperationConditionById(_this.operation, _this.conditionId);
                _this.internalCondType = tmpCond.internalType;
                if (cloneOption == 0) {
                    _this.condition = tmpCond.toJson();
                }
                else {
                    _this.conditionId = Number(_this.internalCondType);
                    _this.condition = new Object();
                    _this.condition['type'] = tmpCond.type;
                    _this.condition['param'] = tmpCond['param'];
                    _this.condition['condition'] = tmpCond['condition'];
                    _this.condition['value'] = tmpCond.value;
                    if (tmpCond['device']) {
                        _this.condition['category'] = tmpCond.device.category;
                        _this.condition['address'] = tmpCond.device.address;
                    }
                }
            }
            else {
                _this.condition = new Object();
            }
        });
    };
    ConditionComponent.prototype.getParamsForSelectedCategory = function () {
        if (this.condition['category']) {
            return this.deviceManager.getCategoryById(this.condition['category']).parameters;
        }
        return new Array();
    };
    ConditionComponent.prototype.loadConditionTypes = function () {
        this.types = new Array();
        this.types.push('device');
        this.types.push('time');
        this.types.push('alarm');
        //for (var type in EConditionType) {
        //  var isValueProperty = parseInt(type, 10) >= 0;
        //  if (isValueProperty) {
        //    this.types.push(EConditionType[type]);
        //  }
        //}
    };
    ConditionComponent.prototype.loadLogicalConditions = function () {
        this.logicalConditions = new Array();
        this.logicalConditions.push('equal');
        this.logicalConditions.push('greater');
        this.logicalConditions.push('greater_equal');
        this.logicalConditions.push('less');
        this.logicalConditions.push('less_equal');
        this.logicalConditions.push('any');
        this.logicalConditions.push('rising');
        this.logicalConditions.push('falling');
        this.logicalConditions.push('not_equal');
    };
    ConditionComponent.prototype.getDevicesForSelectedCategory = function () {
        var _this = this;
        if (this.condition['category']) {
            var devs = this.deviceManager.devicesList.filter(function (item) { return item.category == _this.condition['category']; });
            console.log('>>>> devices: ', devs);
            return devs;
        }
        return new Array();
    };
    ConditionComponent.prototype.onConditionSave = function () {
        var res = new Object();
        var condType;
        if (this.conditionId == 0) {
            condType = _models_consts__WEBPACK_IMPORTED_MODULE_4__["EStartStopConditionType"].regular;
            this.condition['extId'] = this.generateExtendedConditionId(this.operation.conditions);
        }
        else if (this.conditionId == 1) {
            condType = _models_consts__WEBPACK_IMPORTED_MODULE_4__["EStartStopConditionType"].start;
            this.condition['extId'] = this.generateExtendedConditionId(this.operation.startConditions());
        }
        else if (this.conditionId == 2) {
            condType = _models_consts__WEBPACK_IMPORTED_MODULE_4__["EStartStopConditionType"].stop;
            this.condition['extId'] = this.generateExtendedConditionId(this.operation.stopConditions());
        }
        if (this.validator.validateCondition(this.condition, res)) {
            if (this.conditionId < 3) {
                this.operation.conditions.push(this.builder.createCondition(this.condition, this.deviceManager, condType));
            }
            else {
                this.operationManager.setOperationConditionById(this.operation, this.conditionId, this.builder.createCondition(this.condition, this.deviceManager, this.internalCondType));
            }
            this.router.navigate(['/operation', this.operation.id, _models_consts__WEBPACK_IMPORTED_MODULE_4__["SRC_PAGE_COND_EDIT"]]);
        }
        else {
            this.flashMessage.show(res['error'], {
                cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                timeout: 5000
            });
        }
    };
    ConditionComponent.prototype.onCancel = function () {
        this.router.navigate(['/operation', this.operation.id, _models_consts__WEBPACK_IMPORTED_MODULE_4__["SRC_PAGE_COND_EDIT"]]);
    };
    ConditionComponent.prototype.generateExtendedConditionId = function (conditions) {
        return new _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"]().getLetterByIndex(conditions.length);
    };
    ConditionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-condition',
            template: __webpack_require__(/*! ./condition.component.html */ "./src/app/components/condition/condition.component.html"),
            styles: [__webpack_require__(/*! ./condition.component.css */ "./src/app/components/condition/condition.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_operation_manager_service__WEBPACK_IMPORTED_MODULE_3__["OperationManagerService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_5__["DeviceManagerService"],
            _services_builder_service__WEBPACK_IMPORTED_MODULE_6__["BuilderService"],
            _services_validate_service__WEBPACK_IMPORTED_MODULE_7__["ValidateService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_8__["FlashMessagesService"]])
    ], ConditionComponent);
    return ConditionComponent;
}());



/***/ }),

/***/ "./src/app/components/device-advance-control-pwm/device-advance-control-pwm.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/components/device-advance-control-pwm/device-advance-control-pwm.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".switch-style{\r\n  display: flex;\r\n}\r\n\r\n.panel-style{\r\n  background: #202020;\r\n  border-radius: 6px;\r\n}\r\n\r\n.pwm-icon{\r\n\tborder-radius: 25px;\r\n    border: 2px solid;\r\n    float: right;\r\n    height: 50px;\r\n    width: 50px;\r\n    background: #000000;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9kZXZpY2UtYWR2YW5jZS1jb250cm9sLXB3bS9kZXZpY2UtYWR2YW5jZS1jb250cm9sLXB3bS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBYTtBQUNmOztBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLGtCQUFrQjtBQUNwQjs7QUFFQTtDQUNDLG1CQUFtQjtJQUNoQixpQkFBaUI7SUFDakIsWUFBWTtJQUNaLFlBQVk7SUFDWixXQUFXO0lBQ1gsbUJBQW1CO0FBQ3ZCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9kZXZpY2UtYWR2YW5jZS1jb250cm9sLXB3bS9kZXZpY2UtYWR2YW5jZS1jb250cm9sLXB3bS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN3aXRjaC1zdHlsZXtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4ucGFuZWwtc3R5bGV7XHJcbiAgYmFja2dyb3VuZDogIzIwMjAyMDtcclxuICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbn1cclxuXHJcbi5wd20taWNvbntcclxuXHRib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgYm9yZGVyOiAycHggc29saWQ7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGJhY2tncm91bmQ6ICMwMDAwMDA7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/device-advance-control-pwm/device-advance-control-pwm.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/device-advance-control-pwm/device-advance-control-pwm.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"ml-3 mr-3 mt-4 mb-5 p-3 panel-style\">\n  <div>\n    Duty cycle:\n  </div>\n  <div class=\"row\">\n    <div class=\"switch-style mt-3 mb-3 col-lg-6\">\n      <ui-switch\n        size=\"small\"\n        switchColor=\"#fcfcfc\"\n        color=\"rgb(35,255,150);\"\n        defaultBgColor=\"rgb(255,80,80);\"\n        defaultBoColor=\"black\"\n        [(ngModel)]=\"allDevicesSwitch\"></ui-switch>\n      <div class=\"ml-3 \">\n        Apply for all devices\n      </div>\n    </div>\n    <div class=\"col-lg-6\">\n        <div class=\"mr-2 pwm-icon\" [style.background]=\"getStyle()\"></div>\n    </div>\n  </div>\n  <ng5-slider [(value)]=\"value\" [options]=\"options\" (userChange)=\"onUserChange($event)\"></ng5-slider>\n</div>\n"

/***/ }),

/***/ "./src/app/components/device-advance-control-pwm/device-advance-control-pwm.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/device-advance-control-pwm/device-advance-control-pwm.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: DeviceAdvanceControlPwmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceAdvanceControlPwmComponent", function() { return DeviceAdvanceControlPwmComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");
/* harmony import */ var _models_device__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models/device */ "./src/app/models/device.ts");
/* harmony import */ var _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../utils/deviceConsts */ "./src/app/utils/deviceConsts.ts");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__);







var DeviceAdvanceControlPwmComponent = /** @class */ (function () {
    function DeviceAdvanceControlPwmComponent(restService, flashMessage) {
        this.restService = restService;
        this.flashMessage = flashMessage;
    }
    DeviceAdvanceControlPwmComponent.prototype.ngOnInit = function () {
        this.options = new ng5_slider__WEBPACK_IMPORTED_MODULE_2__["Options"]();
        this.options.floor = 0;
        this.options.ceil = 255;
        this.allDevicesSwitch = false;
    };
    DeviceAdvanceControlPwmComponent.prototype.ngOnChanges = function (changes) {
        console.log('>>> change: ', changes);
        if (changes.device && changes.device.currentValue && changes.device.currentValue.category != '5') {
            return;
        }
        this.loadDutyCycleValue();
    };
    DeviceAdvanceControlPwmComponent.prototype.onUserChange = function (event) {
        var _this = this;
        var command = _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__["DeviceConsts"].PARAM_DUTY_CYCLE;
        if (this.allDevicesSwitch) {
            command = _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__["DeviceConsts"].PARAM_DUTY_CYCLE_ALL;
        }
        this.restService.setDeviceParameter(this.device, command, this.value).subscribe(function (data) {
            //data = this.convertFromBufferToJson(data);
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Setting device parameter failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                console.log('>> success');
            }
        });
    };
    DeviceAdvanceControlPwmComponent.prototype.loadDutyCycleValue = function () {
        var _this = this;
        this.restService.getDeviceParameter(this.device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__["DeviceConsts"].PARAM_DUTY_CYCLE).subscribe(function (data) {
            console.log('adv pwm data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Loading device parameter failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.value = data.value;
            }
        });
    };
    DeviceAdvanceControlPwmComponent.prototype.getStyle = function () {
        var maxBlue = 175;
        var red = 255 * Math.sin((Math.PI * this.value) / 0x1fe);
        var green = 255 * Math.sin((Math.PI * this.value) / 0x1fe);
        var blue = (this.value * maxBlue) / 255;
        return 'rgb(' + red + ',' + green + ',' + blue + ')';
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_device__WEBPACK_IMPORTED_MODULE_3__["Device"])
    ], DeviceAdvanceControlPwmComponent.prototype, "device", void 0);
    DeviceAdvanceControlPwmComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-device-advance-control-pwm',
            template: __webpack_require__(/*! ./device-advance-control-pwm.component.html */ "./src/app/components/device-advance-control-pwm/device-advance-control-pwm.component.html"),
            styles: [__webpack_require__(/*! ./device-advance-control-pwm.component.css */ "./src/app/components/device-advance-control-pwm/device-advance-control-pwm.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_5__["RestService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__["FlashMessagesService"]])
    ], DeviceAdvanceControlPwmComponent);
    return DeviceAdvanceControlPwmComponent;
}());



/***/ }),

/***/ "./src/app/components/device-advance-control-rgb/device-advance-control-rgb.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/components/device-advance-control-rgb/device-advance-control-rgb.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".panel-style{\r\n  background: #202020;\r\n  border-radius: 6px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .red-slider .ng5-slider .ng5-slider-pointer {\r\n\tbackground: rgb(255, 0, 0) !important;\r\n}\r\n\r\n::ng-deep .rgb-sliders .green-slider .ng5-slider .ng5-slider-pointer {\r\n\tbackground: rgb(0, 255, 0) !important;\r\n}\r\n\r\n::ng-deep .rgb-sliders .blue-slider .ng5-slider .ng5-slider-pointer {\r\n\tbackground: rgb(0, 0, 255) !important;\r\n}\r\n\r\n::ng-deep .rgb-sliders .speed-slider .ng5-slider .ng5-slider-pointer {\r\n  background: rgb(150, 150, 150) !important;\r\n}\r\n\r\n.speed-slider{\r\n  display: flex;\r\n}\r\n\r\n.horizontal-center {\r\n  text-align: center;\r\n  justify-content: center;\r\n  display: flex;\r\n}\r\n\r\n.horizontal-center-buttons {\r\n  text-align: center;\r\n}\r\n\r\n.speed-style {\r\n  text-align: center;\r\n  width: 30%;\r\n}\r\n\r\n.btn-group-custom{\r\n  display: contents;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9kZXZpY2UtYWR2YW5jZS1jb250cm9sLXJnYi9kZXZpY2UtYWR2YW5jZS1jb250cm9sLXJnYi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CO0VBQ25CLGtCQUFrQjtBQUNwQjs7QUFFQTtDQUNDLHFDQUFxQztBQUN0Qzs7QUFFQTtDQUNDLHFDQUFxQztBQUN0Qzs7QUFFQTtDQUNDLHFDQUFxQztBQUN0Qzs7QUFFQTtFQUNFLHlDQUF5QztBQUMzQzs7QUFFQTtFQUNFLGFBQWE7QUFDZjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQix1QkFBdUI7RUFDdkIsYUFBYTtBQUNmOztBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7QUFDWjs7QUFFQTtFQUNFLGlCQUFpQjtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGV2aWNlLWFkdmFuY2UtY29udHJvbC1yZ2IvZGV2aWNlLWFkdmFuY2UtY29udHJvbC1yZ2IuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wYW5lbC1zdHlsZXtcclxuICBiYWNrZ3JvdW5kOiAjMjAyMDIwO1xyXG4gIGJvcmRlci1yYWRpdXM6IDZweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5yZ2Itc2xpZGVycyAucmVkLXNsaWRlciAubmc1LXNsaWRlciAubmc1LXNsaWRlci1wb2ludGVyIHtcclxuXHRiYWNrZ3JvdW5kOiByZ2IoMjU1LCAwLCAwKSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnJnYi1zbGlkZXJzIC5ncmVlbi1zbGlkZXIgLm5nNS1zbGlkZXIgLm5nNS1zbGlkZXItcG9pbnRlciB7XHJcblx0YmFja2dyb3VuZDogcmdiKDAsIDI1NSwgMCkgIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5yZ2Itc2xpZGVycyAuYmx1ZS1zbGlkZXIgLm5nNS1zbGlkZXIgLm5nNS1zbGlkZXItcG9pbnRlciB7XHJcblx0YmFja2dyb3VuZDogcmdiKDAsIDAsIDI1NSkgIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5yZ2Itc2xpZGVycyAuc3BlZWQtc2xpZGVyIC5uZzUtc2xpZGVyIC5uZzUtc2xpZGVyLXBvaW50ZXIge1xyXG4gIGJhY2tncm91bmQ6IHJnYigxNTAsIDE1MCwgMTUwKSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uc3BlZWQtc2xpZGVye1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5ob3Jpem9udGFsLWNlbnRlciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5ob3Jpem9udGFsLWNlbnRlci1idXR0b25zIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zcGVlZC1zdHlsZSB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHdpZHRoOiAzMCU7XHJcbn1cclxuXHJcbi5idG4tZ3JvdXAtY3VzdG9te1xyXG4gIGRpc3BsYXk6IGNvbnRlbnRzO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/device-advance-control-rgb/device-advance-control-rgb.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/device-advance-control-rgb/device-advance-control-rgb.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"ml-3 mr-3 mt-4 mb-5 p-3 panel-style\">\n\n  <div class=\"row\">\n    <div class=\"rgb-sliders col-lg-9\">\n      <div class=\"red-slider mb-2\">\n      <ng5-slider id=\"red\" [(value)]=\"redValue\" [options]=\"redSliderOptions\" (userChange)=\"onUserChange('red', $event)\"></ng5-slider>\n      </div>\n      <div class=\"green-slider mb-2\">\n      <ng5-slider id=\"green\" [(value)]=\"greenValue\" [options]=\"greenSliderOptions\" (userChange)=\"onUserChange('green', $event)\"></ng5-slider>\n      </div>\n      <div class=\"blue-slider mb-2\">\n      <ng5-slider id=\"blue\" [(value)]=\"blueValue\" [options]=\"blueSliderOptions\" (userChange)=\"onUserChange('blue', $event)\"></ng5-slider>\n      </div>\n      <div class=\"speed-slider mb-3\">\n        <div class=\"pt-4 speed-style\">SPEED:</div>\n      <ng5-slider id=\"speed\" [(value)]=\"speedValue\" [options]=\"speedSliderOptions\" (userChange)=\"onUserChange('speed', $event)\"></ng5-slider>\n      </div>\n    </div>\n    <div class=\"col-lg-3 horizontal-center\">\n      <app-radial-color-picker\n      [size]=\"240\"\n      [centerRadius]=\"30\"\n      [redValue]=\"redValue\"\n      [greenValue]=\"greenValue\"\n      [blueValue]=\"blueValue\"\n      (color)=\"onColorPickerChange($event)\"\n      ></app-radial-color-picker>\n    </div>\n  </div>\n  <div class=\"mt-3 mb-2 horizontal-center-buttons\">\n  \n      <div class=\"btn-group-custom btn-group btn-group-toggle \" data-toggle=\"buttons\">\n        <label class=\"btn btn-primary mt-2\" [class.active]=\"isModeActive(mode)\" *ngFor=\"let mode of modesList\" (click)=\"setMode(mode)\">\n          <input type=\"radio\" name=\"options\" id=\"MODE{{mode}}\" autocomplete=\"off\" checked=\"\">\n          MODE {{mode}}\n        </label>\n      </div>\n\n      <div class=\"btn-group-custom horizontal-center-buttons\">\n          <button class=\"btn btn-danger mt-2 ml-4 mr-4\" (click)=\"setMode('0')\">OFF</button>\n      </div>\n\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/components/device-advance-control-rgb/device-advance-control-rgb.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/device-advance-control-rgb/device-advance-control-rgb.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: DeviceAdvanceControlRgbComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceAdvanceControlRgbComponent", function() { return DeviceAdvanceControlRgbComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");
/* harmony import */ var _models_device__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models/device */ "./src/app/models/device.ts");
/* harmony import */ var _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../utils/deviceConsts */ "./src/app/utils/deviceConsts.ts");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__);







var DeviceAdvanceControlRgbComponent = /** @class */ (function () {
    function DeviceAdvanceControlRgbComponent(restService, flashMessage) {
        this.restService = restService;
        this.flashMessage = flashMessage;
    }
    DeviceAdvanceControlRgbComponent.prototype.ngOnInit = function () {
        this.redSliderOptions = new ng5_slider__WEBPACK_IMPORTED_MODULE_2__["Options"]();
        this.redSliderOptions.floor = 0;
        this.redSliderOptions.ceil = 0xfff;
        this.greenSliderOptions = new ng5_slider__WEBPACK_IMPORTED_MODULE_2__["Options"]();
        this.greenSliderOptions.floor = 0;
        this.greenSliderOptions.ceil = 0xfff;
        this.blueSliderOptions = new ng5_slider__WEBPACK_IMPORTED_MODULE_2__["Options"]();
        this.blueSliderOptions.floor = 0;
        this.blueSliderOptions.ceil = 0xfff;
        this.speedSliderOptions = new ng5_slider__WEBPACK_IMPORTED_MODULE_2__["Options"]();
        this.speedSliderOptions.showTicksValues = true;
        this.speedSliderOptions.stepsArray = [
            { value: 0 },
            { value: 1 },
            { value: 2 },
            { value: 3 }
        ];
        this.modesList = ['1', '2', '3', '4', '5', '6'];
    };
    DeviceAdvanceControlRgbComponent.prototype.ngOnChanges = function (changes) {
        console.log('>>> change: ', changes);
        if (changes.device && changes.device.currentValue && changes.device.currentValue.category != '7') {
            return;
        }
        this.loadDeviceParamsValue();
    };
    DeviceAdvanceControlRgbComponent.prototype.onUserChange = function (param, event) {
        var _this = this;
        this.restService.setDeviceParameter(this.device, param, event.value).subscribe(function (data) {
            //data = this.convertFromBufferToJson(data);
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Setting device parameter failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                console.log('>> success');
            }
        });
    };
    DeviceAdvanceControlRgbComponent.prototype.loadDeviceParamsValue = function () {
        var _this = this;
        this.restService.getDeviceParameters(this.device).subscribe(function (data) {
            console.log('adv rgb data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Loading device parameters failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                var allParamsDefined = true;
                for (var param = 0; param < data.parameters.length; param++) {
                    if (!data.parameters[param].defined) {
                        allParamsDefined = false;
                    }
                    else {
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__["DeviceConsts"].PARAM_RED) {
                            _this.redValue = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__["DeviceConsts"].PARAM_GREEN) {
                            _this.greenValue = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__["DeviceConsts"].PARAM_BLUE) {
                            _this.blueValue = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__["DeviceConsts"].PARAM_SPEED) {
                            _this.speedValue = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__["DeviceConsts"].PARAM_MODE) {
                            _this.mode = data.parameters[param]['value'];
                        }
                    }
                }
                if (!allParamsDefined) {
                    _this.flashMessage.show('Some device params are undefined, component may not work properly', {
                        cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                        timeout: 5000
                    });
                }
            }
        });
    };
    DeviceAdvanceControlRgbComponent.prototype.onColorPickerChange = function (event) {
        var _this = this;
        var color = JSON.parse(event);
        this.redValue = color.red * 0xf;
        this.greenValue = color.green * 0xf;
        this.blueValue = color.blue * 0xf;
        var rgb = (this.redValue * 0x1000000) + (this.greenValue * 0x1000) + this.blueValue;
        this.restService.setDeviceParameter(this.device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__["DeviceConsts"].PARAM_RGB, rgb).subscribe(function (data) {
            //data = this.convertFromBufferToJson(data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Setting device parameter failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                console.log('>> success');
            }
        });
    };
    DeviceAdvanceControlRgbComponent.prototype.setMode = function (mode) {
        var _this = this;
        this.restService.setDeviceParameter(this.device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_4__["DeviceConsts"].PARAM_MODE, mode).subscribe(function (data) {
            //data = this.convertFromBufferToJson(data);
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Setting device parameter failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                console.log('>> success');
                _this.mode = mode;
            }
        });
    };
    DeviceAdvanceControlRgbComponent.prototype.isModeActive = function (mode) {
        return mode == this.mode;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_device__WEBPACK_IMPORTED_MODULE_3__["Device"])
    ], DeviceAdvanceControlRgbComponent.prototype, "device", void 0);
    DeviceAdvanceControlRgbComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-device-advance-control-rgb',
            template: __webpack_require__(/*! ./device-advance-control-rgb.component.html */ "./src/app/components/device-advance-control-rgb/device-advance-control-rgb.component.html"),
            styles: [__webpack_require__(/*! ./device-advance-control-rgb.component.css */ "./src/app/components/device-advance-control-rgb/device-advance-control-rgb.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_5__["RestService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__["FlashMessagesService"]])
    ], DeviceAdvanceControlRgbComponent);
    return DeviceAdvanceControlRgbComponent;
}());



/***/ }),

/***/ "./src/app/components/device-manager/device-manager.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/device-manager/device-manager.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-group-custom {\r\n  display: contents;\r\n}\r\n\r\n.name-filter {\r\n  display: flex;\r\n}\r\n\r\n.search-device {\r\n  display: flex;\r\n}\r\n\r\n.modal-header-custom{\r\n  width: 100%;\r\n  text-align: center;\r\n}\r\n\r\n.modal-content-center{\r\n  text-align: center;\r\n}\r\n\r\n.wide-button{\r\n  width: 50%;\r\n}\r\n\r\n.advanced-mode-switch{\r\n  display: flex;\r\n}\r\n\r\n.search-results {\r\n  max-height: 200px;\r\n  overflow-y: auto;\r\n  background: #222;\r\n  border: 6px solid #222;\r\n  border-radius: 0.25rem;\r\n}\r\n\r\n.search-result {\r\n  cursor: pointer;\r\n}\r\n\r\n.flash-message-container {\r\n  position: fixed;\r\n  z-index: 10;\r\n  width: 100%;\r\n}\r\n\r\n.flash-message {\r\n  display: block;\r\n  margin-right: 10%;\r\n  margin-left: 10%;\r\n}\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9kZXZpY2UtbWFuYWdlci9kZXZpY2UtbWFuYWdlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UsYUFBYTtBQUNmOztBQUVBO0VBQ0UsYUFBYTtBQUNmOztBQUVBO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLFVBQVU7QUFDWjs7QUFFQTtFQUNFLGFBQWE7QUFDZjs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLHNCQUFzQjtFQUN0QixzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsZUFBZTtFQUNmLFdBQVc7RUFDWCxXQUFXO0FBQ2I7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGdCQUFnQjtBQUNsQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGV2aWNlLW1hbmFnZXIvZGV2aWNlLW1hbmFnZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idG4tZ3JvdXAtY3VzdG9tIHtcclxuICBkaXNwbGF5OiBjb250ZW50cztcclxufVxyXG5cclxuLm5hbWUtZmlsdGVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4uc2VhcmNoLWRldmljZSB7XHJcbiAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLm1vZGFsLWhlYWRlci1jdXN0b217XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4ubW9kYWwtY29udGVudC1jZW50ZXJ7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4ud2lkZS1idXR0b257XHJcbiAgd2lkdGg6IDUwJTtcclxufVxyXG5cclxuLmFkdmFuY2VkLW1vZGUtc3dpdGNoe1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5zZWFyY2gtcmVzdWx0cyB7XHJcbiAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgb3ZlcmZsb3cteTogYXV0bztcclxuICBiYWNrZ3JvdW5kOiAjMjIyO1xyXG4gIGJvcmRlcjogNnB4IHNvbGlkICMyMjI7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxufVxyXG5cclxuLnNlYXJjaC1yZXN1bHQge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmZsYXNoLW1lc3NhZ2UtY29udGFpbmVyIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgei1pbmRleDogMTA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5mbGFzaC1tZXNzYWdlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW4tcmlnaHQ6IDEwJTtcclxuICBtYXJnaW4tbGVmdDogMTAlO1xyXG59XHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/device-manager/device-manager.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/device-manager/device-manager.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"flash-message-container\">\n  <div class=\"flash-message\">\n    <flash-messages></flash-messages>\n  </div>\n</div>\n<div class=\"container mt-2 \">\n\n  <div class=\"advanced-mode-switch mt-3 mb-3\">\n    <ui-switch\n      size=\"small\"\n      switchColor=\"#fcfcfc\"\n      color=\"rgb(35,255,150);\"\n      defaultBgColor=\"rgb(255,80,80);\"\n      defaultBoColor=\"black\"\n      [(ngModel)]=\"advancedModeEnabled\"\n      (change)=\"onSwitchModeChange()\"></ui-switch>\n    <div class=\"ml-3\">\n      Advanced commands\n    </div>\n  </div>\n\n  <div *ngIf=\"!advancedModeEnabled\" class=\"panel panel-default\">\n    <div class=\"panel-body\">\n      <div class=\"mb-2 search-device\">\n\n        <input class=\"form-control col-lg-6 col-xs-10 mr-1\" type=\"text\" [(ngModel)]=\"keysearch\" (input)=\"onKeySearch()\" placeholder=\"Search device by name, uid, category:address\">\n        <button type=\"button\" *ngIf=\"keysearch?.length > 0\" class=\"btn btn-default\" (click)=\"clearKeySearch()\" style=\"min-width: 70px;\">Clear</button>\n\n      </div>\n\n      <div *ngIf=\"searchResults?.length > 0\" class=\"search-results col-lg-6 col-xs-10 mb-2 pl-2\">\n        <span *ngFor=\"let result of searchResults\" (click)=\"onSearchResultClick(result)\" class=\"search-result\">\n          {{result.name}}<br/>\n        </span>\n      </div>\n\n      <div class=\"form-group\">\n        <div class=\"mb-1\">\n          Select category:\n        </div>\n        <div class=\"bs-component\">\n          <div class=\"btn-group-custom btn-group btn-group-toggle \" data-toggle=\"buttons\">\n            <label class=\"btn btn-secondary mb-1\" [ngClass]=\"(selectedCategory && (selectedCategory.id == cat.id)) ? 'active':''\" *ngFor=\"let cat of deviceManager.getAllCategories()\" (click)=\"loadDevices(cat, null)\">\n              <input type=\"radio\" name=\"options\" id=\"{{cat.name}}\" autocomplete=\"off\" checked=\"\">\n              {{cat.name}}\n            </label>\n          </div>\n        </div>\n\n      </div>\n\n      <div class=\"form-group\" *ngIf=\"deviceManager.getDevicesInCategory(selectedCategory)?.length > 0\">\n        <div class=\"mb-1\">\n          Select device:\n        </div>\n\n        <div  class=\"mb-1\">\n          <select class=\"form-control col-lg-6 col-xs-12\"  id=\"select\" [(ngModel)]=\"selectedDevice\" (change)=\"onChange()\">\n            <option *ngFor=\"let device of filteredDevicesList | orderBy: 'name'\" [ngValue]=\"device\">\n              {{device.name}} [{{device.category | number : '2.0-0'}}:{{device.address | number : '2.0-0'}}]\n            </option>\n          </select>\n        </div>\n        <div class=\"mb-2 name-filter\">\n\n          <input class=\"form-control col-lg-6 col-xs-10 mr-1\" type=\"text\" [(ngModel)]=\"filter\" (input)=\"onFilterChange()\" placeholder=\"Filter names\">\n          <button type=\"button\" *ngIf=\"filter?.length > 0\" class=\"btn btn-default\" (click)=\"clearFilters()\" style=\"min-width: 70px;\">Clear</button>\n\n        </div>\n\n      </div>\n\n      <div *ngIf=\"selectedDevice !== undefined\">\n        <div class=\"form-group\">\n          <div class=\"mb-1\">\n            Device name:\n          </div>\n\n          <span class=\"mb-1\">\n            <input type=\"text\" [(ngModel)]=\"selectedDeviceName\" class=\"form-control col-lg-6 col-xs-12 mb-1\" id=\"inputName\">\n            <button *ngIf=\"selectedDevice && (selectedDevice.name != selectedDeviceName)\" class=\"btn btn-primary\" (click)=\"onSaveName()\">Save</button>\n          </span>\n\n        </div>\n\n        <div class=\"advanced-mode-switch mt-3 mb-3 mt-5\" *ngIf=\"showAdvanceControlSwitch()\">\n          <ui-switch\n            size=\"small\"\n            switchColor=\"#fcfcfc\"\n            color=\"rgb(35,255,150);\"\n            defaultBgColor=\"rgb(255,80,80);\"\n            defaultBoColor=\"black\"\n            [(ngModel)]=\"advancedControlEnabled\"\n            (change)=\"onSwitchControlChange()\"></ui-switch>\n          <div class=\"ml-3\">\n            Visual control\n          </div>\n        </div>\n\n        <div class=\"row\" *ngIf=\"!advancedControlEnabled\">\n          <div class=\"col-lg-6\">\n            <div class=\"mb-1\">\n              Device parameters:\n            </div>\n\n            <div class=\"bs-component\">\n              <table class=\"table table-striped table-hover \">\n                <thead>\n                <tr>\n                  <th>Parameter</th>\n                  <th>Value</th>\n                  <th>Hex</th>\n                </tr>\n                </thead>\n                <tbody>\n                <tr *ngFor=\"let parameter of selectedCategory.parameters\">\n                  <td>{{parameter}}</td>\n                  <td>{{selectedDevice.parameters[parameter]}}</td>\n                  <td>{{selectedDevice.parameters[parameter] | hexTransform}}</td>\n                </tr>\n\n                </tbody>\n              </table>\n            </div>\n\n            <button class=\"btn btn-primary mb-2\" (click)=\"loadParameters()\">Load parameters</button>\n          </div>\n\n          <div class=\"col-lg-6\" *ngIf=\"selectedDevice != undefined\">\n            <div class=\"mb-1\">\n              Device commands:\n            </div>\n\n            <div class=\"mb-1 mr-3 ml-3\">\n              <div class=\"mb-1 row\" *ngFor=\"let command of selectedCategory.commands\">\n                <button class=\"btn btn-secondary col-lg-4\" (click)=\"makeCommand(command)\" >{{command}}</button>\n                <input type=\"text\" placeholder=\"decimal\" [(ngModel)]=\"selectedDevice.commands[command]\" class=\"form-control col-lg-4 col-6\" id=\"inputValue\" (input)=\"onDecValChange($event.target.value, command)\">\n                <input type=\"text\" placeholder=\"hex\" [(ngModel)]=\"hexValues[command]\" class=\"form-control col-lg-4 col-6\" id=\"inputValue\" (input)=\"onHexValChange($event.target.value, command)\">\n              </div>\n            </div>\n\n          </div>\n        </div>\n\n        <div class=\"\" *ngIf=\"advancedControlEnabled\">\n            <app-device-advance-control-pwm [device]=\"selectedDevice\" *ngIf=\"checkCategorySelected('PWM')\"></app-device-advance-control-pwm>\n            <app-device-advance-control-rgb [device]=\"selectedDevice\" *ngIf=\"checkCategorySelected('RGB')\"></app-device-advance-control-rgb>\n        </div>\n\n        <div class=\"form-group mt-3\">\n          <div class=\"mb-1\">\n            Device details:\n          </div>\n          <div class=\"mb-3\">\n            Device category: {{selectedDevice.category}}<br/>\n            Device address: {{selectedDevice.address}}<br/>\n            Device active: {{selectedDevice.active}}<br/>\n            Device UID: {{selectedDevice.uid}} ({{selectedDevice.uid | hexTransform}})<br/>\n            Reset counter: {{selectedDevice.resetCounter}}<br/>\n            Watchdog counter: {{selectedDevice.wdtCounter}}<br/>\n            Last seen time: {{selectedDevice.lastSeen}}<br/>\n            <span *ngIf=\"selectedDevice.lastStartup != null\">Last startup time: {{selectedDevice.lastStartup}}</span>\n            <span *ngIf=\"selectedDevice.lastStartup == null\">Last startup time: before system startup, no data available</span>\n          </div>\n        </div>\n\n        <div class=\"form-group mt-3\">\n          <button class=\"btn btn-danger mr-2\" (click)=\"showPopup(BOOT_REQUEST)\" >Boot device</button>\n          <button class=\"btn btn-danger\" (click)=\"showPopup(REMOVE_REQUEST)\" >Remove device</button>\n        </div>\n      </div>\n\n    </div>\n  </div>\n\n  <div *ngIf=\"advancedModeEnabled\" class=\"panel panel-default\">\n\n    <div class=\"btn-group btn-group-toggle mb-3\" data-toggle=\"buttons\">\n      <label class=\"btn btn-secondary\" *ngFor=\"let func of advancedFunctions\" (click)=\"setAdvFunc(func)\">\n        <input type=\"radio\" name=\"options\" autocomplete=\"off\" checked=\"\"> {{func}}\n      </label>\n    </div>\n\n    <div *ngIf=\"selectedAdvFunc == ADV_FUNC_UPLOAD\">\n      <div class=\"mb-1\">\n        Select firmware:\n      </div>\n\n      <div  class=\"mb-1\">\n        <select class=\"form-control col-lg-6 col-xs-12\"  id=\"selectFirmware\" [(ngModel)]=\"selectedFile\">\n          <option *ngFor=\"let file of filesList\">\n            {{file}}\n          </option>\n        </select>\n      </div>\n\n      <div class=\"form-group mt-3\" *ngIf=\"selectedFile !== undefined\">\n        <button class=\"btn btn-primary\" (click)=\"uploadFirmware()\" >Upload</button>\n      </div>\n\n    </div>\n\n    <div *ngIf=\"selectedAdvFunc == ADV_FUNC_REMOVE\">\n      <div class=\"mb-1\">\n        Select category:\n      </div>\n\n      <div  class=\"mb-1\">\n        <select class=\"form-control col-lg-6 col-xs-12\"  id=\"selectCat\" [(ngModel)]=\"selectedCatAdv\">\n          <option *ngFor=\"let cat of deviceManager.getAllCategories()\">\n            {{cat.name}}\n          </option>\n        </select>\n      </div>\n\n      <div class=\"mb-1\">\n        Address from:\n      </div>\n      <div>\n        <input type=\"text\" [(ngModel)]=\"removeAdrBegin\" class=\"form-control col-lg-6 col-xs-12 mb-1\" id=\"adrStart\">\n      </div>\n\n      <div class=\"mb-1\">\n        Address to:\n      </div>\n      <div>\n        <input type=\"text\" [(ngModel)]=\"removeAdrEnd\" class=\"form-control col-lg-6 col-xs-12 mb-1\" id=\"adrStop\">\n      </div>\n\n      <div class=\"form-group mt-3\" *ngIf=\"selectedCatAdv !== undefined\">\n        <button class=\"btn btn-primary\" (click)=\"removeDevices()\" >Remove</button>\n      </div>\n\n    </div>\n    <div *ngIf=\"selectedAdvFunc == ADV_FUNC_OTHER\">\n      <button class=\"btn btn-primary\" (click)=\"initDevices()\" >Init devices</button>\n\n    </div>\n\n\n  </div>\n\n</div>\n\n"

/***/ }),

/***/ "./src/app/components/device-manager/device-manager.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/device-manager/device-manager.component.ts ***!
  \***********************************************************************/
/*! exports provided: DeviceManagerComponent, DeviceAdvanceCommandComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceManagerComponent", function() { return DeviceManagerComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceAdvanceCommandComponent", function() { return DeviceAdvanceCommandComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _models_device__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../models/device */ "./src/app/models/device.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/index.js");
/* harmony import */ var ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap/modal/bs-modal-ref.service */ "./node_modules/ngx-bootstrap/modal/bs-modal-ref.service.js");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_9__);










var DeviceManagerComponent = /** @class */ (function () {
    function DeviceManagerComponent(flashMessage, restService, router, modalService, deviceManager) {
        this.flashMessage = flashMessage;
        this.restService = restService;
        this.router = router;
        this.modalService = modalService;
        this.deviceManager = deviceManager;
        this.BOOT_REQUEST = 'boot';
        this.REMOVE_REQUEST = 'remove';
        this.ADV_FUNC_UPLOAD = 'Firmware uploading';
        this.ADV_FUNC_REMOVE = 'Devices removing';
        this.ADV_FUNC_OTHER = 'Other';
    }
    DeviceManagerComponent.prototype.ngOnInit = function () {
        console.log(">>>> on init");
        this.filter = '';
        //this.loadCategories();
        this.advancedModeEnabled = false;
        this.advancedControlEnabled = false;
        this.advanceControlCategories = ['PWM', 'RGB'];
        this.advancedFunctions = [this.ADV_FUNC_UPLOAD, this.ADV_FUNC_REMOVE, this.ADV_FUNC_OTHER];
        this.selectedAdvFunc = this.advancedFunctions[0];
        this.deviceManager.loadDevices();
    };
    DeviceManagerComponent.prototype.loadDepsForSelectedDevice = function (address, category) {
        this.selectedDeviceName = this.selectedDevice.name;
        this.refreshCommandValues();
        this.loadDeviceDetails();
    };
    DeviceManagerComponent.prototype.loadDevices = function (category, selectDevice) {
        console.log('loadDevices for cat ' + category.name);
        this.filter = '';
        this.selectedCategory = category;
        if (this.advancedControlEnabled &&
            this.selectedCategory &&
            this.advanceControlCategories.indexOf(this.selectedCategory.name) < 0) {
            this.advancedControlEnabled = false;
        }
        this.filteredDevicesList = new Array();
        for (var _i = 0, _a = this.deviceManager.getDevicesInCategory(category); _i < _a.length; _i++) {
            var dev = _a[_i];
            this.filteredDevicesList.push(new _models_device__WEBPACK_IMPORTED_MODULE_5__["Device"](dev));
        }
        if (selectDevice) {
            for (var _b = 0, _c = this.filteredDevicesList; _b < _c.length; _b++) {
                var dev = _c[_b];
                if (dev.address == selectDevice.address && dev.category == selectDevice.category) {
                    this.selectDevice(dev);
                    break;
                }
            }
        }
        else {
            this.selectDevice(this.filteredDevicesList[0]);
        }
    };
    DeviceManagerComponent.prototype.selectDevice = function (dev) {
        this.selectedDevice = dev;
        this.loadDepsForSelectedDevice(this.selectedDevice.address, this.selectedDevice.category);
    };
    DeviceManagerComponent.prototype.loadDeviceDetails = function () {
        var _this = this;
        console.log('loadDeviceDetails');
        this.restService.getDeviceDetails(this.selectedDevice).subscribe(function (data) {
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Loading device details failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.selectedDevice.lastSeen = new Date(data.lastSeen);
                _this.selectedDevice.resetCounter = data.resetCounter;
                _this.selectedDevice.wdtCounter = data.wdtCounter;
                _this.selectedDevice.lastStartup = null;
                if (data.lastStartup != 0) {
                    _this.selectedDevice.lastStartup = new Date(data.lastStartup);
                }
            }
        });
    };
    DeviceManagerComponent.prototype.onSaveName = function () {
        var _this = this;
        if (this.selectedDevice != undefined) {
            this.selectedDevice.name = this.selectedDeviceName;
            console.log('set device name: ' + this.selectedDevice.name);
        }
        console.log('dev cat: ', this.selectedDevice['category']);
        this.restService.setDeviceName(this.selectedDevice).subscribe(function (data) {
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Setting device name failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.flashMessage.show('Device name saved successfully!', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    DeviceManagerComponent.prototype.loadParameters = function () {
        var _this = this;
        console.log('dev cat: ', this.selectedDevice['category']);
        this.restService.getDeviceParameters(this.selectedDevice).subscribe(function (data) {
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Loading device parameters failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                for (var param = 0; param < data.parameters.length; param++) {
                    if (data.parameters[param].defined) {
                        //this.selectedDevice[data.parameters[param].parameter] = data.parameters[param].value;
                        var paramName = data.parameters[param].parameter;
                        var paramValue = data.parameters[param].value;
                        _this.selectedDevice.parameters[paramName] = paramValue;
                    }
                    else {
                        //this.selectedDevice[data.parameters[param].parameter] = 'undefined';
                        var paramName = data.parameters[param].parameter;
                        _this.selectedDevice.parameters[paramName] = 'undefined';
                    }
                }
            }
        });
    };
    DeviceManagerComponent.prototype.makeCommand = function (command) {
        var _this = this;
        console.log('>>>> make command ' + command);
        this.restService.setDeviceParameter(
        //this.selectedDevice,command, this.commandValues[command]).subscribe(data => {
        this.selectedDevice, command, this.selectedDevice.commands[command]).subscribe(function (data) {
            //data = this.convertFromBufferToJson(data);
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Setting device parameter failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.flashMessage.show('Device parameter set successfully!', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    DeviceManagerComponent.prototype.refreshCommandValues = function () {
        this.selectedDevice.commands = new Object();
        this.hexValues = new Object();
        for (var _i = 0, _a = this.selectedCategory['commands']; _i < _a.length; _i++) {
            var command = _a[_i];
            this.selectedDevice.commands[command] = 0;
            this.hexValues[command] = 0;
        }
        this.selectedDevice.parameters = new Object();
        for (var _b = 0, _c = this.selectedCategory['parameters']; _b < _c.length; _b++) {
            var param = _c[_b];
            this.selectedDevice.parameters[param] = 0;
        }
    };
    DeviceManagerComponent.prototype.onDecValChange = function (value, command) {
        console.log('>>>> DEC: ', value);
        if (!isNaN(value)) {
            var hex = Number(value).toString(16).toUpperCase();
            if (hex != this.hexValues[command]) {
                this.hexValues[command] = hex;
            }
        }
        else {
            this.hexValues[command] = 0;
        }
    };
    DeviceManagerComponent.prototype.onHexValChange = function (value, command) {
        console.log('>>>> HEX: ', value);
        var dec = parseInt(value, 16);
        if (!isNaN(dec)) {
            if (dec != this.selectedDevice.commands[command]) {
                this.selectedDevice.commands[command] = dec;
            }
        }
        else {
            this.selectedDevice.commands[command] = 0;
        }
    };
    DeviceManagerComponent.prototype.clearFilters = function () {
        this.filter = '';
        this.filteredDevicesList = new Array();
        for (var _i = 0, _a = this.deviceManager.getAllDevices(); _i < _a.length; _i++) {
            var dev = _a[_i];
            this.filteredDevicesList.push(dev);
        }
        this.selectedDevice = this.deviceManager.getDevicesInCategory(this.selectedCategory)[0];
        this.loadDepsForSelectedDevice(this.selectedDevice.address, this.selectedDevice.category);
    };
    DeviceManagerComponent.prototype.getDeviceByCatAndAdr = function (adr, cat) {
        return this.deviceManager.getDevice(cat, adr);
    };
    DeviceManagerComponent.prototype.onFilterChange = function () {
        var vals = this.filter.toLowerCase().split(' ');
        this.filteredDevicesList = new Array();
        for (var _i = 0, _a = this.deviceManager.getDevicesInCategory(this.selectedCategory); _i < _a.length; _i++) {
            var dev = _a[_i];
            var allValsMatch = true;
            var addrMatch = false;
            var guidMatch = false;
            for (var _b = 0, vals_1 = vals; _b < vals_1.length; _b++) {
                var val = vals_1[_b];
                if (dev.name.indexOf(val) === -1) {
                    allValsMatch = false;
                }
                if (dev.address == val) {
                    addrMatch = true;
                }
                if (dev.uid.toString(16).toLowerCase() == val.toLowerCase()) {
                    guidMatch = true;
                }
            }
            if (allValsMatch || addrMatch || guidMatch) {
                this.filteredDevicesList.push(dev);
            }
        }
        if (this.filteredDevicesList.length > 0) {
            this.selectedDevice = this.filteredDevicesList[0];
            this.loadDepsForSelectedDevice(this.selectedDevice.address, this.selectedDevice.category);
        }
        else {
            this.selectedDevice = undefined;
            this.selectedDeviceName = '';
        }
    };
    DeviceManagerComponent.prototype.onKeySearch = function () {
        this.searchResults = new Array();
        if (!this.keysearch) {
            return;
        }
        var vals = this.keysearch.toLowerCase().split(' ');
        for (var _i = 0, _a = this.deviceManager.getAllDevices(); _i < _a.length; _i++) {
            var dev = _a[_i];
            var allValsMatch = true;
            var addrMatch = false;
            var guidMatch = false;
            for (var _b = 0, vals_2 = vals; _b < vals_2.length; _b++) {
                var val = vals_2[_b];
                if (dev.name.indexOf(val) === -1) {
                    allValsMatch = false;
                }
                if (dev.uid.toString(16).toLowerCase() == val.toLowerCase()) {
                    guidMatch = true;
                }
                if (val.indexOf(':') > 0) {
                    var catAdr = val.split(':');
                    if (catAdr[0] == dev.category && catAdr[1] == dev.address) {
                        addrMatch = true;
                    }
                }
            }
            if (allValsMatch || addrMatch || guidMatch) {
                this.searchResults.push(dev);
            }
        }
        if (this.searchResults.length == 1) {
            this.onSearchResultClick(this.searchResults[0]);
        }
    };
    DeviceManagerComponent.prototype.clearKeySearch = function () {
        this.keysearch = '';
        this.searchResults = [];
    };
    DeviceManagerComponent.prototype.onSearchResultClick = function (selectedDevice) {
        console.log('>>> selected result: ' + selectedDevice.name);
        for (var _i = 0, _a = this.deviceManager.getAllCategories(); _i < _a.length; _i++) {
            var cat = _a[_i];
            if (cat.id == selectedDevice.category) {
                this.selectedCategory = cat;
                this.loadDevices(cat, selectedDevice);
                break;
            }
        }
    };
    DeviceManagerComponent.prototype.onChange = function () {
        this.loadDepsForSelectedDevice(this.selectedDevice.address, this.selectedDevice.category);
    };
    DeviceManagerComponent.prototype.removeDevice = function () {
        var _this = this;
        console.log('>>>> remove device: ', this.selectedDevice);
        this.restService.removeDevice(this.selectedDevice.uid).subscribe(function (data) {
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Removing device failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.flashMessage.show('Device removed', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    DeviceManagerComponent.prototype.bootDevice = function () {
        var _this = this;
        console.log('>>>> boot device: ', this.selectedDevice);
        this.restService.bootDevice(this.selectedDevice.uid).subscribe(function (data) {
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Setting device into boot mode failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.flashMessage.show('Device set into boot mode', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    DeviceManagerComponent.prototype.showPopup = function (request) {
        // this.popupRequest = request;
        this.bsModalRef = this.modalService.show(DeviceAdvanceCommandComponent);
        this.bsModalRef.content.closeBtnName = 'Close';
        this.bsModalRef.content.parentController = this;
        this.bsModalRef.content.popupRequest = request;
    };
    DeviceManagerComponent.prototype.loadFilesList = function () {
        var _this = this;
        this.restService.getFilesList().subscribe(function (data) {
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Loading files list failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
                _this.router.navigate(['home']);
            }
            else {
                _this.filesList = data.files;
                if (_this.filesList !== undefined && _this.filesList.length > 0) {
                    _this.selectedFile = _this.filesList[0];
                }
            }
        });
    };
    DeviceManagerComponent.prototype.onSwitchModeChange = function () {
        if (this.filesList === undefined) {
            this.loadFilesList();
        }
    };
    DeviceManagerComponent.prototype.onSwitchControlChange = function () {
    };
    DeviceManagerComponent.prototype.setAdvFunc = function (func) {
        this.selectedAdvFunc = func;
    };
    DeviceManagerComponent.prototype.uploadFirmware = function () {
        var _this = this;
        console.log('>>> upload firmware');
        this.restService.uploadFirmware(this.selectedFile).subscribe(function (data) {
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Uploading firmware failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.flashMessage.show('Uploading firmware started', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    DeviceManagerComponent.prototype.removeDevices = function () {
        var _this = this;
        console.log('>>>> remove devices');
        var catId = this.selectedCatAdv.id;
        console.log('>>>> cat id: ' + catId);
        this.restService.removeDeviceAdvance(catId, this.removeAdrBegin, this.removeAdrEnd).subscribe(function (data) {
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Removing devices failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.flashMessage.show('Devices removed', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    DeviceManagerComponent.prototype.initDevices = function () {
        var _this = this;
        console.log('>>>> init devices');
        this.restService.initDevices().subscribe(function (data) {
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Initializing devices failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.flashMessage.show('Devices initialized successfully', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    DeviceManagerComponent.prototype.showAdvanceControlSwitch = function () {
        return this.selectedCategory && this.advanceControlCategories.indexOf(this.selectedCategory['name']) >= 0;
    };
    DeviceManagerComponent.prototype.checkCategorySelected = function (cat) {
        return this.selectedCategory['name'] == cat;
    };
    DeviceManagerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-device-manager',
            template: __webpack_require__(/*! ./device-manager.component.html */ "./src/app/components/device-manager/device-manager.component.html"),
            styles: [__webpack_require__(/*! ./device-manager.component.css */ "./src/app/components/device-manager/device-manager.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__["FlashMessagesService"],
            _services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["BsModalService"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_8__["DeviceManagerService"]])
    ], DeviceManagerComponent);
    return DeviceManagerComponent;
}());

var DeviceAdvanceCommandComponent = /** @class */ (function () {
    function DeviceAdvanceCommandComponent(bsModalRef) {
        this.bsModalRef = bsModalRef;
    }
    DeviceAdvanceCommandComponent.prototype.ngOnInit = function () { };
    DeviceAdvanceCommandComponent.prototype.onPopupConfirm = function () {
        if (this.popupRequest == this.parentController.BOOT_REQUEST) {
            this.parentController.bootDevice();
        }
        else if (this.popupRequest == this.parentController.REMOVE_REQUEST) {
            this.parentController.removeDevice();
        }
        this.bsModalRef.hide();
    };
    DeviceAdvanceCommandComponent.prototype.onPopupCancel = function () {
        this.bsModalRef.hide();
    };
    DeviceAdvanceCommandComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'modal-content',
            template: "\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title pull-left modal-header-custom\">WARNING</h4>\n    </div>\n    <div class=\"modal-body modal-body-custom\">\n    <div class=\"modal-content-center\">\n        Do you really want to {{popupRequest}} device?\n    </div>\n    <div modal-input-section class=\"modal-content-center mt-3\">\n        <button type=\"button\" class=\"btn btn-danger wide-button mr-2\" onclick=\"this.blur();\" (click)=\"onPopupCancel()\">No</button>\n        <button type=\"button\" class=\"btn btn-success\" onclick=\"this.blur();\"  (click)=\"onPopupConfirm()\">Yes</button>\n    </div>\n    </div>\n  ",
            styles: [__webpack_require__(/*! ./device-manager.component.css */ "./src/app/components/device-manager/device-manager.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_7__["BsModalRef"]])
    ], DeviceAdvanceCommandComponent);
    return DeviceAdvanceCommandComponent;
}());



/***/ }),

/***/ "./src/app/components/event-manager/event-manager.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/components/event-manager/event-manager.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".op-name {\r\n   white-space: nowrap;\r\n   overflow: hidden;\r\n   text-overflow: ellipsis;\r\n   /*width: 100px; */\r\n}\r\n\r\n.op-buttons {\r\n  white-space: nowrap;\r\n}\r\n\r\n.flash-message-container {\r\n  position: fixed;\r\n  z-index: 10;\r\n  width: 100%;\r\n}\r\n\r\n.flash-message {\r\n  display: block;\r\n  margin-right: 10%;\r\n  margin-left: 10%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ldmVudC1tYW5hZ2VyL2V2ZW50LW1hbmFnZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtHQUNHLG1CQUFtQjtHQUNuQixnQkFBZ0I7R0FDaEIsdUJBQXVCO0dBQ3ZCLGlCQUFpQjtBQUNwQjs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGVBQWU7RUFDZixXQUFXO0VBQ1gsV0FBVztBQUNiOztBQUVBO0VBQ0UsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixnQkFBZ0I7QUFDbEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2V2ZW50LW1hbmFnZXIvZXZlbnQtbWFuYWdlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm9wLW5hbWUge1xyXG4gICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgLyp3aWR0aDogMTAwcHg7ICovXHJcbn1cclxuXHJcbi5vcC1idXR0b25zIHtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG59XHJcblxyXG4uZmxhc2gtbWVzc2FnZS1jb250YWluZXIge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB6LWluZGV4OiAxMDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmZsYXNoLW1lc3NhZ2Uge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbi1yaWdodDogMTAlO1xyXG4gIG1hcmdpbi1sZWZ0OiAxMCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/event-manager/event-manager.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/event-manager/event-manager.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"flash-message-container\">\n  <div class=\"flash-message\">\n    <flash-messages></flash-messages>\n  </div>\n</div>\n<div class=\"container  mt-2\">\n\n  <div class=\"panel panel-default\">\n    <div class=\"panel-body\">\n      <div class=\"form-group\">\n        <div>\n          <button type=\"button\" *ngIf=\"operationManager.changed\" class=\"btn btn-success mb-2 mr-2\" (click)=\"onSaveOperations()\">Save</button>\n          <button type=\"button\" *ngIf=\"operationManager.changed\" class=\"btn btn-default mr-4 mb-2\" (click)=\"onCancelOperations()\">Cancel</button>\n          <button type=\"button\" (click)=\"goToOperationPage(0)\" class=\"btn btn-primary mb-2 mr-2\">Add Regular</button>\n          <button type=\"button\" (click)=\"goToOperationPage(1)\" class=\"btn btn-primary mb-2 mr-2\">Add Delayed</button>\n          <button type=\"button\" *ngIf=\"filter?.length > 0\" class=\"btn btn-default mb-2\" (click)=\"clearFilters()\">Clear filters</button>\n\n          <div>\n            <input class=\"form-control\" type=\"text\" [(ngModel)]=\"filter\" placeholder=\"Filter names\">\n          </div>\n\n        </div>\n\n        <div class=\"mt-2 mb-2\">\n          Operations List:\n        </div>\n\n        <div class=\"bs-component\">\n\n\n          <div class=\"list-group\" id=\"eventList\">\n            <button *ngFor=\"let op of operationManager.operations | nameFilter:filter | orderBy: 'name': false:true\" (click)=\"goToOperationPage(op.id)\"\n               class=\"list-group-item list-group-item-action d-flex justify-content-between align-items-center\"\n               [style.background-color]=\"getElementBackground(op)\">\n              <div class=\"op-name\">\n                {{op.name}}\n              </div>\n              <div class=\"op-buttons\">\n                <button type=\"button\" *ngIf=\"op.active == '1'\" class=\"btn btn-success mr-2\" (click)=\"onDesactivate(op, $event)\">Active</button>\n                <button type=\"button\" *ngIf=\"op.active == '0'\" class=\"btn btn-warning mr-2\" (click)=\"onActivate(op, $event)\">Inactive</button>\n                <button type=\"button\" *ngIf=\"op.deleted == '1'\" class=\"btn btn-outline-danger mr-2\" (click)=\"onUndeleteOperation(op, $event)\">Undelete</button>\n                <button type=\"button\" *ngIf=\"op.deleted == '0'\" class=\"btn btn-danger\" (click)=\"onDeleteOperation(op, $event)\">Delete</button>\n              </div>\n            </button>\n          </div>\n        </div>\n\n        <div class=\"mt-3\">\n          <button type=\"button\" *ngIf=\"operationManager.changed\" class=\"btn btn-success mr-2 mb-2\" (click)=\"onSaveOperations()\">Save</button>\n          <button type=\"button\" *ngIf=\"operationManager.changed\" class=\"btn btn-default mr-4 mb-2\" (click)=\"onCancelOperations()\">Cancel</button>\n          <button type=\"button\" (click)=\"goToOperationPage(0)\" class=\"btn btn-primary mb-2 mr-2\">Add Regular</button>\n          <button type=\"button\" (click)=\"goToOperationPage(1)\" class=\"btn btn-primary mb-2\">Add Delayed</button>\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>\n\n"

/***/ }),

/***/ "./src/app/components/event-manager/event-manager.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/event-manager/event-manager.component.ts ***!
  \*********************************************************************/
/*! exports provided: EventManagerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventManagerComponent", function() { return EventManagerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_operation_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/operation-manager.service */ "./src/app/services/operation-manager.service.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../models/consts */ "./src/app/models/consts.ts");








var EventManagerComponent = /** @class */ (function () {
    function EventManagerComponent(flashMessage, restService, router, operationManager, deviceManager) {
        this.flashMessage = flashMessage;
        this.restService = restService;
        this.router = router;
        this.operationManager = operationManager;
        this.deviceManager = deviceManager;
    }
    EventManagerComponent.prototype.ngOnInit = function () {
        console.log(">>>> on init");
        this.filter = '';
        this.deviceManager.loadDevices();
        this.operationManager.loadOperations();
    };
    EventManagerComponent.prototype.clearFilters = function () {
        this.filter = '';
    };
    EventManagerComponent.prototype.onActivate = function (operation, event) {
        this.operationManager.activateOperation(operation, true);
        event.preventDefault();
        event.stopPropagation();
    };
    EventManagerComponent.prototype.onDesactivate = function (operation, event) {
        this.operationManager.activateOperation(operation, false);
        event.preventDefault();
        event.stopPropagation();
    };
    EventManagerComponent.prototype.onSaveOperations = function () {
        this.operationManager.saveOperations();
    };
    EventManagerComponent.prototype.onCancelOperations = function () {
        this.operationManager.cancelChanges();
    };
    EventManagerComponent.prototype.onDeleteOperation = function (operation, event) {
        this.operationManager.deleteOperation(operation);
        event.preventDefault();
        event.stopPropagation();
    };
    EventManagerComponent.prototype.onUndeleteOperation = function (operation, event) {
        this.operationManager.undeleteOperation(operation);
        event.preventDefault();
        event.stopPropagation();
    };
    EventManagerComponent.prototype.getElementBackground = function (op) {
        if (op.type == 'delayed') {
            return "#0e3344";
        }
        else {
            return "";
        }
    };
    EventManagerComponent.prototype.goToOperationPage = function (id) {
        this.router.navigate(['/operation', id, _models_consts__WEBPACK_IMPORTED_MODULE_7__["SRC_PAGE_EVENT_MANAGER"]]);
    };
    EventManagerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-event-manager',
            template: __webpack_require__(/*! ./event-manager.component.html */ "./src/app/components/event-manager/event-manager.component.html"),
            styles: [__webpack_require__(/*! ./event-manager.component.css */ "./src/app/components/event-manager/event-manager.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__["FlashMessagesService"],
            _services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _services_operation_manager_service__WEBPACK_IMPORTED_MODULE_4__["OperationManagerService"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_5__["DeviceManagerService"]])
    ], EventManagerComponent);
    return EventManagerComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/home/home.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".log-size-style {\r\n  \tfont-size: 10px;\r\n}\r\n\r\n.filter-bar {\r\n  \tdisplay: flex;\r\n}\r\n\r\n.bottom-align {\r\n\tmargin: 0px;\r\n\tposition: absolute;\r\n\tbottom: 0px;\r\n}\r\n\r\n.img-gear {\r\n  cursor: pointer;\r\n  height: 100%;\r\n  width: 5%;\r\n}\r\n\r\n.logs-window {\r\n\theight: 320px;\r\n\toverflow: auto;\r\n\tbackground-color: #222;\r\n\tpadding-left: 15px;\r\n\tresize: vertical;\r\n\tborder: 1px solid transparent;\r\n    border-radius: 0.25rem;\r\n}\r\n\r\n.log-row {\r\n\t-webkit-margin-after: 0.3em;\r\n\t        margin-block-end: 0.3em;\r\n\tmargin-bottom: 0;\r\n}\r\n\r\n.font-small {\r\n\tfont-size: x-small;\r\n\tcursor: pointer;\r\n}\r\n\r\n.font-large {\r\n\tfont-size: large;\r\n\tcursor: pointer;\r\n}\r\n\r\n.font-regular {\r\n\tcursor: pointer;\r\n}\r\n\r\n.font-selector {\r\n\ttext-align: right;\r\n}\r\n\r\n.head-section {\r\n\tpadding: 2rem;\r\n}\r\n\r\n.right-align-font-selector {\r\n    right: 0;\r\n    margin-right: 16px;\r\n}\r\n\r\nh2, .h2 {\r\n  font-size: 5vw;\r\n}\r\n\r\n.flash-message-container {\r\n  position: fixed;\r\n  z-index: 10;\r\n  width: 100%;\r\n}\r\n\r\n.flash-message {\r\n  display: block;\r\n  margin-right: 10%;\r\n  margin-left: 10%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ob21lL2hvbWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtHQUNHLGVBQWU7QUFDbEI7O0FBRUE7R0FDRyxhQUFhO0FBQ2hCOztBQUVBO0NBQ0MsV0FBVztDQUNYLGtCQUFrQjtDQUNsQixXQUFXO0FBQ1o7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsWUFBWTtFQUNaLFNBQVM7QUFDWDs7QUFFQTtDQUNDLGFBQWE7Q0FDYixjQUFjO0NBQ2Qsc0JBQXNCO0NBQ3RCLGtCQUFrQjtDQUNsQixnQkFBZ0I7Q0FDaEIsNkJBQTZCO0lBQzFCLHNCQUFzQjtBQUMxQjs7QUFFQTtDQUNDLDJCQUF1QjtTQUF2Qix1QkFBdUI7Q0FDdkIsZ0JBQWdCO0FBQ2pCOztBQUVBO0NBQ0Msa0JBQWtCO0NBQ2xCLGVBQWU7QUFDaEI7O0FBRUE7Q0FDQyxnQkFBZ0I7Q0FDaEIsZUFBZTtBQUNoQjs7QUFFQTtDQUNDLGVBQWU7QUFDaEI7O0FBRUE7Q0FDQyxpQkFBaUI7QUFDbEI7O0FBRUE7Q0FDQyxhQUFhO0FBQ2Q7O0FBRUE7SUFDSSxRQUFRO0lBQ1Isa0JBQWtCO0FBQ3RCOztBQUdBO0VBQ0UsY0FBYztBQUNoQjs7QUFFQTtFQUNFLGVBQWU7RUFDZixXQUFXO0VBQ1gsV0FBVztBQUNiOztBQUVBO0VBQ0UsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixnQkFBZ0I7QUFDbEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZy1zaXplLXN0eWxlIHtcclxuICBcdGZvbnQtc2l6ZTogMTBweDtcclxufVxyXG5cclxuLmZpbHRlci1iYXIge1xyXG4gIFx0ZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmJvdHRvbS1hbGlnbiB7XHJcblx0bWFyZ2luOiAwcHg7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdGJvdHRvbTogMHB4O1xyXG59XHJcblxyXG4uaW1nLWdlYXIge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgd2lkdGg6IDUlO1xyXG59XHJcblxyXG4ubG9ncy13aW5kb3cge1xyXG5cdGhlaWdodDogMzIwcHg7XHJcblx0b3ZlcmZsb3c6IGF1dG87XHJcblx0YmFja2dyb3VuZC1jb2xvcjogIzIyMjtcclxuXHRwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcblx0cmVzaXplOiB2ZXJ0aWNhbDtcclxuXHRib3JkZXI6IDFweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XHJcbn1cclxuXHJcbi5sb2ctcm93IHtcclxuXHRtYXJnaW4tYmxvY2stZW5kOiAwLjNlbTtcclxuXHRtYXJnaW4tYm90dG9tOiAwO1xyXG59XHJcblxyXG4uZm9udC1zbWFsbCB7XHJcblx0Zm9udC1zaXplOiB4LXNtYWxsO1xyXG5cdGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmZvbnQtbGFyZ2Uge1xyXG5cdGZvbnQtc2l6ZTogbGFyZ2U7XHJcblx0Y3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4uZm9udC1yZWd1bGFyIHtcclxuXHRjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5mb250LXNlbGVjdG9yIHtcclxuXHR0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG5cclxuLmhlYWQtc2VjdGlvbiB7XHJcblx0cGFkZGluZzogMnJlbTtcclxufVxyXG5cclxuLnJpZ2h0LWFsaWduLWZvbnQtc2VsZWN0b3Ige1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDE2cHg7XHJcbn1cclxuXHJcblxyXG5oMiwgLmgyIHtcclxuICBmb250LXNpemU6IDV2dztcclxufVxyXG5cclxuLmZsYXNoLW1lc3NhZ2UtY29udGFpbmVyIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgei1pbmRleDogMTA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5mbGFzaC1tZXNzYWdlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW4tcmlnaHQ6IDEwJTtcclxuICBtYXJnaW4tbGVmdDogMTAlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"flash-message-container\">\n  <div class=\"flash-message\">\n    <flash-messages></flash-messages>\n  </div>\n</div>\n<div class=\"container\">\n  <div class=\"jumbotron text-center mt-4 head-section\">\n    <h2>IntelliHouseManager</h2>\n  </div>\n\n  <div class=\"panel\" *ngIf=\"authService.loggedIn()\">\n    <div class=\"panel-body\">\n\n      <div class=\"form-group\">\n\n        <div class=\"row mb-2\">\n          <div class=\"col-lg-5 col-sm-8 col-xs-8\">\n            <label class=\"bottom-align\" for=\"logAreaText\">IntelliHouse System Logs:</label>\n          </div>\n          <div class=\"col-lg-1 col-sm-4 col-xs-4 font-selector\">\n            <div class=\"bottom-align right-align-font-selector\">\n              <span class=\"font-small\" (click)=\"setFontSize('x-small')\">T</span>&nbsp;&nbsp;\n              <span class=\"font-regular\" (click)=\"setFontSize('')\">T</span>&nbsp;&nbsp;\n              <span class=\"font-large\" (click)=\"setFontSize('large')\">T</span>\n            </div>\n          </div>\n          <div class=\"col-lg-6 col-sm-12 col-xs-12 filter-bar\">\n            <input type=\"text\" class=\"form-control\" id=\"logFilterText\" [(ngModel)]=\"logFilter\" (ngModelChange)=\"onFilterChange($event)\" placeholder=\"Filter logs\">\n            <img src=\"assets/img/gear_icon.svg\" class=\"img-gear ml-2\" (click)=\"showPopup()\"/>\n          </div>\n        </div>\n        <!--<textarea class=\"form-control\" id=\"logAreaText\" rows=\"15\" [(ngModel)]=\"logs\" (scroll)=\"onScroll($event)\" readonly=\"true\"></textarea>-->\n        <div class=\"logs-window\" (scroll)=\"onScroll($event)\" [style.font-size]=\"getFontSize()\" id=\"logAreaText\">\n          <p *ngFor=\"let log of logsArray\" class=\"log-row\" [style.color]=\"getLogStyle(log.log)\">{{log.log | logsFilter}}</p>\n        </div>\n        <div class=\"text-right\">\n          <span class=\"log-size-style ml-2\">(size: {{logsSize}})</span>\n        </div>\n\n      </div>\n    </div>\n  </div>\n  <div class=\"panel\" *ngIf=\"!authService.loggedIn()\">\n    <h3 class=\"page-header\">Login</h3>\n    <form (submit)=\"onLoginSubmit()\">\n      <div class=\"form-group col-lg-3 col-xs-12 col-sm-12\">\n        <label>Username</label>\n        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"username\" name=\"username\">\n      </div>\n      <div class=\"form-group col-lg-3 col-xs-12 col-sm-12\">\n        <label>Password</label>\n        <input type=\"password\" class=\"form-control\" [(ngModel)]=\"password\" name=\"password\">\n      </div>\n      <div class=\"form-group\">\n        <input type=\"checkbox\" [(ngModel)]=\"rememberme\" name=\"remeberme\">\n        <label>Remember me</label>\n      </div>\n      <input type=\"submit\" class=\"btn btn-primary\" value=\"Login\">\n    </form>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent, LogsSettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogsSettingsComponent", function() { return LogsSettingsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_tools_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/tools.service */ "./src/app/services/tools.service.ts");
/* harmony import */ var rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/observable/TimerObservable */ "./node_modules/rxjs-compat/_esm5/observable/TimerObservable.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/index.js");
/* harmony import */ var ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/modal/bs-modal-ref.service */ "./node_modules/ngx-bootstrap/modal/bs-modal-ref.service.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");










var HomeComponent = /** @class */ (function () {
    function HomeComponent(toolsService, modalService, flashMessage, router, authService) {
        this.toolsService = toolsService;
        this.modalService = modalService;
        this.flashMessage = flashMessage;
        this.router = router;
        this.authService = authService;
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.logFilter = '';
        this.logsArray = [];
        this.startLogsSubscription();
        this.startLogsSizeSubscription();
        this.logsFontSize = '';
        this.minFilterLength = 2;
    };
    HomeComponent.prototype.ngOnDestroy = function () {
        this.unsubscribeLogsSubscription();
        this.unsubscribeLogsSizeSubscription();
    };
    HomeComponent.prototype.onFilterChange = function (event) {
        console.log('>>> filter: ', this.logFilter);
        if (this.logFilter.length > 2) {
            this.logsArray = [];
        }
        this.getLatestLogs();
    };
    HomeComponent.prototype.getLatestLogs = function () {
        var _this = this;
        var lastLogId = '';
        if (this.logsArray && this.logsArray.length > 0) {
            lastLogId = this.logsArray[0]['_id'];
        }
        var searchFilter = '';
        if (this.logFilter.length > this.minFilterLength) {
            searchFilter = this.logFilter;
        }
        this.toolsService.getLatestLogs(lastLogId, searchFilter).subscribe(function (data) {
            if (data['success']) {
                _this.logsArray = data['logs'].concat(_this.logsArray);
            }
            else {
                console.log('>>> getLatestLogs failure: ', data);
                _this.flashMessage.show('Loading latest logs failed: ' + data['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    HomeComponent.prototype.getOlderLogs = function () {
        var _this = this;
        var oldestLogId = '';
        if (this.logsArray && this.logsArray.length > 0) {
            oldestLogId = this.logsArray[this.logsArray.length - 1]['_id'];
            var searchFilter = '';
            if (this.logFilter.length > this.minFilterLength) {
                searchFilter = this.logFilter;
            }
            this.toolsService.getOlderLogs(oldestLogId, searchFilter).subscribe(function (data) {
                if (data['success']) {
                    _this.logsArray = _this.logsArray.concat(data['logs']);
                }
                else {
                    console.log('>>> getOlderLogs failure: ', data);
                    _this.flashMessage.show('Loading older logs failed: ' + data['result'], {
                        cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                        timeout: 5000
                    });
                }
            });
        }
    };
    HomeComponent.prototype.getLogsQnty = function () {
        var _this = this;
        this.toolsService.getLogsQnty().subscribe(function (data) {
            if (data['success']) {
                _this.logsSize = data['logSize'];
            }
            else {
                console.log('>>> getLogsQnty failure: ', data);
                _this.flashMessage.show('Loading logs qnty failed: ' + data['result'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    HomeComponent.prototype.startLogsSubscription = function () {
        var _this = this;
        var timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_3__["TimerObservable"].create(0, 5000);
        this.logsSubscription = timer.subscribe(function (t) {
            _this.getLatestLogs();
        });
    };
    HomeComponent.prototype.startLogsSizeSubscription = function () {
        var _this = this;
        var timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_3__["TimerObservable"].create(0, 60000);
        this.logsSizeSubscription = timer.subscribe(function (t) {
            _this.getLogsQnty();
        });
    };
    HomeComponent.prototype.unsubscribeLogsSubscription = function () {
        if (this.logsSubscription) {
            this.logsSubscription.unsubscribe();
        }
    };
    HomeComponent.prototype.unsubscribeLogsSizeSubscription = function () {
        if (this.logsSizeSubscription) {
            this.logsSizeSubscription.unsubscribe();
        }
    };
    HomeComponent.prototype.onScroll = function (event) {
        var srcElement = event.srcElement;
        if (srcElement && srcElement.scrollHeight <= (srcElement.scrollTop + srcElement.clientHeight)) {
            this.getOlderLogs();
        }
    };
    HomeComponent.prototype.showPopup = function () {
        // this.popupRequest = request;
        this.bsModalRef = this.modalService.show(LogsSettingsComponent);
        this.bsModalRef.content.closeBtnName = 'Close';
        this.bsModalRef.content.parentController = this;
    };
    HomeComponent.prototype.getLogStyle = function (log) {
        if (log.indexOf('[DEBUG') == 0) {
            return 'darkslategrey';
        }
        else if (log.indexOf('[LOW_INFO') == 0) {
            return 'teal';
        }
        else if (log.indexOf('[INFO') == 0) {
            return 'cyan';
        }
        else if (log.indexOf('[WARNING') == 0) {
            return 'darkorange';
        }
        else if (log.indexOf('[ERROR') == 0) {
            return 'red';
        }
        else if (log.indexOf('[SUCCESS') == 0) {
            return 'chartreuse';
        }
        else {
            return 'white';
        }
    };
    HomeComponent.prototype.getFontSize = function () {
        return this.logsFontSize;
    };
    HomeComponent.prototype.setFontSize = function (size) {
        this.logsFontSize = size;
    };
    HomeComponent.prototype.onLoginSubmit = function () {
        var _this = this;
        var user = {
            username: this.username,
            password: this.password,
            rememberme: this.rememberme
        };
        console.log('>>>>> on login submit');
        this.authService.authenticateUser(user).subscribe(function (data) {
            if (data.success) {
                _this.authService.storeUserData(data.token, data.user, data.expiresIn);
                _this.flashMessage.show('You are now logged in', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.flashMessage.show(data.msg, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            _this.router.navigate(['home']);
        });
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/components/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_tools_service__WEBPACK_IMPORTED_MODULE_2__["ToolsService"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__["BsModalService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__["FlashMessagesService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_8__["AuthService"]])
    ], HomeComponent);
    return HomeComponent;
}());

var LogsSettingsComponent = /** @class */ (function () {
    function LogsSettingsComponent(bsModalRef, restService, flashMessage) {
        this.bsModalRef = bsModalRef;
        this.restService = restService;
        this.flashMessage = flashMessage;
    }
    LogsSettingsComponent.prototype.ngOnInit = function () {
        this.loadFilters();
    };
    LogsSettingsComponent.prototype.onPopupConfirm = function () {
        this.saveFilters();
        this.bsModalRef.hide();
    };
    LogsSettingsComponent.prototype.onPopupCancel = function () {
        this.bsModalRef.hide();
    };
    LogsSettingsComponent.prototype.loadFilters = function () {
        var _this = this;
        this.restService.getJson('log_filters').subscribe(function (data) {
            //data = this.convertFromBufferToJson(data);
            console.log('data filters: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Loading log filters failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                var jsonObject = JSON.parse(data.json);
                var logFilters = jsonObject['filters'];
                _this.filters = logFilters.join('\n');
            }
        });
    };
    LogsSettingsComponent.prototype.saveFilters = function () {
        var _this = this;
        var filtersObject = { 'filters': this.filters.split('\n') };
        this.restService.saveJson('log_filters', filtersObject).subscribe(function (data) {
            //data = this.convertFromBufferToJson(data);
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Saving filters list failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.flashMessage.show('Filters saved successfully', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    LogsSettingsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'modal-content',
            template: "\n    <div class=\"modal-header\">\n      <h5 class=\"modal-title pull-left modal-header-custom\">Log filters</h5>\n    </div>\n    <div class=\"modal-body modal-body-custom\">\n      <div class=\"modal-content-center\">\n        <textarea class=\"form-control\" id=\"logFiltersAreaText\" rows=\"5\" [(ngModel)]=\"filters\"></textarea>\n      </div>\n      <div modal-input-section class=\"modal-content-center mt-3\">\n          <button type=\"button\" class=\"btn btn-default mr-2\" onclick=\"this.blur();\" (click)=\"onPopupCancel()\">Cancel</button>\n          <button type=\"button\" class=\"btn btn-success\" onclick=\"this.blur();\"  (click)=\"onPopupConfirm()\">Save</button>\n      </div>\n    </div>\n  ",
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/components/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_5__["BsModalRef"],
            _services_rest_service__WEBPACK_IMPORTED_MODULE_7__["RestService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__["FlashMessagesService"]])
    ], LogsSettingsComponent);
    return LogsSettingsComponent;
}());



/***/ }),

/***/ "./src/app/components/living-room-ceiling-effect/living-room-ceiling-effect.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/components/living-room-ceiling-effect/living-room-ceiling-effect.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ceiling-style {\r\n  background-color: #1b698e\r\n}\r\n\r\n.panel-border {\r\n  border-style: inset;\r\n}\r\n\r\n.custom-btn-primary:hover{\r\n  color: #ffffff;\r\n  background-color: #2a9fd6;\r\n  border-color: #2a9fd6;\r\n}\r\n\r\n.custom-btn-primary:active{\r\n  color: #ffffff;\r\n  background-color: #1b698e;\r\n  border-color: #15506c;\r\n}\r\n\r\n.button-style {\r\n  width: 80%;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-bottom: 10px;\r\n  margin-top: 10px;\r\n  height: 80px;\r\n  display: block;\r\n}\r\n\r\n.btn-back {\r\n  width: 80%;\r\n  height: 40px;\r\n  display: block;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n}\r\n\r\n.btn-speed {\r\n  width: 30%;\r\n  margin-bottom: 20px;\r\n  margin-top: 10px;\r\n  margin-left: 10px;\r\n  margin-right: 10px;\r\n}\r\n\r\n.speed-section {\r\n  text-align: center;\r\n}\r\n\r\n.panel-top {\r\n  height: 35px;\r\n  margin-left: 20px;\r\n  margin-right: 20px;\r\n  background-color: black;\r\n  border-radius: 10px;\r\n}\r\n\r\n.panel-middle-top, .panel-middle-bottom {\r\n  height: 38px;\r\n  background-color: black;\r\n  border-radius: 10px;\r\n}\r\n\r\n.panels-vertical {\r\n  height: 300px;\r\n  margin-left: 20px;\r\n  margin-right: 20px;\r\n  display: flex;\r\n  margin-top: 5px;\r\n  margin-bottom: 5px;\r\n}\r\n\r\n.panel-bottom {\r\n  height: 35px;\r\n  margin-left: 20px;\r\n  margin-right: 20px;\r\n  background-color: black;\r\n  border-radius: 10px;\r\n}\r\n\r\n.panel-middle-vertical {\r\n  height: 70%;\r\n  display: flex;\r\n  margin-top: 5px;\r\n  margin-bottom: 5px;\r\n}\r\n\r\n.panel-vertical-left, .panel-vertical-right {\r\n  width: 40px;\r\n  height: 100%;\r\n  background-color: black;\r\n  border-radius: 10px;\r\n}\r\n\r\n.panel-vertical-middle {\r\n  height: 100%;\r\n  width: 100%;\r\n  padding: 20px;\r\n  margin: auto;\r\n}\r\n\r\n.panel-middle-vertical-left, .panel-middle-vertical-right {\r\n  width: 47px;\r\n  height: 100%;\r\n  background-color: black;\r\n  border-radius: 10px;\r\n}\r\n\r\n.panel-middle-center {\r\n  height: 100%;\r\n  width: 100%;\r\n}\r\n\r\n.img-div-container {\r\n\r\n  position: relative;\r\n  text-align: center;\r\n}\r\n\r\n.img-style {\r\n  position: absolute;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  margin: auto;\r\n  width: 25px;\r\n  height: 25px;\r\n}\r\n\r\n.main-panel {\r\n  max-width: 1200px;\r\n  margin: auto;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9saXZpbmctcm9vbS1jZWlsaW5nLWVmZmVjdC9saXZpbmctcm9vbS1jZWlsaW5nLWVmZmVjdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0U7QUFDRjs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGNBQWM7RUFDZCx5QkFBeUI7RUFDekIscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLFVBQVU7RUFDVixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQix1QkFBdUI7RUFDdkIsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsWUFBWTtFQUNaLHVCQUF1QjtFQUN2QixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsZUFBZTtFQUNmLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHVCQUF1QjtFQUN2QixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsYUFBYTtFQUNiLGVBQWU7RUFDZixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLHVCQUF1QjtFQUN2QixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxZQUFZO0VBQ1osV0FBVztFQUNYLGFBQWE7RUFDYixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLHVCQUF1QjtFQUN2QixtQkFBbUI7QUFDckI7O0FBR0E7RUFDRSxZQUFZO0VBQ1osV0FBVztBQUNiOztBQUdBOztFQUVFLGtCQUFrQjtFQUNsQixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLFNBQVM7RUFDVCxPQUFPO0VBQ1AsUUFBUTtFQUNSLFlBQVk7RUFDWixXQUFXO0VBQ1gsWUFBWTtBQUNkOztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLFlBQVk7QUFDZCIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbGl2aW5nLXJvb20tY2VpbGluZy1lZmZlY3QvbGl2aW5nLXJvb20tY2VpbGluZy1lZmZlY3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jZWlsaW5nLXN0eWxlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMWI2OThlXHJcbn1cclxuXHJcbi5wYW5lbC1ib3JkZXIge1xyXG4gIGJvcmRlci1zdHlsZTogaW5zZXQ7XHJcbn1cclxuXHJcbi5jdXN0b20tYnRuLXByaW1hcnk6aG92ZXJ7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJhOWZkNjtcclxuICBib3JkZXItY29sb3I6ICMyYTlmZDY7XHJcbn1cclxuXHJcbi5jdXN0b20tYnRuLXByaW1hcnk6YWN0aXZle1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMxYjY5OGU7XHJcbiAgYm9yZGVyLWNvbG9yOiAjMTU1MDZjO1xyXG59XHJcblxyXG4uYnV0dG9uLXN0eWxlIHtcclxuICB3aWR0aDogODAlO1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgaGVpZ2h0OiA4MHB4O1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uYnRuLWJhY2sge1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgaGVpZ2h0OiA0MHB4O1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxufVxyXG5cclxuLmJ0bi1zcGVlZCB7XHJcbiAgd2lkdGg6IDMwJTtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG4uc3BlZWQtc2VjdGlvbiB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4ucGFuZWwtdG9wIHtcclxuICBoZWlnaHQ6IDM1cHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcbi5wYW5lbC1taWRkbGUtdG9wLCAucGFuZWwtbWlkZGxlLWJvdHRvbSB7XHJcbiAgaGVpZ2h0OiAzOHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcbi5wYW5lbHMtdmVydGljYWwge1xyXG4gIGhlaWdodDogMzAwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgbWFyZ2luLXRvcDogNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDVweDtcclxufVxyXG5cclxuLnBhbmVsLWJvdHRvbSB7XHJcbiAgaGVpZ2h0OiAzNXB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gIG1hcmdpbi1yaWdodDogMjBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG4ucGFuZWwtbWlkZGxlLXZlcnRpY2FsIHtcclxuICBoZWlnaHQ6IDcwJTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIG1hcmdpbi10b3A6IDVweDtcclxuICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbn1cclxuXHJcbi5wYW5lbC12ZXJ0aWNhbC1sZWZ0LCAucGFuZWwtdmVydGljYWwtcmlnaHQge1xyXG4gIHdpZHRoOiA0MHB4O1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG4ucGFuZWwtdmVydGljYWwtbWlkZGxlIHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICBtYXJnaW46IGF1dG87XHJcbn1cclxuXHJcbi5wYW5lbC1taWRkbGUtdmVydGljYWwtbGVmdCwgLnBhbmVsLW1pZGRsZS12ZXJ0aWNhbC1yaWdodCB7XHJcbiAgd2lkdGg6IDQ3cHg7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcblxyXG4ucGFuZWwtbWlkZGxlLWNlbnRlciB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmltZy1kaXYtY29udGFpbmVyIHtcclxuXHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLmltZy1zdHlsZSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICBib3R0b206IDA7XHJcbiAgbGVmdDogMDtcclxuICByaWdodDogMDtcclxuICBtYXJnaW46IGF1dG87XHJcbiAgd2lkdGg6IDI1cHg7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG59XHJcblxyXG4ubWFpbi1wYW5lbCB7XHJcbiAgbWF4LXdpZHRoOiAxMjAwcHg7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG59XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/living-room-ceiling-effect/living-room-ceiling-effect.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/living-room-ceiling-effect/living-room-ceiling-effect.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row main-panel\">\n    <div class=\"col-md-6 col-xs-6\" >\n      <div class=\"row\">\n        <div class=\"col-md-6 col-xs-6\">\n          <button id=\"livingroomceilingeffectset1\" type=\"button\" class=\"btn btn-primary custom-btn-primary button-style\" onclick=\"this.blur();\" (click)=\"loadEffectConfig($event)\" long-press  (onLongPress)=\"saveEffectConfig($event)\">SET1</button>\n          <button id=\"livingroomceilingeffectset2\" type=\"button\" class=\"btn btn-primary custom-btn-primary button-style\" onclick=\"this.blur();\" (click)=\"loadEffectConfig($event)\" long-press  (onLongPress)=\"saveEffectConfig($event)\">SET2</button>\n          <button id=\"livingroomceilingeffectset3\" type=\"button\" class=\"btn btn-primary custom-btn-primary button-style\" onclick=\"this.blur();\" (click)=\"loadEffectConfig($event)\" long-press  (onLongPress)=\"saveEffectConfig($event)\">SET3</button>\n          <button *ngIf=\"!isManualModeOn()\" id=\"livingroomceilingeffectmanual\" type=\"button\" class=\"btn btn-primary custom-btn-primary button-style\" onclick=\"this.blur();\" (click)=\"setManualMode()\">Dostosuj</button>\n        </div>\n        <div class=\"col-md-6 col-xs-6\">\n          <button type=\"button\" class=\"btn btn-primary custom-btn-primary button-style\" onclick=\"this.blur();\" (click)=\"setDevicesMode(1)\" >EFEKT 1</button>\n          <button type=\"button\" class=\"btn btn-primary custom-btn-primary button-style\" onclick=\"this.blur();\" (click)=\"setDevicesMode(3)\" >EFEKT 2</button>\n          <button type=\"button\" class=\"btn btn-primary custom-btn-primary button-style\" onclick=\"this.blur();\" (click)=\"setDevicesMode(5)\" >EFEKT 3</button>\n          Prędkość:\n          <div id=\"speed_section\" class=\"speed-section\">\n            <button type=\"button\" class=\"btn btn-primary custom-btn-primary btn-speed\" onclick=\"this.blur();\" (click)=\"increaseSpeed()\" >+</button>\n            {{speed}}\n            <button type=\"button\" class=\"btn btn-primary custom-btn-primary btn-speed\" onclick=\"this.blur();\" (click)=\"decreaseSpeed()\" >-</button>\n          </div>\n\n        </div>\n      </div>\n      <button type=\"button\" class=\"btn btn-success custom-btn-primary btn-back mt-3\" onclick=\"this.blur();\"  [routerLink]=\"['/livingroom']\" >WRÓĆ</button>\n\n    </div>\n\n\n\n    <div class=\"col-md-6 col-xs-6 pt-3\" >\n      <div id=\"ceiling\" class=\"ceiling-style pt-2 pb-2 pr-2 pl-2\" *ngIf=\"isManualModeOn()\" >\n\n          <div id=\"salon_listwa_zew_krotka1\" class=\"panel-border panel-top img-div-container\" [style.background-color]=\"getStyle('salon_listwa_zew_krotka1')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">\n              <img *ngIf=\"getSelected('salon_listwa_zew_krotka1')\" src=\"../../../assets/img/checkbox.png\" class=\"img-style\">\n          </div>\n          <div class=\"panels-vertical\">\n            <div id=\"salon_listwa_zew_dluga1\" class=\"panel-border panel-vertical-left img-div-container\" [style.background-color]=\"getStyle('salon_listwa_zew_dluga1')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">\n                <img *ngIf=\"getSelected('salon_listwa_zew_dluga1')\" src=\"../../../assets/img/checkbox.png\" class=\"img-style\">\n            </div>\n\n            <div class=\"panel-vertical-middle\">\n\n              <div id=\"salon_listwa_wew_krotka1\" class=\"panel-border panel-middle-top img-div-container\" [style.background-color]=\"getStyle('salon_listwa_wew_krotka1')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">\n                  <img *ngIf=\"getSelected('salon_listwa_wew_krotka1')\" src=\"../../../assets/img/checkbox.png\" class=\"img-style\">\n              </div>\n\n              <div class=\" panel-middle-vertical\">\n                <div id=\"salon_listwa_wew_dluga1\" class=\"panel-border panel-middle-vertical-left img-div-container\" [style.background-color]=\"getStyle('salon_listwa_wew_dluga1')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">\n                    <img *ngIf=\"getSelected('salon_listwa_wew_dluga1')\" src=\"../../../assets/img/checkbox.png\" class=\"img-style\">\n                </div>\n                <div class=\"panel-middle-center\">\n                </div>\n                <div id=\"salon_listwa_wew_dluga2\" class=\"panel-border panel-middle-vertical-right img-div-container\" [style.background-color]=\"getStyle('salon_listwa_wew_dluga2')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">\n                    <img *ngIf=\"getSelected('salon_listwa_wew_dluga2')\" src=\"../../../assets/img/checkbox.png\" class=\"img-style\">\n                </div>\n              </div>\n\n              <div id=\"salon_listwa_wew_krotka2\" class=\"panel-border panel-middle-bottom img-div-container\" [style.background-color]=\"getStyle('salon_listwa_wew_krotka2')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">\n                  <img *ngIf=\"getSelected('salon_listwa_wew_krotka2')\" src=\"../../../assets/img/checkbox.png\" class=\"img-style\">\n              </div>\n\n\n            </div>\n\n            <div id=\"salon_listwa_zew_dluga2\" class=\"panel-border panel-vertical-right img-div-container\" [style.background-color]=\"getStyle('salon_listwa_zew_dluga2')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">\n                <img *ngIf=\"getSelected('salon_listwa_zew_dluga2')\" src=\"../../../assets/img/checkbox.png\" class=\"img-style\">\n            </div>\n          </div>\n\n          <div id=\"salon_listwa_zew_krotka2\" class=\"panel-border panel-bottom img-div-container\" [style.background-color]=\"getStyle('salon_listwa_zew_krotka2')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">\n              <img *ngIf=\"getSelected('salon_listwa_zew_krotka2')\" src=\"../../../assets/img/checkbox.png\" class=\"img-style\">\n          </div>\n\n        </div>\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/living-room-ceiling-effect/living-room-ceiling-effect.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/living-room-ceiling-effect/living-room-ceiling-effect.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: LivingRoomCeilingEffectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivingRoomCeilingEffectComponent", function() { return LivingRoomCeilingEffectComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var _services_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/config.service */ "./src/app/services/config.service.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utils/deviceConsts */ "./src/app/utils/deviceConsts.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../utils/utils */ "./src/app/utils/utils.ts");









var LivingRoomCeilingEffectComponent = /** @class */ (function () {
    function LivingRoomCeilingEffectComponent(restService, deviceManager, configService, route, router) {
        this.restService = restService;
        this.deviceManager = deviceManager;
        this.configService = configService;
        this.route = route;
        this.router = router;
        this.currentDevices = [
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_LONG,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_LONG,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT
        ];
    }
    LivingRoomCeilingEffectComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('>>> onInit');
        this.utils = new _utils_utils__WEBPACK_IMPORTED_MODULE_7__["Utils"]();
        this.deviceManager.loadDevices(function () { _this.loadDevices(); });
    };
    LivingRoomCeilingEffectComponent.prototype.loadDevices = function () {
        var _this = this;
        console.log('Loading devices config');
        this.configService.getDevices(this.currentDevices).subscribe(function (data) {
            console.log('data: ', data);
            if (data.success) {
                console.log('GetDevices success, devices list: ', data.devices);
                _this.devicesMap = new Object();
                for (var _i = 0, _a = data.devices; _i < _a.length; _i++) {
                    var dev = _a[_i];
                    console.log('dev:', dev);
                    _this.devicesMap[dev.key] = new Object();
                    _this.devicesMap[dev.key].device = _this.deviceManager.getDevice(dev.category, dev.address);
                    _this.devicesMap[dev.key].red = 0;
                    _this.devicesMap[dev.key].green = 0;
                    _this.devicesMap[dev.key].blue = 0;
                    _this.devicesMap[dev.key].mode = 0;
                    _this.devicesMap[dev.key].speed = 0;
                    _this.devicesMap[dev.key].selected = false;
                }
                _this.loadDevicesParameters();
                console.log('>>>> devices map: ', _this.devicesMap);
            }
            else {
                console.log('GetDevices error: ', data.msg);
            }
        });
    };
    LivingRoomCeilingEffectComponent.prototype.loadDevicesParameters = function () {
        var _this = this;
        console.log('Loading devices parameters');
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            this.loadDeviceParameters(devKey, function (key, data) {
                for (var param = 0; param < data.parameters.length; param++) {
                    if (!data.parameters[param].defined) {
                        console.log('Device parameter ' + param + ' is undefined');
                    }
                    else {
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RED) {
                            _this.devicesMap[key].red = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_GREEN) {
                            _this.devicesMap[key].green = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_BLUE) {
                            _this.devicesMap[key].blue = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_SPEED) {
                            _this.devicesMap[key].speed = data.parameters[param]['value'];
                            _this.speed = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE) {
                            _this.devicesMap[key].mode = data.parameters[param]['value'];
                        }
                    }
                }
            });
        }
    };
    LivingRoomCeilingEffectComponent.prototype.loadDeviceParameters = function (deviceKey, callback) {
        this.restService.getDeviceParameters(this.devicesMap[deviceKey].device).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Loading device parameters failed: ' + data.result);
            }
            else {
                callback(deviceKey, data);
            }
        });
    };
    LivingRoomCeilingEffectComponent.prototype.getId = function (id) {
        return id.substring(0, id.length - 1);
    };
    LivingRoomCeilingEffectComponent.prototype.getStyle = function (id) {
        id = this.getId(id);
        var retVal = 'rgb(';
        if (this.devicesMap && this.devicesMap[id]) {
            retVal += ((this.devicesMap[id].red >> 4) & 0xff);
            retVal += ',';
            retVal += ((this.devicesMap[id].green >> 4) & 0xff);
            retVal += ',';
            retVal += ((this.devicesMap[id].blue >> 4) & 0xff);
        }
        else {
            retVal += '0,0,0';
        }
        retVal += ')';
        return retVal;
    };
    LivingRoomCeilingEffectComponent.prototype.loadEffectConfig = function (event) {
        var _this = this;
        var setName = event['target'].attributes['id'].value;
        console.log('Loading effect set ' + setName);
        this.clearDeviceSelection();
        this.configService.getDevicesConfigs(setName).subscribe(function (data) {
            console.log('data: ', data);
            if (data.success) {
                console.log('config: ', data.configs);
                _this.setDeviceParameter(_this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
                _this.setDeviceParameter(_this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
                for (var _i = 0, _a = data.configs; _i < _a.length; _i++) {
                    var config = _a[_i];
                    var key = _this.getDeviceKey(config.address, config.category);
                    _this.setDeviceParameter(_this.devicesMap[key].device, config.parameter, config.value);
                    _this.updateCurrentDevices(key, config.value, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
                    console.log('Loading config for device ' + key + ', parameter ' + config.parameter + ' from config = ' + config.value);
                }
            }
            else {
                console.log('GetDevicesConfigs error: ', data.msg);
            }
        });
    };
    LivingRoomCeilingEffectComponent.prototype.saveEffectConfig = function (event) {
        var setName = event['target'].attributes['id'].value;
        console.log('Saving effect set ' + setName);
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            var rgb = this.utils.getRgb(this.devicesMap[devKey].red, this.devicesMap[devKey].green, this.devicesMap[devKey].blue);
            console.log('Saving rgb value ' + rgb + ' for device ' + devKey);
            this.configService.addDeviceConfig(setName, this.devicesMap[devKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, rgb).subscribe(function (data) {
                console.log('Saving config success');
            });
        }
    };
    LivingRoomCeilingEffectComponent.prototype.updateCurrentDevices = function (devKey, rgb, mode) {
        this.devicesMap[devKey].mode = mode;
        this.devicesMap[devKey].red = this.utils.getRed12bit(rgb);
        this.devicesMap[devKey].green = this.utils.getGreen12bit(rgb);
        this.devicesMap[devKey].blue = this.utils.getBlue12bit(rgb);
        console.log('>>>> dev: ', this.devicesMap[devKey]);
    };
    LivingRoomCeilingEffectComponent.prototype.updateCurrentDevicesMode = function (devKey, mode) {
        this.devicesMap[devKey].mode = mode;
        console.log('>>>> dev: ', this.devicesMap[devKey]);
    };
    LivingRoomCeilingEffectComponent.prototype.setDeviceParameter = function (dev, param, val) {
        this.restService.setDeviceParameter(dev, param, val).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Setting device parameter failed: ' + data.result);
            }
            else {
                console.log('Device parameter set successfully! ');
            }
        });
    };
    LivingRoomCeilingEffectComponent.prototype.setDevicesMode = function (mode) {
        console.log('Set effect in mode ' + mode);
        this.clearDeviceSelection();
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            this.updateCurrentDevicesMode(devKey, mode);
        }
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, mode);
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, mode);
    };
    LivingRoomCeilingEffectComponent.prototype.increaseSpeed = function () {
        console.log('Increasing speed');
        if (this.speed < 3) {
            this.speed++;
            for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
                var devKey = _a[_i];
                this.devicesMap[devKey].speed = this.speed;
            }
            this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_SPEED, this.speed);
            this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_SPEED, this.speed);
        }
    };
    LivingRoomCeilingEffectComponent.prototype.decreaseSpeed = function () {
        console.log('Decreasing speed');
        if (this.speed > 0) {
            this.speed--;
            for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
                var devKey = _a[_i];
                this.devicesMap[devKey].speed = this.speed;
            }
            this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_SPEED, this.speed);
            this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_SPEED, this.speed);
        }
    };
    LivingRoomCeilingEffectComponent.prototype.goToColorPickerPage = function (event) {
        var devKey = event['target'].attributes['id'].value;
        devKey = this.getId(devKey);
        var dev = this.devicesMap[devKey].device;
        var addresses = new Array();
        if (this.devicesMap[devKey].mode != _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL) {
            this.setDevicesMode(_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
        }
        if (!this.devicesMap[devKey].selected) {
            this.clearDeviceSelection();
            addresses.push(dev.address);
        }
        else {
            for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
                var key = _a[_i];
                if (this.devicesMap[key].selected) {
                    addresses.push(this.devicesMap[key].device.address);
                }
            }
        }
        this.router.navigate(['/colorpicker', dev.category, addresses.join(),
            this.devicesMap[devKey].red, this.devicesMap[devKey].green, this.devicesMap[devKey].blue, 'livingroomceilingeffect']);
    };
    LivingRoomCeilingEffectComponent.prototype.isManualModeOn = function () {
        return (this.devicesMap
            && this.devicesMap[this.currentDevices[0]].mode
            && this.devicesMap[this.currentDevices[0]].mode == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
    };
    LivingRoomCeilingEffectComponent.prototype.setManualMode = function () {
        console.log('Switch to manual mode');
        this.clearDeviceSelection();
        if (this.devicesMap[this.currentDevices[0]].mode == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL) {
            return;
        }
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            //this.setDeviceParameter(this.devicesMap[devKey].device, DeviceConsts.PARAM_RGB, 0);
            this.updateCurrentDevicesMode(devKey, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
        }
    };
    LivingRoomCeilingEffectComponent.prototype.getDeviceKey = function (address, category) {
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            if (this.devicesMap[devKey].device.address == address && this.devicesMap[devKey].device.category == category) {
                return devKey;
            }
        }
    };
    LivingRoomCeilingEffectComponent.prototype.getSelected = function (id) {
        id = this.getId(id);
        return (this.devicesMap && this.devicesMap[id].selected);
    };
    LivingRoomCeilingEffectComponent.prototype.selectDevice = function (event) {
        var id;
        if (event['target'].attributes.id) {
            id = event['target'].attributes.id.value;
        }
        else if (event['target'].parentNode.attributes.id) {
            id = event['target'].parentNode.attributes.id.value;
        }
        if (id) {
            id = this.getId(id);
            if (this.devicesMap[id].selected) {
                this.devicesMap[id].selected = false;
            }
            else {
                this.devicesMap[id].selected = true;
            }
        }
    };
    LivingRoomCeilingEffectComponent.prototype.clearDeviceSelection = function () {
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var key = _a[_i];
            this.devicesMap[key].selected = false;
        }
    };
    LivingRoomCeilingEffectComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-living-room-ceiling-effect',
            template: __webpack_require__(/*! ./living-room-ceiling-effect.component.html */ "./src/app/components/living-room-ceiling-effect/living-room-ceiling-effect.component.html"),
            styles: [__webpack_require__(/*! ./living-room-ceiling-effect.component.css */ "./src/app/components/living-room-ceiling-effect/living-room-ceiling-effect.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__["DeviceManagerService"],
            _services_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], LivingRoomCeilingEffectComponent);
    return LivingRoomCeilingEffectComponent;
}());



/***/ }),

/***/ "./src/app/components/living-room-lights/living-room-lights.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/components/living-room-lights/living-room-lights.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.panel-color {\r\n  background-color: #1b698e;\r\n}\r\n\r\n\r\n.main-component {\r\n  max-width: 1200px;\r\n  width: 85%;\r\n  padding-top: 10px;\r\n  padding-bottom: 0px;\r\n  background-color: #060606;\r\n}\r\n\r\n\r\n.panel-top, .panel-bottom {\r\n  padding-top: 5px;\r\n  padding-bottom: 5px;\r\n  display: block;\r\n}\r\n\r\n\r\n.panel-top-left {\r\n  border-top-left-radius: 10px;\r\n  border-bottom-left-radius: 0px;\r\n}\r\n\r\n\r\n.outside-column {\r\n  width: 80px;\r\n}\r\n\r\n\r\n.panel-top-right {\r\n  border-top-right-radius: 10px;\r\n  border-bottom-right-radius: 0px;\r\n}\r\n\r\n\r\n.panel-bottom-left {\r\n  border-bottom-left-radius: 10px;\r\n  border-top-left-radius: 0px;\r\n}\r\n\r\n\r\n.panel-bottom-right {\r\n  border-bottom-right-radius: 10px;\r\n  border-top-right-radius: 0px;\r\n}\r\n\r\n\r\n.btn-center {\r\n  margin: auto;\r\n}\r\n\r\n\r\n.btn-top-left, .btn-bottom-left {\r\n  margin-right: 20%;\r\n  float: right;\r\n  position: relative;\r\n}\r\n\r\n\r\n.btn-top-right, .btn-bottom-right {\r\n  margin-left: 20%;\r\n}\r\n\r\n\r\n.panel-top-center, .panel-bottom-center {\r\n    width: 23%;\r\n}\r\n\r\n\r\n.row-center {\r\n  display: flex;\r\n  justify-content: center;\r\n}\r\n\r\n\r\n.panel-distance {\r\n  height: 20px;\r\n}\r\n\r\n\r\n.panel-distance-center-background {\r\n  background-color: #1b698e;\r\n  width: 69%;\r\n}\r\n\r\n\r\n.panel-distance-center-top {\r\n  width: 100%;\r\n  height: inherit;\r\n  border-top-left-radius: 10px;\r\n  border-top-right-radius: 10px;\r\n  border-bottom-left-radius: 0px;\r\n  border-bottom-right-radius: 0px;\r\n  background-color: #060606;\r\n}\r\n\r\n\r\n.panel-distance-center-bottom {\r\n  width: 100%;\r\n  height: inherit;\r\n  border-bottom-right-radius: 10px;\r\n  border-bottom-left-radius: 10px;\r\n  border-top-right-radius: 0px;\r\n  border-top-left-radius: 0px;\r\n  background-color: #060606;\r\n}\r\n\r\n\r\n.panel-middle-distance {\r\n  width: 6%;\r\n}\r\n\r\n\r\n.panel-middle-center-margin {\r\n  width: 5%;\r\n}\r\n\r\n\r\n.panel-middle-center {\r\n  display: grid;\r\n  width: 57%;\r\n  border-top-left-radius: 10px;\r\n  border-top-right-radius: 10px;\r\n  border-bottom-left-radius: 10px;\r\n  border-bottom-right-radius: 10px;\r\n}\r\n\r\n\r\n.panel-middle-left {\r\n  width: 80px;\r\n  padding-left: 9px;\r\n}\r\n\r\n\r\n.panel-middle-right {\r\n  padding-left: 9px;\r\n  float: right;\r\n}\r\n\r\n\r\n.panel-middle-side-upper {\r\n  padding-top: 8px;\r\n  padding-bottom: 17px;\r\n}\r\n\r\n\r\n.panel-middle-side-downer {\r\n  padding-top: 17px;\r\n  padding-bottom: 8px;\r\n}\r\n\r\n\r\n.btn-top-center, .btn-bottom-center {\r\n  text-align: center;\r\n}\r\n\r\n\r\n.panel-middle-center-col {\r\n  height: 145px;\r\n  display: grid;\r\n  width: 18%;\r\n}\r\n\r\n\r\n.panel-middle-center-internal {\r\n  margin-top: auto;\r\n  margin-bottom: auto;\r\n}\r\n\r\n\r\n.panel-middle-center-btn {\r\n  margin: auto;\r\n}\r\n\r\n\r\n.button-width {\r\n  width: 130px;\r\n}\r\n\r\n\r\n.left-button {\r\n  float: left;\r\n}\r\n\r\n\r\n.right-button {\r\n  float: right;\r\n}\r\n\r\n\r\n.button-panel {\r\n  padding-top: 20px;\r\n  width: 80%;\r\n  margin: auto;\r\n}\r\n\r\n\r\n.custom-btn {\r\n  width: 100%;\r\n}\r\n\r\n\r\n.custom-btn-primary:hover{\r\n  color: #ffffff;\r\n  background-color: #2a9fd6;\r\n  border-color: #2a9fd6;\r\n}\r\n\r\n\r\n.custom-btn-primary:active{\r\n  color: #ffffff;\r\n  background-color: #1b698e;\r\n  border-color: #15506c;\r\n}\r\n\r\n\r\n.btn-increase {\r\n  width: 10%;\r\n  display: table;\r\n  margin: auto;\r\n  margin-bottom: 5px;\r\n}\r\n\r\n\r\n.btn-decrease {\r\n  width: 10%;\r\n  display: table;\r\n  margin: auto;\r\n  margin-top: 5px;\r\n}\r\n\r\n\r\n/*.icon_bg {*/\r\n\r\n\r\n/*position: absolute;*/\r\n\r\n\r\n/*}*/\r\n\r\n\r\n.input-box {\r\n  text-align: center;\r\n  width: 50%;\r\n  display: block;\r\n  margin: auto;\r\n}\r\n\r\n\r\n.light-bg {\r\n  z-index: 1;\r\n  width: 60px;\r\n  height: 60px;\r\n  background-color: black;\r\n  position: relative;\r\n  border-radius: 30px;\r\n}\r\n\r\n\r\n.light-icon {\r\n  background-image: url('light_icon2.png');\r\n  background-position: center;\r\n  background-repeat: no-repeat;\r\n  background-size: cover;\r\n  position: absolute;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n\r\n\r\n.modal-body-custom {\r\n  text-align: center;\r\n}\r\n\r\n\r\n.modal-input-section {\r\n  display: inline-block;\r\n}\r\n\r\n\r\n.modal-footer-custom {\r\n  text-align: center;\r\n  display: inherit;\r\n}\r\n\r\n\r\n.button-center {\r\n  text-align: center;\r\n}\r\n\r\n\r\n.set-name-input {\r\n  display: inline;\r\n  width: 200px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9saXZpbmctcm9vbS1saWdodHMvbGl2aW5nLXJvb20tbGlnaHRzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0VBQ0UseUJBQXlCO0FBQzNCOzs7QUFHQTtFQUNFLGlCQUFpQjtFQUNqQixVQUFVO0VBQ1YsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQix5QkFBeUI7QUFDM0I7OztBQUdBO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixjQUFjO0FBQ2hCOzs7QUFFQTtFQUNFLDRCQUE0QjtFQUM1Qiw4QkFBOEI7QUFDaEM7OztBQUVBO0VBQ0UsV0FBVztBQUNiOzs7QUFFQTtFQUNFLDZCQUE2QjtFQUM3QiwrQkFBK0I7QUFDakM7OztBQUVBO0VBQ0UsK0JBQStCO0VBQy9CLDJCQUEyQjtBQUM3Qjs7O0FBRUE7RUFDRSxnQ0FBZ0M7RUFDaEMsNEJBQTRCO0FBQzlCOzs7QUFFQTtFQUNFLFlBQVk7QUFDZDs7O0FBRUE7RUFDRSxpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGtCQUFrQjtBQUNwQjs7O0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7OztBQUVBO0lBQ0ksVUFBVTtBQUNkOzs7QUFFQTtFQUNFLGFBQWE7RUFDYix1QkFBdUI7QUFDekI7OztBQUdBO0VBQ0UsWUFBWTtBQUNkOzs7QUFFQTtFQUNFLHlCQUF5QjtFQUN6QixVQUFVO0FBQ1o7OztBQUVBO0VBQ0UsV0FBVztFQUNYLGVBQWU7RUFDZiw0QkFBNEI7RUFDNUIsNkJBQTZCO0VBQzdCLDhCQUE4QjtFQUM5QiwrQkFBK0I7RUFDL0IseUJBQXlCO0FBQzNCOzs7QUFFQTtFQUNFLFdBQVc7RUFDWCxlQUFlO0VBQ2YsZ0NBQWdDO0VBQ2hDLCtCQUErQjtFQUMvQiw0QkFBNEI7RUFDNUIsMkJBQTJCO0VBQzNCLHlCQUF5QjtBQUMzQjs7O0FBRUE7RUFDRSxTQUFTO0FBQ1g7OztBQUVBO0VBQ0UsU0FBUztBQUNYOzs7QUFFQTtFQUNFLGFBQWE7RUFDYixVQUFVO0VBQ1YsNEJBQTRCO0VBQzVCLDZCQUE2QjtFQUM3QiwrQkFBK0I7RUFDL0IsZ0NBQWdDO0FBQ2xDOzs7QUFFQTtFQUNFLFdBQVc7RUFDWCxpQkFBaUI7QUFDbkI7OztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLFlBQVk7QUFDZDs7O0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsb0JBQW9CO0FBQ3RCOzs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixtQkFBbUI7QUFDckI7OztBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCOzs7QUFFQTtFQUNFLGFBQWE7RUFDYixhQUFhO0VBQ2IsVUFBVTtBQUNaOzs7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixtQkFBbUI7QUFDckI7OztBQUVBO0VBQ0UsWUFBWTtBQUNkOzs7QUFHQTtFQUNFLFlBQVk7QUFDZDs7O0FBRUE7RUFDRSxXQUFXO0FBQ2I7OztBQUVBO0VBQ0UsWUFBWTtBQUNkOzs7QUFHQTtFQUNFLGlCQUFpQjtFQUNqQixVQUFVO0VBQ1YsWUFBWTtBQUNkOzs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7O0FBRUE7RUFDRSxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLHFCQUFxQjtBQUN2Qjs7O0FBRUE7RUFDRSxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLHFCQUFxQjtBQUN2Qjs7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsY0FBYztFQUNkLFlBQVk7RUFDWixrQkFBa0I7QUFDcEI7OztBQUVBO0VBQ0UsVUFBVTtFQUNWLGNBQWM7RUFDZCxZQUFZO0VBQ1osZUFBZTtBQUNqQjs7O0FBRUEsYUFBYTs7O0FBQ1gsc0JBQXNCOzs7QUFDeEIsSUFBSTs7O0FBRUo7RUFDRSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLGNBQWM7RUFDZCxZQUFZO0FBQ2Q7OztBQUVBO0VBQ0UsVUFBVTtFQUNWLFdBQVc7RUFDWCxZQUFZO0VBQ1osdUJBQXVCO0VBQ3ZCLGtCQUFrQjtFQUNsQixtQkFBbUI7QUFDckI7OztBQUVBO0VBQ0Usd0NBQTREO0VBQzVELDJCQUEyQjtFQUMzQiw0QkFBNEI7RUFDNUIsc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTtBQUNkOzs7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjs7O0FBRUE7RUFDRSxxQkFBcUI7QUFDdkI7OztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGdCQUFnQjtBQUNsQjs7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7OztBQUVBO0VBQ0UsZUFBZTtFQUNmLFlBQVk7QUFDZCIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbGl2aW5nLXJvb20tbGlnaHRzL2xpdmluZy1yb29tLWxpZ2h0cy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi5wYW5lbC1jb2xvciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFiNjk4ZTtcclxufVxyXG5cclxuXHJcbi5tYWluLWNvbXBvbmVudCB7XHJcbiAgbWF4LXdpZHRoOiAxMjAwcHg7XHJcbiAgd2lkdGg6IDg1JTtcclxuICBwYWRkaW5nLXRvcDogMTBweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwNjA2MDY7XHJcbn1cclxuXHJcblxyXG4ucGFuZWwtdG9wLCAucGFuZWwtYm90dG9tIHtcclxuICBwYWRkaW5nLXRvcDogNXB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5wYW5lbC10b3AtbGVmdCB7XHJcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMTBweDtcclxuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAwcHg7XHJcbn1cclxuXHJcbi5vdXRzaWRlLWNvbHVtbiB7XHJcbiAgd2lkdGg6IDgwcHg7XHJcbn1cclxuXHJcbi5wYW5lbC10b3AtcmlnaHQge1xyXG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAwcHg7XHJcbn1cclxuXHJcbi5wYW5lbC1ib3R0b20tbGVmdCB7XHJcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwcHg7XHJcbn1cclxuXHJcbi5wYW5lbC1ib3R0b20tcmlnaHQge1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwcHg7XHJcbn1cclxuXHJcbi5idG4tY2VudGVyIHtcclxuICBtYXJnaW46IGF1dG87XHJcbn1cclxuXHJcbi5idG4tdG9wLWxlZnQsIC5idG4tYm90dG9tLWxlZnQge1xyXG4gIG1hcmdpbi1yaWdodDogMjAlO1xyXG4gIGZsb2F0OiByaWdodDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5idG4tdG9wLXJpZ2h0LCAuYnRuLWJvdHRvbS1yaWdodCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwJTtcclxufVxyXG5cclxuLnBhbmVsLXRvcC1jZW50ZXIsIC5wYW5lbC1ib3R0b20tY2VudGVyIHtcclxuICAgIHdpZHRoOiAyMyU7XHJcbn1cclxuXHJcbi5yb3ctY2VudGVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG5cclxuLnBhbmVsLWRpc3RhbmNlIHtcclxuICBoZWlnaHQ6IDIwcHg7XHJcbn1cclxuXHJcbi5wYW5lbC1kaXN0YW5jZS1jZW50ZXItYmFja2dyb3VuZCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFiNjk4ZTtcclxuICB3aWR0aDogNjklO1xyXG59XHJcblxyXG4ucGFuZWwtZGlzdGFuY2UtY2VudGVyLXRvcCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiBpbmhlcml0O1xyXG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMHB4O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA2MDYwNjtcclxufVxyXG5cclxuLnBhbmVsLWRpc3RhbmNlLWNlbnRlci1ib3R0b20ge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogaW5oZXJpdDtcclxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTBweDtcclxuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwcHg7XHJcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwNjA2MDY7XHJcbn1cclxuXHJcbi5wYW5lbC1taWRkbGUtZGlzdGFuY2Uge1xyXG4gIHdpZHRoOiA2JTtcclxufVxyXG5cclxuLnBhbmVsLW1pZGRsZS1jZW50ZXItbWFyZ2luIHtcclxuICB3aWR0aDogNSU7XHJcbn1cclxuXHJcbi5wYW5lbC1taWRkbGUtY2VudGVyIHtcclxuICBkaXNwbGF5OiBncmlkO1xyXG4gIHdpZHRoOiA1NyU7XHJcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMTBweDtcclxuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMTBweDtcclxuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG4ucGFuZWwtbWlkZGxlLWxlZnQge1xyXG4gIHdpZHRoOiA4MHB4O1xyXG4gIHBhZGRpbmctbGVmdDogOXB4O1xyXG59XHJcblxyXG4ucGFuZWwtbWlkZGxlLXJpZ2h0IHtcclxuICBwYWRkaW5nLWxlZnQ6IDlweDtcclxuICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuXHJcbi5wYW5lbC1taWRkbGUtc2lkZS11cHBlciB7XHJcbiAgcGFkZGluZy10b3A6IDhweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTdweDtcclxufVxyXG5cclxuLnBhbmVsLW1pZGRsZS1zaWRlLWRvd25lciB7XHJcbiAgcGFkZGluZy10b3A6IDE3cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDhweDtcclxufVxyXG5cclxuLmJ0bi10b3AtY2VudGVyLCAuYnRuLWJvdHRvbS1jZW50ZXIge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnBhbmVsLW1pZGRsZS1jZW50ZXItY29sIHtcclxuICBoZWlnaHQ6IDE0NXB4O1xyXG4gIGRpc3BsYXk6IGdyaWQ7XHJcbiAgd2lkdGg6IDE4JTtcclxufVxyXG5cclxuLnBhbmVsLW1pZGRsZS1jZW50ZXItaW50ZXJuYWwge1xyXG4gIG1hcmdpbi10b3A6IGF1dG87XHJcbiAgbWFyZ2luLWJvdHRvbTogYXV0bztcclxufVxyXG5cclxuLnBhbmVsLW1pZGRsZS1jZW50ZXItYnRuIHtcclxuICBtYXJnaW46IGF1dG87XHJcbn1cclxuXHJcblxyXG4uYnV0dG9uLXdpZHRoIHtcclxuICB3aWR0aDogMTMwcHg7XHJcbn1cclxuXHJcbi5sZWZ0LWJ1dHRvbiB7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcbi5yaWdodC1idXR0b24ge1xyXG4gIGZsb2F0OiByaWdodDtcclxufVxyXG5cclxuXHJcbi5idXR0b24tcGFuZWwge1xyXG4gIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG59XHJcblxyXG4uY3VzdG9tLWJ0biB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5jdXN0b20tYnRuLXByaW1hcnk6aG92ZXJ7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJhOWZkNjtcclxuICBib3JkZXItY29sb3I6ICMyYTlmZDY7XHJcbn1cclxuXHJcbi5jdXN0b20tYnRuLXByaW1hcnk6YWN0aXZle1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMxYjY5OGU7XHJcbiAgYm9yZGVyLWNvbG9yOiAjMTU1MDZjO1xyXG59XHJcblxyXG4uYnRuLWluY3JlYXNlIHtcclxuICB3aWR0aDogMTAlO1xyXG4gIGRpc3BsYXk6IHRhYmxlO1xyXG4gIG1hcmdpbjogYXV0bztcclxuICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbn1cclxuXHJcbi5idG4tZGVjcmVhc2Uge1xyXG4gIHdpZHRoOiAxMCU7XHJcbiAgZGlzcGxheTogdGFibGU7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDVweDtcclxufVxyXG5cclxuLyouaWNvbl9iZyB7Ki9cclxuICAvKnBvc2l0aW9uOiBhYnNvbHV0ZTsqL1xyXG4vKn0qL1xyXG5cclxuLmlucHV0LWJveCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG59XHJcblxyXG4ubGlnaHQtYmcge1xyXG4gIHotaW5kZXg6IDE7XHJcbiAgd2lkdGg6IDYwcHg7XHJcbiAgaGVpZ2h0OiA2MHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG59XHJcblxyXG4ubGlnaHQtaWNvbiB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2ltZy9saWdodF9pY29uMi5wbmdcIik7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5tb2RhbC1ib2R5LWN1c3RvbSB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4ubW9kYWwtaW5wdXQtc2VjdGlvbiB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcblxyXG4ubW9kYWwtZm9vdGVyLWN1c3RvbSB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGRpc3BsYXk6IGluaGVyaXQ7XHJcbn1cclxuXHJcbi5idXR0b24tY2VudGVyIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zZXQtbmFtZS1pbnB1dCB7XHJcbiAgZGlzcGxheTogaW5saW5lO1xyXG4gIHdpZHRoOiAyMDBweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/living-room-lights/living-room-lights.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/living-room-lights/living-room-lights.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid main-component\">\n  <div class=\"row row-center\" id=\"panel-top\">\n\n    <div class=\"panel-color panel-top-left outside-column panel-top\">\n      <div class=\"light-bg btn-center\">\n        <div id=\"salon_zew_01\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_01')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n        </div>\n      </div>\n    </div>\n    <div class=\"panel-top-center  panel-color panel-top\">\n      <div class=\"light-bg btn-top-left\">\n        <div id=\"salon_zew_02\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_02')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n        </div>\n      </div>\n    </div>\n    <div class=\"panel-top-center  panel-color panel-top\">\n      <div class=\"light-bg btn-center\">\n        <div id=\"salon_zew_03\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_03')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n        </div>\n      </div>\n    </div>\n    <div class=\"panel-top-center  panel-color panel-top\">\n      <div class=\"light-bg btn-top-right\">\n        <div id=\"salon_zew_04\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_04')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n        </div>\n      </div>\n    </div>\n    <div class=\"panel-color panel-top-right outside-column panel-top\">\n      <div class=\"light-bg btn-center\">\n        <div id=\"salon_zew_05\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_05')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n        </div>\n      </div>\n    </div>\n\n  </div>\n  <div class=\"row panel-distance row-center\" id=\"panel-distance\">\n    <div class=\"outside-column panel-color\" id=\"panel-distance-left\">\n    </div>\n    <div class=\"panel-distance-center-background\">\n      <div class=\"panel-distance-center-top\" id=\"panel-distance-center\"><br/>\n      </div>\n    </div>\n    <div class=\"outside-column panel-color\" id=\"panel-distance-right\">\n    </div>\n  </div>\n  <div class=\"row row-center\" id=\"panel-middle\">\n\n    <div class=\"panel-color outside-column panel-middle-left\" id=\"panel-middle-left\">\n      <div class=\"panel-middle-side-upper\">\n        <div class=\"light-bg\">\n          <div id=\"salon_zew_14\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_14')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n          </div>\n        </div>\n      </div>\n      <div class=\"panel-middle-side-downer\">\n        <div class=\"light-bg\">\n          <div id=\"salon_zew_13\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_13')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"panel-middle-distance\" id=\"panel-middle-distance\">\n    </div>\n    <div class=\"panel-color panel-middle-center\" id=\"panel-middle-center\">\n      <div class=\"row panel-middle-center-internal\" id=\"panel-middle-center-internal\">\n        <div class=\"panel-middle-center-margin\"></div>\n        <div class=\"panel-middle-center-col\">\n          <div class=\"light-bg panel-middle-center-btn\">\n            <div id=\"salon_wew_01\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_wew_01')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n            </div>\n          </div>\n          <div class=\"light-bg panel-middle-center-btn\">\n            <div id=\"salon_wew_02\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_wew_02')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n            </div>\n          </div>\n        </div>\n        <div class=\"panel-middle-center-col\">\n          <div class=\"light-bg panel-middle-center-btn\">\n            <div id=\"salon_wew_03\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_wew_03')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n            </div>\n          </div>\n        </div>\n        <div class=\"panel-middle-center-col\">\n          <div class=\"light-bg panel-middle-center-btn\">\n            <div id=\"salon_wew_04\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_wew_04')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n            </div>\n          </div>\n          <div class=\"light-bg panel-middle-center-btn\">\n            <div id=\"salon_wew_05\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_wew_05')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n            </div>\n          </div>\n        </div>\n        <div class=\"panel-middle-center-col\">\n          <div class=\"light-bg panel-middle-center-btn\">\n            <div id=\"salon_wew_06\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_wew_06')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n            </div>\n          </div>\n        </div>\n        <div class=\"panel-middle-center-col\">\n          <div class=\"light-bg panel-middle-center-btn\">\n            <div id=\"salon_wew_07\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_wew_07')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n            </div>\n          </div>\n          <div class=\"light-bg panel-middle-center-btn\">\n            <div id=\"salon_wew_08\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_wew_08')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n            </div>\n          </div>\n        </div>\n        <div class=\"panel-middle-center-margin\"></div>\n      </div>\n\n    </div>\n    <div class=\"panel-middle-distance\" id=\"panel-middle-distance2\">\n    </div>\n    <div class=\"panel-color outside-column panel-middle-right\" id=\"panel-middle-right\">\n      <div class=\"panel-middle-side-upper\">\n        <div class=\"light-bg\">\n          <div id=\"salon_zew_06\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_06')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n          </div>\n        </div>\n      </div>\n      <div class=\"panel-middle-side-downer\">\n        <div class=\"light-bg\">\n          <div id=\"salon_zew_07\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_07')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </div>\n  <div class=\"row panel-distance row-center\" id=\"panel-distance\">\n    <div class=\"outside-column panel-color\" id=\"panel-distance-left\">\n    </div>\n    <div class=\"panel-distance-center-background\">\n      <div class=\"panel-distance-center-bottom\" id=\"panel-distance-center\"><br/>\n      </div>\n    </div>\n    <div class=\"outside-column panel-color\" id=\"panel-distance-right\">\n    </div>\n  </div>\n\n  <div class=\"row row-center\" id=\"panel-bottom\">\n    <div class=\"panel-color panel-bottom-left outside-column panel-bottom\">\n      <div class=\"light-bg btn-center\">\n        <div id=\"salon_zew_12\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_12')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n        </div>\n      </div>\n    </div>\n    <div class=\"panel-bottom-center  panel-color panel-bottom\">\n      <div class=\"light-bg btn-bottom-left\">\n        <div id=\"salon_zew_11\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_11')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n        </div>\n      </div>\n    </div>\n    <div class=\"panel-bottom-center panel-color panel-bottom\">\n      <div class=\"light-bg btn-center\">\n        <div id=\"salon_zew_10\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_10')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n        </div>\n      </div>\n    </div>\n    <div class=\"panel-bottom-center panel-color panel-bottom\">\n      <div class=\"light-bg btn-bottom-right\">\n        <div id=\"salon_zew_09\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_09')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n        </div>\n      </div>\n    </div>\n    <div class=\"panel-bottom-right outside-column panel-color panel-bottom\">\n      <div class=\"light-bg btn-center\">\n        <div id=\"salon_zew_08\" class=\"light-icon\"  [style.opacity]=\"getStyle('salon_zew_08')\" (click)=\"switch($event)\"  long-press  (onLongPress)=\"showPopup($event)\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n\n  <div class=\"row button-panel\" id=\"button-panel\">\n    <div class=\"button-center col-lg-3 col-xs-3 col-sm-3\">\n      <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn button-width\" onclick=\"this.blur();\" [routerLink]=\"['/livingroom']\" (click)=\"cancel()\">ANULUJ</button>\n    </div>\n    <div class=\"button-center col-lg-6 col-xs-6 col-sm-6\" >\n      <input type=\"text\" *ngIf=\"showSetNameInput()\" [(ngModel)]=\"lightSetName\" class=\"form-control set-name-input\" id=\"inputSetName\">\n    </div>\n    <div class=\"button-center col-lg-3 col-xs-3 col-sm-3\">\n      <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn button-width\" onclick=\"this.blur();\"  [routerLink]=\"['/livingroom']\" (click)=\"save()\">ZAPISZ</button>\n    </div>\n  </div>\n</div>\n\n<!--<popup (confirmClick)=\"onSavePopup()\" (cancelClick)=\"onCancelPopup()\">-->\n  <!--Wprowadź wartość (0-255):-->\n  <!--<button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn btn-increase\" onclick=\"this.blur();\"  (click)=\"onIncreaseDutyCycle()\">+</button>-->\n  <!--<input type=\"text\" class=\"form-control input-box\" (input)=\"onPopupValueChange()\"  id=\"light2\" [(ngModel)]=\"popupDutyCycle\">-->\n  <!--<button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn btn-decrease\" onclick=\"this.blur();\" (click)=\"onDecreaseDutyCycle()\">-</button>-->\n\n  <!--<nouislider [connect]=\"true\" [min]=\"0\" [max]=\"50\" [(ngModel)]=\"someRange\"></nouislider>-->\n\n<!--</popup>-->\n"

/***/ }),

/***/ "./src/app/components/living-room-lights/living-room-lights.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/living-room-lights/living-room-lights.component.ts ***!
  \*******************************************************************************/
/*! exports provided: LivingRoomLightsComponent, LightAdjustComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivingRoomLightsComponent", function() { return LivingRoomLightsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LightAdjustComponent", function() { return LightAdjustComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var _services_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/config.service */ "./src/app/services/config.service.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utils/deviceConsts */ "./src/app/utils/deviceConsts.ts");
/* harmony import */ var _utils_configConsts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../utils/configConsts */ "./src/app/utils/configConsts.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/index.js");
/* harmony import */ var ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/modal/bs-modal-ref.service */ "./node_modules/ngx-bootstrap/modal/bs-modal-ref.service.js");










var LivingRoomLightsComponent = /** @class */ (function () {
    // someRange=[1, 50];
    function LivingRoomLightsComponent(restService, deviceManager, configService, route, modalService) {
        this.restService = restService;
        this.deviceManager = deviceManager;
        this.configService = configService;
        this.route = route;
        this.modalService = modalService;
        this.currentDevices = [
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_01,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_02,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_03,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_04,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_05,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_06,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_07,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_08,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_01,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_02,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_03,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_04,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_05,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_06,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_07,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_08,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_09,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_10,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_11,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_12,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_13,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_14
        ];
    }
    LivingRoomLightsComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('>>> onInit');
        this.route.params.subscribe(function (params) {
            _this.lightSet = params['id'];
            console.log('>>>> init set: ' + _this.lightSet);
        });
        this.loadSetName();
        this.deviceManager.loadDevices(function () { _this.loadDevices(); });
        this.showName = (this.lightSet != _utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_MANUAL);
    };
    LivingRoomLightsComponent.prototype.showSetNameInput = function () {
        return this.lightSet != _utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_MANUAL;
    };
    LivingRoomLightsComponent.prototype.showPopup = function (event) {
        this.popupDevKey = event['target'].attributes['id'].value;
        this.popupDutyCycle = this.devicesMap[this.popupDevKey].dutyCycle;
        this.bsModalRef = this.modalService.show(LightAdjustComponent);
        this.bsModalRef.content.closeBtnName = 'Close';
        this.bsModalRef.content.parentController = this;
    };
    LivingRoomLightsComponent.prototype.onPopupValueChange = function () {
        console.log('DutyCycle in popup: ' + this.popupDutyCycle);
        this.setDeviceParameter(this.devicesMap[this.popupDevKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, this.popupDutyCycle);
    };
    LivingRoomLightsComponent.prototype.onSavePopup = function () {
        this.devicesMap[this.popupDevKey].dutyCycle = this.popupDutyCycle;
    };
    LivingRoomLightsComponent.prototype.onCancelPopup = function () {
        this.setDeviceParameter(this.devicesMap[this.popupDevKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, this.devicesMap[this.popupDevKey].dutyCycle);
    };
    LivingRoomLightsComponent.prototype.onIncreaseDutyCycle = function () {
        this.popupDutyCycle += 5;
        if (this.popupDutyCycle > 255) {
            this.popupDutyCycle = 255;
        }
        this.setDeviceParameter(this.devicesMap[this.popupDevKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, this.popupDutyCycle);
    };
    LivingRoomLightsComponent.prototype.onDecreaseDutyCycle = function () {
        if (this.popupDutyCycle > 4) {
            this.popupDutyCycle -= 5;
        }
        else {
            this.popupDutyCycle = 0;
        }
        this.setDeviceParameter(this.devicesMap[this.popupDevKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, this.popupDutyCycle);
    };
    LivingRoomLightsComponent.prototype.save = function () {
        var _this = this;
        if (this.lightSet != _utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_MANUAL) {
            for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
                var devKey = _a[_i];
                this.configService.addDeviceConfig(this.lightSet, this.devicesMap[devKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, this.devicesMap[devKey].dutyCycle).subscribe(function (data) {
                    console.log('Saving config ' + _this.lightSet + ', device ' + _this.devicesMap[devKey].device + ' duty cycle = ' + _this.devicesMap[devKey].dutyCycle);
                });
            }
            this.saveSetName();
        }
    };
    LivingRoomLightsComponent.prototype.cancel = function () {
        console.log('>>> CANCEL');
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            this.setDeviceParameter(this.devicesMap[devKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, this.devicesMap[devKey].initDutyCycle);
        }
    };
    LivingRoomLightsComponent.prototype.loadSetName = function () {
        var _this = this;
        this.configService.getBasicConfig(this.lightSet).subscribe(function (data) {
            if (data.success) {
                _this.lightSetName = data.config.value;
            }
            else {
                console.log('GetConfig error: ', data.msg);
            }
        });
    };
    LivingRoomLightsComponent.prototype.saveSetName = function () {
        var _this = this;
        this.configService.addBasicConfig(this.lightSet, this.lightSetName).subscribe(function (data) {
            if (data.success) {
                console.log('LighSetName saved: ', _this.lightSetName);
            }
            else {
                console.log('Saving lightSetName error: ', data.msg);
            }
        });
    };
    LivingRoomLightsComponent.prototype.loadDevices = function () {
        var _this = this;
        console.log('Loading devices config');
        this.configService.getDevices(this.currentDevices).subscribe(function (data) {
            console.log('data: ', data);
            if (data.success) {
                console.log('GetDevices success, devices list: ', data.devices);
                _this.devicesMap = new Object();
                for (var _i = 0, _a = data.devices; _i < _a.length; _i++) {
                    var dev = _a[_i];
                    console.log('dev:', dev);
                    _this.devicesMap[dev.key] = new Object();
                    _this.devicesMap[dev.key].device = _this.deviceManager.getDevice(dev.category, dev.address);
                    _this.devicesMap[dev.key].dutyCycle = 0;
                    _this.devicesMap[dev.key].initDutyCycle = 0;
                }
                if (_this.lightSet == _utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_MANUAL) {
                    _this.loadDevicesParameters();
                }
                else {
                    _this.loadDevicesParametersFromConfig(_this.lightSet);
                }
                console.log('>>>> devices map: ', _this.devicesMap);
            }
            else {
                console.log('GetDevices error: ', data.msg);
            }
        });
    };
    LivingRoomLightsComponent.prototype.loadDevicesParameters = function () {
        var _this = this;
        console.log('Loading devices parameters');
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            this.loadDeviceParameter(devKey, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, function (key, val) {
                _this.devicesMap[key].dutyCycle = val;
                _this.devicesMap[key].initDutyCycle = val;
            });
            console.log('Device ' + devKey + ' has duty cycle = ' + this.devicesMap[devKey].dutyCycle);
        }
    };
    LivingRoomLightsComponent.prototype.loadDevicesParametersFromConfig = function (set) {
        var _this = this;
        console.log('Loading devices parameters from config: ' + set);
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            this.loadDeviceParameter(devKey, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, function (key, val) {
                _this.devicesMap[key].initDutyCycle = val;
            });
            console.log('Device ' + devKey + ' has duty cycle = ' + this.devicesMap[devKey].dutyCycle);
        }
        this.configService.getDevicesConfigs(this.lightSet).subscribe(function (data) {
            console.log('data: ', data);
            if (data.success) {
                console.log('config: ', data.configs);
                for (var _i = 0, _a = data.configs; _i < _a.length; _i++) {
                    var config = _a[_i];
                    var key = _this.getDeviceKey(config.address, config.category);
                    _this.devicesMap[key].dutyCycle = config.value;
                    console.log('Device ' + devKey + ' has duty cycle in config = ' + _this.devicesMap[key].dutyCycle);
                }
                console.log('Setting devices parameter');
                for (var _b = 0, _c = _this.currentDevices; _b < _c.length; _b++) {
                    var devKey = _c[_b];
                    _this.setDeviceParameter(_this.devicesMap[devKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, _this.devicesMap[devKey].dutyCycle);
                }
            }
            else {
                console.log('GetDevicesConfigs error: ', data.msg);
            }
        });
    };
    LivingRoomLightsComponent.prototype.getDeviceKey = function (address, category) {
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            if (this.devicesMap[devKey].device.address == address && this.devicesMap[devKey].device.category == category) {
                return devKey;
            }
        }
    };
    LivingRoomLightsComponent.prototype.loadDeviceParameter = function (deviceKey, param, callback) {
        this.restService.getDeviceParameter(this.devicesMap[deviceKey].device, param).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Loading device parameters failed: ' + data.result);
            }
            else {
                callback(deviceKey, data.value);
            }
        });
    };
    LivingRoomLightsComponent.prototype.switch = function (event) {
        var id = event['target']['attributes']['id'].value;
        var newDutyCycle;
        if (this.devicesMap[id].dutyCycle > 0) {
            newDutyCycle = 0;
        }
        else {
            newDutyCycle = 255;
        }
        this.devicesMap[id].dutyCycle = newDutyCycle;
        this.setDeviceParameter(this.devicesMap[id].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, newDutyCycle);
    };
    LivingRoomLightsComponent.prototype.getStyle = function (id) {
        if (this.devicesMap && this.devicesMap[id]) {
            return this.devicesMap[id].dutyCycle / 255;
        }
        else {
            return 1;
        }
    };
    LivingRoomLightsComponent.prototype.setDeviceParameter = function (dev, param, val) {
        this.restService.setDeviceParameter(dev, param, val).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Setting device parameter failed: ' + data.result);
            }
            else {
                console.log('Device parameter set successfully!');
            }
        });
    };
    LivingRoomLightsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-living-room-lights',
            template: __webpack_require__(/*! ./living-room-lights.component.html */ "./src/app/components/living-room-lights/living-room-lights.component.html"),
            styles: [__webpack_require__(/*! ./living-room-lights.component.css */ "./src/app/components/living-room-lights/living-room-lights.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__["DeviceManagerService"],
            _services_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["BsModalService"]])
    ], LivingRoomLightsComponent);
    return LivingRoomLightsComponent;
}());

var LightAdjustComponent = /** @class */ (function () {
    function LightAdjustComponent(bsModalRef) {
        this.bsModalRef = bsModalRef;
    }
    LightAdjustComponent.prototype.ngOnInit = function () {
    };
    LightAdjustComponent.prototype.onIncreaseDutyCycle = function () {
        this.parentController.onIncreaseDutyCycle();
    };
    LightAdjustComponent.prototype.onDecreaseDutyCycle = function () {
        this.parentController.onDecreaseDutyCycle();
    };
    LightAdjustComponent.prototype.onPopupValueChange = function () {
        this.parentController.onPopupValueChange();
    };
    LightAdjustComponent.prototype.onSavePopup = function () {
        this.parentController.onSavePopup();
        this.bsModalRef.hide();
    };
    LightAdjustComponent.prototype.onCancelPopup = function () {
        this.parentController.onCancelPopup();
        this.bsModalRef.hide();
    };
    LightAdjustComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'modal-content',
            template: "\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title pull-left\">Dostosuj jasno\u015B\u0107</h4>\n    </div>\n    <div class=\"modal-body modal-body-custom\">\n    <div>\n        Wprowad\u017A warto\u015B\u0107 (0-255):\n    </div>\n    <div modal-input-section>\n         <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn btn-increase\" onclick=\"this.blur();\"  (click)=\"onIncreaseDutyCycle()\">+</button>\n        <input *ngIf=\"parentController !== undefined\" type=\"text\" class=\"form-control input-box\" (input)=\"onPopupValueChange()\"  id=\"light2\" [(ngModel)]=\"parentController.popupDutyCycle\">\n        <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn btn-decrease\" onclick=\"this.blur();\" (click)=\"onDecreaseDutyCycle()\">-</button>\n    </div>\n      \n  <!--<nouislider [connect]=\"true\" [min]=\"0\" [max]=\"50\" [(ngModel)]=\"someRange\"></nouislider>-->\n\n    </div>\n    <div class=\"modal-footer modal-footer-custom\">\n      <button type=\"button\" class=\"btn btn-default\" (click)=\"onSavePopup()\">ZAPISZ</button>\n      <button type=\"button\" class=\"btn btn-default\" (click)=\"onCancelPopup()\">ANULUJ</button>\n    </div>\n  ",
            styles: [__webpack_require__(/*! ./living-room-lights.component.css */ "./src/app/components/living-room-lights/living-room-lights.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_9__["BsModalRef"]])
    ], LightAdjustComponent);
    return LightAdjustComponent;
}());



/***/ }),

/***/ "./src/app/components/living-room-wall-effect/living-room-wall-effect.component.css":
/*!******************************************************************************************!*\
  !*** ./src/app/components/living-room-wall-effect/living-room-wall-effect.component.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".window-frame {\r\n  background-color: darkblue;\r\n}\r\n\r\n.frame-top {\r\n  height: 20px;\r\n  display: flex;\r\n  flex-flow: row wrap;\r\n  font-size: 0;\r\n  line-height: 0px;\r\n  justify-content: flex-start;\r\n}\r\n\r\n.frame-middle {\r\n  height: 20px;\r\n  display: flex;\r\n  flex-flow: row wrap;\r\n  /*justify-content: space-around;*/\r\n}\r\n\r\n.frame-bottom {\r\n  height: 20px;\r\n  display: flex;\r\n  flex-flow: row wrap;\r\n  /*justify-content: space-around;*/\r\n}\r\n\r\n.window-level {\r\n  height: 22vh;\r\n  display: flex;\r\n  flex-flow: row wrap;\r\n  /*justify-content: space-around;*/\r\n}\r\n\r\n.window-style {\r\n  height: 100px;\r\n  margin-left: 50px;\r\n  margin-right: 50px;\r\n  margin-top: 15px;\r\n  margin-bottom: 15px;\r\n  border-radius: 10px;\r\n  border-style: inset;\r\n}\r\n\r\n.frame-upper-left {\r\n  height: 100%;\r\n  width: 20px;\r\n  border-top-left-radius: 20px;\r\n  outline: none;\r\n}\r\n\r\n.frame-top-center {\r\n  height: 100%;\r\n  flex: 1;\r\n  outline: none;\r\n}\r\n\r\n.frame-top-right {\r\n  height: 100%;\r\n  width: 20px;\r\n  border-top-right-radius: 20px;\r\n  outline: none;\r\n}\r\n\r\n.frame-left {\r\n  width: 20px;\r\n}\r\n\r\n.frame-rigth {\r\n  width: 20px;\r\n}\r\n\r\n.frame-middle-left {\r\n  width: 20px;\r\n}\r\n\r\n.frame-middle-center {\r\n  flex: 1;\r\n}\r\n\r\n.frame-middle-right {\r\n  width: 20px;\r\n}\r\n\r\n.frame-bottom-left {\r\n  width: 20px;\r\n  border-bottom-left-radius: 20px;\r\n\r\n}\r\n\r\n.frame-bottom-center {\r\n  flex: 1;\r\n}\r\n\r\n.frame-bottom-right {\r\n  width: 20px;\r\n  border-bottom-right-radius: 20px;\r\n}\r\n\r\n.windows-style {\r\n  background-color: #1b698e;\r\n  display: grid;\r\n  border-radius: 10px;\r\n}\r\n\r\n.custom-btn-primary:hover{\r\n  color: #ffffff;\r\n  background-color: #2a9fd6;\r\n  border-color: #2a9fd6;\r\n}\r\n\r\n.custom-btn-primary:active{\r\n  color: #ffffff;\r\n  background-color: #1b698e;\r\n  border-color: #15506c;\r\n}\r\n\r\n.button-style {\r\n   width: 80%;\r\n   margin-left: auto;\r\n   margin-right: auto;\r\n   margin-bottom: 10px;\r\n   margin-top: 10px;\r\n   height: 80px;\r\n   display: block;\r\n }\r\n\r\n.button-effect-style {\r\n  width: 80%;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-bottom: 10px;\r\n  margin-top: 10px;\r\n  height: 60px;\r\n  display: block;\r\n}\r\n\r\n.btn-back {\r\n  width: 80%;\r\n  height: 40px;\r\n  display: block;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n}\r\n\r\n.btn-speed {\r\n  width: 30%;\r\n  margin-bottom: 20px;\r\n  margin-top: 10px;\r\n  margin-left: 10px;\r\n  margin-right: 10px;\r\n}\r\n\r\n.speed-section {\r\n  text-align: center;\r\n}\r\n\r\n.main-panel {\r\n  max-width: 1200px;\r\n  margin: auto;\r\n}\r\n\r\n.img-div-container {\r\n\r\n  position: relative;\r\n  text-align: center;\r\n}\r\n\r\n.img-style {\r\n  position: absolute;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  margin: auto;\r\n  width: 25px;\r\n  height: 25px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9saXZpbmctcm9vbS13YWxsLWVmZmVjdC9saXZpbmctcm9vbS13YWxsLWVmZmVjdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsMEJBQTBCO0FBQzVCOztBQUVBO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQiwyQkFBMkI7QUFDN0I7O0FBRUE7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixpQ0FBaUM7QUFDbkM7O0FBRUE7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixpQ0FBaUM7QUFDbkM7O0FBRUE7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixpQ0FBaUM7QUFDbkM7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxZQUFZO0VBQ1osV0FBVztFQUNYLDRCQUE0QjtFQUM1QixhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxZQUFZO0VBQ1osT0FBTztFQUNQLGFBQWE7QUFDZjs7QUFFQTtFQUNFLFlBQVk7RUFDWixXQUFXO0VBQ1gsNkJBQTZCO0VBQzdCLGFBQWE7QUFDZjs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtFQUNFLE9BQU87QUFDVDs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtFQUNFLFdBQVc7RUFDWCwrQkFBK0I7O0FBRWpDOztBQUVBO0VBQ0UsT0FBTztBQUNUOztBQUVBO0VBQ0UsV0FBVztFQUNYLGdDQUFnQztBQUNsQzs7QUFFQTtFQUNFLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2IsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLHFCQUFxQjtBQUN2Qjs7QUFFQTtHQUNHLFVBQVU7R0FDVixpQkFBaUI7R0FDakIsa0JBQWtCO0dBQ2xCLG1CQUFtQjtHQUNuQixnQkFBZ0I7R0FDaEIsWUFBWTtHQUNaLGNBQWM7Q0FDaEI7O0FBRUQ7RUFDRSxVQUFVO0VBQ1YsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLFVBQVU7RUFDVixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBR0E7RUFDRSxpQkFBaUI7RUFDakIsWUFBWTtBQUNkOztBQUVBOztFQUVFLGtCQUFrQjtFQUNsQixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLFNBQVM7RUFDVCxPQUFPO0VBQ1AsUUFBUTtFQUNSLFlBQVk7RUFDWixXQUFXO0VBQ1gsWUFBWTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9saXZpbmctcm9vbS13YWxsLWVmZmVjdC9saXZpbmctcm9vbS13YWxsLWVmZmVjdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndpbmRvdy1mcmFtZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogZGFya2JsdWU7XHJcbn1cclxuXHJcbi5mcmFtZS10b3Age1xyXG4gIGhlaWdodDogMjBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogcm93IHdyYXA7XHJcbiAgZm9udC1zaXplOiAwO1xyXG4gIGxpbmUtaGVpZ2h0OiAwcHg7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG59XHJcblxyXG4uZnJhbWUtbWlkZGxlIHtcclxuICBoZWlnaHQ6IDIwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWZsb3c6IHJvdyB3cmFwO1xyXG4gIC8qanVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7Ki9cclxufVxyXG5cclxuLmZyYW1lLWJvdHRvbSB7XHJcbiAgaGVpZ2h0OiAyMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1mbG93OiByb3cgd3JhcDtcclxuICAvKmp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kOyovXHJcbn1cclxuXHJcbi53aW5kb3ctbGV2ZWwge1xyXG4gIGhlaWdodDogMjJ2aDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogcm93IHdyYXA7XHJcbiAgLypqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDsqL1xyXG59XHJcblxyXG4ud2luZG93LXN0eWxlIHtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xyXG4gIG1hcmdpbi1yaWdodDogNTBweDtcclxuICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBib3JkZXItc3R5bGU6IGluc2V0O1xyXG59XHJcblxyXG4uZnJhbWUtdXBwZXItbGVmdCB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHdpZHRoOiAyMHB4O1xyXG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDIwcHg7XHJcbiAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLmZyYW1lLXRvcC1jZW50ZXIge1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBmbGV4OiAxO1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbn1cclxuXHJcbi5mcmFtZS10b3AtcmlnaHQge1xyXG4gIGhlaWdodDogMTAwJTtcclxuICB3aWR0aDogMjBweDtcclxuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMjBweDtcclxuICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG4uZnJhbWUtbGVmdCB7XHJcbiAgd2lkdGg6IDIwcHg7XHJcbn1cclxuXHJcbi5mcmFtZS1yaWd0aCB7XHJcbiAgd2lkdGg6IDIwcHg7XHJcbn1cclxuXHJcbi5mcmFtZS1taWRkbGUtbGVmdCB7XHJcbiAgd2lkdGg6IDIwcHg7XHJcbn1cclxuXHJcbi5mcmFtZS1taWRkbGUtY2VudGVyIHtcclxuICBmbGV4OiAxO1xyXG59XHJcblxyXG4uZnJhbWUtbWlkZGxlLXJpZ2h0IHtcclxuICB3aWR0aDogMjBweDtcclxufVxyXG5cclxuLmZyYW1lLWJvdHRvbS1sZWZ0IHtcclxuICB3aWR0aDogMjBweDtcclxuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAyMHB4O1xyXG5cclxufVxyXG5cclxuLmZyYW1lLWJvdHRvbS1jZW50ZXIge1xyXG4gIGZsZXg6IDE7XHJcbn1cclxuXHJcbi5mcmFtZS1ib3R0b20tcmlnaHQge1xyXG4gIHdpZHRoOiAyMHB4O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAyMHB4O1xyXG59XHJcblxyXG4ud2luZG93cy1zdHlsZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFiNjk4ZTtcclxuICBkaXNwbGF5OiBncmlkO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcbi5jdXN0b20tYnRuLXByaW1hcnk6aG92ZXJ7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJhOWZkNjtcclxuICBib3JkZXItY29sb3I6ICMyYTlmZDY7XHJcbn1cclxuXHJcbi5jdXN0b20tYnRuLXByaW1hcnk6YWN0aXZle1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMxYjY5OGU7XHJcbiAgYm9yZGVyLWNvbG9yOiAjMTU1MDZjO1xyXG59XHJcblxyXG4uYnV0dG9uLXN0eWxlIHtcclxuICAgd2lkdGg6IDgwJTtcclxuICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgaGVpZ2h0OiA4MHB4O1xyXG4gICBkaXNwbGF5OiBibG9jaztcclxuIH1cclxuXHJcbi5idXR0b24tZWZmZWN0LXN0eWxlIHtcclxuICB3aWR0aDogODAlO1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgaGVpZ2h0OiA2MHB4O1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uYnRuLWJhY2sge1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgaGVpZ2h0OiA0MHB4O1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxufVxyXG5cclxuLmJ0bi1zcGVlZCB7XHJcbiAgd2lkdGg6IDMwJTtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG4uc3BlZWQtc2VjdGlvbiB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5cclxuLm1haW4tcGFuZWwge1xyXG4gIG1heC13aWR0aDogMTIwMHB4O1xyXG4gIG1hcmdpbjogYXV0bztcclxufVxyXG5cclxuLmltZy1kaXYtY29udGFpbmVyIHtcclxuXHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLmltZy1zdHlsZSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICBib3R0b206IDA7XHJcbiAgbGVmdDogMDtcclxuICByaWdodDogMDtcclxuICBtYXJnaW46IGF1dG87XHJcbiAgd2lkdGg6IDI1cHg7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/living-room-wall-effect/living-room-wall-effect.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/components/living-room-wall-effect/living-room-wall-effect.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col-md-6 col-xs-6\" >\n      <div class=\"row main-panel\">\n        <div class=\"col-md-6 col-xs-6\">\n          <button id=\"livingroomwalleffectset1\" type=\"button\" class=\"btn btn-primary custom-btn-primary button-style\" onclick=\"this.blur();\" (click)=\"loadEffectConfig($event)\" long-press  (onLongPress)=\"saveEffectConfig($event)\">SET1</button>\n          <button id=\"livingroomwalleffectset2\" type=\"button\" class=\"btn btn-primary custom-btn-primary button-style\" onclick=\"this.blur();\" (click)=\"loadEffectConfig($event)\" long-press  (onLongPress)=\"saveEffectConfig($event)\">SET2</button>\n          <button id=\"livingroomwalleffectset3\" type=\"button\" class=\"btn btn-primary custom-btn-primary button-style\" onclick=\"this.blur();\" (click)=\"loadEffectConfig($event)\" long-press  (onLongPress)=\"saveEffectConfig($event)\">SET3</button>\n          <button *ngIf=\"!isManualModeOn()\" id=\"livingroomwalleffectmanual\" type=\"button\" class=\"btn btn-primary custom-btn-primary button-style\" onclick=\"this.blur();\" (click)=\"setManualMode()\">Dostosuj</button>\n        </div>\n        <div class=\"col-md-6 col-xs-6\">\n          <button type=\"button\" class=\"btn btn-primary custom-btn-primary button-effect-style\" onclick=\"this.blur();\" (click)=\"setDevicesMode(1)\" >EFEKT 1</button>\n          <button type=\"button\" class=\"btn btn-primary custom-btn-primary button-effect-style\" onclick=\"this.blur();\" (click)=\"setDevicesMode(2)\" >EFEKT 2</button>\n          <button type=\"button\" class=\"btn btn-primary custom-btn-primary button-effect-style\" onclick=\"this.blur();\" (click)=\"setDevicesMode(3)\" >EFEKT 3</button>\n          <button type=\"button\" class=\"btn btn-primary custom-btn-primary button-effect-style\" onclick=\"this.blur();\" (click)=\"setDevicesMode(5)\" >EFEKT 4</button>\n          Prędkość:\n          <div id=\"speed_section\" class=\"speed-section\">\n            <button type=\"button\" class=\"btn btn-primary custom-btn-primary btn-speed\" onclick=\"this.blur();\" (click)=\"increaseSpeed()\" >+</button>\n            {{speed}}\n            <button type=\"button\" class=\"btn btn-primary custom-btn-primary btn-speed\" onclick=\"this.blur();\" (click)=\"decreaseSpeed()\" >-</button>\n          </div>\n\n        </div>\n      </div>\n      <button type=\"button\" class=\"btn btn-success custom-btn-primary btn-back mt-3\" onclick=\"this.blur();\"  [routerLink]=\"['/livingroom']\" >WRÓĆ</button>\n\n    </div>\n\n    <div class=\"col-md-6 col-xs-6 pt-3\" >\n      <div id=\"windows\" class=\"windows-style pt-2 pb-2 pr-2 pl-2\" *ngIf=\"isManualModeOn()\" >\n        <div id=\"salon_okno_gora\" class=\"window-style img-div-container\" [style.background-color]=\"getStyle('salon_okno_gora')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">\n          <img *ngIf=\"getSelected('salon_okno_gora')\" src=\"../../../assets/img/checkbox.png\" class=\"img-style\">\n        </div>\n\n        <div id=\"salon_okno_srodek\" class=\"window-style img-div-container\" [style.background-color]=\"getStyle('salon_okno_srodek')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">\n          <img *ngIf=\"getSelected('salon_okno_srodek')\" src=\"../../../assets/img/checkbox.png\" class=\"img-style\">\n        </div>\n\n        <div id=\"salon_okno_dol\" class=\"window-style img-div-container\" [style.background-color]=\"getStyle('salon_okno_dol')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">\n          <img *ngIf=\"getSelected('salon_okno_dol')\" src=\"../../../assets/img/checkbox.png\" class=\"img-style\">\n        </div>\n\n      </div>\n    </div>\n\n\n    <!--<div class=\"col-md-6 col-xs-6\">-->\n      <!--<div id=\"windows\" class=\"windows-style\" *ngIf=\"isManualModeOn()\">-->\n      <!--<div id=\"windows\" class=\"windows-style\" >-->\n        <!--<div class=\"row frame-top\">-->\n          <!--<div class=\"window-frame frame-upper-left\">-->\n          <!--</div><div class=\"window-frame frame-top-center\">-->\n          <!--</div><div class=\"window-frame frame-top-right\">-->\n          <!--</div>-->\n        <!--</div>-->\n<!---->\n<!---->\n        <!--<div class=\"row window-level\">-->\n          <!--<div class=\"window-frame frame-left\">-->\n          <!--</div>-->\n          <!--<div id=\"salon_okno_gora\" class=\"window-style\" [style.background-color]=\"getStyle('salon_okno_gora')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">-->\n            <!--<div *ngIf=\"getSelected('salon_okno_gora')\">SELECTED</div>-->\n          <!--</div>-->\n          <!--<div class=\"window-frame frame-rigth\">-->\n          <!--</div>-->\n        <!--</div>-->\n        <!--<div class=\"row frame-middle\">-->\n          <!--<div class=\"window-frame frame-middle-left\">-->\n          <!--</div>-->\n          <!--<div class=\"window-frame frame-middle-center\">-->\n          <!--</div>-->\n          <!--<div class=\"window-frame frame-middle-right\">-->\n          <!--</div>-->\n        <!--</div>-->\n        <!--<div class=\"row window-level\">-->\n          <!--<div class=\"window-frame frame-left\">-->\n          <!--</div>-->\n          <!--<div id=\"salon_okno_srodek\" class=\"window-style\" [style.background-color]=\"getStyle('salon_okno_srodek')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">-->\n            <!--<div *ngIf=\"getSelected('salon_okno_srodek')\">SELECTED</div>-->\n          <!--</div>-->\n          <!--<div class=\"window-frame frame-rigth\">-->\n          <!--</div>-->\n        <!--</div>-->\n        <!--<div class=\"row frame-middle\">-->\n          <!--<div class=\"window-frame frame-middle-left\">-->\n          <!--</div>-->\n          <!--<div class=\"window-frame frame-middle-center\">-->\n          <!--</div>-->\n          <!--<div class=\"window-frame frame-middle-right\">-->\n          <!--</div>-->\n        <!--</div>-->\n        <!--<div class=\"row window-level\">-->\n          <!--<div class=\"window-frame frame-left\">-->\n          <!--</div>-->\n          <!--<div id=\"salon_okno_dol\" class=\"window-style\" [style.background-color]=\"getStyle('salon_okno_dol')\" (click)=\"goToColorPickerPage($event)\" long-press (onLongPress)=\"selectDevice($event)\">-->\n            <!--<div *ngIf=\"getSelected('salon_okno_dol')\">SELECTED</div>-->\n          <!--</div>-->\n          <!--<div class=\"window-frame frame-rigth\">-->\n          <!--</div>-->\n        <!--</div>-->\n        <!--<div class=\"row frame-bottom\">-->\n          <!--<div class=\"window-frame frame-bottom-left\">-->\n          <!--</div>-->\n          <!--<div class=\"window-frame frame-bottom-center\">-->\n          <!--</div>-->\n          <!--<div class=\"window-frame frame-bottom-right\">-->\n          <!--</div>-->\n        <!--</div>-->\n      <!--</div>-->\n<!---->\n      <!--<button type=\"button\" class=\"btn btn-primary custom-btn-primary btn-back\" onclick=\"this.blur();\"  [routerLink]=\"['/livingroom']\" >WRÓĆ</button>-->\n<!---->\n    <!--</div>-->\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/living-room-wall-effect/living-room-wall-effect.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/components/living-room-wall-effect/living-room-wall-effect.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: LivingRoomWallEffectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivingRoomWallEffectComponent", function() { return LivingRoomWallEffectComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var _services_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/config.service */ "./src/app/services/config.service.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utils/deviceConsts */ "./src/app/utils/deviceConsts.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../utils/utils */ "./src/app/utils/utils.ts");









var LivingRoomWallEffectComponent = /** @class */ (function () {
    function LivingRoomWallEffectComponent(restService, deviceManager, configService, route, router) {
        this.restService = restService;
        this.deviceManager = deviceManager;
        this.configService = configService;
        this.route = route;
        this.router = router;
        this.currentDevices = [
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_TOP,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_MIDDLE,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_BOTTOM
        ];
    }
    LivingRoomWallEffectComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('>>> onInit');
        this.utils = new _utils_utils__WEBPACK_IMPORTED_MODULE_7__["Utils"]();
        this.deviceManager.loadDevices(function () { _this.loadDevices(); });
    };
    LivingRoomWallEffectComponent.prototype.loadDevices = function () {
        var _this = this;
        console.log('Loading devices config');
        this.configService.getDevices(this.currentDevices).subscribe(function (data) {
            console.log('data: ', data);
            if (data.success) {
                console.log('GetDevices success, devices list: ', data.devices);
                _this.devicesMap = new Object();
                for (var _i = 0, _a = data.devices; _i < _a.length; _i++) {
                    var dev = _a[_i];
                    console.log('dev:', dev);
                    _this.devicesMap[dev.key] = new Object();
                    _this.devicesMap[dev.key].device = _this.deviceManager.getDevice(dev.category, dev.address);
                    _this.devicesMap[dev.key].red = 0;
                    _this.devicesMap[dev.key].green = 0;
                    _this.devicesMap[dev.key].blue = 0;
                    _this.devicesMap[dev.key].mode = 0;
                    _this.devicesMap[dev.key].speed = 0;
                    _this.devicesMap[dev.key].selected = false;
                }
                _this.loadDevicesParameters();
                console.log('>>>> devices map: ', _this.devicesMap);
            }
            else {
                console.log('GetDevices error: ', data.msg);
            }
        });
    };
    LivingRoomWallEffectComponent.prototype.loadDevicesParameters = function () {
        var _this = this;
        console.log('Loading devices parameters');
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            this.loadDeviceParameters(devKey, function (key, data) {
                for (var param = 0; param < data.parameters.length; param++) {
                    if (!data.parameters[param].defined) {
                        console.log('Device parameter ' + param + ' is undefined');
                    }
                    else {
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RED) {
                            _this.devicesMap[key].red = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_GREEN) {
                            _this.devicesMap[key].green = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_BLUE) {
                            _this.devicesMap[key].blue = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_SPEED) {
                            _this.devicesMap[key].speed = data.parameters[param]['value'];
                            _this.speed = data.parameters[param]['value'];
                        }
                        if (data.parameters[param]['parameter'] == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE) {
                            _this.devicesMap[key].mode = data.parameters[param]['value'];
                        }
                    }
                }
            });
        }
    };
    LivingRoomWallEffectComponent.prototype.loadDeviceParameters = function (deviceKey, callback) {
        this.restService.getDeviceParameters(this.devicesMap[deviceKey].device).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Loading device parameters failed: ' + data.result);
            }
            else {
                callback(deviceKey, data);
            }
        });
    };
    LivingRoomWallEffectComponent.prototype.getStyle = function (id) {
        var retVal = 'rgb(';
        if (this.devicesMap && this.devicesMap[id]) {
            retVal += ((this.devicesMap[id].red >> 4) & 0xff);
            retVal += ',';
            retVal += ((this.devicesMap[id].green >> 4) & 0xff);
            retVal += ',';
            retVal += ((this.devicesMap[id].blue >> 4) & 0xff);
        }
        else {
            retVal += '0,0,0';
        }
        retVal += ')';
        return retVal;
    };
    LivingRoomWallEffectComponent.prototype.loadEffectConfig = function (event) {
        var _this = this;
        var setName = event['target'].attributes['id'].value;
        console.log('Loading effect set ' + setName);
        this.clearDeviceSelection();
        this.configService.getDevicesConfigs(setName).subscribe(function (data) {
            console.log('data: ', data);
            if (data.success) {
                console.log('config: ', data.configs);
                _this.setDeviceParameter(_this.devicesMap[_this.currentDevices[0]].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
                for (var _i = 0, _a = data.configs; _i < _a.length; _i++) {
                    var config = _a[_i];
                    var key = _this.getDeviceKey(config.address, config.category);
                    _this.setDeviceParameter(_this.devicesMap[key].device, config.parameter, config.value);
                    _this.updateCurrentDevices(key, config.value, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
                    console.log('Loading config for device ' + key + ', parameter ' + config.parameter + ' from config = ' + config.value);
                }
            }
            else {
                console.log('GetDevicesConfigs error: ', data.msg);
            }
        });
    };
    LivingRoomWallEffectComponent.prototype.saveEffectConfig = function (event) {
        var setName = event['target'].attributes['id'].value;
        console.log('Saving effect set ' + setName);
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            var rgb = this.utils.getRgb(this.devicesMap[devKey].red, this.devicesMap[devKey].green, this.devicesMap[devKey].blue);
            console.log('Saving rgb value ' + rgb + ' for device ' + devKey);
            this.configService.addDeviceConfig(setName, this.devicesMap[devKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, rgb).subscribe(function (data) {
                console.log('Saving config success');
            });
        }
    };
    LivingRoomWallEffectComponent.prototype.getDeviceKey = function (address, category) {
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            if (this.devicesMap[devKey].device.address == address && this.devicesMap[devKey].device.category == category) {
                return devKey;
            }
        }
    };
    LivingRoomWallEffectComponent.prototype.setDeviceParameter = function (dev, param, val) {
        this.restService.setDeviceParameter(dev, param, val).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Setting device parameter failed: ' + data.result);
            }
            else {
                console.log('Device parameter set successfully! ');
            }
        });
    };
    LivingRoomWallEffectComponent.prototype.setDevicesMode = function (mode) {
        console.log('Set effect in mode ' + mode);
        this.clearDeviceSelection();
        var set = false;
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            if (!set) {
                this.setDeviceParameter(this.devicesMap[devKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, mode);
                set = true;
            }
            this.updateCurrentDevicesMode(devKey, mode);
        }
    };
    LivingRoomWallEffectComponent.prototype.updateCurrentDevices = function (devKey, rgb, mode) {
        this.devicesMap[devKey].mode = mode;
        this.devicesMap[devKey].red = this.utils.getRed12bit(rgb);
        this.devicesMap[devKey].green = this.utils.getGreen12bit(rgb);
        this.devicesMap[devKey].blue = this.utils.getBlue12bit(rgb);
        console.log('>>>> dev: ', this.devicesMap[devKey]);
    };
    LivingRoomWallEffectComponent.prototype.updateCurrentDevicesMode = function (devKey, mode) {
        this.devicesMap[devKey].mode = mode;
        console.log('>>>> dev: ', this.devicesMap[devKey]);
    };
    LivingRoomWallEffectComponent.prototype.increaseSpeed = function () {
        console.log('Increasing speed');
        if (this.speed < 3) {
            this.speed++;
            var set = false;
            for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
                var devKey = _a[_i];
                if (!set) {
                    this.setDeviceParameter(this.devicesMap[devKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_SPEED, this.speed);
                    set = true;
                }
                this.devicesMap[devKey].speed = this.speed;
            }
        }
    };
    LivingRoomWallEffectComponent.prototype.decreaseSpeed = function () {
        console.log('Decreasing speed');
        if (this.speed > 0) {
            this.speed--;
            var set = false;
            for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
                var devKey = _a[_i];
                if (!set) {
                    this.setDeviceParameter(this.devicesMap[devKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_SPEED, this.speed);
                    set = true;
                }
                this.devicesMap[devKey].speed = this.speed;
            }
        }
    };
    LivingRoomWallEffectComponent.prototype.goToColorPickerPage = function (event) {
        var devKey = event['target'].attributes['id'].value;
        var dev = this.devicesMap[devKey].device;
        var addresses = new Array();
        if (this.devicesMap[devKey].mode != _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL) {
            this.setDevicesMode(_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
        }
        if (!this.devicesMap[devKey].selected) {
            this.clearDeviceSelection();
            addresses.push(dev.address);
        }
        else {
            for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
                var key = _a[_i];
                if (this.devicesMap[key].selected) {
                    addresses.push(this.devicesMap[key].device.address);
                }
            }
        }
        this.router.navigate(['/colorpicker', dev.category, addresses.join(),
            this.devicesMap[devKey].red, this.devicesMap[devKey].green, this.devicesMap[devKey].blue, 'livingroomwalleffect']);
    };
    LivingRoomWallEffectComponent.prototype.isManualModeOn = function () {
        return (this.devicesMap
            && this.devicesMap[this.currentDevices[0]].mode
            && this.devicesMap[this.currentDevices[0]].mode == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
    };
    LivingRoomWallEffectComponent.prototype.setManualMode = function () {
        console.log('Switch to manual mode');
        this.clearDeviceSelection();
        if (this.devicesMap[this.currentDevices[0]].mode == _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL) {
            return;
        }
        var set = false;
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var devKey = _a[_i];
            if (!set) {
                this.setDeviceParameter(this.devicesMap[devKey].device, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
                set = true;
            }
            this.updateCurrentDevicesMode(devKey, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].RGB_MODE_MANUAL);
        }
    };
    LivingRoomWallEffectComponent.prototype.getSelected = function (id) {
        return (this.devicesMap && this.devicesMap[id].selected);
    };
    LivingRoomWallEffectComponent.prototype.selectDevice = function (event) {
        var id = event['target'].attributes['id'].value;
        if (this.devicesMap[id].selected) {
            this.devicesMap[id].selected = false;
        }
        else {
            this.devicesMap[id].selected = true;
        }
    };
    LivingRoomWallEffectComponent.prototype.clearDeviceSelection = function () {
        for (var _i = 0, _a = this.currentDevices; _i < _a.length; _i++) {
            var key = _a[_i];
            this.devicesMap[key].selected = false;
        }
    };
    LivingRoomWallEffectComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-living-room-wall-effect',
            template: __webpack_require__(/*! ./living-room-wall-effect.component.html */ "./src/app/components/living-room-wall-effect/living-room-wall-effect.component.html"),
            styles: [__webpack_require__(/*! ./living-room-wall-effect.component.css */ "./src/app/components/living-room-wall-effect/living-room-wall-effect.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__["DeviceManagerService"],
            _services_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], LivingRoomWallEffectComponent);
    return LivingRoomWallEffectComponent;
}());



/***/ }),

/***/ "./src/app/components/living-room/living-room.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/living-room/living-room.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.custom-btn-danger:hover{\r\n  color: #ffffff;\r\n  background-color: #cc0000;\r\n  border-color: #cc0000;\r\n}\r\n\r\n.custom-btn-danger:active{\r\n  color: #ffffff;\r\n  background-color: #750000;\r\n  border-color: #4d0000;\r\n}\r\n\r\n.custom-btn-primary:hover{\r\n  color: #ffffff;\r\n  background-color: #2a9fd6;\r\n  border-color: #2a9fd6;\r\n}\r\n\r\n.custom-btn-primary:active{\r\n  color: #ffffff;\r\n  background-color: #1b698e;\r\n  border-color: #15506c;\r\n}\r\n\r\n.main-panel{\r\n  /*height: 100vh;*/\r\n  max-width: 770px;\r\n  margin: auto;\r\n  padding-top: 10px;\r\n}\r\n\r\n.panel-body {\r\n  padding-top: 10px;\r\n  padding-right: 15px;\r\n  padding-bottom: 15px;\r\n  padding-left: 15px;\r\n}\r\n\r\n.panel-heading{\r\n  padding-left: 10px;\r\n  padding-top: 3px;\r\n  padding-bottom: 3px;\r\n  font-size: 10px;\r\n  color: #888888;\r\n  background-color: #3c3c3c;\r\n  border-color: #282828;\r\n}\r\n\r\n.custom-btn{\r\n  width: 100%;\r\n  margin-top: 0px;\r\n  margin-bottom: 10px;\r\n  padding-top: 14px;\r\n  padding-bottom: 14px;\r\n}\r\n\r\n.double-btn{\r\n  padding-top: 27px;\r\n  padding-bottom: 27px;\r\n}\r\n\r\n.half-btn{\r\n  padding-top: 0px;\r\n  padding-bottom: 0px;\r\n  width: 47%;\r\n}\r\n\r\n.panel-default{\r\n  height: 95vh;\r\n}\r\n\r\n.col-padding{\r\n  padding-left: 10px;\r\n  padding-right: 10px;\r\n}\r\n\r\n.img-style-livingroom{\r\n  width: 90%;\r\n  height: 90%;\r\n}\r\n\r\n.img-style-kitchen{\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n\r\n.btn-img-livingroom{\r\n  padding-top: 0px;\r\n  padding-bottom: 0px;\r\n  height: 120px;\r\n}\r\n\r\n.btn-img-kitchen{\r\n  padding-top: 4px;\r\n  padding-bottom: 4px;\r\n  margin-bottom: 15px;\r\n  height: 119px;\r\n}\r\n\r\n.panel-short {\r\n  margin-bottom: 10px;\r\n  height: 35vh;\r\n}\r\n\r\n.clock-panel {\r\n  text-align: center;\r\n  font-size: 60px;\r\n  color: cyan;\r\n}\r\n\r\n.btn-all-off {\r\n  height: 115px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9saXZpbmctcm9vbS9saXZpbmctcm9vbS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQTtFQUNFLGNBQWM7RUFDZCx5QkFBeUI7RUFDekIscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLHFCQUFxQjtBQUN2Qjs7QUFFQTtFQUNFLGNBQWM7RUFDZCx5QkFBeUI7RUFDekIscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLGNBQWM7RUFDZCx5QkFBeUI7RUFDekIscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtBQUN0Qjs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixvQkFBb0I7QUFDdEI7O0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLFVBQVU7QUFDWjs7QUFFQTtFQUNFLFlBQVk7QUFDZDs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsV0FBVztBQUNiOztBQUVBO0VBQ0UsV0FBVztFQUNYLFlBQVk7QUFDZDs7QUFHQTtFQUNFLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsYUFBYTtBQUNmOztBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsYUFBYTtBQUNmOztBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVk7QUFDZDs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsV0FBVztBQUNiOztBQUVBO0VBQ0UsYUFBYTtBQUNmIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9saXZpbmctcm9vbS9saXZpbmctcm9vbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi5jdXN0b20tYnRuLWRhbmdlcjpob3ZlcntcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2MwMDAwO1xyXG4gIGJvcmRlci1jb2xvcjogI2NjMDAwMDtcclxufVxyXG5cclxuLmN1c3RvbS1idG4tZGFuZ2VyOmFjdGl2ZXtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNzUwMDAwO1xyXG4gIGJvcmRlci1jb2xvcjogIzRkMDAwMDtcclxufVxyXG5cclxuLmN1c3RvbS1idG4tcHJpbWFyeTpob3ZlcntcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmE5ZmQ2O1xyXG4gIGJvcmRlci1jb2xvcjogIzJhOWZkNjtcclxufVxyXG5cclxuLmN1c3RvbS1idG4tcHJpbWFyeTphY3RpdmV7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFiNjk4ZTtcclxuICBib3JkZXItY29sb3I6ICMxNTUwNmM7XHJcbn1cclxuXHJcbi5tYWluLXBhbmVse1xyXG4gIC8qaGVpZ2h0OiAxMDB2aDsqL1xyXG4gIG1heC13aWR0aDogNzcwcHg7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG59XHJcblxyXG4ucGFuZWwtYm9keSB7XHJcbiAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgcGFkZGluZy1yaWdodDogMTVweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcclxuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbn1cclxuXHJcbi5wYW5lbC1oZWFkaW5ne1xyXG4gIHBhZGRpbmctbGVmdDogMTBweDtcclxuICBwYWRkaW5nLXRvcDogM3B4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAzcHg7XHJcbiAgZm9udC1zaXplOiAxMHB4O1xyXG4gIGNvbG9yOiAjODg4ODg4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMzYzNjM2M7XHJcbiAgYm9yZGVyLWNvbG9yOiAjMjgyODI4O1xyXG59XHJcblxyXG4uY3VzdG9tLWJ0bntcclxuICB3aWR0aDogMTAwJTtcclxuICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxufVxyXG5cclxuLmRvdWJsZS1idG57XHJcbiAgcGFkZGluZy10b3A6IDI3cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDI3cHg7XHJcbn1cclxuXHJcbi5oYWxmLWJ0bntcclxuICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XHJcbiAgd2lkdGg6IDQ3JTtcclxufVxyXG5cclxuLnBhbmVsLWRlZmF1bHR7XHJcbiAgaGVpZ2h0OiA5NXZoO1xyXG59XHJcblxyXG4uY29sLXBhZGRpbmd7XHJcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5pbWctc3R5bGUtbGl2aW5ncm9vbXtcclxuICB3aWR0aDogOTAlO1xyXG4gIGhlaWdodDogOTAlO1xyXG59XHJcblxyXG4uaW1nLXN0eWxlLWtpdGNoZW57XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmJ0bi1pbWctbGl2aW5ncm9vbXtcclxuICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XHJcbiAgaGVpZ2h0OiAxMjBweDtcclxufVxyXG5cclxuLmJ0bi1pbWcta2l0Y2hlbntcclxuICBwYWRkaW5nLXRvcDogNHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiA0cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICBoZWlnaHQ6IDExOXB4O1xyXG59XHJcblxyXG4ucGFuZWwtc2hvcnQge1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgaGVpZ2h0OiAzNXZoO1xyXG59XHJcblxyXG4uY2xvY2stcGFuZWwge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LXNpemU6IDYwcHg7XHJcbiAgY29sb3I6IGN5YW47XHJcbn1cclxuXHJcbi5idG4tYWxsLW9mZiB7XHJcbiAgaGVpZ2h0OiAxMTVweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/living-room/living-room.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/living-room/living-room.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row main-panel\">\n  <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 col-padding\">\n    <div class=\"panel panel-default\">\n      <div class=\"panel-heading\">\n        Salon - oświetlenie\n      </div>\n      <div class=\"panel-body\">\n        <button type=\"button\" id=\"livingroomlightsset1\" class=\"btn btn-primary custom-btn-primary custom-btn\" onclick=\"this.blur();\" long-press  touch-button (onTouch)=\"setLivingRoomLightsFromConfig(1)\" (onLongPress)=\"goToLivingRoomLightsPage(1)\" >{{buttonNames['livingroomlightsset1']}}</button>\n        <button type=\"button\" id=\"livingroomlightsset2\" class=\"btn btn-primary custom-btn-primary custom-btn\" onclick=\"this.blur();\" long-press  touch-button (onTouch)=\"setLivingRoomLightsFromConfig(2)\" (onLongPress)=\"goToLivingRoomLightsPage(2)\" >{{buttonNames['livingroomlightsset2']}}</button>\n        <button type=\"button\" id=\"livingroomlightsset3\" class=\"btn btn-primary custom-btn-primary custom-btn\" onclick=\"this.blur();\" long-press  touch-button (onTouch)=\"setLivingRoomLightsFromConfig(3)\" (onLongPress)=\"goToLivingRoomLightsPage(3)\" >{{buttonNames['livingroomlightsset3']}}</button>\n        <button type=\"button\" id=\"livingroomlightsset4\" class=\"btn btn-primary custom-btn-primary custom-btn\" onclick=\"this.blur();\" long-press  touch-button (onTouch)=\"setLivingRoomLightsFromConfig(4)\" (onLongPress)=\"goToLivingRoomLightsPage(4)\" >{{buttonNames['livingroomlightsset4']}}</button>\n        <button type=\"button\"  class=\"btn btn-primary custom-btn-primary custom-btn\" onclick=\"this.blur();\" [routerLink]=\"['/livingroomlights', 'livingroomlightsmanual']\">DOSTOSUJ</button>\n\n        <div>\n          <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn half-btn\" onclick=\"this.blur();\" touch-button (onTouch)=\"increaseAllLivingRoomLights()\" long-press  >+</button>\n          <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn half-btn\" style=\"float: right;\" onclick=\"this.blur();\" touch-button (onTouch)=\"decreaseAllLivingRoomLights()\">-</button>\n        </div>\n\n        <button type=\"button\" class=\"btn btn-danger custom-btn-danger custom-btn mb-0\" onclick=\"this.blur();\"  touch-button (onTouch)=\"livingroomLightsOff()\">WYŁĄCZ</button>\n\n      </div>\n    </div>\n  </div>\n  <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 col-padding\">\n    <div class=\"panel panel-default\">\n      <div class=\"panel-heading\">\n        Salon - efekty\n      </div>\n      <div class=\"panel-body\">\n        <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn btn-img-livingroom\" onclick=\"this.blur();\" [routerLink]=\"['/livingroomwalleffect']\">\n          <img src=\"assets/img/walleffect_sm.png\" class=\"img-style-livingroom\"/>\n        </button>\n        <button type=\"button\" class=\"btn btn-danger custom-btn-danger custom-btn\" onclick=\"this.blur();\" style=\"margin-bottom: 27px;\" touch-button (onTouch)=\"livingroomWallEffectOff()\">WYŁĄCZ</button>\n        <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn btn-img-livingroom\" onclick=\"this.blur();\" [routerLink]=\"['/livingroomceilingeffect']\">\n          <img src=\"assets/img/ceilingeffect_sm.png\" class=\"img-style-livingroom\"/>\n        </button>\n        <button type=\"button\" class=\"btn btn-danger custom-btn-danger custom-btn mb-0\" onclick=\"this.blur();\" touch-button (onTouch)=\"livingroomCeilingEffectOff()\">WYŁĄCZ</button>\n\n      </div>\n    </div>\n  </div>\n  <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 col-padding\">\n    <div class=\"panel panel-default\">\n      <div class=\"panel-heading\">\n        Kuchnia\n      </div>\n      <div class=\"panel-body\">\n        <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn btn-img-kitchen\" onclick=\"this.blur();\" touch-button (onTouch)=\"switchKitchenMainLights()\">\n          <img src=\"assets/img/kitchen_main_sm.png\" class=\"img-style-kitchen\"/>\n        </button>\n\n        <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn btn-img-kitchen\" onclick=\"this.blur();\" touch-button (onTouch)=\"switchKitchenSideLigths()\">\n          <img src=\"assets/img/kitchen_side_sm.png\" class=\"img-style-kitchen\"/>\n        </button>\n\n        <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn btn-img-kitchen mb-0\" onclick=\"this.blur();\" touch-button (onTouch)=\"switchKitchenFurnitureLights()\">\n          <img src=\"assets/img/furniture_sm.png\" class=\"img-style-kitchen\"/>\n        </button>\n\n      </div>\n    </div>\n  </div>\n  <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 col-padding\">\n    <div class=\"panel panel-default\">\n      <div class=\"panel-heading\">\n        Gniazdka\n      </div>\n      <div class=\"panel-body  pb-1\">\n        <button type=\"button\" class=\"btn btn-primary custom-btn-primary custom-btn btn-img-kitchen\" onclick=\"this.blur();\" [routerLink]=\"['/sockets']\">\n          <img src=\"assets/img/sockets_sm.png\" class=\"img-style-kitchen\"/>\n        </button>\n      </div>\n\n      <div class=\"panel-heading\">\n        Oświetlenie\n      </div>\n      <div class=\"panel-body pb-1\">\n        <button type=\"button\" class=\"btn btn-danger custom-btn-danger custom-btn btn-all-off\" onclick=\"this.blur();\"  touch-button (onTouch)=\"switchAllLightsOff()\">WYŁĄCZ</button>\n      </div>\n      <div class=\"clock-panel\">\n        {{hours}}:{{minutes}}\n      </div>\n    </div>\n\n  </div>\n\n\n</div>\n\n\n\n\n\n"

/***/ }),

/***/ "./src/app/components/living-room/living-room.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/living-room/living-room.component.ts ***!
  \*****************************************************************/
/*! exports provided: LivingRoomComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivingRoomComponent", function() { return LivingRoomComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var _services_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/config.service */ "./src/app/services/config.service.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utils/deviceConsts */ "./src/app/utils/deviceConsts.ts");
/* harmony import */ var _utils_configConsts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../utils/configConsts */ "./src/app/utils/configConsts.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/observable/TimerObservable */ "./node_modules/rxjs-compat/_esm5/observable/TimerObservable.js");









var LivingRoomComponent = /** @class */ (function () {
    function LivingRoomComponent(router, restService, deviceManager, configService) {
        this.router = router;
        this.restService = restService;
        this.deviceManager = deviceManager;
        this.configService = configService;
        this.currentDevices = {
            kitchenDevices: [
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_MAIN,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SIDE,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_FURN_LEFT,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_FURN_RIGHT
            ],
            livingroomlightsDevices: [
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_01,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_02,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_03,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_04,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_05,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_06,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_07,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_08,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_01,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_02,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_03,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_04,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_05,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_06,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_07,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_08,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_09,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_10,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_11,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_12,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_13,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_14
            ],
            livingroomWallEffectDevices: [
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_TOP,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_MIDDLE,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_BOTTOM
            ],
            livingroomCeilingEffectDevices: [
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_LONG,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT,
                _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_LONG
            ]
        };
    }
    LivingRoomComponent.prototype.ngOnInit = function () {
        var _this = this;
        var timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_8__["TimerObservable"].create(0, 1000);
        this.subscription = timer.subscribe(function (t) {
            _this.setClock();
        });
        this.loadLivingRoomLightsSetNames();
        this.deviceManager.loadDevices(function () { _this.loadDevicesConfig(); });
    };
    LivingRoomComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    LivingRoomComponent.prototype.setClock = function () {
        var date = new Date();
        this.hours = date.getHours().toString();
        var mins = date.getMinutes();
        if (mins < 10) {
            this.minutes = '0' + mins.toString();
        }
        else {
            this.minutes = mins.toString();
        }
    };
    LivingRoomComponent.prototype.loadDevicesConfig = function () {
        console.log('>>> loading devices config');
        this.getDevices();
    };
    LivingRoomComponent.prototype.loadLivingRoomLightsSetNames = function () {
        var _this = this;
        this.buttonNames = new Object();
        for (var i = 1; i < 5; i++) {
            this.loadSetName(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET + i, function (name, id) {
                _this.buttonNames[id] = name;
            });
        }
    };
    LivingRoomComponent.prototype.loadSetName = function (id, callback) {
        this.configService.getBasicConfig(id).subscribe(function (data) {
            if (data.success) {
                callback(data.config.value, id);
            }
            else {
                console.log('Loading LightSetName ' + id + ' error: ', data.msg);
            }
        });
    };
    LivingRoomComponent.prototype.getAllCurrentDevices = function () {
        return this.currentDevices['kitchenDevices']
            .concat(this.currentDevices['livingroomlightsDevices'])
            .concat(this.currentDevices['livingroomWallEffectDevices'])
            .concat(this.currentDevices['livingroomCeilingEffectDevices']);
    };
    LivingRoomComponent.prototype.goToLivingRoomLightsPage = function (id) {
        this.router.navigate(['/livingroomlights', _utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET + id]);
    };
    LivingRoomComponent.prototype.setLivingRoomLightsFromConfig = function (setId) {
        var _this = this;
        var configName = _utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET + setId;
        this.configService.getDevicesConfigs(configName).subscribe(function (data) {
            console.log('data: ', data);
            if (data.success) {
                console.log('config: ', data.configs);
                for (var _i = 0, _a = data.configs; _i < _a.length; _i++) {
                    var config = _a[_i];
                    var key = _this.getDeviceKey(config.address, config.category);
                    _this.setDeviceParameter(_this.devicesMap[key], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, config.value);
                    console.log('Setting device ' + key + ' duty cycle from config = ' + config.value);
                }
            }
            else {
                console.log('GetDevicesConfigs error: ', data.msg);
            }
        });
    };
    LivingRoomComponent.prototype.getDeviceKey = function (address, category) {
        for (var _i = 0, _a = this.getAllCurrentDevices(); _i < _a.length; _i++) {
            var devKey = _a[_i];
            if (this.devicesMap[devKey].address == address && this.devicesMap[devKey].category == category) {
                return devKey;
            }
        }
    };
    LivingRoomComponent.prototype.switchKitchenMainLights = function () {
        console.log('>>> kuchnia sufit');
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_MAIN], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_SWITCH, 0);
    };
    LivingRoomComponent.prototype.switchKitchenSideLigths = function () {
        console.log('>>> kuchnia boczne');
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SIDE], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_SWITCH, 0);
    };
    LivingRoomComponent.prototype.switchKitchenFurnitureLights = function () {
        var _this = this;
        console.log('>>> kuchnia szafki');
        this.loadDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_FURN_RIGHT], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, function (val_r) {
            _this.loadDeviceParameter(_this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_FURN_LEFT], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, function (val_l) {
                if (val_l > 0 || val_r > 0) {
                    _this.setDeviceParameter(_this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_FURN_LEFT], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, 0);
                    _this.setDeviceParameter(_this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_FURN_RIGHT], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, 0);
                }
                else {
                    _this.setDeviceParameter(_this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_FURN_LEFT], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, 1);
                    _this.setDeviceParameter(_this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_FURN_RIGHT], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, 1);
                }
            });
        });
    };
    LivingRoomComponent.prototype.switchAllLightsOff = function () {
        // this.saveSettings();
        this.livingroomLightsOff();
        this.livingroomWallEffectOff();
        this.livingroomCeilingEffectOff();
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_MAIN], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SIDE], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_FURN_LEFT], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, 0);
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_FURN_RIGHT], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, 0);
    };
    LivingRoomComponent.prototype.increaseAllLivingRoomLights = function () {
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE_UP_ALL, 20);
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE_UP_ALL, 20);
    };
    LivingRoomComponent.prototype.decreaseAllLivingRoomLights = function () {
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE_DOWN_ALL, 20);
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE_DOWN_ALL, 20);
    };
    LivingRoomComponent.prototype.loadDeviceParameter = function (dev, param, callback) {
        this.restService.getDeviceParameter(dev, param).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Loading device parameters failed: ' + data.result);
            }
            else {
                callback(data.value);
            }
        });
    };
    LivingRoomComponent.prototype.setDeviceParameter = function (dev, param, val) {
        this.restService.setDeviceParameter(dev, param, val).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Setting device parameter failed: ' + data.result);
            }
            else {
                console.log('Device parameter set successfully!');
            }
        });
    };
    // getDevice(): void {
    //   console.log('>>>> get device');
    //   this.configService.getDevice('kuchnia_sufit').subscribe(data => {
    //
    //     console.log('data: ', data);
    //     console.log('>>> err success: ', data.success);
    //     console.log('>>> err msg: ', data.msg);
    //     console.log('>>> err dev: ', data.device);
    //
    //   });
    // }
    LivingRoomComponent.prototype.testLoadSettings = function () {
        console.log('>>> LOAD SETTINGS');
        // this.configService.getBasicConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1).subscribe(data => {
        //   console.log('data: ', data);
        //   if (data.success) {
        //     console.log('config: ', data.config);
        //
        //   } else {
        //     console.log('GetDevices error: ', data.msg);
        //   }
        //
        //
        // });
        this.configService.getDevicesConfigs(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1).subscribe(function (data) {
            console.log('data: ', data);
            if (data.success) {
                console.log('config: ', data.configs);
            }
            else {
                console.log('GetDevices error: ', data.msg);
            }
        });
    };
    LivingRoomComponent.prototype.testDelSettings = function () {
        console.log('>>> DELETE SETTINGS');
        this.configService.removeDevicesConfigs().subscribe(function (data) {
            console.log('DELETE DEVICES CONFIG: ', data);
        });
        this.configService.removeDevices().subscribe(function (data) {
            console.log('DELETE DEVICES: ', data);
        });
    };
    LivingRoomComponent.prototype.getDevices = function () {
        var _this = this;
        console.log('>>>> get devices');
        this.configService.getDevices(this.getAllCurrentDevices()).subscribe(function (data) {
            console.log('data: ', data);
            if (data.success) {
                console.log('GetDevices success, devices list: ', data.devices);
                _this.devicesMap = new Object();
                for (var _i = 0, _a = data.devices; _i < _a.length; _i++) {
                    var dev = _a[_i];
                    console.log('dev:', dev);
                    _this.devicesMap[dev.key] = _this.deviceManager.getDevice(dev.category, dev.address);
                }
                console.log('>>>> devices map: ', _this.devicesMap);
            }
            else {
                console.log('GetDevices error: ', data.msg);
            }
        });
    };
    LivingRoomComponent.prototype.removeDevices = function () {
        console.log('>>>> remove devices');
        this.configService.removeDevices().subscribe(function (data) {
            console.log('data: ', data);
        });
    };
    LivingRoomComponent.prototype.saveSettings = function () {
        console.log('>>> saving settings');
        this.configService.addDevice(this.deviceManager.getDevice(3, 30), 'kuchnia_sufit').subscribe(function (data) {
            console.log('>>>> save device - kuchnia sufit');
        });
        this.configService.addDevice(this.deviceManager.getDevice(3, 31), 'kuchnia_boczne').subscribe(function (data) {
            console.log('>>>> save device - kuchnia boczne');
        });
        this.configService.addDevice(this.deviceManager.getDevice(3, 35), 'kuchnia_szafki_prawe').subscribe(function (data) {
            console.log('>>>> save device - kuchnia szafki prawe');
        });
        this.configService.addDevice(this.deviceManager.getDevice(3, 36), 'kuchnia_szafki_lewe').subscribe(function (data) {
            console.log('>>>> save device - kuchnia szafki lewe');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 1), 'salon_zew_10').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_10');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 2), 'salon_zew_09').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_09');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 21), 'salon_zew_12').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_12');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 22), 'salon_zew_11').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_11');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 13), 'salon_zew_04').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_04');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 14), 'salon_zew_03').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_03');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 15), 'salon_zew_02').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_02');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 16), 'salon_zew_01').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_01');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 17), 'salon_zew_14').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_14');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 18), 'salon_zew_13').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_13');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 3), 'salon_zew_08').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_08');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 4), 'salon_zew_07').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_07');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 5), 'salon_zew_06').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_06');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 6), 'salon_zew_05').subscribe(function (data) {
            console.log('>>>> save device - salon_zew_05');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 10), 'salon_wew_07').subscribe(function (data) {
            console.log('>>>> save device - salon_wew_07');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 11), 'salon_wew_04').subscribe(function (data) {
            console.log('>>>> save device - salon_wew_04');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 12), 'salon_wew_01').subscribe(function (data) {
            console.log('>>>> save device - salon_wew_01');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 19), 'salon_wew_02').subscribe(function (data) {
            console.log('>>>> save device - salon_wew_02');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 20), 'salon_wew_06').subscribe(function (data) {
            console.log('>>>> save device - salon_wew_06');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 7), 'salon_wew_03').subscribe(function (data) {
            console.log('>>>> save device - salon_wew_03');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 8), 'salon_wew_05').subscribe(function (data) {
            console.log('>>>> save device - salon_wew_05');
        });
        this.configService.addDevice(this.deviceManager.getDevice(5, 9), 'salon_wew_08').subscribe(function (data) {
            console.log('>>>> save device - salon_wew_08');
        });
        this.configService.addDevice(this.deviceManager.getDevice(3, 41), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_01).subscribe(function (data) {
            console.log('>>>> save device - kuchnia_gniazdko_01');
        });
        this.configService.addDevice(this.deviceManager.getDevice(3, 42), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_02).subscribe(function (data) {
            console.log('>>>> save device - kuchnia_gniazdko_02');
        });
        this.configService.addDevice(this.deviceManager.getDevice(3, 39), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_03).subscribe(function (data) {
            console.log('>>>> save device - kuchnia_gniazdko_03');
        });
        this.configService.addDevice(this.deviceManager.getDevice(3, 40), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_04).subscribe(function (data) {
            console.log('>>>> save device - kuchnia_gniazdko_04');
        });
        this.configService.addDevice(this.deviceManager.getDevice(3, 37), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_05).subscribe(function (data) {
            console.log('>>>> save device - kuchnia_gniazdko_05');
        });
        this.configService.addDevice(this.deviceManager.getDevice(3, 38), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_06).subscribe(function (data) {
            console.log('>>>> save device - kuchnia_gniazdko_06');
        });
        this.configService.addDevice(this.deviceManager.getDevice(3, 5), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_SOCKET_01).subscribe(function (data) {
            console.log('>>>> save device - salon_gniazdko_01');
        });
        this.configService.addDevice(this.deviceManager.getDevice(7, 5), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_TOP).subscribe(function (data) {
            console.log('>>>> save device - LIVINGROOM_WALL_EFFECT_TOP');
        });
        this.configService.addDevice(this.deviceManager.getDevice(7, 4), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_MIDDLE).subscribe(function (data) {
            console.log('>>>> save device - LIVINGROOM_WALL_EFFECT_MIDDLE');
        });
        this.configService.addDevice(this.deviceManager.getDevice(7, 3), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_BOTTOM).subscribe(function (data) {
            console.log('>>>> save device - LIVINGROOM_WALL_EFFECT_BOTTOM');
        });
        this.configService.addDevice(this.deviceManager.getDevice(7, 2), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_LONG).subscribe(function (data) {
            console.log('>>>> save device - LIVINGROOM_CEILING_EFFECT_INSIDE_LONG');
        });
        this.configService.addDevice(this.deviceManager.getDevice(7, 1), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT).subscribe(function (data) {
            console.log('>>>> save device - LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT');
        });
        this.configService.addDevice(this.deviceManager.getDevice(7, 6), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_LONG).subscribe(function (data) {
            console.log('>>>> save device - LIVINGROOM_CEILING_EFFECT_OUTSIDE_LONG');
        });
        this.configService.addDevice(this.deviceManager.getDevice(7, 7), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT).subscribe(function (data) {
            console.log('>>>> save device - LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT');
        });
        this.configService.addBasicConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, 'SET1').subscribe(function (data) {
            console.log('>>>> save basic setting - LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addBasicConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, 'SET2').subscribe(function (data) {
            console.log('>>>> save basic setting LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addBasicConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, 'SET3').subscribe(function (data) {
            console.log('>>>> save basic setting LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addBasicConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, 'SET4').subscribe(function (data) {
            console.log('>>>> save basic setting LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_02], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_03], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_04], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_05], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_06], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_07], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_08], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_09], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_10], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_11], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_12], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_13], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_14], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_02], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_03], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_04], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_05], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_06], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_07], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_08], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_02], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_03], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_04], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_05], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_06], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_07], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_08], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_09], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_10], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_11], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_12], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_13], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_14], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_02], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_03], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_04], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_05], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_06], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_07], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_08], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_02], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_03], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_04], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_05], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_06], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_07], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_08], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_09], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_10], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_11], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_12], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_13], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_14], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_02], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_03], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_04], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_05], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_06], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_07], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_08], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_02], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_03], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_04], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_05], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_06], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_07], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_08], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_09], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_10], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_11], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_12], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_13], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_14], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_02], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_03], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_04], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_05], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_06], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_07], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_LIGHTS_SET_4, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_08], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_WALL_EFFECT_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_TOP], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_WALL_EFFECT_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_MIDDLE], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_WALL_EFFECT_SET_1, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_BOTTOM], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_1');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_WALL_EFFECT_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_TOP], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_WALL_EFFECT_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_MIDDLE], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_WALL_EFFECT_SET_2, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_BOTTOM], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_2');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_WALL_EFFECT_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_TOP], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_WALL_EFFECT_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_MIDDLE], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_3');
        });
        this.configService.addDeviceConfig(_utils_configConsts__WEBPACK_IMPORTED_MODULE_6__["ConfigConsts"].LIVINGROOM_WALL_EFFECT_SET_3, this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_BOTTOM], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, 0).subscribe(function (data) {
            console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_3');
        });
        // this.configService.addDevice(this.deviceManager.getDevice(3,35), 'kuchnia_szafki_prawe');
        // this.configService.addDevice(this.deviceManager.getDevice(3,36), 'kuchnia_szafki_lewe');
    };
    LivingRoomComponent.prototype.livingroomLightsOff = function () {
        console.log('Turn living room lights off');
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_RIM_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE_ALL, 0);
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_LIGHTS_MIDDLE_01], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_DUTY_CYCLE_ALL, 0);
    };
    LivingRoomComponent.prototype.livingroomWallEffectOff = function () {
        console.log('Turn living room wall effect off');
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_WALL_EFFECT_TOP], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, 0);
    };
    LivingRoomComponent.prototype.livingroomCeilingEffectOff = function () {
        console.log('Turn living room ceiling effect off');
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, 0);
        this.setDeviceParameter(this.devicesMap[_utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE, 0);
    };
    LivingRoomComponent.prototype.unblockSwitch = function () {
        var i = 0;
        while (i < 1000) {
            i++;
            // this.setDeviceParameter(this.deviceManager.getDevice(3, 26), DeviceConsts.PARAM_STATE, 0);
            // this.loadDeviceParameter(this.deviceManager.getDevice(3, 26), DeviceConsts.PARAM_STATE, (val) => {
            // if (val > 0) {
            this.setDeviceParameter(this.deviceManager.getDevice(3, 26), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, 0);
            // } else {
            this.setDeviceParameter(this.deviceManager.getDevice(3, 26), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, 1);
            //  }
            //});
        }
    };
    LivingRoomComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-living-room',
            template: __webpack_require__(/*! ./living-room.component.html */ "./src/app/components/living-room/living-room.component.html"),
            styles: [__webpack_require__(/*! ./living-room.component.css */ "./src/app/components/living-room/living-room.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__["DeviceManagerService"],
            _services_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]])
    ], LivingRoomComponent);
    return LivingRoomComponent;
}());



/***/ }),

/***/ "./src/app/components/navbar/navbar.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbmF2YmFyL25hdmJhci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\n  <a class=\"navbar-brand\" href=\"#\">IntelliHouseManager</a>\n  <button class=\"navbar-toggler collapsed\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarColor02\" aria-controls=\"navbarColor02\" aria-expanded=\"false\" aria-label=\"Toggle navigation\" style=\"\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n  <div class=\"navbar-collapse collapse\" id=\"navbarColor02\" style=\"\">\n    <ul class=\"navbar-nav\">\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = {exact:true}><a class=\"nav-link\" [routerLink]=\"['/']\">Home</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = {exact:true}><a class=\"nav-link\" [routerLink]=\"['/devicemanager']\">Device Manager</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = {exact:true}><a class=\"nav-link\" [routerLink]=\"['/eventmanager']\">Event Manager</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = {exact:true}><a class=\"nav-link\" [routerLink]=\"['/livingroom']\">LivingRoom</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = {exact:true}><a class=\"nav-link\" [routerLink]=\"['/alarmpanel']\">AlarmPanel</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = {exact:true}><a class=\"nav-link\" [routerLink]=\"['/widgetsgroupslist']\">Actions Widgets</a></li>\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = {exact:true}><a class=\"nav-link\" [routerLink]=\"['/backuprestore']\">Backup-Restore</a></li>\n    </ul>\n    <ul class=\"nav navbar-nav ml-auto\">\n      <li class=\"nav-item\" *ngIf=\"authService.loggedIn()\"><a class=\"nav-link\" (click)=\"onLogoutClick()\" href=\"#\">Logout</a></li>\n    </ul>\n  </div>\n</nav>\n\n"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.ts ***!
  \*******************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__);





var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(authService, router, flashMessage) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.onLogoutClick = function () {
        this.authService.logout();
        this.flashMessage.show('You are logged out', {
            cssClass: 'alert-success',
            timeout: 3000
        });
        this.router.navigate(['/home']);
        return false;
    };
    NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/components/navbar/navbar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__["FlashMessagesService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/components/operation/operation.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/operation/operation.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".item-name {\r\n  white-space: nowrap;\r\n  overflow: hidden;\r\n  text-overflow: ellipsis;\r\n  /*width: 100px; */\r\n}\r\n\r\n.item-buttons {\r\n  white-space: nowrap;\r\n}\r\n\r\n.flash-message-container {\r\n  position: fixed;\r\n  z-index: 10;\r\n  width: 100%;\r\n}\r\n\r\n.flash-message {\r\n  display: block;\r\n  margin-right: 10%;\r\n  margin-left: 10%;\r\n}\r\n\r\n.ext-logic {\r\n  display: flex;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9vcGVyYXRpb24vb3BlcmF0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLHVCQUF1QjtFQUN2QixpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsV0FBVztFQUNYLFdBQVc7QUFDYjs7QUFFQTtFQUNFLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsYUFBYTtBQUNmIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9vcGVyYXRpb24vb3BlcmF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaXRlbS1uYW1lIHtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgLyp3aWR0aDogMTAwcHg7ICovXHJcbn1cclxuXHJcbi5pdGVtLWJ1dHRvbnMge1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbn1cclxuXHJcbi5mbGFzaC1tZXNzYWdlLWNvbnRhaW5lciB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHotaW5kZXg6IDEwO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZmxhc2gtbWVzc2FnZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMCU7XHJcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcclxufVxyXG5cclxuLmV4dC1sb2dpYyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/operation/operation.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/operation/operation.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"flash-message-container\">\n  <div class=\"flash-message\">\n    <flash-messages></flash-messages>\n  </div>\n</div>\n<div class=\"container mt-2\">\n  <div>\n    <div class=\"panel-body\">\n      <div class=\"mb-1\">\n        <label class=\"form-control-label\" for=\"inputName\">Operation name:</label>\n        <input type=\"text\" [(ngModel)]=\"operation.name\" class=\"form-control\" id=\"inputName\" style=\"width: 50%; margin-bottom: 5px;\">\n      </div>\n\n      <div class=\"mb-1\">\n        <button type=\"button\" class=\"btn btn-success\" *ngIf=\"operation.active == '1'\" (click)=\"onChangeActiveStatus()\">Active</button>\n        <button type=\"button\" class=\"btn btn-danger\" *ngIf=\"operation.active == '0'\" (click)=\"onChangeActiveStatus()\">Inactive</button>\n      </div>\n\n      <div class=\"bs-component mt-3\" *ngIf=\"operation.type == 'normal'\">\n        <div class=\"pb-2\">\n          <label class=\"form-control-label col-lg-4\" for=\"conditionList\">Conditions list:</label>\n          <a class=\"btn btn-primary\" [routerLink]=\"['/condition', 0, 0]\"  >\n            Add condition\n          </a>\n        </div>\n\n        <div class=\"list-group\" id=\"conditionList\">\n          <button *ngFor=\"let condition of operation.conditions\" (click)=\"goToConditionPage(condition)\"\n                  class=\"list-group-item list-group-item-action d-flex justify-content-between align-items-center\">\n            <div class=\"item-name\">\n              {{condition.toString()}}\n            </div>\n            <div class=\"item-buttons\">\n              <button type=\"button\" class=\"btn btn-secondary mr-2\" (click)=\"cloneCondition(condition.id)\">Clone</button>\n              <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteCondition(condition.id)\">Delete</button>\n            </div>\n          </button>\n        </div>\n\n        <div class=\"mb-2 mt-2 ext-logic\">\n\n          <input class=\"form-control col-lg-3 col-xs-10 mr-1\" type=\"text\" [(ngModel)]=\"operation.conditionsExtLogic\" (keypress)=\"restrictLogicPattern($event)\" placeholder=\"Extended logic\">\n          <button type=\"button\" *ngIf=\"operation.conditionsExtLogic?.length > 0\" class=\"btn btn-default\" (click)=\"clearExtLogic(null)\" style=\"min-width: 70px;\">Clear</button>\n\n        </div>\n\n      </div>\n\n      <div class=\"bs-component mt-3\" *ngIf=\"operation.type == 'delayed'\">\n        <div class=\"pb-2\">\n          <label class=\"form-control-label col-lg-4\" for=\"startConditionList\">Start Conditions list:</label>\n          <a class=\"btn btn-primary\" [routerLink]=\"['/condition', 1, 0]\"  >\n            Add condition\n          </a>\n        </div>\n\n        <div class=\"list-group\" id=\"startConditionList\">\n          <button *ngFor=\"let condition of operation.startConditions()\" (click)=\"goToConditionPage(condition)\"\n                  class=\"list-group-item list-group-item-action d-flex justify-content-between align-items-center\">\n            <div class=\"item-name\">\n              {{condition.toString()}}\n            </div>\n            <div class=\"item-buttons\">\n              <button type=\"button\" class=\"btn btn-secondary mr-2\" (click)=\"cloneCondition(condition.id)\">Clone</button>\n              <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteCondition(condition.id)\">Delete</button>\n            </div>\n          </button>\n        </div>\n\n        <div class=\"mb-2 mt-2 ext-logic\">\n\n          <input class=\"form-control col-lg-3 col-xs-10 mr-1\" type=\"text\" [(ngModel)]=\"operation.conditionsExtLogic\" (keypress)=\"restrictLogicPattern($event)\" placeholder=\"Extended logic\">\n          <button type=\"button\" *ngIf=\"operation.conditionsExtLogic?.length > 0\" class=\"btn btn-default\" (click)=\"clearExtLogic(null)\" style=\"min-width: 70px;\">Clear</button>\n\n        </div>\n\n      </div>\n\n      <div class=\"bs-component mt-3\" *ngIf=\"operation.type == 'delayed'\">\n        <div class=\"pb-2\">\n          <label class=\"form-control-label col-lg-4\" for=\"stopConditionList\">Stop Conditions list:</label>\n          <a class=\"btn btn-primary\" [routerLink]=\"['/condition', 2, 0]\"  >\n            Add condition\n          </a>\n        </div>\n\n        <div class=\"list-group\" id=\"stopConditionList\">\n          <button *ngFor=\"let condition of operation.stopConditions()\" (click)=\"goToConditionPage(condition)\"\n                  class=\"list-group-item list-group-item-action d-flex justify-content-between align-items-center\">\n            <div class=\"item-name\">\n              {{condition.toString()}}\n            </div>\n            <div class=\"item-buttons\">\n              <button type=\"button\" class=\"btn btn-secondary mr-2\" (click)=\"cloneCondition(condition.id)\">Clone</button>\n              <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteCondition(condition.id)\">Delete</button>\n            </div>\n          </button>\n        </div>\n\n        <div class=\"mb-2 mt-2 ext-logic\">\n\n          <input class=\"form-control col-lg-3 col-xs-10 mr-1\" type=\"text\" [(ngModel)]=\"operation.stopConditionsExtLogic\" (keypress)=\"restrictLogicPattern($event)\" placeholder=\"Extended logic\">\n          <button type=\"button\" *ngIf=\"operation.stopConditionsExtLogic?.length > 0\" class=\"btn btn-default\" (click)=\"clearExtLogic('stop')\" style=\"min-width: 70px;\">Clear</button>\n\n        </div>\n\n      </div>\n\n\n\n      <div class=\"bs-component mt-3\">\n\n        <div class=\"pb-2\">\n          <label class=\"form-control-label col-lg-4\" for=\"actionList\">Actions list:</label>\n          <a class=\"btn btn-primary\" [routerLink]=\"['/action', 0]\"  >\n            Add action\n          </a>\n        </div>\n\n        <div class=\"list-group\" id=\"actionList\">\n          <button *ngFor=\"let action of operation.actions\" (click)=\"goToActionPage(action)\"\n                  class=\"list-group-item list-group-item-action d-flex justify-content-between align-items-center\">\n            <div class=\"item-name\">\n              {{action.toString()}}\n            </div>\n            <div class=\"item-buttons\">\n              <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteAction(action.id)\">Delete</button>\n            </div>\n          </button>\n        </div>\n\n      </div>\n\n      <div class=\"bs-component mt-2\" *ngIf=\"operation.type == 'delayed'\" >\n        <label class=\"form-control-label\" for=\"inputDelay\">Delay value in seconds:</label>\n        <input type=\"text\" [(ngModel)]=\"operation.delay\" class=\"form-control\" id=\"inputDelay\" style=\"width: 20%;\">\n      </div>\n\n      <div class=\" mt-3 mb-3\">\n        <button class=\"btn btn-success mr-2\" (click)=\"onSaveOperation()\">Save Operation</button>\n        <button class=\"btn btn-default\" (click)=\"onCancelOperation()\">Cancel</button>\n      </div>\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/operation/operation.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/operation/operation.component.ts ***!
  \*************************************************************/
/*! exports provided: OperationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OperationComponent", function() { return OperationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_operation_manager_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/operation-manager.service */ "./src/app/services/operation-manager.service.ts");
/* harmony import */ var _services_validate_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/validate.service */ "./src/app/services/validate.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../models/consts */ "./src/app/models/consts.ts");








var OperationComponent = /** @class */ (function () {
    function OperationComponent(flashMessage, route, router, operationManager, validator) {
        this.flashMessage = flashMessage;
        this.route = route;
        this.router = router;
        this.operationManager = operationManager;
        this.validator = validator;
    }
    OperationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.operationId = params[_models_consts__WEBPACK_IMPORTED_MODULE_6__["URL_PARAM_ID"]];
            var sourcePage = params[_models_consts__WEBPACK_IMPORTED_MODULE_6__["URL_PARAM_SRC"]];
            if (_this.operationId == 0) {
                _this.operationManager.createOperation('regular');
            }
            else if (_this.operationId == 1) {
                _this.operationManager.createOperation('delayed');
            }
            else if (sourcePage != _models_consts__WEBPACK_IMPORTED_MODULE_6__["SRC_PAGE_ACT_EDIT"] && sourcePage != _models_consts__WEBPACK_IMPORTED_MODULE_6__["SRC_PAGE_COND_EDIT"]) {
                _this.operationManager.cloneOperation(_this.operationId);
            }
            _this.operation = _this.operationManager.tmpOperation;
            console.log('>>>> this operation: ', _this.operation);
        });
    };
    OperationComponent.prototype.onChangeActiveStatus = function () {
        if (this.operation.active) {
            this.operation.active = false;
        }
        else {
            this.operation.active = true;
        }
    };
    OperationComponent.prototype.onSaveOperation = function () {
        var res = new Object();
        if (this.validator.validateOperation(this.operation, res)) {
            this.operationManager.saveOperation(this.operation);
            this.router.navigate(['/eventmanager']);
        }
        else {
            this.flashMessage.show(res['error'], {
                cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                timeout: 5000
            });
        }
    };
    OperationComponent.prototype.onCancelOperation = function () {
        this.operationManager.cancelEditOperation();
        this.router.navigate(['/eventmanager']);
    };
    OperationComponent.prototype.deleteCondition = function (conditionId) {
        this.operationManager.removeOperationConditionById(this.operation, conditionId);
    };
    OperationComponent.prototype.cloneCondition = function (conditionId) {
        this.router.navigate(['/condition', conditionId, 1]);
    };
    OperationComponent.prototype.deleteAction = function (actionId) {
        this.operationManager.removeOperationActionById(this.operation, actionId);
    };
    OperationComponent.prototype.goToConditionPage = function (con) {
        this.router.navigate(['/condition', con.id, 0]);
    };
    OperationComponent.prototype.goToActionPage = function (act) {
        this.router.navigate(['/action', act.id]);
    };
    OperationComponent.prototype.clearExtLogic = function (type) {
        if (type == 'stop') {
            this.operation['stopConditionsExtLogic'] = '';
        }
        else {
            this.operation.conditionsExtLogic = '';
        }
    };
    OperationComponent.prototype.restrictLogicPattern = function (event) {
        var pattern = /^[A-Z()!|&]$/;
        return pattern.test(event.key);
    };
    OperationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-operation',
            template: __webpack_require__(/*! ./operation.component.html */ "./src/app/components/operation/operation.component.html"),
            styles: [__webpack_require__(/*! ./operation.component.css */ "./src/app/components/operation/operation.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5__["FlashMessagesService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_operation_manager_service__WEBPACK_IMPORTED_MODULE_3__["OperationManagerService"],
            _services_validate_service__WEBPACK_IMPORTED_MODULE_4__["ValidateService"]])
    ], OperationComponent);
    return OperationComponent;
}());



/***/ }),

/***/ "./src/app/components/radial-color-picker/radial-color-picker.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/components/radial-color-picker/radial-color-picker.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".color-palette:hover {\r\n  cursor: pointer;\r\n}\r\n\r\n.component-container {\r\n  position: relative;\r\n}\r\n\r\n.center-circle {\r\n  position: absolute;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yYWRpYWwtY29sb3ItcGlja2VyL3JhZGlhbC1jb2xvci1waWNrZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQWU7QUFDakI7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3JhZGlhbC1jb2xvci1waWNrZXIvcmFkaWFsLWNvbG9yLXBpY2tlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbG9yLXBhbGV0dGU6aG92ZXIge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmNvbXBvbmVudC1jb250YWluZXIge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuLmNlbnRlci1jaXJjbGUge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/radial-color-picker/radial-color-picker.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/components/radial-color-picker/radial-color-picker.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"color-picker\" class=\"component-container\" [style.width]=\"getSizeInPx()\" [style.height]=\"getSizeInPx()\">\n<!--<canvas #canvas class=\"color-slider\"  [width]=\"getSize()\" [height]=\"getSize()\" (mousedown)=\"onMouseDown($event)\" (touchstart)=\"touchstart($event)\" (touchmove)=\"touchmove($event)\"  (mousemove)=\"onMouseMove($event)\">-->\n<!--</canvas>-->\n  <canvas #canvas [width]=\"getSize()\" [height]=\"getSize()\" (mousedown)=\"onMouseDown($event)\" (touchmove)=\"touchmove($event)\"  (mousemove)=\"onMouseMove($event)\">\n  </canvas>\n  <div class=\"center-circle\"\n       [style.top]=\"getCenterPosition()\"\n       [style.left]=\"getCenterPosition()\"\n       [style.width]=\"getCenterSize()\"\n       [style.height]=\"getCenterSize()\"\n       [style.border-radius]=\"getCenterBorderRadius()\"\n       [style.background-color]=\"getCenterColor()\"\n       (click)=\"onCenterRadiusClick()\"\n  ></div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/radial-color-picker/radial-color-picker.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/radial-color-picker/radial-color-picker.component.ts ***!
  \*********************************************************************************/
/*! exports provided: RadialColorPickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RadialColorPickerComponent", function() { return RadialColorPickerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var RadialColorPickerComponent = /** @class */ (function () {
    function RadialColorPickerComponent() {
        this.color = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.centerRadiusClick = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.mousedown = false;
        // xc : number;
        // yc : number;
        // r : number;
        this.stopColors = ['#F00', '#FF0', '#0F0', '#0FF', '#00F', '#F0F'];
    }
    RadialColorPickerComponent.prototype.ngOnInit = function () {
        // this.xc = this.size / 2;
        // this.yc = this.size / 2;
        // this.r = this.xc - this.lineWidth;
    };
    RadialColorPickerComponent.prototype.getSize = function () {
        if (!this.size) {
            this.size = 100;
        }
        return this.size;
    };
    RadialColorPickerComponent.prototype.drawImage = function () {
        var _this = this;
        if (!this.ctx) {
            this.ctx = this.canvas.nativeElement.getContext('2d');
        }
        this.ctx.clearRect(0, 0, 1000, 1000);
        var base_image = new Image();
        base_image.src = '../../../assets/img/rainbow.png';
        this.ctx.drawImage(base_image, 0, 0);
        base_image.onload = function () {
            _this.ctx.clearRect(0, 0, _this.size, _this.size);
            _this.ctx.drawImage(base_image, 0, 0, _this.size, _this.size);
        };
    };
    // draw() {
    // 	if (!this.ctx) {
    //   	this.ctx = this.canvas.nativeElement.getContext('2d');
    // 	}
    //
    // 	this.drawMultiRadiantCircle(this.ctx, this.xc , this.yc, this.r, this.stopColors);
    // 	this.drawSelectedColor();
    //
    // }
    // drawMultiRadiantCircle(ctx, xc, yc, r, radientColors) {
    //   var partLength = (2 * Math.PI) / radientColors.length;
    //   var start = 0;
    //   var gradient = null;
    //   var startColor = null,
    //       endColor = null;
    //
    //   for (var i = 0; i < radientColors.length; i++) {
    //       startColor = radientColors[i];
    //       endColor = radientColors[(i + 1) % radientColors.length];
    //
    //       // x start / end of the next arc to draw
    //       var xStart = +xc + Math.cos(start) * r;
    //       var xEnd = +xc + Math.cos(start + partLength) * r;
    //       // y start / end of the next arc to draw
    //       var yStart = +yc + Math.sin(start) * r;
    //       var yEnd = +yc + Math.sin(start + partLength) * r;
    //
    //       ctx.beginPath();
    //
    //       gradient = ctx.createLinearGradient(xStart, yStart, xEnd, yEnd);
    //       gradient.addColorStop(0, startColor);
    //       gradient.addColorStop(1.0, endColor);
    //
    //       ctx.strokeStyle = gradient;
    //       ctx.arc(xc, yc, r, start, start + partLength + 0.05);
    //       ctx.lineWidth = this.lineWidth;
    //       ctx.stroke();
    //       ctx.closePath();
    //
    //       start += partLength;
    //   }
    // }
    // drawSelectedColor() {
    // 	if (!this.ctx) {
    //   	this.ctx = this.canvas.nativeElement.getContext('2d');
    // 	}
    //
    // 	this.ctx.beginPath();
    //   this.ctx.arc(this.xc, this.yc, this.centerRadius, 0, 2 * Math.PI, false);
    //   this.ctx.fillStyle = "rgb("+this.redValue+", "+this.greenValue+", "+this.blueValue+")";
    //   this.ctx.fill();
    //   //this.ctx.lineWidth = 1;
    //   //this.ctx.strokeStyle = '#000';
    //   //this.ctx.stroke();
    // }
    RadialColorPickerComponent.prototype.ngAfterViewInit = function () {
        this.drawImage();
    };
    RadialColorPickerComponent.prototype.touchmove = function (ev) {
        var touch = ev.touches[0];
        var cordX = touch.clientX - (touch.target.offsetParent.offsetLeft + touch.target.offsetParent.offsetParent.offsetLeft);
        var cordY = touch.pageY - touch.target.offsetParent.offsetTop - touch.target.offsetParent.offsetParent.offsetTop;
        this.emitColor(cordX, cordY);
        ev.preventDefault();
        ev.stopPropagation();
    };
    RadialColorPickerComponent.prototype.onMouseDown = function (evt) {
        this.mousedown = true;
        //console.log('>>>onMouseDown evt.offsetX: ' + evt.offsetX + ', evt.offsetY: ' + evt.offsetY);
        this.emitColor(evt.offsetX, evt.offsetY);
    };
    RadialColorPickerComponent.prototype.onMouseMove = function (evt) {
        if (this.mousedown) {
            this.emitColor(evt.offsetX, evt.offsetY);
            evt.preventDefault();
            evt.stopPropagation();
        }
    };
    RadialColorPickerComponent.prototype.onMouseUp = function (evt) {
        this.mousedown = false;
    };
    RadialColorPickerComponent.prototype.emitColor = function (x, y) {
        var rgbaColor = this.getColorAtPosition(x, y);
        if (rgbaColor != '') {
            console.log('>>> rgb color: ' + rgbaColor);
            this.color.emit(rgbaColor);
        }
        else {
            console.log('>>> noc olor selected');
        }
    };
    RadialColorPickerComponent.prototype.getColorAtPosition = function (x, y) {
        var imageData = this.ctx.getImageData(x, y, 1, 1).data;
        if (imageData[0] > 0 || imageData[1] > 0 || imageData[2] > 0) {
            this.redValue = imageData[0];
            this.greenValue = imageData[1];
            this.blueValue = imageData[2];
            // this.drawSelectedColor();
            return "{\"red\": " + imageData[0] + ", \"green\" : " + imageData[1] + ", \"blue\" : " + imageData[2] + "}";
        }
        else {
            return '';
        }
    };
    RadialColorPickerComponent.prototype.ngOnChanges = function (changes) {
        console.log('>>> change: ', changes);
        if (changes.redValue) {
            this.redValue = changes.redValue.currentValue / 0xf;
        }
        if (changes.greenValue) {
            this.greenValue = changes.greenValue.currentValue / 0xf;
        }
        if (changes.blueValue) {
            this.blueValue = changes.blueValue.currentValue / 0xf;
        }
        //this.drawSelectedColor();
    };
    RadialColorPickerComponent.prototype.getCenterPosition = function () {
        return '' + ((this.size / 2) - this.centerRadius) + 'px';
    };
    RadialColorPickerComponent.prototype.getCenterSize = function () {
        return '' + (this.centerRadius * 2) + 'px';
    };
    RadialColorPickerComponent.prototype.getCenterBorderRadius = function () {
        return '' + this.centerRadius + 'px';
    };
    RadialColorPickerComponent.prototype.getCenterColor = function () {
        return 'rgb(' + this.redValue + ', ' + this.greenValue + ', ' + this.blueValue + ')';
        // return 'rgb(123,123,222)';
    };
    RadialColorPickerComponent.prototype.getSizeInPx = function () {
        return '' + this.getSize() + 'px';
    };
    RadialColorPickerComponent.prototype.onCenterRadiusClick = function () {
        this.centerRadiusClick.emit(true);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('canvas'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], RadialColorPickerComponent.prototype, "canvas", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], RadialColorPickerComponent.prototype, "color", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], RadialColorPickerComponent.prototype, "centerRadiusClick", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], RadialColorPickerComponent.prototype, "size", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], RadialColorPickerComponent.prototype, "centerRadius", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], RadialColorPickerComponent.prototype, "redValue", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], RadialColorPickerComponent.prototype, "greenValue", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], RadialColorPickerComponent.prototype, "blueValue", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:mouseup', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [MouseEvent]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], RadialColorPickerComponent.prototype, "onMouseUp", null);
    RadialColorPickerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-radial-color-picker',
            template: __webpack_require__(/*! ./radial-color-picker.component.html */ "./src/app/components/radial-color-picker/radial-color-picker.component.html"),
            styles: [__webpack_require__(/*! ./radial-color-picker.component.css */ "./src/app/components/radial-color-picker/radial-color-picker.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], RadialColorPickerComponent);
    return RadialColorPickerComponent;
}());



/***/ }),

/***/ "./src/app/components/sockets/sockets.component.css":
/*!**********************************************************!*\
  !*** ./src/app/components/sockets/sockets.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".socket-row{\r\n  display: flex;\r\n  width: 50%;\r\n}\r\n\r\n.buttons{\r\n  width: 50%;\r\n  text-align: right;\r\n}\r\n\r\n.socket-name{\r\n  width: 50%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zb2NrZXRzL3NvY2tldHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQWE7RUFDYixVQUFVO0FBQ1o7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UsVUFBVTtBQUNaIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9zb2NrZXRzL3NvY2tldHMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zb2NrZXQtcm93e1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgd2lkdGg6IDUwJTtcclxufVxyXG5cclxuLmJ1dHRvbnN7XHJcbiAgd2lkdGg6IDUwJTtcclxuICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG5cclxuLnNvY2tldC1uYW1le1xyXG4gIHdpZHRoOiA1MCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/sockets/sockets.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/sockets/sockets.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pt-3 pl-3\">\n  <div class=\"socket-row\">\n    <div class=\"socket-name\">\n      Kuchnia gniazdko 1\n    </div>\n    <div class=\"buttons\">\n      <button type=\"button\" class=\"btn btn-success mb-2\" (click)=\"setSocketPower(true, 'kuchnia_gniazdko_01')\">WŁĄCZ</button>\n      <button type=\"button\" class=\"btn btn-danger mb-2\" (click)=\"setSocketPower(false, 'kuchnia_gniazdko_01')\">WYŁĄCZ</button>\n    </div>\n  </div>\n  <div class=\"socket-row\">\n    <div class=\"socket-name\">\n      Kuchnia gniazdko 2\n    </div>\n    <div class=\"buttons\">\n      <button type=\"button\" class=\"btn btn-success mb-2\" (click)=\"setSocketPower(true, 'kuchnia_gniazdko_02')\">WŁĄCZ</button>\n      <button type=\"button\" class=\"btn btn-danger mb-2\" (click)=\"setSocketPower(false, 'kuchnia_gniazdko_02')\">WYŁĄCZ</button>\n    </div>\n  </div>\n  <div class=\"socket-row\">\n    <div class=\"socket-name\">\n      Kuchnia gniazdko 3\n    </div>\n    <div class=\"buttons\">\n      <button type=\"button\" class=\"btn btn-success mb-2\" (click)=\"setSocketPower(true, 'kuchnia_gniazdko_03')\">WŁĄCZ</button>\n      <button type=\"button\" class=\"btn btn-danger mb-2\" (click)=\"setSocketPower(false, 'kuchnia_gniazdko_03')\">WYŁĄCZ</button>\n    </div>\n  </div>\n  <div class=\"socket-row\">\n    <div class=\"socket-name\">\n      Kuchnia gniazdko 4\n    </div>\n    <div class=\"buttons\">\n      <button type=\"button\" class=\"btn btn-success mb-2\" (click)=\"setSocketPower(true, 'kuchnia_gniazdko_04')\">WŁĄCZ</button>\n      <button type=\"button\" class=\"btn btn-danger mb-2\" (click)=\"setSocketPower(false, 'kuchnia_gniazdko_04')\">WYŁĄCZ</button>\n    </div>\n  </div>\n  <div class=\"socket-row\">\n    <div class=\"socket-name\">\n     Kuchnia gniazdko 5\n    </div>\n    <div class=\"buttons\">\n      <button type=\"button\" class=\"btn btn-success mb-2\" (click)=\"setSocketPower(true, 'kuchnia_gniazdko_05')\">WŁĄCZ</button>\n      <button type=\"button\" class=\"btn btn-danger mb-2\" (click)=\"setSocketPower(false, 'kuchnia_gniazdko_05')\">WYŁĄCZ</button>\n    </div>\n  </div>\n  <div class=\"socket-row\">\n    <div class=\"socket-name\">\n      Kuchnia gniazdko 6\n    </div>\n    <div class=\"buttons\">\n      <button type=\"button\" class=\"btn btn-success mb-2\" (click)=\"setSocketPower(true, 'kuchnia_gniazdko_06')\">WŁĄCZ</button>\n      <button type=\"button\" class=\"btn btn-danger mb-2\" (click)=\"setSocketPower(false, 'kuchnia_gniazdko_06')\">WYŁĄCZ</button>\n    </div>\n  </div>\n  <div class=\"socket-row\">\n    <div class=\"socket-name\">\n      Salon gniazdko\n    </div>\n    <div class=\"buttons\">\n      <button type=\"button\" class=\"btn btn-success mb-2\" (click)=\"setSocketPower(true, 'salon_gniazdko_01')\">WŁĄCZ</button>\n      <button type=\"button\" class=\"btn btn-danger mb-2\" (click)=\"setSocketPower(false, 'salon_gniazdko_01')\">WYŁĄCZ</button>\n    </div>\n  </div>\n  <button type=\"button\" class=\"btn btn-primary custom-btn-primary btn-back mt-3\" onclick=\"this.blur();\"  [routerLink]=\"['/livingroom']\" >WRÓĆ</button>\n</div>\n"

/***/ }),

/***/ "./src/app/components/sockets/sockets.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/sockets/sockets.component.ts ***!
  \*********************************************************/
/*! exports provided: SocketsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocketsComponent", function() { return SocketsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var _services_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/config.service */ "./src/app/services/config.service.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utils/deviceConsts */ "./src/app/utils/deviceConsts.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var SocketsComponent = /** @class */ (function () {
    function SocketsComponent(restService, deviceManager, configService, route, router) {
        this.restService = restService;
        this.deviceManager = deviceManager;
        this.configService = configService;
        this.route = route;
        this.router = router;
        this.currentDevices = [
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_01,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_02,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_03,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_04,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_05,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].KITCHEN_SOCKET_06,
            _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].LIVINGROOM_SOCKET_01
        ];
    }
    SocketsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.deviceManager.loadDevices(function () { _this.loadDevices(); });
    };
    SocketsComponent.prototype.loadDevices = function () {
        var _this = this;
        console.log('Loading devices config');
        this.configService.getDevices(this.currentDevices).subscribe(function (data) {
            console.log('data: ', data);
            if (data.success) {
                console.log('GetDevices success, devices list: ', data.devices);
                _this.devicesMap = new Object();
                for (var _i = 0, _a = data.devices; _i < _a.length; _i++) {
                    var dev = _a[_i];
                    console.log('dev:', dev);
                    _this.devicesMap[dev.key] = _this.deviceManager.getDevice(dev.category, dev.address);
                }
                console.log('>>>> devices map: ', _this.devicesMap);
            }
            else {
                console.log('GetDevices error: ', data.msg);
            }
        });
    };
    SocketsComponent.prototype.setSocketPower = function (status, key) {
        console.log('>>> set power ' + (status ? 'ON' : 'OFF') + ' for socket: ' + key);
        this.setDeviceParameter(this.devicesMap[key], _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_STATE, status ? 1 : 0);
    };
    SocketsComponent.prototype.setDeviceParameter = function (dev, param, val) {
        this.restService.setDeviceParameter(dev, param, val).subscribe(function (data) {
            if (data.errorCode) {
                console.log('Setting device parameter failed: ' + data.result);
            }
            else {
                console.log('Device parameter set successfully!');
            }
        });
    };
    SocketsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sockets',
            template: __webpack_require__(/*! ./sockets.component.html */ "./src/app/components/sockets/sockets.component.html"),
            styles: [__webpack_require__(/*! ./sockets.component.css */ "./src/app/components/sockets/sockets.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__["DeviceManagerService"],
            _services_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], SocketsComponent);
    return SocketsComponent;
}());



/***/ }),

/***/ "./src/app/components/time-picker/time-picker.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/time-picker/time-picker.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGltZS1waWNrZXIvdGltZS1waWNrZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/time-picker/time-picker.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/time-picker/time-picker.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-lg-2\">\n      <select class=\"form-control\" id=\"selectHour\" [(ngModel)]=\"selectedHour\" (change)=\"onHourChange($event.target.selectedIndex)\">\n        <option *ngFor=\"let hour of getHours()\" [ngValue]=\"hour\">\n          {{hour}}\n        </option>\n      </select>\n  </div>\n  <div class=\"col-lg-2\">\n      <select class=\"form-control\" id=\"selectMin\" [(ngModel)]=\"selectedMinute\" (change)=\"onMinuteChange($event.target.selectedIndex)\">\n        <option *ngFor=\"let minute of getMinutes()\" [ngValue]=\"minute\">\n          {{minute | number:'2.0-0'}}\n        </option>\n      </select>\n  </div>\n  <div class=\"col-lg-2\">\n      <select class=\"form-control\" id=\"selectSec\" [(ngModel)]=\"selectedSecond\" (change)=\"onSecondChange($event.target.selectedIndex)\">\n        <option *ngFor=\"let second of getMinutes()\" [ngValue]=\"second\">\n          {{second | number : '2.0-0'}}\n        </option>\n      </select>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/time-picker/time-picker.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/time-picker/time-picker.component.ts ***!
  \*****************************************************************/
/*! exports provided: TimePickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimePickerComponent", function() { return TimePickerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TimePickerComponent = /** @class */ (function () {
    function TimePickerComponent() {
        this.secondsFromMidnightChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    Object.defineProperty(TimePickerComponent.prototype, "secondsFromMidnight", {
        get: function () {
            return this.seconds;
        },
        // @Input()  secondsFromMidnight: number;
        set: function (secs) {
            this.seconds = secs;
            this.secondsFromMidnightChange.emit(secs);
        },
        enumerable: true,
        configurable: true
    });
    TimePickerComponent.prototype.ngOnInit = function () {
        console.log('>>>>> seconds: ', this.seconds);
        this.selectedHour = Math.floor(this.seconds / 3600);
        var minutes = this.seconds - (3600 * this.selectedHour);
        this.selectedMinute = Math.floor(minutes / 60);
        this.selectedSecond = minutes % 60;
    };
    TimePickerComponent.prototype.onHourChange = function (selectedIndex) {
        console.log(">>>>> onchange: ", selectedIndex);
        this.secondsFromMidnight = selectedIndex * 3600 + this.selectedMinute * 60 + this.selectedSecond;
    };
    TimePickerComponent.prototype.onMinuteChange = function (selectedIndex) {
        console.log(">>>>> onchange: ", selectedIndex);
        this.secondsFromMidnight = this.selectedHour * 3600 + selectedIndex * 60 + this.selectedSecond;
    };
    TimePickerComponent.prototype.onSecondChange = function (selectedIndex) {
        console.log(">>>>> onchange: ", selectedIndex);
        this.secondsFromMidnight = this.selectedHour * 3600 + this.selectedMinute * 60 + selectedIndex;
    };
    TimePickerComponent.prototype.getHours = function () {
        var hours = new Array();
        for (var val = 0; val < 24; val++) {
            hours.push(val);
        }
        return hours;
    };
    TimePickerComponent.prototype.getMinutes = function () {
        var minutes = new Array();
        for (var val = 0; val < 60; val++) {
            minutes.push(val);
        }
        return minutes;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Number])
    ], TimePickerComponent.prototype, "secondsFromMidnight", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], TimePickerComponent.prototype, "secondsFromMidnightChange", void 0);
    TimePickerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-time-picker',
            template: __webpack_require__(/*! ./time-picker.component.html */ "./src/app/components/time-picker/time-picker.component.html"),
            styles: [__webpack_require__(/*! ./time-picker.component.css */ "./src/app/components/time-picker/time-picker.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TimePickerComponent);
    return TimePickerComponent;
}());



/***/ }),

/***/ "./src/app/components/view/view.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/view/view.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdmlldy92aWV3LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/view/view.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/view/view.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/components/view/view.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/view/view.component.ts ***!
  \***************************************************/
/*! exports provided: ViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewComponent", function() { return ViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ViewComponent = /** @class */ (function () {
    function ViewComponent() {
    }
    ViewComponent.prototype.ngOnInit = function () {
    };
    ViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'view',
            template: __webpack_require__(/*! ./view.component.html */ "./src/app/components/view/view.component.html"),
            styles: [__webpack_require__(/*! ./view.component.css */ "./src/app/components/view/view.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ViewComponent);
    return ViewComponent;
}());



/***/ }),

/***/ "./src/app/components/widget-action-select/widget-action-select.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/components/widget-action-select/widget-action-select.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".drop-down{\r\n  display: contents;\r\n  margin-bottom: 15px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy93aWRnZXQtYWN0aW9uLXNlbGVjdC93aWRnZXQtYWN0aW9uLXNlbGVjdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQWlCO0VBQ2pCLG1CQUFtQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvd2lkZ2V0LWFjdGlvbi1zZWxlY3Qvd2lkZ2V0LWFjdGlvbi1zZWxlY3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kcm9wLWRvd257XHJcbiAgZGlzcGxheTogY29udGVudHM7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/widget-action-select/widget-action-select.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/components/widget-action-select/widget-action-select.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"form-group\" *ngIf=\"setup.fieldRequest.devCategory\">\n    <div style=\"margin-bottom: 5px;\">\n      Select category:\n    </div>\n\n    <div class=\"mb-2 drop-down\">\n      <select class=\"col-sm-6 form-control\" id=\"selectCond\" [(ngModel)]=\"action.devCategory\" >\n        <option *ngFor=\"let cat of deviceManager.categoriesList\" [ngValue]=\"cat.id\">\n          {{cat.name}}\n        </option>\n      </select>\n    </div>\n\n  </div>\n\n\n  <div class=\"form-group\" *ngIf=\"setup.fieldRequest.devAddress\">\n    <div style=\"margin-bottom: 5px;\">\n      Select device:\n    </div>\n\n    <div  class=\"mb-2 drop-down\">\n      <select class=\"col-sm-6 form-control\" id=\"selectDev\" [(ngModel)]=\"action.devAddress\">\n        <option *ngFor=\"let device of getDevicesForSelectedCategory() | orderBy: 'name'\" [ngValue]=\"device.address\">\n          {{device.name}}\n        </option>\n      </select>\n    </div>\n\n  </div>\n\n  <div class=\"form-group\" *ngIf=\"setup.fieldRequest.devCommand\">\n    <div style=\"margin-bottom: 5px;\">\n      Select parameter:\n    </div>\n\n    <div  class=\"mb-2 drop-down\">\n      <select class=\"col-sm-6 form-control\" id=\"selectParam\" [(ngModel)]=\"action.devCommand\" >\n        <option *ngFor=\"let param of getParamsForSelectedCategory()\" [ngValue]=\"param\">\n          {{param}}\n        </option>\n      </select>\n    </div>\n\n  </div>\n\n  <div class=\"form-group\" *ngIf=\"setup.fieldRequest.devCommandValue\">\n    <div style=\"margin-bottom: 5px;\">\n      Input value:\n    </div>\n\n    <div  class=\"mb-2 drop-down\">\n      <input type=\"number\" [(ngModel)]=\"action.devCommandValue\" class=\"col-sm-6 form-control\" id=\"inputName\" >\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/widget-action-select/widget-action-select.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/widget-action-select/widget-action-select.component.ts ***!
  \***********************************************************************************/
/*! exports provided: WidgetActionSelectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetActionSelectComponent", function() { return WidgetActionSelectComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var _models_widget_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models/widget-action */ "./src/app/models/widget-action.ts");
/* harmony import */ var _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../models/widget-action-setup */ "./src/app/models/widget-action-setup.ts");





var WidgetActionSelectComponent = /** @class */ (function () {
    function WidgetActionSelectComponent(deviceManager) {
        this.deviceManager = deviceManager;
    }
    WidgetActionSelectComponent.prototype.ngOnInit = function () {
        this.deviceManager.loadDevices();
        if (this.setup.defaultCategory) {
            this.action.devCategory = this.setup.defaultCategory;
        }
        if (this.setup.defaultCommand) {
            this.action.devCommand = this.setup.defaultCommand;
        }
        if (!this.action.widgetEvent) {
            this.action.widgetEvent = this.setup.widgetEvent;
        }
        if (!this.action.widgetKey) {
            this.action.widgetKey = this.setup.widgetKey;
        }
    };
    WidgetActionSelectComponent.prototype.getDevicesForSelectedCategory = function () {
        var _this = this;
        if (this.action.devCategory) {
            var devs = this.deviceManager.devicesList.filter(function (item) { return item.category == _this.action.devCategory; });
            console.log('>>>> devices: ', devs);
            return devs;
        }
        return new Array();
    };
    WidgetActionSelectComponent.prototype.getParamsForSelectedCategory = function () {
        var params = new Array();
        if (this.action.devCategory) {
            params = params.concat(this.deviceManager.getCategoryById(this.action.devCategory).commands);
            return params;
        }
        return params;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_4__["WidgetActionSetup"])
    ], WidgetActionSelectComponent.prototype, "setup", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_widget_action__WEBPACK_IMPORTED_MODULE_3__["WidgetAction"])
    ], WidgetActionSelectComponent.prototype, "action", void 0);
    WidgetActionSelectComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-widget-action-select',
            template: __webpack_require__(/*! ./widget-action-select.component.html */ "./src/app/components/widget-action-select/widget-action-select.component.html"),
            styles: [__webpack_require__(/*! ./widget-action-select.component.css */ "./src/app/components/widget-action-select/widget-action-select.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_device_manager_service__WEBPACK_IMPORTED_MODULE_2__["DeviceManagerService"]])
    ], WidgetActionSelectComponent);
    return WidgetActionSelectComponent;
}());



/***/ }),

/***/ "./src/app/components/widget-actions-list/widget-actions-list.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/components/widget-actions-list/widget-actions-list.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".widget-container-style {\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n\r\n.widget-style {\r\n  background-color: #353535;\r\n  border-radius: 10px;\r\n  padding: 10px;\r\n}\r\n\r\n.widget-buttons {\r\n  text-align: center;\r\n  display: grid;\r\n}\r\n\r\n.wide-button{\r\n  width: 50%;\r\n}\r\n\r\n.modal-content-center {\r\n  text-align: center;\r\n}\r\n\r\n.half-size {\r\n  width: 50%;\r\n}\r\n\r\n.modal-body {\r\n  text-align: -webkit-center;\r\n}\r\n\r\n.flash-message-container {\r\n  position: fixed;\r\n  z-index: 10;\r\n  width: 100%;\r\n}\r\n\r\n.flash-message {\r\n  display: block;\r\n  margin-right: 10%;\r\n  margin-left: 10%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy93aWRnZXQtYWN0aW9ucy1saXN0L3dpZGdldC1hY3Rpb25zLWxpc3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCO0FBQ3pCOztBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQixhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtBQUNmOztBQUVBO0VBQ0UsVUFBVTtBQUNaOztBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0UsVUFBVTtBQUNaOztBQUVBO0VBQ0UsMEJBQTBCO0FBQzVCOztBQUVBO0VBQ0UsZUFBZTtFQUNmLFdBQVc7RUFDWCxXQUFXO0FBQ2I7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGdCQUFnQjtBQUNsQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvd2lkZ2V0LWFjdGlvbnMtbGlzdC93aWRnZXQtYWN0aW9ucy1saXN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2lkZ2V0LWNvbnRhaW5lci1zdHlsZSB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4ud2lkZ2V0LXN0eWxlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzUzNTM1O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgcGFkZGluZzogMTBweDtcclxufVxyXG5cclxuLndpZGdldC1idXR0b25zIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZGlzcGxheTogZ3JpZDtcclxufVxyXG5cclxuLndpZGUtYnV0dG9ue1xyXG4gIHdpZHRoOiA1MCU7XHJcbn1cclxuXHJcbi5tb2RhbC1jb250ZW50LWNlbnRlciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uaGFsZi1zaXplIHtcclxuICB3aWR0aDogNTAlO1xyXG59XHJcblxyXG4ubW9kYWwtYm9keSB7XHJcbiAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XHJcbn1cclxuXHJcbi5mbGFzaC1tZXNzYWdlLWNvbnRhaW5lciB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHotaW5kZXg6IDEwO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZmxhc2gtbWVzc2FnZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMCU7XHJcbiAgbWFyZ2luLWxlZnQ6IDEwJTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/widget-actions-list/widget-actions-list.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/components/widget-actions-list/widget-actions-list.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"flash-message-container\">\n  <div class=\"flash-message\">\n    <flash-messages></flash-messages>\n  </div>\n</div>\n<div class=\"container mt-2\">\n  <div class=\"panel\">\n    <div class=\"panel-body\">\n      <div class=\"form-group\">\n        <div>\n          <button type=\"button\" (click)=\"back()\" class=\"btn btn-default mb-2 mr-2\">Back</button>\n          <button type=\"button\" (click)=\"createAction()\" class=\"btn btn-primary mb-2 mr-2\">Add new widget actions</button>\n        </div>\n\n        <div class=\"mt-2 mb-2\">\n          Widget actions list:\n        </div>\n\n        <div class=\"bs-component\">\n\n          <div class=\"list-group\" id=\"actionsList\" *ngIf=\"widget != undefined\">\n\n            <button *ngFor=\"let action of actions\" (click)=\"showActionSelectPopup(action)\"\n                    class=\"list-group-item list-group-item-action d-flex justify-content-between align-items-center\">\n              <div class=\"action-group-name\">\n                {{action.getName(deviceManager)}}\n              </div>\n              <div class=\"action-group-buttons\">\n                <button type=\"button\" class=\"btn btn-danger\" (click)=\"onDeleteActionButton(action, $event)\">Delete</button>\n              </div>\n            </button>\n          </div>\n        </div>\n\n        <div class=\"mt-3\">\n          <!--<button type=\"button\" *ngIf=\"operationManager.changed\" class=\"btn btn-success mb-2 mr-2\" (click)=\"onSaveOperations()\">Save</button>-->\n          <!--<button type=\"button\" *ngIf=\"operationManager.changed\" class=\"btn btn-default mr-4 mb-2\" (click)=\"onCancelOperations()\">Cancel</button>-->\n          <!--<button type=\"button\" (click)=\"goToOperationPage(0)\" class=\"btn btn-primary mb-2 mr-2\">Add new page</button>-->\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/components/widget-actions-list/widget-actions-list.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/widget-actions-list/widget-actions-list.component.ts ***!
  \*********************************************************************************/
/*! exports provided: WidgetActionsListComponent, WidgetActionPopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetActionsListComponent", function() { return WidgetActionsListComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetActionPopupComponent", function() { return WidgetActionPopupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _services_actions_widgets_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/actions-widgets.service */ "./src/app/services/actions-widgets.service.ts");
/* harmony import */ var _models_widget_action__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../models/widget-action */ "./src/app/models/widget-action.ts");
/* harmony import */ var ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/modal/bs-modal-ref.service */ "./node_modules/ngx-bootstrap/modal/bs-modal-ref.service.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/index.js");










var WidgetActionsListComponent = /** @class */ (function () {
    function WidgetActionsListComponent(route, location, flashMessage, deviceManager, actionWidgetsService, router, modalService) {
        this.route = route;
        this.location = location;
        this.flashMessage = flashMessage;
        this.deviceManager = deviceManager;
        this.actionWidgetsService = actionWidgetsService;
        this.router = router;
        this.modalService = modalService;
    }
    WidgetActionsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.deviceManager.loadDevices();
        this.route.queryParams.subscribe(function (params) {
            console.log('params: ', params);
            _this.actionSetup = JSON.parse(params.actionsetup);
            _this.groupkey = params.groupkey;
            // this.location.replaceState(this.location.path().split('?')[0], '');
            _this.loadWidget();
        });
    };
    WidgetActionsListComponent.prototype.back = function () {
        this.router.navigate(['/widgets', 1, this.groupkey]);
    };
    WidgetActionsListComponent.prototype.loadWidget = function () {
        var _this = this;
        this.actionWidgetsService.getWidget(this.actionSetup.widgetKey, (function (callback) {
            _this.widget = callback;
            _this.actions = new Array();
            for (var _i = 0, _a = _this.widget.actions; _i < _a.length; _i++) {
                var action = _a[_i];
                if (action.widgetEvent == _this.actionSetup.widgetEvent) {
                    _this.actions.push(action);
                }
            }
        }));
    };
    WidgetActionsListComponent.prototype.deleteAction = function (action) {
        var _this = this;
        this.actionWidgetsService.deleteWidgetAction(action.key, (function (callback) {
            if (callback['success'] === false) {
                _this.flashMessage.show('Delete widget action failed: ' + callback['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else if (callback['success'] === true) {
                _this.flashMessage.show('Widget action deleted', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
                _this.loadWidget();
            }
        }));
    };
    WidgetActionsListComponent.prototype.createAction = function () {
        this.newAction = new _models_widget_action__WEBPACK_IMPORTED_MODULE_7__["WidgetAction"]();
        this.showActionSelectPopup(this.newAction);
    };
    WidgetActionsListComponent.prototype.showActionSelectPopup = function (action) {
        this.bsModalRef = this.modalService.show(WidgetActionPopupComponent);
        this.bsModalRef.content.closeBtnName = 'Close';
        this.bsModalRef.content.parentController = this;
        this.bsModalRef.content.popupType = 'widget_action';
        this.bsModalRef.content.setup = this.actionSetup;
        this.bsModalRef.content.action = JSON.parse(JSON.stringify(action));
    };
    WidgetActionsListComponent.prototype.onDeleteActionButton = function (action, event) {
        this.bsModalRef = this.modalService.show(WidgetActionPopupComponent);
        this.bsModalRef.content.closeBtnName = 'Close';
        this.bsModalRef.content.parentController = this;
        this.bsModalRef.content.popupType = 'widget_action_delete';
        this.bsModalRef.content.action = action;
        event.preventDefault();
        event.stopPropagation();
    };
    WidgetActionsListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-widget-actions-list',
            template: __webpack_require__(/*! ./widget-actions-list.component.html */ "./src/app/components/widget-actions-list/widget-actions-list.component.html"),
            styles: [__webpack_require__(/*! ./widget-actions-list.component.css */ "./src/app/components/widget-actions-list/widget-actions-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5__["FlashMessagesService"],
            _services_device_manager_service__WEBPACK_IMPORTED_MODULE_4__["DeviceManagerService"],
            _services_actions_widgets_service__WEBPACK_IMPORTED_MODULE_6__["ActionsWidgetsService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_9__["BsModalService"]])
    ], WidgetActionsListComponent);
    return WidgetActionsListComponent;
}());

var WidgetActionPopupComponent = /** @class */ (function () {
    function WidgetActionPopupComponent(bsModalRef) {
        this.bsModalRef = bsModalRef;
    }
    WidgetActionPopupComponent.prototype.ngOnInit = function () {
    };
    WidgetActionPopupComponent.prototype.onSaveWidgetAction = function () {
        var _this = this;
        if (!this.action.key) {
            this.parentController.actionWidgetsService.createNewWidgetAction(this.action, (function (callback) {
                if (callback['success'] === false) {
                    _this.parentController.flashMessage.show('Creating widget action failed: ' + callback['msg'], {
                        cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                        timeout: 5000
                    });
                }
                else if (callback['success'] === true) {
                    _this.parentController.flashMessage.show('Widget action created', {
                        cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                        timeout: 5000
                    });
                    _this.parentController.loadWidget();
                }
            }));
        }
        else {
            this.parentController.actionWidgetsService.updateWidgetAction(this.action, (function (callback) {
                if (callback['success'] === false) {
                    _this.parentController.flashMessage.show('Updating widget action failed: ' + callback['msg'], {
                        cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                        timeout: 5000
                    });
                }
                else if (callback['success'] === true) {
                    _this.parentController.flashMessage.show('Widget action updated', {
                        cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                        timeout: 5000
                    });
                    _this.parentController.loadWidget();
                }
            }));
        }
        this.bsModalRef.hide();
    };
    WidgetActionPopupComponent.prototype.onDeleteWidgetAction = function () {
        this.parentController.deleteAction(this.action);
        this.bsModalRef.hide();
    };
    WidgetActionPopupComponent.prototype.onPopupCancel = function () {
        this.bsModalRef.hide();
    };
    WidgetActionPopupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'modal-content',
            template: "\n     \n      \n      <div *ngIf=\"popupType == 'widget_action_delete'\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title pull-left modal-header-custom\">WARNING</h4>\n        </div>\n        <div class=\"modal-body modal-content-center\">\n            <div>\n                Do you really want to remove the widget action?\n            </div>\n            <div modal-input-section class=\"modal-content-center mt-3\">\n                <button type=\"button\" class=\"btn btn-danger wide-button mr-2\" onclick=\"this.blur();\" (click)=\"onPopupCancel()\">No</button>\n                <button type=\"button\" class=\"btn btn-success\" onclick=\"this.blur();\"  (click)=\"onDeleteWidgetAction()\">Yes</button>\n            </div>\n        </div>\n      </div>\n\n     \n\n      <div *ngIf=\"popupType == 'widget_action'\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title pull-left modal-header-custom\">Edit widget action</h4>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"mt-3\">\n            <app-widget-action-select [(setup)]=\"setup\" [(action)]=\"action\"></app-widget-action-select> \n          </div>\n          <div modal-input-section class=\"modal-content-center mt-5\">\n            <button type=\"button\" class=\"btn btn-default mr-2\" onclick=\"this.blur();\" (click)=\"onPopupCancel()\">Cancel</button>\n            <button type=\"button\" class=\"btn btn-success\" onclick=\"this.blur();\"  (click)=\"onSaveWidgetAction()\">Save</button>\n          </div>\n        </div>\n      </div>\n    \n  ",
            styles: [__webpack_require__(/*! ./widget-actions-list.component.css */ "./src/app/components/widget-actions-list/widget-actions-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_bootstrap_modal_bs_modal_ref_service__WEBPACK_IMPORTED_MODULE_8__["BsModalRef"]])
    ], WidgetActionPopupComponent);
    return WidgetActionPopupComponent;
}());



/***/ }),

/***/ "./src/app/components/widgets/action-widget-buttons/action-widget-buttons.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-buttons/action-widget-buttons.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.widget-name {\r\n  text-align: center;\r\n  background: #151515;\r\n  border-radius: 7px;\r\n  border-width: 1px;\r\n  border-color: #5a5a5a;\r\n  border-style: solid;\r\n  max-width: 140px;\r\n  text-overflow: ellipsis;\r\n  overflow: hidden;\r\n  white-space: nowrap;\r\n  padding-left: 5px;\r\n  padding-right: 5px;\r\n}\r\n\r\n.btn-text {\r\n  text-overflow: ellipsis;\r\n  overflow: hidden;\r\n  white-space: nowrap;\r\n}\r\n\r\n.buttons-container {\r\n  height: 140px;\r\n  \r\n}\r\n\r\n.buttons-row-container {\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n}\r\n\r\n.buttons-half-row {\r\n  height: 50%;\r\n}\r\n\r\n.one-button {\r\n  width: 100px;\r\n  height: 100px;\r\n}\r\n\r\n.single-button {\r\n  width: 55px;\r\n  height: 55px;\r\n}\r\n\r\n.double-button {\r\n  height: 55px;\r\n  width: 120px;\r\n}\r\n\r\n.left-col-button {\r\n  margin-right: 5px;\r\n}\r\n\r\n.right-col-button {\r\n  margin-left: 5px;\r\n}\r\n\r\n.upper-row-buttons {\r\n  padding-top: 5px;\r\n}\r\n\r\n.bottom-row-buttons {\r\n  padding-bottom: 5px;\r\n}\r\n\r\n::ng-deep .buttons-container .buttons-row-container .btn {\r\n  padding: 0px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy93aWRnZXRzL2FjdGlvbi13aWRnZXQtYnV0dG9ucy9hY3Rpb24td2lkZ2V0LWJ1dHRvbnMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsdUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLHVCQUF1QjtFQUN2QixnQkFBZ0I7RUFDaEIsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsYUFBYTs7QUFFZjs7QUFFQTtFQUNFLGFBQWE7RUFDYix1QkFBdUI7RUFDdkIsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsV0FBVztBQUNiOztBQUVBO0VBQ0UsWUFBWTtFQUNaLGFBQWE7QUFDZjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxZQUFZO0VBQ1osWUFBWTtBQUNkOztBQUVBO0VBQ0UsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsWUFBWTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy93aWRnZXRzL2FjdGlvbi13aWRnZXQtYnV0dG9ucy9hY3Rpb24td2lkZ2V0LWJ1dHRvbnMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4ud2lkZ2V0LW5hbWUge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kOiAjMTUxNTE1O1xyXG4gIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICBib3JkZXItd2lkdGg6IDFweDtcclxuICBib3JkZXItY29sb3I6ICM1YTVhNWE7XHJcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICBtYXgtd2lkdGg6IDE0MHB4O1xyXG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbi5idG4tdGV4dCB7XHJcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG59XHJcblxyXG4uYnV0dG9ucy1jb250YWluZXIge1xyXG4gIGhlaWdodDogMTQwcHg7XHJcbiAgXHJcbn1cclxuXHJcbi5idXR0b25zLXJvdy1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLmJ1dHRvbnMtaGFsZi1yb3cge1xyXG4gIGhlaWdodDogNTAlO1xyXG59XHJcblxyXG4ub25lLWJ1dHRvbiB7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbn1cclxuXHJcbi5zaW5nbGUtYnV0dG9uIHtcclxuICB3aWR0aDogNTVweDtcclxuICBoZWlnaHQ6IDU1cHg7XHJcbn1cclxuXHJcbi5kb3VibGUtYnV0dG9uIHtcclxuICBoZWlnaHQ6IDU1cHg7XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG59XHJcblxyXG4ubGVmdC1jb2wtYnV0dG9uIHtcclxuICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG5cclxuLnJpZ2h0LWNvbC1idXR0b24ge1xyXG4gIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbn1cclxuXHJcbi51cHBlci1yb3ctYnV0dG9ucyB7XHJcbiAgcGFkZGluZy10b3A6IDVweDtcclxufVxyXG5cclxuLmJvdHRvbS1yb3ctYnV0dG9ucyB7XHJcbiAgcGFkZGluZy1ib3R0b206IDVweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5idXR0b25zLWNvbnRhaW5lciAuYnV0dG9ucy1yb3ctY29udGFpbmVyIC5idG4ge1xyXG4gIHBhZGRpbmc6IDBweDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/widgets/action-widget-buttons/action-widget-buttons.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-buttons/action-widget-buttons.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"widget\">\n\t\n  <div *ngIf=\"widget.type=='1-button'\" class=\"buttons-container buttons-row-container\">\n  \t<button type=\"button\" class=\"btn one-button btn-text\" [ngClass]=\"(buttonColors[0])?'btn-danger':'btn-secondary'\" (click)=\"onClick(1)\">{{buttonNames[0]}}</button>\n  </div>\n\n  <div *ngIf=\"widget.type=='2-buttons'\" class=\"buttons-container\">\n  \t<div class=\"buttons-row-container buttons-half-row upper-row-buttons\">\n  \t\t<button type=\"button\" class=\"btn double-button btn-text\" [ngClass]=\"(buttonColors[0])?'btn-danger':'btn-secondary'\" (click)=\"onClick(1)\">{{buttonNames[0]}}</button>\n  \t</div>\n  \t<div class=\"buttons-row-container buttons-half-row bottom-row-buttons\">\n  \t\t<button type=\"button\" class=\"btn double-button btn-text\" [ngClass]=\"(buttonColors[1])?'btn-danger':'btn-secondary'\" (click)=\"onClick(2)\">{{buttonNames[1]}}</button>\n  \t</div>\n  </div>\n\n  <div *ngIf=\"widget.type=='3-buttons'\" class=\"buttons-container\">\n  \t<div class=\"buttons-row-container buttons-half-row upper-row-buttons\">\n  \t\t<button type=\"button\" class=\"btn single-button left-col-button btn-text\" [ngClass]=\"(buttonColors[0])?'btn-danger':'btn-secondary'\" (click)=\"onClick(1)\">{{buttonNames[0]}}</button>\n  \t\t<button type=\"button\" class=\"btn single-button right-col-button btn-text\" [ngClass]=\"(buttonColors[1])?'btn-danger':'btn-secondary'\" (click)=\"onClick(2)\">{{buttonNames[1]}}</button>\n  \t</div>\n  \t<div class=\"buttons-row-container buttons-half-row bottom-row-buttons\">\n  \t\t<button type=\"button\" class=\"btn double-button btn-text\" [ngClass]=\"(buttonColors[2])?'btn-danger':'btn-secondary'\" (click)=\"onClick(3)\">{{buttonNames[2]}}</button>\n  \t</div>\n  </div>\n\n  <div *ngIf=\"widget.type=='4-buttons'\" class=\"buttons-container\">\n  \t<div class=\"buttons-row-container buttons-half-row upper-row-buttons\">\n  \t\t<button type=\"button\" class=\"btn single-button left-col-button btn-text\" [ngClass]=\"(buttonColors[0])?'btn-danger':'btn-secondary'\" (click)=\"onClick(1)\">{{buttonNames[0]}}</button>\n  \t\t<button type=\"button\" class=\"btn single-button right-col-button btn-text\" [ngClass]=\"(buttonColors[1])?'btn-danger':'btn-secondary'\" (click)=\"onClick(2)\">{{buttonNames[1]}}</button>\n  \t</div>\n  \t<div class=\"buttons-row-container buttons-half-row bottom-row-buttons\">\n  \t\t<button type=\"button\" class=\"btn single-button left-col-button btn-text\" [ngClass]=\"(buttonColors[2])?'btn-danger':'btn-secondary'\" (click)=\"onClick(3)\">{{buttonNames[2]}}</button>\n  \t\t<button type=\"button\" class=\"btn single-button right-col-button btn-text\" [ngClass]=\"(buttonColors[3])?'btn-danger':'btn-secondary'\" (click)=\"onClick(4)\">{{buttonNames[3]}}</button>\n  \t</div>\n  </div>\n\n  <div *ngIf=\"widget.name != undefined\" class=\"widget-name mt-2\" (click)=\"onNameClicked()\">{{widget.name}}</div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/widgets/action-widget-buttons/action-widget-buttons.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-buttons/action-widget-buttons.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: ActionWidgetButtonsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionWidgetButtonsComponent", function() { return ActionWidgetButtonsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_action_widget__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../models/action-widget */ "./src/app/models/action-widget.ts");
/* harmony import */ var _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../models/widget-action-setup */ "./src/app/models/widget-action-setup.ts");
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../models/consts */ "./src/app/models/consts.ts");






var ActionWidgetButtonsComponent = /** @class */ (function () {
    function ActionWidgetButtonsComponent() {
        this.nameClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onAction = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onActionSetup = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ActionWidgetButtonsComponent.prototype.ngOnInit = function () {
        this.btnMap = { 1: _models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_EVENT_BUTTON_1"], 2: _models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_EVENT_BUTTON_2"], 3: _models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_EVENT_BUTTON_3"], 4: _models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_EVENT_BUTTON_4"] };
        this.buttonNames = ['1', '2', '3', '4'];
        this.buttonColors = [false, false, false, false];
        if (this.widget.details) {
            var widgetDetails = JSON.parse(this.widget.details);
            var buttonSettings = widgetDetails[_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_BUTTON_SETTINGS"]] ? widgetDetails[_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_BUTTON_SETTINGS"]] : [];
            for (var i = 0; i < buttonSettings.length; i++) {
                this.buttonNames[i] = buttonSettings[i][_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_BUTTON_NAME"]] ? buttonSettings[i][_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_BUTTON_NAME"]] : '' + (i + 1);
                this.buttonColors[i] = (buttonSettings[i][_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_BUTTON_COLOR"]] != undefined) ? buttonSettings[i][_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_BUTTON_COLOR"]] : false;
            }
        }
        if (this.widget && this.widget.actions) {
            this.actionMap = {};
            for (var _i = 0, _a = this.widget.actions; _i < _a.length; _i++) {
                var action = _a[_i];
                if (!this.actionMap[action.widgetEvent]) {
                    this.actionMap[action.widgetEvent] = [];
                }
                this.actionMap[action.widgetEvent].push(action);
            }
        }
    };
    ActionWidgetButtonsComponent.prototype.onClick = function (btnId) {
        if (this.editMode) {
            var setup = new _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_3__["WidgetActionSetup"](new _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_3__["RequestedFields"](true, true, true, true), null, null, this.widget.key, this.btnMap[btnId]);
            this.onActionSetup.emit(setup);
        }
        else {
            var actions = this.actionMap[this.btnMap[btnId]];
            if (actions) {
                for (var _i = 0, actions_1 = actions; _i < actions_1.length; _i++) {
                    var action = actions_1[_i];
                    this.onAction.emit(action);
                }
            }
        }
    };
    ActionWidgetButtonsComponent.prototype.onNameClicked = function () {
        if (this.editMode) {
            this.nameClicked.emit(this.widget);
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetButtonsComponent.prototype, "nameClicked", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetButtonsComponent.prototype, "onAction", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetButtonsComponent.prototype, "onActionSetup", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_action_widget__WEBPACK_IMPORTED_MODULE_2__["ActionWidget"])
    ], ActionWidgetButtonsComponent.prototype, "widget", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], ActionWidgetButtonsComponent.prototype, "editMode", void 0);
    ActionWidgetButtonsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-action-widget-buttons',
            template: __webpack_require__(/*! ./action-widget-buttons.component.html */ "./src/app/components/widgets/action-widget-buttons/action-widget-buttons.component.html"),
            styles: [__webpack_require__(/*! ./action-widget-buttons.component.css */ "./src/app/components/widgets/action-widget-buttons/action-widget-buttons.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ActionWidgetButtonsComponent);
    return ActionWidgetButtonsComponent;
}());



/***/ }),

/***/ "./src/app/components/widgets/action-widget-color-picker/action-widget-color-picker.component.css":
/*!********************************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-color-picker/action-widget-color-picker.component.css ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".widget-name {\r\n  text-align: center;\r\n  background: #151515;\r\n  border-radius: 7px;\r\n  border-width: 1px;\r\n  border-color: #5a5a5a;\r\n  border-style: solid;\r\n\tmax-width: 140px;\r\n\ttext-overflow: ellipsis;\r\n\toverflow: hidden;\r\n\twhite-space: nowrap;\r\n\tpadding-left: 5px;\r\n\tpadding-right: 5px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy93aWRnZXRzL2FjdGlvbi13aWRnZXQtY29sb3ItcGlja2VyL2FjdGlvbi13aWRnZXQtY29sb3ItcGlja2VyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIscUJBQXFCO0VBQ3JCLG1CQUFtQjtDQUNwQixnQkFBZ0I7Q0FDaEIsdUJBQXVCO0NBQ3ZCLGdCQUFnQjtDQUNoQixtQkFBbUI7Q0FDbkIsaUJBQWlCO0NBQ2pCLGtCQUFrQjtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvd2lkZ2V0cy9hY3Rpb24td2lkZ2V0LWNvbG9yLXBpY2tlci9hY3Rpb24td2lkZ2V0LWNvbG9yLXBpY2tlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndpZGdldC1uYW1lIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgYmFja2dyb3VuZDogIzE1MTUxNTtcclxuICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgYm9yZGVyLXdpZHRoOiAxcHg7XHJcbiAgYm9yZGVyLWNvbG9yOiAjNWE1YTVhO1xyXG4gIGJvcmRlci1zdHlsZTogc29saWQ7XHJcblx0bWF4LXdpZHRoOiAxNDBweDtcclxuXHR0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuXHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcblx0cGFkZGluZy1sZWZ0OiA1cHg7XHJcblx0cGFkZGluZy1yaWdodDogNXB4O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/widgets/action-widget-color-picker/action-widget-color-picker.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-color-picker/action-widget-color-picker.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"widget\">\n      <app-radial-color-picker\n      [size]=\"140\"\n      [centerRadius]=\"20\"\n      [redValue]=\"redValue\"\n      [greenValue]=\"greenValue\"\n      [blueValue]=\"blueValue\"\n      (color)=\"onColorPickerChange($event)\"\n      (centerRadiusClick)=\"onCenterRadiusClick()\"\n      ></app-radial-color-picker>\n  <div *ngIf=\"widget.name != undefined\" class=\"widget-name mt-2\" (click)=\"onNameClicked()\">{{widget.name}}</div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/widgets/action-widget-color-picker/action-widget-color-picker.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-color-picker/action-widget-color-picker.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: ActionWidgetColorPickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionWidgetColorPickerComponent", function() { return ActionWidgetColorPickerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_action_widget__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../models/action-widget */ "./src/app/models/action-widget.ts");
/* harmony import */ var _models_widget_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../models/widget-action */ "./src/app/models/widget-action.ts");
/* harmony import */ var _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../models/widget-action-setup */ "./src/app/models/widget-action-setup.ts");
/* harmony import */ var _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../utils/deviceConsts */ "./src/app/utils/deviceConsts.ts");
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../models/consts */ "./src/app/models/consts.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/observable/TimerObservable */ "./node_modules/rxjs-compat/_esm5/observable/TimerObservable.js");










var ActionWidgetColorPickerComponent = /** @class */ (function () {
    function ActionWidgetColorPickerComponent(deviceManager) {
        this.deviceManager = deviceManager;
        this.nameClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onAction = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onActionSetup = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.setRgbMode = true;
    }
    ActionWidgetColorPickerComponent.prototype.ngOnInit = function () {
        if (this.widget && this.widget.actions) {
            this.actionMap = {};
            for (var _i = 0, _a = this.widget.actions; _i < _a.length; _i++) {
                var action = _a[_i];
                if (!this.actionMap[action.widgetEvent]) {
                    this.actionMap[action.widgetEvent] = [];
                }
                this.actionMap[action.widgetEvent].push(action);
            }
        }
    };
    ActionWidgetColorPickerComponent.prototype.ngOnDestroy = function () {
        this.unsubscribeRgbModeSubscription();
    };
    ActionWidgetColorPickerComponent.prototype.unsubscribeRgbModeSubscription = function () {
        if (this.rgbModeSubscription) {
            this.rgbModeSubscription.unsubscribe();
        }
    };
    ActionWidgetColorPickerComponent.prototype.startRgbModeSubscription = function () {
        var _this = this;
        var timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_8__["TimerObservable"].create(1000, 100000);
        this.rgbModeSubscription = timer.subscribe(function (t) {
            _this.setRgbMode = true;
        });
    };
    ActionWidgetColorPickerComponent.prototype.onColorPickerChange = function (event) {
        console.log('>>>> event: ', event);
        if (this.editMode) {
            var setup = new _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_4__["WidgetActionSetup"](new _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_4__["RequestedFields"](false, true, false, false), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].DEV_CATEGORY_RGB, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, this.widget.key, _models_consts__WEBPACK_IMPORTED_MODULE_6__["WIDGET_EVENT_RGB"]);
            this.onActionSetup.emit(setup);
        }
        else {
            var actions = this.actionMap[_models_consts__WEBPACK_IMPORTED_MODULE_6__["WIDGET_EVENT_RGB"]];
            if (actions) {
                for (var _i = 0, actions_1 = actions; _i < actions_1.length; _i++) {
                    var action = actions_1[_i];
                    if (this.setRgbMode) {
                        var helpAction = new _models_widget_action__WEBPACK_IMPORTED_MODULE_3__["WidgetAction"]();
                        helpAction.devCategory = action.devCategory;
                        helpAction.devAddress = action.devAddress;
                        helpAction.devCommand = _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE;
                        helpAction.devCommandValue = 4;
                        this.onAction.emit(helpAction);
                        this.setRgbMode = false;
                    }
                    var color = JSON.parse(event);
                    this.redValue = color.red * 0xf;
                    this.greenValue = color.green * 0xf;
                    this.blueValue = color.blue * 0xf;
                    var rgb = (this.redValue * 0x1000000) + (this.greenValue * 0x1000) + this.blueValue;
                    action.devCommandValue = rgb;
                    this.onAction.emit(action);
                }
                this.unsubscribeRgbModeSubscription();
                this.startRgbModeSubscription();
            }
        }
    };
    ActionWidgetColorPickerComponent.prototype.onCenterRadiusClick = function () {
        console.log('>>>> center radius click');
        if (this.editMode) {
            var setup = new _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_4__["WidgetActionSetup"](new _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_4__["RequestedFields"](false, true, true, true), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].DEV_CATEGORY_RGB, null, this.widget.key, _models_consts__WEBPACK_IMPORTED_MODULE_6__["WIDGET_EVENT_RADIUS_CENTER"]);
            this.onActionSetup.emit(setup);
        }
        else {
            var actions = this.actionMap[_models_consts__WEBPACK_IMPORTED_MODULE_6__["WIDGET_EVENT_RADIUS_CENTER"]];
            if (actions) {
                for (var _i = 0, actions_2 = actions; _i < actions_2.length; _i++) {
                    var action = actions_2[_i];
                    this.redValue = 0;
                    this.greenValue = 0;
                    this.blueValue = 0;
                    this.onAction.emit(action);
                }
            }
        }
    };
    ActionWidgetColorPickerComponent.prototype.onNameClicked = function () {
        if (this.editMode) {
            this.nameClicked.emit(this.widget);
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetColorPickerComponent.prototype, "nameClicked", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetColorPickerComponent.prototype, "onAction", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetColorPickerComponent.prototype, "onActionSetup", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_action_widget__WEBPACK_IMPORTED_MODULE_2__["ActionWidget"])
    ], ActionWidgetColorPickerComponent.prototype, "widget", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], ActionWidgetColorPickerComponent.prototype, "editMode", void 0);
    ActionWidgetColorPickerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-action-widget-color-picker',
            template: __webpack_require__(/*! ./action-widget-color-picker.component.html */ "./src/app/components/widgets/action-widget-color-picker/action-widget-color-picker.component.html"),
            styles: [__webpack_require__(/*! ./action-widget-color-picker.component.css */ "./src/app/components/widgets/action-widget-color-picker/action-widget-color-picker.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_device_manager_service__WEBPACK_IMPORTED_MODULE_7__["DeviceManagerService"]])
    ], ActionWidgetColorPickerComponent);
    return ActionWidgetColorPickerComponent;
}());



/***/ }),

/***/ "./src/app/components/widgets/action-widget-rgb-slider/action-widget-rgb-slider.component.css":
/*!****************************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-rgb-slider/action-widget-rgb-slider.component.css ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n::ng-deep .rgb-sliders .red-slider .ng5-slider .ng5-slider-pointer {\r\n  background: rgb(255, 0, 0) !important;\r\n  top: -8px;\r\n  height: 20px;\r\n  width: 20px;\r\n  left: 0px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .green-slider .ng5-slider .ng5-slider-pointer {\r\n  background: rgb(0, 255, 0) !important;\r\n  top: -8px;\r\n  height: 20px;\r\n  width: 20px;\r\n  left: 0px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .blue-slider .ng5-slider .ng5-slider-pointer {\r\n  background: rgb(0, 0, 255) !important;\r\n  top: -8px;\r\n  height: 20px;\r\n  width: 20px;\r\n  left: 0px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .red-slider .ng5-slider .ng5-slider-pointer:after {\r\n  width: 0px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .green-slider .ng5-slider .ng5-slider-pointer:after {\r\n  width: 0px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .blue-slider .ng5-slider .ng5-slider-pointer:after {\r\n  width: 0px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .red-slider .ng5-slider .ng5-slider-bubble {\r\n  font-size: 12px;\r\n  bottom: 7px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .green-slider .ng5-slider .ng5-slider-bubble {\r\n  font-size: 12px;\r\n  bottom: 7px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .blue-slider .ng5-slider .ng5-slider-bubble {\r\n  font-size: 12px;\r\n  bottom: 7px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .red-slider .ng5-slider {\r\n  height: 0px;\r\n  margin-top: 29px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .green-slider .ng5-slider {\r\n  height: 0px;\r\n  margin-top: 29px;\r\n}\r\n\r\n::ng-deep .rgb-sliders .blue-slider .ng5-slider {\r\n  height: 0px;\r\n  margin-top: 29px;\r\n}\r\n\r\n.rgb-sliders {\r\n  height: 140px;\r\n}\r\n\r\n.widget-name {\r\n  text-align: center;\r\n  background: #151515;\r\n  border-radius: 7px;\r\n  border-width: 1px;\r\n  border-color: #5a5a5a;\r\n  border-style: solid;\r\n  max-width: 140px;\r\n  text-overflow: ellipsis;\r\n  overflow: hidden;\r\n  white-space: nowrap;\r\n  padding-left: 5px;\r\n  padding-right: 5px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy93aWRnZXRzL2FjdGlvbi13aWRnZXQtcmdiLXNsaWRlci9hY3Rpb24td2lkZ2V0LXJnYi1zbGlkZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7RUFDRSxxQ0FBcUM7RUFDckMsU0FBUztFQUNULFlBQVk7RUFDWixXQUFXO0VBQ1gsU0FBUztBQUNYOztBQUVBO0VBQ0UscUNBQXFDO0VBQ3JDLFNBQVM7RUFDVCxZQUFZO0VBQ1osV0FBVztFQUNYLFNBQVM7QUFDWDs7QUFFQTtFQUNFLHFDQUFxQztFQUNyQyxTQUFTO0VBQ1QsWUFBWTtFQUNaLFdBQVc7RUFDWCxTQUFTO0FBQ1g7O0FBRUE7RUFDRSxVQUFVO0FBQ1o7O0FBRUE7RUFDRSxVQUFVO0FBQ1o7O0FBRUE7RUFDRSxVQUFVO0FBQ1o7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsV0FBVztBQUNiOztBQUVBO0VBQ0UsZUFBZTtFQUNmLFdBQVc7QUFDYjs7QUFFQTtFQUNFLGVBQWU7RUFDZixXQUFXO0FBQ2I7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsdUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtBQUNwQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvd2lkZ2V0cy9hY3Rpb24td2lkZ2V0LXJnYi1zbGlkZXIvYWN0aW9uLXdpZGdldC1yZ2Itc2xpZGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuOjpuZy1kZWVwIC5yZ2Itc2xpZGVycyAucmVkLXNsaWRlciAubmc1LXNsaWRlciAubmc1LXNsaWRlci1wb2ludGVyIHtcclxuICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAwLCAwKSAhaW1wb3J0YW50O1xyXG4gIHRvcDogLThweDtcclxuICBoZWlnaHQ6IDIwcHg7XHJcbiAgd2lkdGg6IDIwcHg7XHJcbiAgbGVmdDogMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnJnYi1zbGlkZXJzIC5ncmVlbi1zbGlkZXIgLm5nNS1zbGlkZXIgLm5nNS1zbGlkZXItcG9pbnRlciB7XHJcbiAgYmFja2dyb3VuZDogcmdiKDAsIDI1NSwgMCkgIWltcG9ydGFudDtcclxuICB0b3A6IC04cHg7XHJcbiAgaGVpZ2h0OiAyMHB4O1xyXG4gIHdpZHRoOiAyMHB4O1xyXG4gIGxlZnQ6IDBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5yZ2Itc2xpZGVycyAuYmx1ZS1zbGlkZXIgLm5nNS1zbGlkZXIgLm5nNS1zbGlkZXItcG9pbnRlciB7XHJcbiAgYmFja2dyb3VuZDogcmdiKDAsIDAsIDI1NSkgIWltcG9ydGFudDtcclxuICB0b3A6IC04cHg7XHJcbiAgaGVpZ2h0OiAyMHB4O1xyXG4gIHdpZHRoOiAyMHB4O1xyXG4gIGxlZnQ6IDBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5yZ2Itc2xpZGVycyAucmVkLXNsaWRlciAubmc1LXNsaWRlciAubmc1LXNsaWRlci1wb2ludGVyOmFmdGVyIHtcclxuICB3aWR0aDogMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnJnYi1zbGlkZXJzIC5ncmVlbi1zbGlkZXIgLm5nNS1zbGlkZXIgLm5nNS1zbGlkZXItcG9pbnRlcjphZnRlciB7XHJcbiAgd2lkdGg6IDBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5yZ2Itc2xpZGVycyAuYmx1ZS1zbGlkZXIgLm5nNS1zbGlkZXIgLm5nNS1zbGlkZXItcG9pbnRlcjphZnRlciB7XHJcbiAgd2lkdGg6IDBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5yZ2Itc2xpZGVycyAucmVkLXNsaWRlciAubmc1LXNsaWRlciAubmc1LXNsaWRlci1idWJibGUge1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBib3R0b206IDdweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5yZ2Itc2xpZGVycyAuZ3JlZW4tc2xpZGVyIC5uZzUtc2xpZGVyIC5uZzUtc2xpZGVyLWJ1YmJsZSB7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIGJvdHRvbTogN3B4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnJnYi1zbGlkZXJzIC5ibHVlLXNsaWRlciAubmc1LXNsaWRlciAubmc1LXNsaWRlci1idWJibGUge1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBib3R0b206IDdweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5yZ2Itc2xpZGVycyAucmVkLXNsaWRlciAubmc1LXNsaWRlciB7XHJcbiAgaGVpZ2h0OiAwcHg7XHJcbiAgbWFyZ2luLXRvcDogMjlweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5yZ2Itc2xpZGVycyAuZ3JlZW4tc2xpZGVyIC5uZzUtc2xpZGVyIHtcclxuICBoZWlnaHQ6IDBweDtcclxuICBtYXJnaW4tdG9wOiAyOXB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnJnYi1zbGlkZXJzIC5ibHVlLXNsaWRlciAubmc1LXNsaWRlciB7XHJcbiAgaGVpZ2h0OiAwcHg7XHJcbiAgbWFyZ2luLXRvcDogMjlweDtcclxufVxyXG5cclxuLnJnYi1zbGlkZXJzIHtcclxuICBoZWlnaHQ6IDE0MHB4O1xyXG59XHJcblxyXG4ud2lkZ2V0LW5hbWUge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kOiAjMTUxNTE1O1xyXG4gIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICBib3JkZXItd2lkdGg6IDFweDtcclxuICBib3JkZXItY29sb3I6ICM1YTVhNWE7XHJcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICBtYXgtd2lkdGg6IDE0MHB4O1xyXG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/widgets/action-widget-rgb-slider/action-widget-rgb-slider.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-rgb-slider/action-widget-rgb-slider.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"widget\">\n  <div class=\"rgb-sliders\">\n    <div class=\"red-slider\">\n      <ng5-slider id=\"red\" [(value)]=\"redValue\" [options]=\"redSliderOptions\" (userChange)=\"onUserChange('red', $event)\"></ng5-slider>\n    </div>\n    <div class=\"green-slider\">\n      <ng5-slider id=\"green\" [(value)]=\"greenValue\" [options]=\"greenSliderOptions\" (userChange)=\"onUserChange('green', $event)\"></ng5-slider>\n    </div>\n    <div class=\"blue-slider\">\n      <ng5-slider id=\"blue\" [(value)]=\"blueValue\" [options]=\"blueSliderOptions\" (userChange)=\"onUserChange('blue', $event)\"></ng5-slider>\n    </div>\n  </div>\n  <div *ngIf=\"widget.name != undefined\" class=\"widget-name mt-2\" (click)=\"onNameClicked()\">{{widget.name}}</div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/widgets/action-widget-rgb-slider/action-widget-rgb-slider.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-rgb-slider/action-widget-rgb-slider.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: ActionWidgetRgbSliderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionWidgetRgbSliderComponent", function() { return ActionWidgetRgbSliderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_action_widget__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../models/action-widget */ "./src/app/models/action-widget.ts");
/* harmony import */ var _models_widget_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../models/widget-action */ "./src/app/models/widget-action.ts");
/* harmony import */ var _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../models/widget-action-setup */ "./src/app/models/widget-action-setup.ts");
/* harmony import */ var _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../utils/deviceConsts */ "./src/app/utils/deviceConsts.ts");
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../models/consts */ "./src/app/models/consts.ts");
/* harmony import */ var _services_device_manager_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");
/* harmony import */ var rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/observable/TimerObservable */ "./node_modules/rxjs-compat/_esm5/observable/TimerObservable.js");











var ActionWidgetRgbSliderComponent = /** @class */ (function () {
    function ActionWidgetRgbSliderComponent(deviceManager) {
        this.deviceManager = deviceManager;
        this.nameClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onAction = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onActionSetup = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.setRgbMode = true;
    }
    ActionWidgetRgbSliderComponent.prototype.ngOnInit = function () {
        this.redSliderOptions = new ng5_slider__WEBPACK_IMPORTED_MODULE_8__["Options"]();
        this.redSliderOptions.floor = 0;
        this.redSliderOptions.ceil = 0xfff;
        this.greenSliderOptions = new ng5_slider__WEBPACK_IMPORTED_MODULE_8__["Options"]();
        this.greenSliderOptions.floor = 0;
        this.greenSliderOptions.ceil = 0xfff;
        this.blueSliderOptions = new ng5_slider__WEBPACK_IMPORTED_MODULE_8__["Options"]();
        this.blueSliderOptions.floor = 0;
        this.blueSliderOptions.ceil = 0xfff;
        this.redValue = 0;
        this.greenValue = 0;
        this.blueValue = 0;
        if (this.widget && this.widget.actions) {
            this.actionMap = {};
            for (var _i = 0, _a = this.widget.actions; _i < _a.length; _i++) {
                var action = _a[_i];
                if (!this.actionMap[action.widgetEvent]) {
                    this.actionMap[action.widgetEvent] = [];
                }
                this.actionMap[action.widgetEvent].push(action);
            }
        }
    };
    ActionWidgetRgbSliderComponent.prototype.ngOnDestroy = function () {
        this.unsubscribeRgbModeSubscription();
    };
    ActionWidgetRgbSliderComponent.prototype.unsubscribeRgbModeSubscription = function () {
        if (this.rgbModeSubscription) {
            this.rgbModeSubscription.unsubscribe();
        }
    };
    ActionWidgetRgbSliderComponent.prototype.startRgbModeSubscription = function () {
        var _this = this;
        var timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_9__["TimerObservable"].create(1000, 100000);
        this.rgbModeSubscription = timer.subscribe(function (t) {
            _this.setRgbMode = true;
        });
    };
    ActionWidgetRgbSliderComponent.prototype.onUserChange = function (param, event) {
        if (this.editMode) {
            var setup = new _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_4__["WidgetActionSetup"](new _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_4__["RequestedFields"](false, true, false, false), _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].DEV_CATEGORY_RGB, _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_RGB, this.widget.key, _models_consts__WEBPACK_IMPORTED_MODULE_6__["WIDGET_EVENT_RGB"]);
            this.onActionSetup.emit(setup);
        }
        else {
            var actions = this.actionMap[_models_consts__WEBPACK_IMPORTED_MODULE_6__["WIDGET_EVENT_RGB"]];
            if (actions) {
                for (var _i = 0, actions_1 = actions; _i < actions_1.length; _i++) {
                    var action = actions_1[_i];
                    if (this.setRgbMode) {
                        var helpAction = new _models_widget_action__WEBPACK_IMPORTED_MODULE_3__["WidgetAction"]();
                        helpAction.devCategory = action.devCategory;
                        helpAction.devAddress = action.devAddress;
                        helpAction.devCommand = _utils_deviceConsts__WEBPACK_IMPORTED_MODULE_5__["DeviceConsts"].PARAM_MODE;
                        helpAction.devCommandValue = 4;
                        this.onAction.emit(helpAction);
                        this.setRgbMode = false;
                    }
                    var rgb = (this.redValue * 0x1000000) + (this.greenValue * 0x1000) + this.blueValue;
                    action.devCommandValue = rgb;
                    this.onAction.emit(action);
                }
                this.unsubscribeRgbModeSubscription();
                this.startRgbModeSubscription();
            }
        }
    };
    ActionWidgetRgbSliderComponent.prototype.onNameClicked = function () {
        if (this.editMode) {
            this.nameClicked.emit(this.widget);
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetRgbSliderComponent.prototype, "nameClicked", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetRgbSliderComponent.prototype, "onAction", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetRgbSliderComponent.prototype, "onActionSetup", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_action_widget__WEBPACK_IMPORTED_MODULE_2__["ActionWidget"])
    ], ActionWidgetRgbSliderComponent.prototype, "widget", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], ActionWidgetRgbSliderComponent.prototype, "editMode", void 0);
    ActionWidgetRgbSliderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-action-widget-rgb-slider',
            template: __webpack_require__(/*! ./action-widget-rgb-slider.component.html */ "./src/app/components/widgets/action-widget-rgb-slider/action-widget-rgb-slider.component.html"),
            styles: [__webpack_require__(/*! ./action-widget-rgb-slider.component.css */ "./src/app/components/widgets/action-widget-rgb-slider/action-widget-rgb-slider.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_device_manager_service__WEBPACK_IMPORTED_MODULE_7__["DeviceManagerService"]])
    ], ActionWidgetRgbSliderComponent);
    return ActionWidgetRgbSliderComponent;
}());



/***/ }),

/***/ "./src/app/components/widgets/action-widget-slider/action-widget-slider.component.css":
/*!********************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-slider/action-widget-slider.component.css ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep .custom-slider .ng5-slider .ng5-slider-pointer {\r\n  top: -8px;\r\n  height: 20px;\r\n  width: 20px;\r\n  left: 0px;\r\n}\r\n\r\n\r\n::ng-deep .custom-slider .ng5-slider .ng5-slider-pointer:after {\r\n  width: 0px;\r\n}\r\n\r\n\r\n::ng-deep .custom-slider .ng5-slider .ng5-slider-bubble {\r\n  font-size: 12px;\r\n  bottom: 7px;\r\n}\r\n\r\n\r\n::ng-deep .custom-slider .ng5-slider {\r\n  height: 0px;\r\n  margin-top: 29px;\r\n}\r\n\r\n\r\n.custom-slider {\r\n  height: 140px;\r\n}\r\n\r\n\r\n.widget-name {\r\n  text-align: center;\r\n  background: #151515;\r\n  border-radius: 7px;\r\n  border-width: 1px;\r\n  border-color: #5a5a5a;\r\n  border-style: solid;\r\n  max-width: 140px;\r\n  text-overflow: ellipsis;\r\n  overflow: hidden;\r\n  white-space: nowrap;\r\n  padding-left: 5px;\r\n  padding-right: 5px;\r\n}\r\n\r\n\r\n.pwm-icon{\r\n  border-radius: 25px;\r\n    border: 2px solid;\r\n    float: right;\r\n    height: 50px;\r\n    width: 50px;\r\n    background: #000000;\r\n}\r\n\r\n\r\n.icon-container {\r\n  text-align: center;\r\n  display: inline-block;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy93aWRnZXRzL2FjdGlvbi13aWRnZXQtc2xpZGVyL2FjdGlvbi13aWRnZXQtc2xpZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxTQUFTO0VBQ1QsWUFBWTtFQUNaLFdBQVc7RUFDWCxTQUFTO0FBQ1g7OztBQUdBO0VBQ0UsVUFBVTtBQUNaOzs7QUFFQTtFQUNFLGVBQWU7RUFDZixXQUFXO0FBQ2I7OztBQUVBO0VBQ0UsV0FBVztFQUNYLGdCQUFnQjtBQUNsQjs7O0FBRUE7RUFDRSxhQUFhO0FBQ2Y7OztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLHVCQUF1QjtFQUN2QixnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7QUFDcEI7OztBQUVBO0VBQ0UsbUJBQW1CO0lBQ2pCLGlCQUFpQjtJQUNqQixZQUFZO0lBQ1osWUFBWTtJQUNaLFdBQVc7SUFDWCxtQkFBbUI7QUFDdkI7OztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLHFCQUFxQjtBQUN2QiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvd2lkZ2V0cy9hY3Rpb24td2lkZ2V0LXNsaWRlci9hY3Rpb24td2lkZ2V0LXNsaWRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOjpuZy1kZWVwIC5jdXN0b20tc2xpZGVyIC5uZzUtc2xpZGVyIC5uZzUtc2xpZGVyLXBvaW50ZXIge1xyXG4gIHRvcDogLThweDtcclxuICBoZWlnaHQ6IDIwcHg7XHJcbiAgd2lkdGg6IDIwcHg7XHJcbiAgbGVmdDogMHB4O1xyXG59XHJcblxyXG5cclxuOjpuZy1kZWVwIC5jdXN0b20tc2xpZGVyIC5uZzUtc2xpZGVyIC5uZzUtc2xpZGVyLXBvaW50ZXI6YWZ0ZXIge1xyXG4gIHdpZHRoOiAwcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAuY3VzdG9tLXNsaWRlciAubmc1LXNsaWRlciAubmc1LXNsaWRlci1idWJibGUge1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBib3R0b206IDdweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5jdXN0b20tc2xpZGVyIC5uZzUtc2xpZGVyIHtcclxuICBoZWlnaHQ6IDBweDtcclxuICBtYXJnaW4tdG9wOiAyOXB4O1xyXG59XHJcblxyXG4uY3VzdG9tLXNsaWRlciB7XHJcbiAgaGVpZ2h0OiAxNDBweDtcclxufVxyXG5cclxuLndpZGdldC1uYW1lIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgYmFja2dyb3VuZDogIzE1MTUxNTtcclxuICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgYm9yZGVyLXdpZHRoOiAxcHg7XHJcbiAgYm9yZGVyLWNvbG9yOiAjNWE1YTVhO1xyXG4gIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgbWF4LXdpZHRoOiAxNDBweDtcclxuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgcGFkZGluZy1yaWdodDogNXB4O1xyXG59XHJcblxyXG4ucHdtLWljb257XHJcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIGJvcmRlcjogMnB4IHNvbGlkO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMDAwMDAwO1xyXG59XHJcblxyXG4uaWNvbi1jb250YWluZXIge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/widgets/action-widget-slider/action-widget-slider.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-slider/action-widget-slider.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"widget\">\n\t\n  <div class=\"custom-slider\">\n  \t<div class=\"mt-3 icon-container\" *ngIf=\"showSliderGraphic\">\n\t\t\t<div class=\"pwm-icon\" [style.background]=\"getIconStyle()\"></div>\n\t\t</div>\n  \t<div [style.padding-top]=\"getPaddingStyle()\">\n  \t\t<ng5-slider [(value)]=\"value\" [options]=\"sliderOptions\" (userChange)=\"onUserChange($event)\"></ng5-slider>\n  \t</div>\n    \n  </div>\n  <div *ngIf=\"widget.name != undefined\" class=\"widget-name mt-2\" (click)=\"onNameClicked()\">{{widget.name}}</div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/widgets/action-widget-slider/action-widget-slider.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/components/widgets/action-widget-slider/action-widget-slider.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: ActionWidgetSliderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionWidgetSliderComponent", function() { return ActionWidgetSliderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_action_widget__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../models/action-widget */ "./src/app/models/action-widget.ts");
/* harmony import */ var _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../models/widget-action-setup */ "./src/app/models/widget-action-setup.ts");
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../models/consts */ "./src/app/models/consts.ts");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");







var ActionWidgetSliderComponent = /** @class */ (function () {
    function ActionWidgetSliderComponent() {
        this.nameClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onAction = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onActionSetup = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ActionWidgetSliderComponent.prototype.ngOnInit = function () {
        var widgetDetails;
        var maxSlider;
        if (this.widget.details) {
            widgetDetails = JSON.parse(this.widget.details);
            this.showSliderGraphic = widgetDetails[_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC"]] ? widgetDetails[_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC"]] : false;
            this.maxSliderValue = widgetDetails[_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_SLIDER_MAX"]] ? widgetDetails[_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_SLIDER_MAX"]] : 255;
            this.graphicColor = widgetDetails[_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR"]] ? widgetDetails[_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR"]] : "ffffff";
        }
        this.sliderOptions = new ng5_slider__WEBPACK_IMPORTED_MODULE_5__["Options"]();
        this.sliderOptions.floor = 0;
        this.sliderOptions.ceil = this.maxSliderValue;
        this.value = 0;
        if (this.widget && this.widget.actions) {
            this.actionMap = {};
            for (var _i = 0, _a = this.widget.actions; _i < _a.length; _i++) {
                var action = _a[_i];
                if (!this.actionMap[action.widgetEvent]) {
                    this.actionMap[action.widgetEvent] = [];
                }
                this.actionMap[action.widgetEvent].push(action);
            }
        }
    };
    ActionWidgetSliderComponent.prototype.onUserChange = function (param, event) {
        if (this.editMode) {
            var setup = new _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_3__["WidgetActionSetup"](new _models_widget_action_setup__WEBPACK_IMPORTED_MODULE_3__["RequestedFields"](true, true, true, false), null, null, this.widget.key, _models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_EVENT_SLIDER"]);
            this.onActionSetup.emit(setup);
        }
        else {
            var actions = this.actionMap[_models_consts__WEBPACK_IMPORTED_MODULE_4__["WIDGET_EVENT_SLIDER"]];
            if (actions) {
                for (var _i = 0, actions_1 = actions; _i < actions_1.length; _i++) {
                    var action = actions_1[_i];
                    action.devCommandValue = this.value;
                    this.onAction.emit(action);
                }
            }
        }
    };
    ActionWidgetSliderComponent.prototype.onNameClicked = function () {
        if (this.editMode) {
            this.nameClicked.emit(this.widget);
        }
    };
    ActionWidgetSliderComponent.prototype.getPaddingStyle = function () {
        if (this.showSliderGraphic) {
            return '';
        }
        else {
            return '50px';
        }
    };
    ActionWidgetSliderComponent.prototype.getIconStyle = function () {
        var red = 255;
        var green = 255;
        var blue = 255;
        if (this.graphicColor.length == 6) {
            red = parseInt('0x' + this.graphicColor.substr(0, 2));
            green = parseInt('0x' + this.graphicColor.substr(2, 2));
            blue = parseInt('0x' + this.graphicColor.substr(4, 2));
        }
        var red = (this.value * red) / this.maxSliderValue;
        var green = (this.value * green) / this.maxSliderValue;
        var blue = (this.value * blue) / this.maxSliderValue;
        return 'rgb(' + red + ',' + green + ',' + blue + ')';
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetSliderComponent.prototype, "nameClicked", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetSliderComponent.prototype, "onAction", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ActionWidgetSliderComponent.prototype, "onActionSetup", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_action_widget__WEBPACK_IMPORTED_MODULE_2__["ActionWidget"])
    ], ActionWidgetSliderComponent.prototype, "widget", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], ActionWidgetSliderComponent.prototype, "editMode", void 0);
    ActionWidgetSliderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-action-widget-slider',
            template: __webpack_require__(/*! ./action-widget-slider.component.html */ "./src/app/components/widgets/action-widget-slider/action-widget-slider.component.html"),
            styles: [__webpack_require__(/*! ./action-widget-slider.component.css */ "./src/app/components/widgets/action-widget-slider/action-widget-slider.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ActionWidgetSliderComponent);
    return ActionWidgetSliderComponent;
}());



/***/ }),

/***/ "./src/app/guards/auth.guard.ts":
/*!**************************************!*\
  !*** ./src/app/guards/auth.guard.ts ***!
  \**************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");




var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.authService.loggedIn()) {
            return true;
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/models/action-group.ts":
/*!****************************************!*\
  !*** ./src/app/models/action-group.ts ***!
  \****************************************/
/*! exports provided: ActionGroup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionGroup", function() { return ActionGroup; });
var ActionGroup = /** @class */ (function () {
    function ActionGroup(key, name) {
        this.key = key;
        this.name = name;
    }
    return ActionGroup;
}());



/***/ }),

/***/ "./src/app/models/action-widget.ts":
/*!*****************************************!*\
  !*** ./src/app/models/action-widget.ts ***!
  \*****************************************/
/*! exports provided: ActionWidget */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionWidget", function() { return ActionWidget; });
var ActionWidget = /** @class */ (function () {
    function ActionWidget(key, groupKey, type, active, name, details) {
        this.key = key;
        this.groupKey = groupKey;
        this.type = type;
        this.active = active;
        this.details = details;
        this.name = name;
    }
    return ActionWidget;
}());



/***/ }),

/***/ "./src/app/models/category.ts":
/*!************************************!*\
  !*** ./src/app/models/category.ts ***!
  \************************************/
/*! exports provided: Category */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Category", function() { return Category; });
var Category = /** @class */ (function () {
    function Category(id, name, commands, params) {
        this.id = id;
        this.name = name;
        this.commands = commands;
        this.parameters = params;
    }
    return Category;
}());



/***/ }),

/***/ "./src/app/models/consts.ts":
/*!**********************************!*\
  !*** ./src/app/models/consts.ts ***!
  \**********************************/
/*! exports provided: EParamConditionType, EStartStopConditionType, SRC_PAGE_EVENT_MANAGER, SRC_PAGE_COND_EDIT, SRC_PAGE_ACT_EDIT, URL_PARAM_ID, URL_PARAM_SRC, URL_PARAM_KEY, URL_PARAM_EDIT, ALARM_ARMED, ALARM_DISARMED, ALARM_ARMING, WIDGET_TYPE_BUTTONS_1, WIDGET_TYPE_BUTTONS_2, WIDGET_TYPE_BUTTONS_3, WIDGET_TYPE_BUTTONS_4, WIDGET_TYPE_SLIDER, WIDGET_TYPE_RGB_SLIDER, WIDGET_TYPE_RGB_PICKER, WIDGET_TYPES, WIDGET_EVENT_RADIUS_CENTER, WIDGET_EVENT_RGB, WIDGET_EVENT_SLIDER, WIDGET_EVENT_BUTTON_1, WIDGET_EVENT_BUTTON_2, WIDGET_EVENT_BUTTON_3, WIDGET_EVENT_BUTTON_4, WIDGET_DETAILS_SLIDER_MAX, WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC, WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR, WIDGET_DETAILS_BUTTON_NAME, WIDGET_DETAILS_BUTTON_COLOR, WIDGET_DETAILS_BUTTON_SETTINGS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EParamConditionType", function() { return EParamConditionType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EStartStopConditionType", function() { return EStartStopConditionType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SRC_PAGE_EVENT_MANAGER", function() { return SRC_PAGE_EVENT_MANAGER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SRC_PAGE_COND_EDIT", function() { return SRC_PAGE_COND_EDIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SRC_PAGE_ACT_EDIT", function() { return SRC_PAGE_ACT_EDIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URL_PARAM_ID", function() { return URL_PARAM_ID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URL_PARAM_SRC", function() { return URL_PARAM_SRC; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URL_PARAM_KEY", function() { return URL_PARAM_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URL_PARAM_EDIT", function() { return URL_PARAM_EDIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ALARM_ARMED", function() { return ALARM_ARMED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ALARM_DISARMED", function() { return ALARM_DISARMED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ALARM_ARMING", function() { return ALARM_ARMING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_TYPE_BUTTONS_1", function() { return WIDGET_TYPE_BUTTONS_1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_TYPE_BUTTONS_2", function() { return WIDGET_TYPE_BUTTONS_2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_TYPE_BUTTONS_3", function() { return WIDGET_TYPE_BUTTONS_3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_TYPE_BUTTONS_4", function() { return WIDGET_TYPE_BUTTONS_4; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_TYPE_SLIDER", function() { return WIDGET_TYPE_SLIDER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_TYPE_RGB_SLIDER", function() { return WIDGET_TYPE_RGB_SLIDER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_TYPE_RGB_PICKER", function() { return WIDGET_TYPE_RGB_PICKER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_TYPES", function() { return WIDGET_TYPES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_EVENT_RADIUS_CENTER", function() { return WIDGET_EVENT_RADIUS_CENTER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_EVENT_RGB", function() { return WIDGET_EVENT_RGB; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_EVENT_SLIDER", function() { return WIDGET_EVENT_SLIDER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_EVENT_BUTTON_1", function() { return WIDGET_EVENT_BUTTON_1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_EVENT_BUTTON_2", function() { return WIDGET_EVENT_BUTTON_2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_EVENT_BUTTON_3", function() { return WIDGET_EVENT_BUTTON_3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_EVENT_BUTTON_4", function() { return WIDGET_EVENT_BUTTON_4; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_DETAILS_SLIDER_MAX", function() { return WIDGET_DETAILS_SLIDER_MAX; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC", function() { return WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR", function() { return WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_DETAILS_BUTTON_NAME", function() { return WIDGET_DETAILS_BUTTON_NAME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_DETAILS_BUTTON_COLOR", function() { return WIDGET_DETAILS_BUTTON_COLOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WIDGET_DETAILS_BUTTON_SETTINGS", function() { return WIDGET_DETAILS_BUTTON_SETTINGS; });
var EParamConditionType;
(function (EParamConditionType) {
    EParamConditionType[EParamConditionType["undefined"] = -1] = "undefined";
    EParamConditionType[EParamConditionType["equal"] = 0] = "equal";
    EParamConditionType[EParamConditionType["greater"] = 1] = "greater";
    EParamConditionType[EParamConditionType["greater_equal"] = 2] = "greater_equal";
    EParamConditionType[EParamConditionType["less"] = 3] = "less";
    EParamConditionType[EParamConditionType["less_equal"] = 4] = "less_equal";
    EParamConditionType[EParamConditionType["any"] = 5] = "any";
    EParamConditionType[EParamConditionType["rising"] = 6] = "rising";
    EParamConditionType[EParamConditionType["falling"] = 7] = "falling";
    EParamConditionType[EParamConditionType["not_equal"] = 8] = "not_equal";
})(EParamConditionType || (EParamConditionType = {}));
;
var EStartStopConditionType;
(function (EStartStopConditionType) {
    EStartStopConditionType[EStartStopConditionType["regular"] = 0] = "regular";
    EStartStopConditionType[EStartStopConditionType["start"] = 1] = "start";
    EStartStopConditionType[EStartStopConditionType["stop"] = 2] = "stop";
})(EStartStopConditionType || (EStartStopConditionType = {}));
;
var SRC_PAGE_EVENT_MANAGER = 'event-manager';
var SRC_PAGE_COND_EDIT = 'condition';
var SRC_PAGE_ACT_EDIT = 'action';
var URL_PARAM_ID = 'id';
var URL_PARAM_SRC = 'src';
var URL_PARAM_KEY = 'key';
var URL_PARAM_EDIT = 'edit';
var ALARM_ARMED = 'ALARM WŁĄCZONY';
var ALARM_DISARMED = 'ALARM WYŁĄCZONY';
var ALARM_ARMING = 'UZBRAJANIE...';
var WIDGET_TYPE_BUTTONS_1 = '1-button';
var WIDGET_TYPE_BUTTONS_2 = '2-buttons';
var WIDGET_TYPE_BUTTONS_3 = '3-buttons';
var WIDGET_TYPE_BUTTONS_4 = '4-buttons';
var WIDGET_TYPE_SLIDER = 'slider';
var WIDGET_TYPE_RGB_SLIDER = 'rgb-slider';
var WIDGET_TYPE_RGB_PICKER = 'rgb-picker';
var WIDGET_TYPES = [
    WIDGET_TYPE_BUTTONS_1,
    WIDGET_TYPE_BUTTONS_2,
    WIDGET_TYPE_BUTTONS_3,
    WIDGET_TYPE_BUTTONS_4,
    WIDGET_TYPE_SLIDER,
    WIDGET_TYPE_RGB_SLIDER,
    WIDGET_TYPE_RGB_PICKER
];
var WIDGET_EVENT_RADIUS_CENTER = 'radius-center';
var WIDGET_EVENT_RGB = 'color-picker-rgb';
var WIDGET_EVENT_SLIDER = 'slider';
var WIDGET_EVENT_BUTTON_1 = 'button-1';
var WIDGET_EVENT_BUTTON_2 = 'button-2';
var WIDGET_EVENT_BUTTON_3 = 'button-3';
var WIDGET_EVENT_BUTTON_4 = 'button-4';
var WIDGET_DETAILS_SLIDER_MAX = 'slider-max';
var WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC = 'slider-graphic';
var WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR = 'slider-graphic-color';
var WIDGET_DETAILS_BUTTON_NAME = 'buttonName';
var WIDGET_DETAILS_BUTTON_COLOR = 'buttonColor';
var WIDGET_DETAILS_BUTTON_SETTINGS = 'buttonSettings';
//export enum EConditionType{
//  device,
//  time,
//  alarm,
//  COND_TYPE_DEVICE
//};
//get


/***/ }),

/***/ "./src/app/models/device.ts":
/*!**********************************!*\
  !*** ./src/app/models/device.ts ***!
  \**********************************/
/*! exports provided: Device */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Device", function() { return Device; });
var Device = /** @class */ (function () {
    function Device(jsonObject) {
        this.address = jsonObject['address'].toString();
        this.category = jsonObject['category'].toString();
        this.uid = jsonObject['uid'];
        this.active = jsonObject['active'];
        this.name = jsonObject['name'];
    }
    return Device;
}());



/***/ }),

/***/ "./src/app/models/operation.ts":
/*!*************************************!*\
  !*** ./src/app/models/operation.ts ***!
  \*************************************/
/*! exports provided: RegularOperation, DelayedOperation, DeviceCondition, TimeCondition, AlarmCondition, DeviceAction, GsmModemAction, RestClientAction */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegularOperation", function() { return RegularOperation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DelayedOperation", function() { return DelayedOperation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceCondition", function() { return DeviceCondition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeCondition", function() { return TimeCondition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlarmCondition", function() { return AlarmCondition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceAction", function() { return DeviceAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GsmModemAction", function() { return GsmModemAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestClientAction", function() { return RestClientAction; });
/* harmony import */ var _consts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./consts */ "./src/app/models/consts.ts");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/utils */ "./src/app/utils/utils.ts");


//###################### interfaces
// operations
var RegularOperation = /** @class */ (function () {
    function RegularOperation(jsonOBject) {
        this.active = (jsonOBject['active'] == '1');
        this.deleted = false;
        this.name = jsonOBject['name'];
        this.type = jsonOBject['type'];
        this.conditionsExtLogic = jsonOBject['extCondLogic'];
        this.id = new _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"]().generateRandom();
    }
    RegularOperation.prototype.toJson = function () {
        var json = new Object();
        var cons = new Array();
        for (var _i = 0, _a = this.conditions; _i < _a.length; _i++) {
            var con = _a[_i];
            cons.push(con.toJson());
        }
        var acts = new Array();
        for (var _b = 0, _c = this.actions; _b < _c.length; _b++) {
            var act = _c[_b];
            acts.push(act.toJson());
        }
        json['name'] = this.name;
        json['active'] = this.active ? '1' : '0';
        json['type'] = this.type;
        json['conditions'] = cons;
        json['actions'] = acts;
        json['extCondLogic'] = this.conditionsExtLogic;
        return json;
    };
    return RegularOperation;
}());

var DelayedOperation = /** @class */ (function () {
    function DelayedOperation(jsonOBject) {
        this.active = (jsonOBject['active'] == '1');
        this.deleted = false;
        this.name = jsonOBject['name'];
        this.type = jsonOBject['type'];
        this.delay = jsonOBject['delay'];
        this.conditionsExtLogic = jsonOBject['extCondLogic'];
        this.stopConditionsExtLogic = jsonOBject['stopExtCondLogic'];
        this.id = new _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"]().generateRandom();
    }
    DelayedOperation.prototype.toJson = function () {
        var json = new Object();
        var stopCons = new Array();
        for (var _i = 0, _a = this.stopConditions(); _i < _a.length; _i++) {
            var con = _a[_i];
            stopCons.push(con.toJson());
        }
        var startCons = new Array();
        for (var _b = 0, _c = this.startConditions(); _b < _c.length; _b++) {
            var con = _c[_b];
            startCons.push(con.toJson());
        }
        var acts = new Array();
        for (var _d = 0, _e = this.actions; _d < _e.length; _d++) {
            var act = _e[_d];
            acts.push(act.toJson());
        }
        json['name'] = this.name;
        json['active'] = this.active ? '1' : '0';
        json['type'] = this.type;
        json['delay'] = this.delay;
        json['conditions_stop'] = stopCons;
        json['conditions_start'] = startCons;
        json['actions'] = acts;
        json['extCondLogic'] = this.conditionsExtLogic;
        json['stopExtCondLogic'] = this.stopConditionsExtLogic;
        return json;
    };
    DelayedOperation.prototype.startConditions = function () {
        var cons = new Array();
        for (var _i = 0, _a = this.conditions; _i < _a.length; _i++) {
            var con = _a[_i];
            if (con.internalType == _consts__WEBPACK_IMPORTED_MODULE_0__["EStartStopConditionType"].start) {
                cons.push(con);
            }
        }
        return cons;
    };
    DelayedOperation.prototype.stopConditions = function () {
        var cons = new Array();
        for (var _i = 0, _a = this.conditions; _i < _a.length; _i++) {
            var con = _a[_i];
            if (con.internalType == _consts__WEBPACK_IMPORTED_MODULE_0__["EStartStopConditionType"].stop) {
                cons.push(con);
            }
        }
        return cons;
    };
    return DelayedOperation;
}());

// ####################### operations
// conditions
var DeviceCondition = /** @class */ (function () {
    function DeviceCondition(jsonObject, deviceManager, internalType) {
        this.type = jsonObject['type'];
        this.internalType = internalType;
        this.param = jsonObject['param'];
        this.condition = jsonObject['condition'];
        this.value = jsonObject['value'].toString();
        this.device = deviceManager.getDevice(jsonObject['category'], jsonObject['address']);
        this.id = new _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"]().generateRandom();
        this.extLogicConditionId = jsonObject['extId'];
    }
    DeviceCondition.prototype.toJson = function () {
        var json = new Object();
        json['type'] = this.type;
        json['category'] = this.device.category.toString();
        json['address'] = this.device.address.toString();
        json['param'] = this.param;
        json['condition'] = this.condition;
        json['value'] = this.value;
        json['extId'] = this.extLogicConditionId;
        return json;
    };
    DeviceCondition.prototype.toString = function () {
        return this.extLogicConditionId + ': ' + this.device.name + ' has ' + this.param + ' ' + this.condition + ' ' + this.value;
    };
    return DeviceCondition;
}());

var TimeCondition = /** @class */ (function () {
    function TimeCondition(jsonObject, internalType) {
        this.type = jsonObject['type'];
        this.internalType = internalType;
        this.condition = jsonObject['condition'];
        this.value = jsonObject['value'].toString();
        this.id = new _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"]().generateRandom();
        this.extLogicConditionId = jsonObject['extId'];
    }
    TimeCondition.prototype.toJson = function () {
        var json = new Object();
        json['type'] = this.type;
        json['condition'] = this.condition;
        json['value'] = this.value;
        json['extId'] = this.extLogicConditionId;
        return json;
    };
    TimeCondition.prototype.toString = function () {
        console.log('>>> time: ', this.value);
        console.log('>>> time: ', new _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"]().convertTime(this.value));
        return this.extLogicConditionId + ': Time ' + this.condition + ' ' + new _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"]().convertTime(this.value);
    };
    return TimeCondition;
}());

var AlarmCondition = /** @class */ (function () {
    function AlarmCondition(jsonObject, internalType) {
        this.type = jsonObject['type'];
        this.internalType = internalType;
        this.value = jsonObject['value'];
        this.id = new _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"]().generateRandom();
        this.extLogicConditionId = jsonObject['extId'];
    }
    AlarmCondition.prototype.toJson = function () {
        var json = new Object();
        json['type'] = this.type;
        json['value'] = this.value;
        json['extId'] = this.extLogicConditionId;
        return json;
    };
    AlarmCondition.prototype.toString = function () {
        var ret = this.extLogicConditionId + ': Alarm is ';
        console.log('>> ret: ' + ret);
        ret += (this.value == '1') ? 'active' : 'inactive';
        console.log('>>>> alarm to string: ' + ret);
        return ret;
    };
    return AlarmCondition;
}());

// ######################### conditions
// actions
var DeviceAction = /** @class */ (function () {
    function DeviceAction(jsonObject, deviceManager) {
        this.type = jsonObject['type'];
        this.param = jsonObject['param'];
        this.value = jsonObject['value'].toString();
        this.device = deviceManager.getDevice(jsonObject['category'], jsonObject['address']);
        this.id = new _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"]().generateRandom();
    }
    DeviceAction.prototype.toJson = function () {
        var json = new Object();
        json['type'] = this.type;
        json['category'] = this.device.category.toString();
        json['address'] = this.device.address.toString();
        json['param'] = this.param;
        json['value'] = this.value;
        return json;
    };
    DeviceAction.prototype.toString = function () {
        return this.device.name + ' set ' + this.param + ' = ' + this.value;
    };
    return DeviceAction;
}());

var GsmModemAction = /** @class */ (function () {
    function GsmModemAction(jsonObject, deviceManager) {
        this.id = new _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"]().generateRandom();
        this.type = jsonObject['type'];
        this.message = jsonObject['message'];
        this.number = jsonObject['number'];
    }
    GsmModemAction.prototype.toJson = function () {
        var json = new Object();
        json['type'] = this.type;
        json['message'] = this.message;
        json['number'] = this.number;
        return json;
    };
    GsmModemAction.prototype.toString = function () {
        return 'Send SMS [' + this.message.substring(0, 20) + '] to ' + this.number;
    };
    return GsmModemAction;
}());

var RestClientAction = /** @class */ (function () {
    function RestClientAction(jsonObject, deviceManager) {
        this.id = new _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"]().generateRandom();
        this.type = jsonObject['type'];
        this.message = jsonObject['message'];
        this.endpoint = jsonObject['endpoint'];
    }
    RestClientAction.prototype.toJson = function () {
        var json = new Object();
        json['type'] = this.type;
        json['message'] = this.message;
        json['endpoint'] = this.endpoint;
        return json;
    };
    RestClientAction.prototype.toString = function () {
        return 'Rest request [' + this.message.substring(0, 20) + '] to ' + this.endpoint;
    };
    return RestClientAction;
}());

//############################# actions


/***/ }),

/***/ "./src/app/models/widget-action-setup.ts":
/*!***********************************************!*\
  !*** ./src/app/models/widget-action-setup.ts ***!
  \***********************************************/
/*! exports provided: WidgetActionSetup, RequestedFields */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetActionSetup", function() { return WidgetActionSetup; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestedFields", function() { return RequestedFields; });
var WidgetActionSetup = /** @class */ (function () {
    function WidgetActionSetup(fieldRequest, defaultCategory, defaultCommand, widgetKey, widgetEvent) {
        this.fieldRequest = fieldRequest;
        this.defaultCategory = defaultCategory;
        this.defaultCommand = defaultCommand;
        this.widgetKey = widgetKey;
        this.widgetEvent = widgetEvent;
    }
    return WidgetActionSetup;
}());

var RequestedFields = /** @class */ (function () {
    function RequestedFields(cat, adr, com, val) {
        this.devCategory = cat;
        this.devAddress = adr;
        this.devCommand = com;
        this.devCommandValue = val;
    }
    return RequestedFields;
}());



/***/ }),

/***/ "./src/app/models/widget-action.ts":
/*!*****************************************!*\
  !*** ./src/app/models/widget-action.ts ***!
  \*****************************************/
/*! exports provided: WidgetAction */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetAction", function() { return WidgetAction; });
var WidgetAction = /** @class */ (function () {
    // constructor(
    //   key: number, widgetKey: number, widgetEvent: string, devCategory : string, devAddress : string,
    //   devCommand : string, devCommandValue : number
    // ) {
    //   this.key = key;
    //   this.widgetKey = widgetKey;
    //   this.widgetEvent = widgetEvent;
    //   this.devCategory = devCategory;
    //   this.devAddress = devAddress;
    //   this.devCommand = devCommand;
    //   this.devCommandValue = devCommandValue;
    // }
    function WidgetAction() {
    }
    WidgetAction.prototype.getName = function (deviceManager) {
        var devName = 'Uknown device';
        if (this.devCategory && this.devAddress) {
            var device = deviceManager.getDevice(this.devCategory, this.devAddress);
            devName = device.name;
        }
        var output = devName;
        if (this.devCommand != undefined) {
            output += ' set ' + this.devCommand;
        }
        if (this.devCommandValue != undefined) {
            output += ' to ' + this.devCommandValue;
        }
        return output;
    };
    return WidgetAction;
}());



/***/ }),

/***/ "./src/app/pipes/hex-transform.pipe.ts":
/*!*********************************************!*\
  !*** ./src/app/pipes/hex-transform.pipe.ts ***!
  \*********************************************/
/*! exports provided: HexTransformPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HexTransformPipe", function() { return HexTransformPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HexTransformPipe = /** @class */ (function () {
    function HexTransformPipe() {
    }
    HexTransformPipe.prototype.transform = function (value, args) {
        if (isNaN(value)) {
            return 'NaN';
        }
        else {
            return value.toString(16).toUpperCase();
        }
    };
    HexTransformPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'hexTransform'
        })
    ], HexTransformPipe);
    return HexTransformPipe;
}());



/***/ }),

/***/ "./src/app/pipes/logs-filter.pipe.ts":
/*!*******************************************!*\
  !*** ./src/app/pipes/logs-filter.pipe.ts ***!
  \*******************************************/
/*! exports provided: LogsFilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogsFilterPipe", function() { return LogsFilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LogsFilterPipe = /** @class */ (function () {
    function LogsFilterPipe() {
    }
    LogsFilterPipe.prototype.transform = function (value, args) {
        if (value) {
            return value.slice(value.indexOf(']') + 1);
        }
    };
    LogsFilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'logsFilter'
        })
    ], LogsFilterPipe);
    return LogsFilterPipe;
}());



/***/ }),

/***/ "./src/app/pipes/name-filter.pipe.ts":
/*!*******************************************!*\
  !*** ./src/app/pipes/name-filter.pipe.ts ***!
  \*******************************************/
/*! exports provided: NameFilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NameFilterPipe", function() { return NameFilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NameFilterPipe = /** @class */ (function () {
    function NameFilterPipe() {
    }
    NameFilterPipe.prototype.transform = function (items, filter) {
        if (!items || !filter) {
            return items;
        }
        return items.filter(function (item) {
            var vals = filter.toLowerCase().split(' ');
            for (var _i = 0, vals_1 = vals; _i < vals_1.length; _i++) {
                var val = vals_1[_i];
                if (item.name.toLowerCase().indexOf(val) === -1) {
                    return false;
                }
            }
            return true;
        });
    };
    NameFilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'nameFilter'
        })
    ], NameFilterPipe);
    return NameFilterPipe;
}());



/***/ }),

/***/ "./src/app/pipes/safe.pipe.ts":
/*!************************************!*\
  !*** ./src/app/pipes/safe.pipe.ts ***!
  \************************************/
/*! exports provided: SafePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SafePipe", function() { return SafePipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");



var SafePipe = /** @class */ (function () {
    function SafePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafePipe.prototype.transform = function (value, type) {
        switch (type) {
            case 'html': return this.sanitizer.bypassSecurityTrustHtml(value);
            case 'style': return this.sanitizer.bypassSecurityTrustStyle(value);
            case 'script': return this.sanitizer.bypassSecurityTrustScript(value);
            case 'url': return this.sanitizer.bypassSecurityTrustUrl(value);
            case 'resourceUrl': return this.sanitizer.bypassSecurityTrustResourceUrl(value);
            default: throw new Error("Invalid safe type specified: " + type);
        }
    };
    SafePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'safe'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
    ], SafePipe);
    return SafePipe;
}());



/***/ }),

/***/ "./src/app/services/actions-widgets.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/actions-widgets.service.ts ***!
  \*****************************************************/
/*! exports provided: ActionsWidgetsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsWidgetsService", function() { return ActionsWidgetsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _tools_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tools.service */ "./src/app/services/tools.service.ts");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _models_action_group__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/action-group */ "./src/app/models/action-group.ts");
/* harmony import */ var _models_action_widget__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/action-widget */ "./src/app/models/action-widget.ts");
/* harmony import */ var _models_widget_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/widget-action */ "./src/app/models/widget-action.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_7__);








var ActionsWidgetsService = /** @class */ (function () {
    function ActionsWidgetsService(toolsService, flashMessage) {
        this.toolsService = toolsService;
        this.flashMessage = flashMessage;
    }
    ActionsWidgetsService.prototype.createNewActionGroup = function (name) {
        var _this = this;
        var key = new _utils_utils__WEBPACK_IMPORTED_MODULE_3__["Utils"]().generateRandom();
        this.toolsService.createUpdateActionWidgetsGroup(key, name).subscribe(function (data) {
            console.log('data: ', data);
            //console.log('>>> err code: ', data.errorCode);
            if (data['success'] === false) {
                _this.flashMessage.show('Creating action widgets group failed: ' + data['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else if (data['success'] === true) {
                _this.flashMessage.show('New action widgets group created', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    ActionsWidgetsService.prototype.updateActionGroup = function (actionGroup, callback) {
        this.toolsService.createUpdateActionWidgetsGroup(actionGroup.key, actionGroup.name).subscribe(function (data) {
            console.log('data: ', data);
            callback(data);
        });
    };
    ActionsWidgetsService.prototype.loadActionGroups = function () {
        var _this = this;
        this.toolsService.getActionWidgetsGroups().subscribe(function (data) {
            console.log('data: ', data);
            //console.log('>>> err code: ', data.errorCode);
            if (data['success'] === false) {
                _this.flashMessage.show('Loading action widgets groups failed: ' + data['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else if (data['success'] === true) {
                _this.actionGroups = new Array();
                for (var _i = 0, _a = data['actionGroups']; _i < _a.length; _i++) {
                    var act = _a[_i];
                    _this.actionGroups.push(new _models_action_group__WEBPACK_IMPORTED_MODULE_4__["ActionGroup"](act.key, act.name));
                }
                console.log('>>>> loaded actions: ', _this.actionGroups);
            }
        });
    };
    ActionsWidgetsService.prototype.deleteActionWidgetsGroup = function (groupKey, callback) {
        var _this = this;
        this.toolsService.deleteActionWidgetsGroup(groupKey).subscribe(function (data) {
            console.log('data: ', data);
            //console.log('>>> err code: ', data.errorCode);
            if (data['success'] === false) {
                _this.flashMessage.show('Delete action widgets group failed: ' + data['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else if (data['success'] === true) {
                _this.flashMessage.show('Action widgets group deleted', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            callback();
        });
    };
    ActionsWidgetsService.prototype.getActionWidgetsGroup = function (key, callback) {
        var _this = this;
        this.toolsService.getActionWidgetsGroup(key).subscribe(function (data) {
            console.log('data: ', data);
            //console.log('>>> err code: ', data.errorCode);
            if (data['success'] === false) {
                _this.flashMessage.show('Loading action widgets group failed: ' + data['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else if (data['success'] === true) {
                _this.selectedActionGroup = data['group'];
                callback(_this.selectedActionGroup);
            }
        });
    };
    ActionsWidgetsService.prototype.createNewWidget = function (groupKey, type, active, name, details, callback) {
        if (groupKey == undefined) {
            this.flashMessage.show('Creating widget failed - group key cannot be empty', {
                cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                timeout: 5000
            });
            return;
        }
        var key = new _utils_utils__WEBPACK_IMPORTED_MODULE_3__["Utils"]().generateRandom();
        this.toolsService.createUpdateActionWidget(key, groupKey, type, active, name, details).subscribe(function (data) {
            console.log('data: ', data);
            //console.log('>>> err code: ', data.errorCode);
            callback(data);
        });
    };
    ActionsWidgetsService.prototype.updateWidget = function (widget, callback) {
        this.toolsService.createUpdateActionWidget(widget.key, widget.groupKey, widget.type, widget.active, widget.name, widget.details).subscribe(function (data) {
            console.log('data: ', data);
            callback(data);
        });
    };
    ActionsWidgetsService.prototype.createNewWidgetAction = function (action, callback) {
        if (action.widgetKey == undefined) {
            this.flashMessage.show('Creating widget action failed - widget key cannot be empty', {
                cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                timeout: 5000
            });
            return;
        }
        action.key = new _utils_utils__WEBPACK_IMPORTED_MODULE_3__["Utils"]().generateRandom();
        this.toolsService.createUpdateWidgetAction(action.key, action.widgetKey, action.widgetEvent, action.devCategory, action.devAddress, action.devCommand, action.devCommandValue).subscribe(function (data) {
            console.log('data: ', data);
            //console.log('>>> err code: ', data.errorCode);
            callback(data);
        });
    };
    ActionsWidgetsService.prototype.updateWidgetAction = function (action, callback) {
        this.toolsService.createUpdateWidgetAction(action.key, action.widgetKey, action.widgetEvent, action.devCategory, action.devAddress, action.devCommand, action.devCommandValue).subscribe(function (data) {
            console.log('data: ', data);
            callback(data);
        });
    };
    // loadWidgetsForGroup(groupKey : number) {
    //
    //   this.toolsService.getActionWidgetsForGroup(groupKey).subscribe(data => {
    //
    //     console.log('data: ', data);
    //     //console.log('>>> err code: ', data.errorCode);
    //     if (data['success'] === false){
    //
    //       this.flashMessage.show('Loading widgets failed: ' + data['msg'], {
    //         cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
    //         timeout: 5000});
    //     } else if (data['success'] === true) {
    //       this.actionWidgets = new Array<ActionWidget>();
    //
    //       for (var widget of data['widgets']) {
    //         this.actionWidgets.push(new ActionWidget(widget.key, widget.groupKey, widget.type, widget.active, widget.details));
    //       }
    //
    //       console.log('>>>> loaded widgets: ', this.actionWidgets);
    //
    //     }
    //   });
    //
    // }
    ActionsWidgetsService.prototype.deleteActionsWidget = function (key, callback) {
        this.toolsService.deleteActionWidget(key).subscribe(function (data) {
            console.log('data: ', data);
            //console.log('>>> err code: ', data.errorCode);
            callback(data);
        });
    };
    ActionsWidgetsService.prototype.getWidget = function (key, callback) {
        var _this = this;
        this.toolsService.getActionWidget(key).subscribe(function (data) {
            console.log('data: ', data);
            //console.log('>>> err code: ', data.errorCode);
            if (data['success'] === false) {
                _this.flashMessage.show('Loading widget failed: ' + data['msg'], {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else if (data['success'] === true) {
                var tmp = data['widget'];
                var widget = new _models_action_widget__WEBPACK_IMPORTED_MODULE_5__["ActionWidget"](tmp.key, tmp.groupKey, tmp.type, tmp.active, tmp.name, tmp.details);
                widget.actions = new Array();
                for (var _i = 0, _a = tmp.actions; _i < _a.length; _i++) {
                    var act = _a[_i];
                    var action = new _models_widget_action__WEBPACK_IMPORTED_MODULE_6__["WidgetAction"]();
                    action.key = act.key;
                    action.widgetKey = act.widgetKey;
                    action.widgetEvent = act.widgetEvent;
                    action.devCategory = act.devCategory;
                    action.devAddress = act.devAddress;
                    action.devCommand = act.devCommand;
                    action.devCommandValue = act.devCommandValue;
                    widget.actions.push(action);
                }
                // callback(data['widget']);
                callback(widget);
            }
        });
    };
    ActionsWidgetsService.prototype.deleteWidgetAction = function (key, callback) {
        this.toolsService.deleteWidgetAction(key).subscribe(function (data) {
            console.log('data: ', data);
            //console.log('>>> err code: ', data.errorCode);
            callback(data);
        });
    };
    ActionsWidgetsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_tools_service__WEBPACK_IMPORTED_MODULE_2__["ToolsService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_7__["FlashMessagesService"]])
    ], ActionsWidgetsService);
    return ActionsWidgetsService;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _utils_storageFactory__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../utils/storageFactory */ "./src/app/utils/storageFactory.ts");






var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.storage = new _utils_storageFactory__WEBPACK_IMPORTED_MODULE_5__["StorageFactory"](localStorage);
    }
    AuthService.prototype.authenticateUser = function (user) {
        console.log('>>>> auth user: ', user);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/users/authenticate', user, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AuthService.prototype.storeUserData = function (token, user, expiresIn) {
        var expiresAt = moment__WEBPACK_IMPORTED_MODULE_4__().add(expiresIn, 'second');
        this.storage.setItem('id_token', token);
        this.storage.setItem('user', JSON.stringify(user));
        this.storage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()));
        this.authToken = token;
        this.user = user;
        this.expires_at = expiresAt;
    };
    AuthService.prototype.logout = function () {
        this.authToken = null;
        this.user = null;
        this.expires_at = null;
        this.storage.clear();
    };
    AuthService.prototype.loadToken = function () {
        var token = this.storage.getItem('id_token');
        this.authToken = token;
    };
    AuthService.prototype.getExpiration = function () {
        var expiration = this.storage.getItem("expires_at");
        var expiresAt = JSON.parse(expiration);
        return moment__WEBPACK_IMPORTED_MODULE_4__(expiresAt);
    };
    AuthService.prototype.loggedIn = function () {
        return moment__WEBPACK_IMPORTED_MODULE_4__().isBefore(this.getExpiration());
        // return true;
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/backup.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/backup.service.ts ***!
  \********************************************/
/*! exports provided: BackupService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BackupService", function() { return BackupService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var BackupService = /** @class */ (function () {
    function BackupService(http) {
        this.http = http;
    }
    BackupService.prototype.getData = function (name) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        var path = '/backup/' + name;
        return this.http.get(path, { headers: headers });
    };
    BackupService.prototype.saveData = function (name, data) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        var path = '/restore/' + name;
        return this.http.post(path, data, { headers: headers });
    };
    BackupService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], BackupService);
    return BackupService;
}());



/***/ }),

/***/ "./src/app/services/builder.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/builder.service.ts ***!
  \*********************************************/
/*! exports provided: BuilderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuilderService", function() { return BuilderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/consts */ "./src/app/models/consts.ts");
/* harmony import */ var _models_operation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/operation */ "./src/app/models/operation.ts");




var BuilderService = /** @class */ (function () {
    function BuilderService() {
    }
    BuilderService.prototype.createOperation = function (jsonObject, devManager) {
        var op;
        if (jsonObject['type'] == 'normal') {
            op = new _models_operation__WEBPACK_IMPORTED_MODULE_3__["RegularOperation"](jsonObject);
            var jsonConditions = jsonObject['conditions'];
            if (jsonConditions) {
                op.conditions = new Array();
                for (var _i = 0, jsonConditions_1 = jsonConditions; _i < jsonConditions_1.length; _i++) {
                    var cond = jsonConditions_1[_i];
                    op.conditions.push(this.createCondition(cond, devManager, _models_consts__WEBPACK_IMPORTED_MODULE_2__["EStartStopConditionType"].regular));
                }
            }
            var jsonActions = jsonObject['actions'];
            if (jsonActions) {
                op.actions = new Array();
                for (var _a = 0, jsonActions_1 = jsonActions; _a < jsonActions_1.length; _a++) {
                    var act = jsonActions_1[_a];
                    op.actions.push(this.createAction(act, devManager));
                }
            }
        }
        else if (jsonObject['type'] == 'delayed') {
            op = new _models_operation__WEBPACK_IMPORTED_MODULE_3__["DelayedOperation"](jsonObject);
            op.conditions = new Array();
            var jsonConditions = jsonObject['conditions_start'];
            if (jsonConditions) {
                for (var _b = 0, jsonConditions_2 = jsonConditions; _b < jsonConditions_2.length; _b++) {
                    var cond = jsonConditions_2[_b];
                    op.conditions.push(this.createCondition(cond, devManager, _models_consts__WEBPACK_IMPORTED_MODULE_2__["EStartStopConditionType"].start));
                }
            }
            var jsonConditions = jsonObject['conditions_stop'];
            if (jsonConditions) {
                for (var _c = 0, jsonConditions_3 = jsonConditions; _c < jsonConditions_3.length; _c++) {
                    var cond = jsonConditions_3[_c];
                    op.conditions.push(this.createCondition(cond, devManager, _models_consts__WEBPACK_IMPORTED_MODULE_2__["EStartStopConditionType"].stop));
                }
            }
            var jsonActions = jsonObject['actions'];
            if (jsonActions) {
                op.actions = new Array();
                for (var _d = 0, jsonActions_2 = jsonActions; _d < jsonActions_2.length; _d++) {
                    var act = jsonActions_2[_d];
                    op.actions.push(this.createAction(act, devManager));
                }
            }
        }
        return op;
    };
    BuilderService.prototype.createAction = function (jsonObject, devManager) {
        if (jsonObject['type'] == 'device') {
            return new _models_operation__WEBPACK_IMPORTED_MODULE_3__["DeviceAction"](jsonObject, devManager);
        }
        else if (jsonObject['type'] == 'gsmModem') {
            return new _models_operation__WEBPACK_IMPORTED_MODULE_3__["GsmModemAction"](jsonObject, devManager);
        }
        else if (jsonObject['type'] == 'rest') {
            return new _models_operation__WEBPACK_IMPORTED_MODULE_3__["RestClientAction"](jsonObject, devManager);
        }
    };
    BuilderService.prototype.createCondition = function (jsonObject, devManager, type) {
        if (jsonObject['type'] == 'device') {
            return new _models_operation__WEBPACK_IMPORTED_MODULE_3__["DeviceCondition"](jsonObject, devManager, type);
        }
        else if (jsonObject['type'] == 'time') {
            return new _models_operation__WEBPACK_IMPORTED_MODULE_3__["TimeCondition"](jsonObject, type);
        }
        else if (jsonObject['type'] == 'alarm') {
            return new _models_operation__WEBPACK_IMPORTED_MODULE_3__["AlarmCondition"](jsonObject, type);
        }
        else {
            return null;
        }
    };
    BuilderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BuilderService);
    return BuilderService;
}());



/***/ }),

/***/ "./src/app/services/config.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/config.service.ts ***!
  \********************************************/
/*! exports provided: ConfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigService", function() { return ConfigService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");




var ConfigService = /** @class */ (function () {
    function ConfigService(http) {
        this.http = http;
    }
    ConfigService.prototype.addDevice = function (device, key) {
        var req = {
            "key": key,
            "category": device.category,
            "address": device.address
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/config/addDevice', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ConfigService.prototype.getDevice = function (key) {
        var req = {
            "key": key
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/config/getDevice', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ConfigService.prototype.getDevices = function (keys) {
        var req = {
            "keys": keys
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/config/getDevices', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ConfigService.prototype.removeDevices = function () {
        var req;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/config/delDevices', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ConfigService.prototype.addBasicConfig = function (key, value) {
        var req = {
            "key": key,
            "value": value
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/config/addConfig', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ConfigService.prototype.getBasicConfig = function (key) {
        var req = {
            "key": key
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/config/getConfig', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ConfigService.prototype.removeBasicConfigs = function () {
        var req;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/config/delConfig', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ConfigService.prototype.addDeviceConfig = function (key, device, parameter, value) {
        var req = {
            "key": key,
            "value": value,
            "address": device.address,
            "category": device.category,
            "parameter": parameter
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/config/addDeviceConfig', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ConfigService.prototype.getDevicesConfigs = function (key) {
        var req = {
            "key": key
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/config/getDevicesConfig', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ConfigService.prototype.removeDevicesConfigs = function () {
        var req;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/config/delDevicesConfig', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ConfigService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], ConfigService);
    return ConfigService;
}());



/***/ }),

/***/ "./src/app/services/device-manager.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/device-manager.service.ts ***!
  \****************************************************/
/*! exports provided: DeviceManagerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceManagerService", function() { return DeviceManagerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _models_device__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/device */ "./src/app/models/device.ts");
/* harmony import */ var _models_category__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/category */ "./src/app/models/category.ts");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");








var DeviceManagerService = /** @class */ (function () {
    function DeviceManagerService(http, restService, flashMessage) {
        this.http = http;
        this.restService = restService;
        this.flashMessage = flashMessage;
    }
    DeviceManagerService.prototype.loadDevices = function (callback) {
        var _this = this;
        this.loadCategories();
        if (this.devicesList) {
            if (callback) {
                callback();
            }
            return;
        }
        this.restService.getDevicesList().subscribe(function (data) {
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Loading devices failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.devicesList = new Array();
                for (var _i = 0, _a = data.devices; _i < _a.length; _i++) {
                    var dev = _a[_i];
                    _this.devicesList.push(new _models_device__WEBPACK_IMPORTED_MODULE_3__["Device"](dev));
                }
                if (callback) {
                    callback();
                }
            }
        });
    };
    DeviceManagerService.prototype.getDevice = function (category, address) {
        if (this.devicesList) {
            for (var _i = 0, _a = this.devicesList; _i < _a.length; _i++) {
                var dev = _a[_i];
                if (dev.address == address && dev.category == category) {
                    return dev;
                }
            }
        }
        return new _models_device__WEBPACK_IMPORTED_MODULE_3__["Device"]({ 'category': category, 'address': address, 'name': 'notFound' });
    };
    DeviceManagerService.prototype.getAllDevices = function () {
        return this.devicesList;
    };
    DeviceManagerService.prototype.getDevicesInCategory = function (cat) {
        var devList = new Array();
        if (cat) {
            for (var _i = 0, _a = this.devicesList; _i < _a.length; _i++) {
                var dev = _a[_i];
                if (dev.category == cat.id) {
                    devList.push(dev);
                }
            }
        }
        return devList;
    };
    DeviceManagerService.prototype.getCategoryById = function (categoryId) {
        if (typeof (categoryId) == "number") {
            categoryId = categoryId.toString();
        }
        for (var _i = 0, _a = this.categoriesList; _i < _a.length; _i++) {
            var cat = _a[_i];
            if (cat.id == categoryId) {
                return cat;
            }
        }
        return null;
    };
    DeviceManagerService.prototype.loadCategories = function () {
        var _this = this;
        if (this.categoriesList) {
            return;
        }
        this.categoriesList = new Array();
        this.restService.getCategories().subscribe(function (data) {
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Loading categories failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                for (var i = 0; i < data.categories.length; i++) {
                    _this.categoriesList.push(new _models_category__WEBPACK_IMPORTED_MODULE_4__["Category"](data.categories[i]['id'].toString(), data.categories[i]['name'], data.categories[i]['commands'], data.categories[i]['parameters']));
                }
            }
        });
    };
    DeviceManagerService.prototype.getAllCategories = function () {
        return this.categoriesList;
    };
    DeviceManagerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            _rest_service__WEBPACK_IMPORTED_MODULE_5__["RestService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_6__["FlashMessagesService"]])
    ], DeviceManagerService);
    return DeviceManagerService;
}());



/***/ }),

/***/ "./src/app/services/operation-manager.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/operation-manager.service.ts ***!
  \*******************************************************/
/*! exports provided: OperationManagerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OperationManagerService", function() { return OperationManagerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./rest.service */ "./src/app/services/rest.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _builder_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./builder.service */ "./src/app/services/builder.service.ts");
/* harmony import */ var _device_manager_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./device-manager.service */ "./src/app/services/device-manager.service.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _models_consts__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../models/consts */ "./src/app/models/consts.ts");









var OperationManagerService = /** @class */ (function () {
    function OperationManagerService(restService, flashMessage, builder, deviceManager) {
        this.restService = restService;
        this.flashMessage = flashMessage;
        this.builder = builder;
        this.deviceManager = deviceManager;
    }
    OperationManagerService.prototype.loadOperations = function () {
        var _this = this;
        if (this.operations) {
            return;
        }
        this.restService.getJson('operation_list').subscribe(function (data) {
            //data = this.convertFromBufferToJson(data);
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Loading operations list failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                var jsonObject = JSON.parse(data.json);
                _this.loadOperationsList(jsonObject['Operations']);
                _this.changed = false;
            }
        });
    };
    OperationManagerService.prototype.loadOperationsList = function (operationList) {
        if (operationList && operationList.length > 0) {
            this.operations = new Array();
            this.operationsOrigin = new Array();
        }
        for (var _i = 0, operationList_1 = operationList; _i < operationList_1.length; _i++) {
            var op = operationList_1[_i];
            var operation = this.builder.createOperation(op, this.deviceManager);
            this.operations.push(operation);
            // this.operationsOrigin.push(Object.assign(Object.create(operation),operation));
            this.operationsOrigin.push(lodash__WEBPACK_IMPORTED_MODULE_6___default.a.cloneDeep(operation));
        }
    };
    OperationManagerService.prototype.getOperationById = function (id) {
        for (var _i = 0, _a = this.operations; _i < _a.length; _i++) {
            var op = _a[_i];
            if (op.id == id) {
                return op;
            }
        }
        return undefined;
    };
    OperationManagerService.prototype.getOperationConditionById = function (operation, condId) {
        for (var _i = 0, _a = operation['conditions']; _i < _a.length; _i++) {
            var con = _a[_i];
            if (con.id == condId) {
                return con;
            }
        }
        return undefined;
    };
    OperationManagerService.prototype.getOperationActionById = function (operation, actId) {
        for (var _i = 0, _a = operation['actions']; _i < _a.length; _i++) {
            var act = _a[_i];
            if (act.id == actId) {
                return act;
            }
        }
        return undefined;
    };
    OperationManagerService.prototype.setOperationActionById = function (operation, actId, action) {
        for (var i = 0; i < operation.actions.length; i++) {
            if (operation.actions[i].id == actId) {
                operation.actions[i] = action;
                break;
            }
        }
    };
    OperationManagerService.prototype.setOperationConditionById = function (operation, conId, condition) {
        for (var i = 0; i < operation.conditions.length; i++) {
            if (operation.conditions[i].id == conId) {
                operation.conditions[i] = condition;
                break;
            }
        }
    };
    OperationManagerService.prototype.removeOperationActionById = function (operation, actId) {
        for (var i = 0; i < operation.actions.length; i++) {
            if (operation.actions[i].id == actId) {
                operation.actions.splice(i, 1);
                break;
            }
        }
    };
    OperationManagerService.prototype.removeOperationConditionById = function (operation, condId) {
        var condType;
        for (var i = 0; i < operation.conditions.length; i++) {
            if (operation.conditions[i].id == condId) {
                condType = operation.conditions[i].internalType;
                operation.conditions.splice(i, 1);
                break;
            }
        }
        if (condType == _models_consts__WEBPACK_IMPORTED_MODULE_8__["EStartStopConditionType"].regular) {
            for (var i = 0; i < operation.conditions.length; i++) {
                operation.conditions[i].extLogicConditionId = new _utils_utils__WEBPACK_IMPORTED_MODULE_7__["Utils"]().getLetterByIndex(i);
            }
        }
        else if (condType == _models_consts__WEBPACK_IMPORTED_MODULE_8__["EStartStopConditionType"].start) {
            for (var i = 0; i < operation.startConditions().length; i++) {
                operation.startConditions()[i].extLogicConditionId = new _utils_utils__WEBPACK_IMPORTED_MODULE_7__["Utils"]().getLetterByIndex(i);
            }
        }
        else if (condType == _models_consts__WEBPACK_IMPORTED_MODULE_8__["EStartStopConditionType"].stop) {
            for (var i = 0; i < operation.stopConditions().length; i++) {
                operation.stopConditions()[i].extLogicConditionId = new _utils_utils__WEBPACK_IMPORTED_MODULE_7__["Utils"]().getLetterByIndex(i);
            }
        }
    };
    OperationManagerService.prototype.activateOperation = function (operation, active) {
        operation.active = active;
        this.changed = true;
    };
    OperationManagerService.prototype.deleteOperation = function (operation) {
        var index;
        for (index = 0; index < this.operations.length; index++) {
            if (operation.id == this.operations[index].id) {
                //this.operations.splice(index, 1);
                operation.deleted = true;
                this.changed = true;
                break;
            }
        }
    };
    OperationManagerService.prototype.undeleteOperation = function (operation) {
        var index;
        for (index = 0; index < this.operations.length; index++) {
            if (operation.id == this.operations[index].id) {
                operation.deleted = false;
                break;
            }
        }
    };
    OperationManagerService.prototype.cancelChanges = function () {
        this.changed = false;
        this.operations = new Array();
        for (var _i = 0, _a = this.operationsOrigin; _i < _a.length; _i++) {
            var op = _a[_i];
            // this.operations.push(Object.assign(Object.create(op), op));
            this.operations.push(lodash__WEBPACK_IMPORTED_MODULE_6___default.a.cloneDeep(op));
        }
    };
    OperationManagerService.prototype.saveOperations = function () {
        var _this = this;
        var ops = new Object();
        ops['Operations'] = new Array();
        var tmpOperations = new Array();
        for (var _i = 0, _a = this.operations; _i < _a.length; _i++) {
            var op = _a[_i];
            if (!op.deleted) {
                ops['Operations'].push(op.toJson());
                // tmpOperations.push(Object.assign(Object.create(op), op));
                tmpOperations.push(lodash__WEBPACK_IMPORTED_MODULE_6___default.a.cloneDeep(op));
            }
        }
        console.log('>>> new operations: ', ops);
        this.restService.saveJson('operation_list', ops).subscribe(function (data) {
            //data = this.convertFromBufferToJson(data);
            console.log('data: ', data);
            console.log('>>> err code: ', data.errorCode);
            if (data.errorCode) {
                _this.flashMessage.show('Saving operations list failed: ' + data.result, {
                    cssClass: 'custom-danger-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
            else {
                _this.operations = tmpOperations;
                _this.operationsOrigin = tmpOperations;
                _this.changed = false;
                _this.flashMessage.show('Operations saved successfully', {
                    cssClass: 'custom-success-alert', showCloseBtn: true, closeOnClick: true,
                    timeout: 5000
                });
            }
        });
    };
    OperationManagerService.prototype.saveOperation = function (operation) {
        for (var index = 0; index < this.operations.length; index++) {
            if (this.operations[index].id == operation.id) {
                this.operations.splice(index, 1);
                break;
            }
        }
        this.operations.push(operation);
        this.saveOperations();
    };
    OperationManagerService.prototype.cloneOperation = function (operationId) {
        var tmp = this.getOperationById(operationId);
        // this.tmpOperation = Object.assign(Object.create(tmp),tmp);
        this.tmpOperation = lodash__WEBPACK_IMPORTED_MODULE_6___default.a.cloneDeep(tmp);
        return this.tmpOperation;
    };
    OperationManagerService.prototype.cancelEditOperation = function () {
        this.tmpOperation = null;
    };
    OperationManagerService.prototype.createOperation = function (type) {
        if (type == 'regular') {
            this.tmpOperation = this.builder.createOperation({ 'active': "1", 'name': "", 'type': "normal", 'conditions': [], 'actions': [], 'conditionsExtLogic': '' }, this.deviceManager);
        }
        else if (type = 'delayed') {
            this.tmpOperation = this.builder.createOperation({ 'active': "1", 'name': "", 'type': "delayed", 'delay': "", 'conditions_start': [], 'conditions_stop': [], 'actions': [], 'conditionsExtLogic': '' }, this.deviceManager);
        }
    };
    OperationManagerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__["FlashMessagesService"],
            _builder_service__WEBPACK_IMPORTED_MODULE_4__["BuilderService"],
            _device_manager_service__WEBPACK_IMPORTED_MODULE_5__["DeviceManagerService"]])
    ], OperationManagerService);
    return OperationManagerService;
}());



/***/ }),

/***/ "./src/app/services/rest.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/rest.service.ts ***!
  \******************************************/
/*! exports provided: RestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestService", function() { return RestService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");




var RestService = /** @class */ (function () {
    function RestService(http) {
        this.http = http;
    }
    RestService.prototype.setDeviceParameter = function (device, param, val) {
        var req = {
            "devCat": device.category,
            "devAddr": device.address,
            "param": param,
            "val": val
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/setDeviceParameter', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.getDeviceParameter = function (device, param) {
        var req = {
            "devCat": device.category,
            "devAddr": device.address,
            "param": param
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/getDeviceParameter', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.getJson = function (jsonId) {
        var req = {
            "jsonId": jsonId
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/getJson', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.saveJson = function (jsonId, jsonBody) {
        var req = {
            "jsonId": jsonId,
            "jsonBody": jsonBody
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/saveJson', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.setDeviceName = function (device) {
        var req = {
            "devCat": device.category,
            "devAddr": device.address,
            "name": device.name
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/setDeviceName', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.getDevicesList = function () {
        var req = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/getDevicesList', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.getFilesList = function () {
        var req = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/getFilesList', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.uploadFirmware = function (fileName) {
        var req = {
            "fileName": fileName
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/uploadFirmware', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.bootDevice = function (uid) {
        var req = {
            "uid": uid
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/bootDevice', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.initDevices = function () {
        var req = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/initDevices', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.removeDevice = function (uid) {
        var req = {
            "uid": uid
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/removeDevice', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.removeDeviceAdvance = function (cat, addrStart, addrStop) {
        var req = {
            "cat": cat,
            "addrStart": addrStart,
            "addrStop": addrStop
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/removeDeviceAdvance', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.getDeviceDetails = function (device) {
        var req = {
            "devCat": device.category,
            "devAddr": device.address
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/getDeviceDetails', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.getDeviceParameters = function (device) {
        console.log('>>> cat: ' + device.category);
        var req = {
            "devCat": device.category,
            "devAddr": device.address
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/getDeviceParameters', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.getCategories = function () {
        var req = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/getCategories', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.sendMessage = function (number, message) {
        var req = {
            "number": number,
            "message": message
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/sendMessage', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.armSystem = function (armDisarm, hash) {
        var req = {
            "armDisarm": armDisarm,
            "hash": hash
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/armSystem', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService.prototype.getArmSystemStatus = function () {
        var req = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/rest/getArmSystemStatus', req, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], RestService);
    return RestService;
}());



/***/ }),

/***/ "./src/app/services/tools.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/tools.service.ts ***!
  \*******************************************/
/*! exports provided: ToolsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToolsService", function() { return ToolsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");




var ToolsService = /** @class */ (function () {
    function ToolsService(http) {
        this.http = http;
    }
    ToolsService.prototype.getImage = function () {
        var req = {};
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/getImage', req, { headers: headers });
    };
    ToolsService.prototype.getLatestLogs = function (latestLogId, filter) {
        var req = {
            "logId": latestLogId,
            "filter": filter
        };
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/getLatestLogs', req, { headers: headers });
    };
    ToolsService.prototype.getOlderLogs = function (oldestLogId, filter) {
        var req = {
            "logId": oldestLogId,
            "filter": filter
        };
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/getOlderLogs', req, { headers: headers });
    };
    ToolsService.prototype.getLogsQnty = function () {
        var req = {};
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/getLogsQnty', req, { headers: headers });
    };
    ToolsService.prototype.createUpdateActionWidgetsGroup = function (key, name) {
        var req = {
            "key": key,
            "name": name
        };
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/upsertActionWidgetsGroup', req, { headers: headers });
    };
    ToolsService.prototype.getActionWidgetsGroups = function () {
        var req = {};
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/getActionWidgetsGroups', req, { headers: headers });
    };
    ToolsService.prototype.getActionWidgetsGroup = function (key) {
        var req = {
            "key": key
        };
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/getActionWidgetsGroup', req, { headers: headers });
    };
    ToolsService.prototype.deleteActionWidgetsGroup = function (key) {
        var req = {
            "key": key
        };
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/deleteActionWidgetsGroup', req, { headers: headers });
    };
    ToolsService.prototype.createUpdateActionWidget = function (key, groupKey, type, active, name, details) {
        var req = {
            "key": key,
            "groupKey": groupKey,
            "type": type,
            "active": active
        };
        if (details && details.length > 0) {
            req['details'] = details;
        }
        if (name && name.length > 0) {
            req['name'] = name;
        }
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/upsertActionWidget', req, { headers: headers });
    };
    ToolsService.prototype.getActionWidgetsForGroup = function (groupKey) {
        var req = { "groupKey": groupKey };
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/getActionWidgets', req, { headers: headers });
    };
    ToolsService.prototype.getActionWidget = function (key) {
        var req = {
            "key": key
        };
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/getActionWidget', req, { headers: headers });
    };
    ToolsService.prototype.deleteActionWidget = function (key) {
        var req = {
            "key": key
        };
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/deleteActionWidget', req, { headers: headers });
    };
    ToolsService.prototype.createUpdateWidgetAction = function (key, widgetKey, widgetEvent, devCategory, devAddress, devCommand, devCommandValue) {
        var req = {
            "key": key,
            "widgetKey": widgetKey,
            "widgetEvent": widgetEvent,
            "devCategory": devCategory,
            "devAddress": devAddress,
            "devCommand": devCommand,
            "devCommandValue": devCommandValue
        };
        if (devCommand && devCommand.length > 0) {
            req['devCommand'] = devCommand;
        }
        if (devCommandValue != undefined) {
            req['devCommandValue'] = devCommandValue;
        }
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/upsertWidgetAction', req, { headers: headers });
    };
    ToolsService.prototype.deleteWidgetAction = function (key) {
        var req = {
            "key": key
        };
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/tools/deleteWidgetAction', req, { headers: headers });
    };
    ToolsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ToolsService);
    return ToolsService;
}());



/***/ }),

/***/ "./src/app/services/validate.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/validate.service.ts ***!
  \**********************************************/
/*! exports provided: ValidateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidateService", function() { return ValidateService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ValidateService = /** @class */ (function () {
    function ValidateService() {
    }
    ValidateService.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    ValidateService.prototype.validatePhoneNumber = function (number) {
        var re = /^\+[0-9]{11}$/;
        return re.test(number);
    };
    ValidateService.prototype.validateExtendedConditionLogicPattern = function (logic) {
        var re = /^[A-Z()|&]+$/;
        return re.test(logic);
    };
    ValidateService.prototype.validateOperation = function (operation, result) {
        if (!operation.name || (operation.name && operation.name == '')) {
            result.error = 'Operation name cannot be empty';
            return false;
        }
        if (operation.active === undefined) {
            result.error = 'Missing operation attribute: active';
            return false;
        }
        if (!operation.type) {
            result.error = 'Missing operation attribute: type';
            return false;
        }
        if (!operation.actions || (operation.actions && operation.actions.length == 0)) {
            result.error = 'Operation must contains at least one action';
            return false;
        }
        if (operation.type == 'normal') {
            if (!operation.conditions || (operation.conditions && operation.conditions.length == 0)) {
                result.error = 'Operation must contains at least one condition';
                return false;
            }
        }
        if (operation.type == 'delayed') {
            if (!operation.conditions || (operation.conditions && operation.startConditions().length == 0)) {
                result.error = 'Operation must contains at least one start condition';
                return false;
            }
            if (!operation.conditions || (operation.conditions && operation.stopConditions().length == 0)) {
                result.error = 'Operation must contains at least one stop condition';
                return false;
            }
            if (!operation.delay ||
                (operation.delay && operation.delay === "")) {
                result.error = 'Operation delay value cannot be empty';
                return false;
            }
            if (!operation.delay ||
                (operation.delay && isNaN(Number(operation.delay)))) {
                result.error = 'Operation delay parameter must be a number';
                return false;
            }
            if (operation.conditionsExtLogic && !this.validateExtendedLogic(operation.conditionsExtLogic, operation.startConditions(), result)) {
                return false;
            }
            if (operation.stopConditionsExtLogic && !this.validateExtendedLogic(operation.stopConditionsExtLogic, operation.stopConditions(), result)) {
                return false;
            }
        }
        else {
            if (operation.conditionsExtLogic && !this.validateExtendedLogic(operation.conditionsExtLogic, operation.conditions, result)) {
                return false;
            }
        }
        return true;
    };
    ValidateService.prototype.validateExtendedLogic = function (logicText, conditions, result) {
        if (!this.validateExtendedConditionLogicPattern(logicText)) {
            result.error = 'Invalid format of extended logic';
            return false;
        }
        var condIds = new Array();
        for (var _i = 0, conditions_1 = conditions; _i < conditions_1.length; _i++) {
            var cond = conditions_1[_i];
            condIds.push(cond.extLogicConditionId);
        }
        var idPattern = /^[A-Z]$/;
        for (var index = 0; index < logicText.length; index++) {
            var sign = logicText.substring(index, index + 1);
            if (idPattern.test(sign) && condIds.indexOf(sign) < 0) {
                result.error = 'Extended logic contains invalid condition ID';
                return false;
            }
        }
        return true;
    };
    ValidateService.prototype.validateAction = function (action, result) {
        if (!action || !action['type']) {
            result.error = 'Action type must be selected';
            return false;
        }
        if (action['type']) {
            if (action['type'] == 'gsmModem') {
                return this.validateGsmModemAction(action, result);
            }
            if (action['type'] == 'rest') {
                return this.validateRestClientAction(action, result);
            }
            if (action['type'] == 'device') {
                if (this.validateDevice(action, result)) {
                    return this.validateDeviceAction(action, result);
                }
                else {
                    return false;
                }
            }
        }
        return true;
    };
    ValidateService.prototype.validateGsmModemAction = function (action, result) {
        if (!action.number ||
            (action.number && !this.validatePhoneNumber(action.number))) {
            result.error = 'Phone number must have a valid format';
            return false;
        }
        if (!action.message) {
            result.error = 'Message must be provided';
            return false;
        }
        return true;
    };
    ValidateService.prototype.validateRestClientAction = function (action, result) {
        if (!action.endpoint || action.endpoint == '') {
            result.error = 'Endpoint cannot be empty';
            return false;
        }
        if (!action.message) {
            result.error = 'Message must be provided';
            return false;
        }
        return true;
    };
    ValidateService.prototype.validateDeviceAction = function (action, result) {
        if (!action.param) {
            result.error = 'Device parameter must be selected';
            return false;
        }
        if (action.value === null || action.value === "") {
            result.error = 'Parameter value cannot be empty';
            return false;
        }
        if (isNaN(Number(action.value))) {
            result.error = 'Parameter value must be a number';
            return false;
        }
        return true;
    };
    ValidateService.prototype.validateCondition = function (condition, result) {
        if (!condition || !condition['type']) {
            result.error = 'Condition type must be selected';
            return false;
        }
        if (condition['type']) {
            if (condition['type'] == 'time') {
                return this.validateTimeCondition(condition, result);
            }
            if (condition['type'] == 'device') {
                if (this.validateDevice(condition, result)) {
                    return this.validateDeviceCondition(condition, result);
                }
                else {
                    return false;
                }
            }
        }
        return true;
    };
    //
    ValidateService.prototype.validateTimeCondition = function (condition, result) {
        if (!condition.condition) {
            result.error = 'Condition value must be selected';
            return false;
        }
        if (condition.value === null || condition.value === "") {
            result.error = 'Time value cannot be empty';
            return false;
        }
        if (isNaN(Number(condition.value))) {
            result.error = 'Time value must be a number';
            return false;
        }
        return true;
    };
    //
    ValidateService.prototype.validateDeviceCondition = function (condition, result) {
        if (!condition.condition) {
            result.error = 'Condition value must be selected';
            return false;
        }
        if (!condition.param) {
            result.error = 'Device parameter must be selected';
            return false;
        }
        if (condition.value === null || condition.value === "") {
            result.error = 'Parameter value cannot be empty';
            return false;
        }
        if (isNaN(Number(condition.value))) {
            result.error = 'Parameter value must be a number';
            return false;
        }
        return true;
    };
    ValidateService.prototype.validateDevice = function (device, result) {
        if (!device['category']) {
            result.error = 'Device category must be selected';
            return false;
        }
        if (!device['address']) {
            result.error = 'Device must be selected';
            return false;
        }
        return true;
    };
    ValidateService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ValidateService);
    return ValidateService;
}());



/***/ }),

/***/ "./src/app/utils/configConsts.ts":
/*!***************************************!*\
  !*** ./src/app/utils/configConsts.ts ***!
  \***************************************/
/*! exports provided: ConfigConsts */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigConsts", function() { return ConfigConsts; });
var ConfigConsts = /** @class */ (function () {
    function ConfigConsts() {
    }
    ConfigConsts.LIVINGROOM_LIGHTS_SET = 'livingroomlightsset';
    ConfigConsts.LIVINGROOM_LIGHTS_SET_1 = 'livingroomlightsset1';
    ConfigConsts.LIVINGROOM_LIGHTS_SET_2 = 'livingroomlightsset2';
    ConfigConsts.LIVINGROOM_LIGHTS_SET_3 = 'livingroomlightsset3';
    ConfigConsts.LIVINGROOM_LIGHTS_SET_4 = 'livingroomlightsset4';
    ConfigConsts.LIVINGROOM_LIGHTS_SET_MANUAL = 'livingroomlightsmanual';
    ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_1 = 'livingroomwalleffectset1';
    ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_2 = 'livingroomwalleffectset2';
    ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_3 = 'livingroomwalleffectset3';
    ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_4 = 'livingroomwalleffectset4';
    ConfigConsts.LIVINGROOM_CEILING_EFFECT_SET_1 = 'livingroomceilingeffectset1';
    ConfigConsts.LIVINGROOM_CEILING_EFFECT_SET_2 = 'livingroomceilingeffectset2';
    ConfigConsts.LIVINGROOM_CEILING_EFFECT_SET_3 = 'livingroomceilingeffectset3';
    return ConfigConsts;
}());



/***/ }),

/***/ "./src/app/utils/deviceConsts.ts":
/*!***************************************!*\
  !*** ./src/app/utils/deviceConsts.ts ***!
  \***************************************/
/*! exports provided: DeviceConsts */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceConsts", function() { return DeviceConsts; });
var DeviceConsts = /** @class */ (function () {
    function DeviceConsts() {
    }
    DeviceConsts.KITCHEN_MAIN = 'kuchnia_sufit';
    DeviceConsts.KITCHEN_SIDE = 'kuchnia_boczne';
    DeviceConsts.KITCHEN_FURN_LEFT = 'kuchnia_szafki_lewe';
    DeviceConsts.KITCHEN_FURN_RIGHT = 'kuchnia_szafki_prawe';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_01 = 'salon_zew_01';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_02 = 'salon_zew_02';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_03 = 'salon_zew_03';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_04 = 'salon_zew_04';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_05 = 'salon_zew_05';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_06 = 'salon_zew_06';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_07 = 'salon_zew_07';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_08 = 'salon_zew_08';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_09 = 'salon_zew_09';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_10 = 'salon_zew_10';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_11 = 'salon_zew_11';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_12 = 'salon_zew_12';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_13 = 'salon_zew_13';
    DeviceConsts.LIVINGROOM_LIGHTS_RIM_14 = 'salon_zew_14';
    DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_01 = 'salon_wew_01';
    DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_02 = 'salon_wew_02';
    DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_03 = 'salon_wew_03';
    DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_04 = 'salon_wew_04';
    DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_05 = 'salon_wew_05';
    DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_06 = 'salon_wew_06';
    DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_07 = 'salon_wew_07';
    DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_08 = 'salon_wew_08';
    DeviceConsts.LIVINGROOM_WALL_EFFECT_TOP = 'salon_okno_gora';
    DeviceConsts.LIVINGROOM_WALL_EFFECT_MIDDLE = 'salon_okno_srodek';
    DeviceConsts.LIVINGROOM_WALL_EFFECT_BOTTOM = 'salon_okno_dol';
    DeviceConsts.LIVINGROOM_CEILING_EFFECT_OUTSIDE_LONG = 'salon_listwa_zew_dluga';
    DeviceConsts.LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT = 'salon_listwa_zew_krotka';
    DeviceConsts.LIVINGROOM_CEILING_EFFECT_INSIDE_LONG = 'salon_listwa_wew_dluga';
    DeviceConsts.LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT = 'salon_listwa_wew_krotka';
    DeviceConsts.KITCHEN_SOCKET_01 = 'kuchnia_gniazdko_01';
    DeviceConsts.KITCHEN_SOCKET_02 = 'kuchnia_gniazdko_02';
    DeviceConsts.KITCHEN_SOCKET_03 = 'kuchnia_gniazdko_03';
    DeviceConsts.KITCHEN_SOCKET_04 = 'kuchnia_gniazdko_04';
    DeviceConsts.KITCHEN_SOCKET_05 = 'kuchnia_gniazdko_05';
    DeviceConsts.KITCHEN_SOCKET_06 = 'kuchnia_gniazdko_06';
    DeviceConsts.LIVINGROOM_SOCKET_01 = 'salon_gniazdko_01';
    DeviceConsts.PARAM_STATE = 'state';
    DeviceConsts.PARAM_SWITCH = 'switch';
    DeviceConsts.PARAM_DUTY_CYCLE = 'dutyCycle';
    DeviceConsts.PARAM_DUTY_CYCLE_UP_ALL = 'dutyUpAll';
    DeviceConsts.PARAM_DUTY_CYCLE_DOWN_ALL = 'dutyDownAll';
    DeviceConsts.PARAM_DUTY_CYCLE_ALL = 'dutyCycleAllTheSame';
    DeviceConsts.PARAM_RED = 'red';
    DeviceConsts.PARAM_GREEN = 'green';
    DeviceConsts.PARAM_BLUE = 'blue';
    DeviceConsts.PARAM_SPEED = 'speed';
    DeviceConsts.PARAM_MODE = 'mode';
    DeviceConsts.PARAM_RGB = 'rgb';
    DeviceConsts.COMM_OFF = 'off';
    DeviceConsts.RGB_MODE_OFF = 0;
    DeviceConsts.RGB_MODE_AUTO1 = 1;
    DeviceConsts.RGB_MODE_AUTO2 = 2;
    DeviceConsts.RGB_MODE_RND = 3;
    DeviceConsts.RGB_MODE_MANUAL = 4;
    DeviceConsts.RGB_MODE_RND_ALL = 5;
    DeviceConsts.DEV_CATEGORY_BISTABLE_SENSOR = '2';
    DeviceConsts.DEV_CATEGORY_BISTABLE_SWITCH = '3';
    DeviceConsts.DEV_CATEGORY_PWM = '5';
    DeviceConsts.DEV_CATEGORY_MOTION_DETECTOR = '6';
    DeviceConsts.DEV_CATEGORY_RGB = '7';
    return DeviceConsts;
}());



/***/ }),

/***/ "./src/app/utils/longPress.ts":
/*!************************************!*\
  !*** ./src/app/utils/longPress.ts ***!
  \************************************/
/*! exports provided: LongPress */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LongPress", function() { return LongPress; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LongPress = /** @class */ (function () {
    function LongPress() {
        this.duration = 500;
        this.onLongPress = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onLongPressing = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onLongPressEnd = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.mouseX = 0;
        this.mouseY = 0;
    }
    Object.defineProperty(LongPress.prototype, "press", {
        get: function () { return this.pressing; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LongPress.prototype, "longPress", {
        get: function () { return this.longPressing; },
        enumerable: true,
        configurable: true
    });
    LongPress.prototype.onMouseDown = function (event) {
        var _this = this;
        console.log('>>>> on omuse down');
        // don't do right/middle clicks
        if (event.which !== 1)
            return;
        this.mouseX = event.clientX;
        this.mouseY = event.clientY;
        this.pressing = true;
        this.longPressing = false;
        this.timeout = setTimeout(function () {
            _this.longPressing = true;
            _this.onLongPress.emit(event);
            _this.loop(event);
        }, this.duration);
        this.loop(event);
    };
    LongPress.prototype.onTouchStart = function (event) {
        var _this = this;
        console.log('>>>> on omuse touch');
        this.pressing = true;
        this.longPressing = false;
        this.timeout = setTimeout(function () {
            _this.longPressing = true;
            _this.onLongPress.emit(event);
            _this.loop(event);
        }, this.duration);
        this.loop(event);
    };
    LongPress.prototype.onMouseMove = function (event) {
        console.log('>>>> on omuse movw');
        if (this.pressing && !this.longPressing) {
            var xThres = (event.clientX - this.mouseX) > 10;
            var yThres = (event.clientY - this.mouseY) > 10;
            if (xThres || yThres) {
                this.endPress();
            }
        }
    };
    LongPress.prototype.loop = function (event) {
        var _this = this;
        if (this.longPressing) {
            this.timeout = setTimeout(function () {
                _this.onLongPressing.emit(event);
                _this.loop(event);
            }, 50);
        }
    };
    LongPress.prototype.endPress = function () {
        clearTimeout(this.timeout);
        this.longPressing = false;
        this.pressing = false;
        this.onLongPressEnd.emit(true);
    };
    LongPress.prototype.onMouseUp = function () { this.endPress(); };
    LongPress.prototype.onTouchEnd = function () { this.endPress(); };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], LongPress.prototype, "duration", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], LongPress.prototype, "onLongPress", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], LongPress.prototype, "onLongPressing", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], LongPress.prototype, "onLongPressEnd", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.press'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LongPress.prototype, "press", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.longpress'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LongPress.prototype, "longPress", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('mousedown', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], LongPress.prototype, "onMouseDown", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('touchstart', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], LongPress.prototype, "onTouchStart", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('mousemove', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], LongPress.prototype, "onMouseMove", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('mouseup'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], LongPress.prototype, "onMouseUp", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('touchend'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], LongPress.prototype, "onTouchEnd", null);
    LongPress = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({ selector: '[long-press]' })
    ], LongPress);
    return LongPress;
}());



/***/ }),

/***/ "./src/app/utils/storageFactory.ts":
/*!*****************************************!*\
  !*** ./src/app/utils/storageFactory.ts ***!
  \*****************************************/
/*! exports provided: StorageFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageFactory", function() { return StorageFactory; });
var StorageFactory = /** @class */ (function () {
    function StorageFactory(storage) {
        this.inMemoryStorage = {};
        this.storage = storage;
    }
    StorageFactory.prototype.isSupported = function () {
        try {
            var key = "__some_random_key_you_are_not_going_to_use__";
            this.storage.setItem(key, key);
            var tmp = this.storage.getItem(key);
            if (tmp != key) {
                return false;
            }
            this.storage.removeItem(key);
            return true;
        }
        catch (e) {
            return false;
        }
    };
    StorageFactory.prototype.getItem = function (key) {
        if (this.isSupported()) {
            return this.storage.getItem(key);
        }
        return this.inMemoryStorage[key] || null;
    };
    StorageFactory.prototype.setItem = function (key, value) {
        if (this.isSupported()) {
            this.storage.setItem(key, value);
        }
        else {
            this.inMemoryStorage[key] = value;
        }
    };
    StorageFactory.prototype.removeItem = function (key) {
        if (this.isSupported()) {
            this.storage.removeItem(key);
        }
        else {
            delete this.inMemoryStorage[key];
        }
    };
    StorageFactory.prototype.clear = function () {
        if (this.isSupported()) {
            this.storage.clear();
        }
        else {
            this.inMemoryStorage = {};
        }
    };
    StorageFactory.prototype.key = function (n) {
        if (this.isSupported()) {
            return this.storage.key(n);
        }
        else {
            return Object.keys(this.inMemoryStorage)[n] || null;
        }
    };
    return StorageFactory;
}());



/***/ }),

/***/ "./src/app/utils/touchButton.ts":
/*!**************************************!*\
  !*** ./src/app/utils/touchButton.ts ***!
  \**************************************/
/*! exports provided: TouchButton */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TouchButton", function() { return TouchButton; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TouchButton = /** @class */ (function () {
    function TouchButton() {
        this.onTouch = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    TouchButton.prototype.onTouchStart = function (event) {
        console.log('>>>> on omuse touch');
        this.buttonTouched = true;
        this.onTouch.emit(event);
    };
    TouchButton.prototype.onMouseDown = function (event) {
        console.log('>>>> on omuse down: ' + this.buttonTouched);
        if (this.buttonTouched === undefined || this.buttonTouched === false) {
            this.onTouch.emit(event);
        }
    };
    TouchButton.prototype.onMouseUp = function () {
        this.buttonTouched = false;
        console.log('>>> onmouseup: ' + this.buttonTouched);
    };
    TouchButton.prototype.onTouchEnd = function () { console.log('>>> ontouchend: ' + this.buttonTouched); };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], TouchButton.prototype, "onTouch", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('touchstart', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], TouchButton.prototype, "onTouchStart", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('mousedown', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], TouchButton.prototype, "onMouseDown", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('mouseup'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], TouchButton.prototype, "onMouseUp", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('touchend'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], TouchButton.prototype, "onTouchEnd", null);
    TouchButton = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({ selector: '[touch-button]' })
    ], TouchButton);
    return TouchButton;
}());



/***/ }),

/***/ "./src/app/utils/utils.ts":
/*!********************************!*\
  !*** ./src/app/utils/utils.ts ***!
  \********************************/
/*! exports provided: Utils */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Utils", function() { return Utils; });
var Utils = /** @class */ (function () {
    function Utils() {
    }
    Utils.prototype.convertTime = function (secs) {
        var sec = Number(secs);
        //debugger;
        var hoursNumber = Math.floor(sec / 3600);
        var hours = hoursNumber.toString();
        var minutesNumber = Math.floor((sec - hoursNumber * 3600) / 60);
        var minutes = '' + minutesNumber.toString();
        if (minutes.length == 1) {
            minutes = '0' + minutes;
        }
        var secondsNumber = sec % 60;
        var seconds = secondsNumber.toString();
        if (seconds.length == 1) {
            seconds = '0' + seconds;
        }
        return hours + ':' + minutes + ':' + seconds;
    };
    Utils.prototype.generateRandom = function () {
        var rnd = new Date().getTime();
        rnd *= 10000;
        rnd += Math.floor(Math.random() * 10000);
        return rnd;
    };
    Utils.prototype.getRed12bit = function (rgb) {
        return (rgb / 0x1000000) & 0xfff;
    };
    Utils.prototype.getGreen12bit = function (rgb) {
        return (rgb / 0x1000) & 0xfff;
    };
    Utils.prototype.getBlue12bit = function (rgb) {
        return rgb & 0xfff;
    };
    Utils.prototype.getRed8bit = function (rgb) {
        return (rgb >> 28) & 0xff;
    };
    Utils.prototype.getGreen8bit = function (rgb) {
        return (rgb >> 16) & 0xff;
    };
    Utils.prototype.getBlue8bit = function (rgb) {
        return (rgb >> 4) & 0xff;
    };
    Utils.prototype.getRgb = function (red, green, blue) {
        var val = (red & 0xfff);
        val *= 0x1000;
        val |= (green & 0xfff);
        val *= 0x1000;
        val += (blue & 0xfff);
        return val;
    };
    Utils.prototype.getRgbHex = function (red, green, blue) {
        var colorVal = ((red >> 4) & 0xff);
        var val = ((colorVal < 0x10) ? '0' : '') + colorVal.toString(16);
        colorVal = ((green >> 4) & 0xff);
        val += ((colorVal < 0x10) ? '0' : '') + colorVal.toString(16);
        colorVal = ((blue >> 4) & 0xff);
        val += ((colorVal < 0x10) ? '0' : '') + colorVal.toString(16);
        return val;
    };
    Utils.prototype.rgbHexToRgb = function (hex) {
        var red = parseInt('0x' + hex.substr(0, 2));
        var green = parseInt('0x' + hex.substr(2, 2));
        var blue = parseInt('0x' + hex.substr(4, 2));
        var ret = (red * 16);
        ret *= 0x1000;
        ret |= (green * 16);
        ret *= 0x1000;
        ret += (blue * 16);
        return ret;
    };
    Utils.prototype.hexColorToRgbCss = function (hex) {
        var red = parseInt('0x' + hex.substr(0, 2));
        var green = parseInt('0x' + hex.substr(2, 2));
        var blue = parseInt('0x' + hex.substr(4, 2));
        return 'rgb(' + red + ',' + green + ',' + blue + ')';
    };
    Utils.prototype.getLetterByIndex = function (index) {
        var startIndex = 65; //'A'
        if (index > 25) {
            index = 25;
        }
        return String.fromCharCode(startIndex + index);
    };
    return Utils;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/server/intellihousesystemmanager/angular-src/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map