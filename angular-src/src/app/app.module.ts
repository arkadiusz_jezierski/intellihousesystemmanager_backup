import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { EventManagerComponent } from './components/event-manager/event-manager.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ViewComponent } from './components/view/view.component';
import { DeviceManagerComponent } from './components/device-manager/device-manager.component';
import { OperationComponent } from './components/operation/operation.component';
import { ConditionComponent } from './components/condition/condition.component';
import { ActionComponent } from './components/action/action.component';
import { TimePickerComponent } from './components/time-picker/time-picker.component';
import { LivingRoomComponent } from './components/living-room/living-room.component';
import { LivingRoomLightsComponent } from './components/living-room-lights/living-room-lights.component';
import { LivingRoomWallEffectComponent } from './components/living-room-wall-effect/living-room-wall-effect.component';
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
import { LivingRoomCeilingEffectComponent } from './components/living-room-ceiling-effect/living-room-ceiling-effect.component';
import { SocketsComponent } from "./components/sockets/sockets.component";
import { LightAdjustComponent } from "./components/living-room-lights/living-room-lights.component";
import { AlarmPanelComponent } from './components/alarm-panel/alarm-panel.component';
import { DeviceAdvanceCommandComponent } from './components/device-manager/device-manager.component';
import { LogsSettingsComponent } from './components/home/home.component';
import { DeviceAdvanceControlPwmComponent } from './components/device-advance-control-pwm/device-advance-control-pwm.component';
import { DeviceAdvanceControlRgbComponent } from './components/device-advance-control-rgb/device-advance-control-rgb.component';
import { RadialColorPickerComponent } from './components/radial-color-picker/radial-color-picker.component';
import { ActionsWidgetsListComponent } from './components/actions-widgets-list/actions-widgets-list.component';
import { ActionGroupDeletePopupComponent } from './components/actions-widgets-list/actions-widgets-list.component';
import { ActionsWidgetsComponent } from './components/actions-widgets/actions-widgets.component';
import { WidgetPopupComponent } from './components/actions-widgets/actions-widgets.component';
import { ActionWidgetSliderComponent } from './components/widgets/action-widget-slider/action-widget-slider.component';
import { ActionWidgetColorPickerComponent } from './components/widgets/action-widget-color-picker/action-widget-color-picker.component';
import { ActionWidgetButtonsComponent } from './components/widgets/action-widget-buttons/action-widget-buttons.component';
import { ActionWidgetRgbSliderComponent } from './components/widgets/action-widget-rgb-slider/action-widget-rgb-slider.component';
import { WidgetActionSelectComponent } from './components/widget-action-select/widget-action-select.component';
import { WidgetActionsListComponent } from './components/widget-actions-list/widget-actions-list.component';
import { WidgetActionPopupComponent } from './components/widget-actions-list/widget-actions-list.component';

import { LongPress } from './utils/longPress';
import { TouchButton } from './utils/touchButton';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { ModalModule } from 'ngx-bootstrap';
import { AngularWeatherWidgetModule, WeatherApiName } from 'angular-weather-widget';
import { UiSwitchModule } from 'ngx-ui-switch';
import { Ng5SliderModule } from 'ng5-slider';

import { ValidateService } from './services/validate.service';
import { ActionsWidgetsService } from './services/actions-widgets.service';
import { AuthService } from './services/auth.service';
import { RestService } from './services/rest.service';
import { ToolsService } from './services/tools.service';
import { ConfigService } from './services/config.service';
import { DeviceManagerService } from './services/device-manager.service';
import { OperationManagerService } from './services/operation-manager.service';
import { BuilderService } from './services/builder.service';
import { AuthGuard } from './guards/auth.guard';

import { HexTransformPipe } from './pipes/hex-transform.pipe';
import { NameFilterPipe } from './pipes/name-filter.pipe';
import { SafePipe } from './pipes/safe.pipe';
import { OrderModule } from 'ngx-order-pipe';
import { LogsFilterPipe } from './pipes/logs-filter.pipe';
import { BackupRestoreComponent } from './components/backup-restore/backup-restore.component';


const appRoutes: Routes =  [
  {path:'', component: HomeComponent},
  {path:'devicemanager', component: DeviceManagerComponent, canActivate:[AuthGuard]},
  {path:'eventmanager', component: EventManagerComponent, canActivate:[AuthGuard]},
  {path:'operation/:id/:src', component: OperationComponent, canActivate:[AuthGuard]},
  {path:'condition/:id/:clone', component: ConditionComponent, canActivate:[AuthGuard]},
  {path:'action/:id', component: ActionComponent, canActivate:[AuthGuard]},
  {path:'livingroom', component: LivingRoomComponent, canActivate:[AuthGuard]},
  {path:'livingroomlights/:id', component: LivingRoomLightsComponent, canActivate:[AuthGuard]},
  {path:'livingroomwalleffect', component: LivingRoomWallEffectComponent, canActivate:[AuthGuard]},
  {path:'livingroomceilingeffect', component: LivingRoomCeilingEffectComponent, canActivate:[AuthGuard]},
  {path:'sockets', component: SocketsComponent, canActivate:[AuthGuard]},
  {path:'colorpicker/:cat/:adr/:r/:g/:b/:ret', component: ColorPickerComponent, canActivate:[AuthGuard]},
  {path:'alarmpanel', component: AlarmPanelComponent, canActivate:[AuthGuard]},
  {path:'widgetsgroupslist', component: ActionsWidgetsListComponent, canActivate:[AuthGuard]},
  {path:'widgets/:edit/:key', component: ActionsWidgetsComponent, canActivate:[AuthGuard]},
  {path:'widgetactionslist', component: WidgetActionsListComponent, canActivate:[AuthGuard]},
  {path:'backuprestore', component: BackupRestoreComponent, canActivate:[AuthGuard]}
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    EventManagerComponent,
    ViewComponent,
    DeviceManagerComponent,
    OperationComponent,
    ConditionComponent,
    ActionComponent,
    TimePickerComponent,
    LivingRoomComponent,
    LivingRoomLightsComponent,
    LightAdjustComponent,
    AlarmPanelComponent,
    DeviceAdvanceCommandComponent,
    LogsSettingsComponent,
    LongPress,
    TouchButton,
    LivingRoomWallEffectComponent,
    ColorPickerComponent,
    LivingRoomCeilingEffectComponent,
    SocketsComponent,
    HexTransformPipe,
    NameFilterPipe,
    SafePipe,
    LogsFilterPipe,
    DeviceAdvanceControlPwmComponent,
    DeviceAdvanceControlRgbComponent,
    RadialColorPickerComponent,
    ActionsWidgetsListComponent,
    ActionGroupDeletePopupComponent,
    ActionsWidgetsComponent,
    WidgetPopupComponent,
    WidgetActionPopupComponent,
    ActionWidgetSliderComponent,
    ActionWidgetColorPickerComponent,
    ActionWidgetButtonsComponent,
    ActionWidgetRgbSliderComponent,
    WidgetActionSelectComponent,
    WidgetActionsListComponent,
    BackupRestoreComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule.forRoot(),
    ModalModule.forRoot(),
    OrderModule,
    UiSwitchModule,
    Ng5SliderModule,
    AngularWeatherWidgetModule.forRoot({
      key: '4d95e1f8c20f89c62ed6f9095b10c60c',
      name: WeatherApiName.OPEN_WEATHER_MAP,
      baseUrl: 'http://api.openweathermap.org/data/2.5'
    })

  ],
  providers: [
    ValidateService,
    AuthService,
    AuthGuard,
    RestService,
    ToolsService,
    ActionsWidgetsService,
    ConfigService,
    DeviceManagerService,
    OperationManagerService,
    BuilderService
  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents: [
    LightAdjustComponent,
    DeviceAdvanceCommandComponent,
    LogsSettingsComponent,
    ActionGroupDeletePopupComponent,
    WidgetPopupComponent,
    WidgetActionPopupComponent,
    WidgetActionSelectComponent

  ]
})
export class AppModule { }
