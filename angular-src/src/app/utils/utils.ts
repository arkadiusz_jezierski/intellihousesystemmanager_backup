export class Utils {

  convertTime(secs: string): string {
    var sec: number = Number(secs);
//debugger;
    var hoursNumber: number = Math.floor(sec/3600);
    var hours: string = hoursNumber.toString();

    var minutesNumber: number = Math.floor((sec - hoursNumber * 3600) / 60);
    var minutes: string = '' + minutesNumber.toString();
    if (minutes.length == 1) {
      minutes = '0' + minutes;
    }
    var secondsNumber: number = sec%60;
    var seconds: string = secondsNumber.toString();
    if (seconds.length == 1) {
      seconds = '0' + seconds;
    }
    return hours + ':' + minutes + ':' + seconds;
  }

  generateRandom(): number {
    var rnd: number = new Date().getTime();
    rnd *= 10000;
    rnd += Math.floor(Math.random() * 10000);
    return rnd;
  }

  getRed12bit(rgb :number) :number {
    return (rgb / 0x1000000) & 0xfff;
  }

  getGreen12bit(rgb :number) :number {
    return (rgb / 0x1000) & 0xfff;
  }

  getBlue12bit(rgb :number) :number {
    return rgb & 0xfff;
  }

  getRed8bit(rgb :number) :number {
    return (rgb >> 28) & 0xff;
  }

  getGreen8bit(rgb :number) :number {
    return (rgb >> 16) & 0xff;
  }

  getBlue8bit(rgb :number) :number {
    return (rgb >> 4) & 0xff;
  }

  getRgb(red :number, green :number, blue :number) :number {
    var val = (red & 0xfff);
    val *= 0x1000;
    val |= (green & 0xfff);
    val *= 0x1000;
    val += (blue & 0xfff);

    return val;
  }

  getRgbHex(red :number, green :number, blue :number) :string {
    var colorVal = ((red >> 4) & 0xff);
    var val = ((colorVal < 0x10) ? '0' : '') + colorVal.toString(16);

    colorVal = ((green >> 4) & 0xff);
    val += ((colorVal < 0x10) ? '0' : '') + colorVal.toString(16);

    colorVal = ((blue >> 4) & 0xff);
    val += ((colorVal < 0x10) ? '0' : '') + colorVal.toString(16);

    return val;
  }

  rgbHexToRgb(hex :string) :number {
    var red = parseInt('0x' + hex.substr(0, 2));
    var green = parseInt('0x' + hex.substr(2, 2));
    var blue = parseInt('0x' + hex.substr(4, 2));

    var ret = (red * 16);
    ret *= 0x1000;
    ret |= (green * 16);
    ret *= 0x1000;
    ret += (blue * 16);

    return ret;
  }

  hexColorToRgbCss(hex : string) : string {
    var red = parseInt('0x' + hex.substr(0, 2));
    var green = parseInt('0x' + hex.substr(2, 2));
    var blue = parseInt('0x' + hex.substr(4, 2));

    return 'rgb(' + red +',' + green + ',' + blue + ')';
  }

  getLetterByIndex(index : number) : string {
    let startIndex = 65;  //'A'
    if (index > 25) {
      index = 25;
    }
    return String.fromCharCode(startIndex + index);
  }
}
