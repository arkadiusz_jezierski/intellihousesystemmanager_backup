/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LivingRoomLightsComponent } from './living-room-lights.component';

describe('LivingRoomLightsComponent', () => {
  let component: LivingRoomLightsComponent;
  let fixture: ComponentFixture<LivingRoomLightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivingRoomLightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivingRoomLightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
