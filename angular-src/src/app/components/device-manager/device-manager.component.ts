import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services/rest.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { Device } from '../../models/device';
import { Category } from '../../models/category';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { DeviceAdvanceControlPwmComponent} from "../device-advance-control-pwm/device-advance-control-pwm.component";
import { DeviceManagerService } from '../../services/device-manager.service';

import 'rxjs/add/operator/toPromise';


@Component({
  selector: 'app-device-manager',
  templateUrl: './device-manager.component.html',
  styleUrls: ['./device-manager.component.css']
})
export class DeviceManagerComponent implements OnInit {

  filteredDevicesList: Device[];
  filesList: string[];d
  selectedFile: string;
  selectedDevice :Device;
  hexValues : Object;
  selectedDeviceName :string;
  selectedCategory :Category;
  filter : string;
  keysearch : string;
  searchResults: Device[];
  bsModalRef: BsModalRef;
  advancedModeEnabled: boolean;
  advancedControlEnabled: boolean;
  advancedFunctions: string[];
  selectedAdvFunc: string;
  selectedCatAdv: Category;
  removeAdrBegin: string;
  removeAdrEnd: string;
  advanceControlCategories: string[];

  readonly BOOT_REQUEST = 'boot';
  readonly REMOVE_REQUEST = 'remove';

  readonly ADV_FUNC_UPLOAD = 'Firmware uploading';
  readonly ADV_FUNC_REMOVE = 'Devices removing';
  readonly ADV_FUNC_OTHER = 'Other';

  constructor(
    private flashMessage:FlashMessagesService,
    private restService:RestService,
    private router:Router,
    private modalService: BsModalService,
    private deviceManager: DeviceManagerService
  ) { }

  ngOnInit() {
    console.log(">>>> on init");
    this.filter = '';
    //this.loadCategories();
    this.advancedModeEnabled = false;
    this.advancedControlEnabled = false;
    this.advanceControlCategories = ['PWM', 'RGB'];
    this.advancedFunctions = [this.ADV_FUNC_UPLOAD, this.ADV_FUNC_REMOVE, this.ADV_FUNC_OTHER];
    this.selectedAdvFunc = this.advancedFunctions[0];
    this.deviceManager.loadDevices();
  }

  loadDepsForSelectedDevice(address : string, category : string) :void {
    this.selectedDeviceName = this.selectedDevice.name;
    this.refreshCommandValues();
    this.loadDeviceDetails();
  }


  loadDevices(category: Category, selectDevice : Device) {
    console.log('loadDevices for cat ' + category.name);
    this.filter = '';
    this.selectedCategory = category;

    if (this.advancedControlEnabled &&
      this.selectedCategory &&
      this.advanceControlCategories.indexOf(this.selectedCategory.name) < 0) {
      this.advancedControlEnabled = false;
    }

    this.filteredDevicesList = new Array<Device>();

    for (var dev of this.deviceManager.getDevicesInCategory(category)) {
      this.filteredDevicesList.push(new Device(dev));
    }

    if (selectDevice) {
      for (var dev of this.filteredDevicesList) {
        if (dev.address == selectDevice.address && dev.category == selectDevice.category) {
          this.selectDevice(dev);
          break;
        }
      }

    } else {
      this.selectDevice(this.filteredDevicesList[0])
    }
  }

  selectDevice(dev : Device) : void {
    this.selectedDevice = dev;
    this.loadDepsForSelectedDevice(this.selectedDevice.address, this.selectedDevice.category);
  }


  loadDeviceDetails() :void {
    console.log('loadDeviceDetails');

    this.restService.getDeviceDetails(this.selectedDevice).subscribe(data => {

      console.log('data: ', data);
      console.log('>>> err code: ', data.errorCode);
      if(data.errorCode){

        this.flashMessage.show('Loading device details failed: ' + data.result, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else {
        this.selectedDevice.lastSeen = new Date(data.lastSeen);
        this.selectedDevice.resetCounter = data.resetCounter;
        this.selectedDevice.wdtCounter = data.wdtCounter;
        this.selectedDevice.lastStartup = null;
        if (data.lastStartup != 0) {
          this.selectedDevice.lastStartup = new Date(data.lastStartup);
        }
      }
    });
  }

  onSaveName(): void {
    if (this.selectedDevice != undefined) {
      this.selectedDevice.name = this.selectedDeviceName;
      console.log('set device name: ' + this.selectedDevice.name);
    }
    console.log('dev cat: ', this.selectedDevice['category']);
    this.restService.setDeviceName(this.selectedDevice).subscribe(data => {

        console.log('data: ', data);
        console.log('>>> err code: ', data.errorCode);
        if(data.errorCode){

          this.flashMessage.show('Setting device name failed: ' + data.result, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else {
          this.flashMessage.show('Device name saved successfully!', {
            cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        }
      }
    );
  }

  loadParameters() :void {
    console.log('dev cat: ', this.selectedDevice['category']);

    this.restService.getDeviceParameters(this.selectedDevice).subscribe(data => {

      console.log('data: ', data);
      console.log('>>> err code: ', data.errorCode);
      if(data.errorCode){
        this.flashMessage.show('Loading device parameters failed: ' + data.result, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else {
        for (var param = 0; param < data.parameters.length; param++) {
          if (data.parameters[param].defined) {
            //this.selectedDevice[data.parameters[param].parameter] = data.parameters[param].value;
            var paramName: string = data.parameters[param].parameter;
            var paramValue: number = data.parameters[param].value;
            this.selectedDevice.parameters[paramName] = paramValue;
          } else {
            //this.selectedDevice[data.parameters[param].parameter] = 'undefined';
            var paramName: string = data.parameters[param].parameter;
            this.selectedDevice.parameters[paramName] = 'undefined';
          }
        }
      }
    });
  }

  makeCommand(command :string) :void {
    console.log('>>>> make command ' + command);

    this.restService.setDeviceParameter(
      //this.selectedDevice,command, this.commandValues[command]).subscribe(data => {
      this.selectedDevice,command, this.selectedDevice.commands[command]).subscribe(data => {
        //data = this.convertFromBufferToJson(data);
        console.log('data: ', data);
        console.log('>>> err code: ', data.errorCode);
        if(data.errorCode){

          this.flashMessage.show('Setting device parameter failed: ' + data.result, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else {
          this.flashMessage.show('Device parameter set successfully!', {
            cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        }
      }
    );
  }

  refreshCommandValues() :void {

    this.selectedDevice.commands = new Object();
    this.hexValues = new Object();
    for(var command of this.selectedCategory['commands']) {
      this.selectedDevice.commands[command] = 0;
      this.hexValues[command] = 0;
    }

    this.selectedDevice.parameters = new Object();
    for(var param of this.selectedCategory['parameters']) {
      this.selectedDevice.parameters[param] = 0;
    }
  }



  onDecValChange(value, command) : void {
    console.log('>>>> DEC: ', value);
    if (!isNaN(value)) {
      var hex = Number(value).toString(16).toUpperCase();
      if (hex != this.hexValues[command]) {
        this.hexValues[command] = hex;
      }
    } else {
      this.hexValues[command] = 0;
    }
  }

  onHexValChange(value, command) : void {
    console.log('>>>> HEX: ', value);
    var dec = parseInt(value, 16);
    if (!isNaN(dec)) {
      if (dec != this.selectedDevice.commands[command]) {
        this.selectedDevice.commands[command] = dec;
      }
    } else {
      this.selectedDevice.commands[command] = 0;
    }
  }

  clearFilters() {
    this.filter = '';
    this.filteredDevicesList = new Array<Device>();

    for (var dev of this.deviceManager.getAllDevices()) {
      this.filteredDevicesList.push(dev);
    }

    this.selectedDevice = this.deviceManager.getDevicesInCategory(this.selectedCategory)[0];
    this.loadDepsForSelectedDevice(this.selectedDevice.address, this.selectedDevice.category);

  }

  getDeviceByCatAndAdr(adr : string, cat : string) : Device {
    return this.deviceManager.getDevice(cat, adr);
  }

  onFilterChange() : void {
    var vals = this.filter.toLowerCase().split(' ');
    this.filteredDevicesList = new Array<Device>();

    for (var dev of this.deviceManager.getDevicesInCategory(this.selectedCategory)) {
      var allValsMatch : Boolean = true;
      var addrMatch : Boolean = false;
      var guidMatch : Boolean = false;
      for (var val of vals) {
        if (dev.name.indexOf(val) === -1) {
          allValsMatch = false;
        }

        if (dev.address == val) {
          addrMatch = true;
        }

        if (dev.uid.toString(16).toLowerCase() == val.toLowerCase()) {
          guidMatch = true;
        }
      }

      if (allValsMatch || addrMatch || guidMatch) {
        this.filteredDevicesList.push(dev);
      }
    }


    if (this.filteredDevicesList.length > 0) {
      this.selectedDevice = this.filteredDevicesList[0];
      this.loadDepsForSelectedDevice(this.selectedDevice.address, this.selectedDevice.category);
    } else {
      this.selectedDevice = undefined;
      this.selectedDeviceName = '';
    }

  }

  onKeySearch() : void {
    this.searchResults = new Array<Device>();

    if (!this.keysearch) {
      return;
    }

    var vals = this.keysearch.toLowerCase().split(' ');

    for (var dev of this.deviceManager.getAllDevices()) {
      var allValsMatch : Boolean = true;
      var addrMatch : Boolean = false;
      var guidMatch : Boolean = false;
      for (var val of vals) {
        if (dev.name.indexOf(val) === -1) {
          allValsMatch = false;
        }

        if (dev.uid.toString(16).toLowerCase() == val.toLowerCase()) {
          guidMatch = true;
        }

        if (val.indexOf(':') > 0) {
          var catAdr = val.split(':');
          if (catAdr[0] == dev.category && catAdr[1] == dev.address) {
            addrMatch = true
          }
        }
      }

      if (allValsMatch || addrMatch || guidMatch) {
        this.searchResults.push(dev);
      }
    }

    if (this.searchResults.length == 1) {
      this.onSearchResultClick(this.searchResults[0]);
    }
  }

  clearKeySearch() : void {
    this.keysearch = '';
    this.searchResults = [];

  }

  onSearchResultClick(selectedDevice : Device) : void {
    console.log('>>> selected result: ' + selectedDevice.name);
    for (var cat of this.deviceManager.getAllCategories()) {
      if (cat.id == selectedDevice.category) {
        this.selectedCategory = cat;
        this.loadDevices(cat, selectedDevice);
        break;
      }
    }
  }

  onChange(): void {
    this.loadDepsForSelectedDevice(this.selectedDevice.address, this.selectedDevice.category);
  }

  removeDevice(): void {
    console.log('>>>> remove device: ', this.selectedDevice);

    this.restService.removeDevice(this.selectedDevice.uid).subscribe(data => {

        console.log('data: ', data);
        console.log('>>> err code: ', data.errorCode);
        if(data.errorCode){

          this.flashMessage.show('Removing device failed: ' + data.result, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else {
          this.flashMessage.show('Device removed', {
            cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        }
      }
    );
  }

  bootDevice(): void {
    console.log('>>>> boot device: ', this.selectedDevice);

    this.restService.bootDevice(this.selectedDevice.uid).subscribe(data => {

        console.log('data: ', data);
        console.log('>>> err code: ', data.errorCode);
        if(data.errorCode){

          this.flashMessage.show('Setting device into boot mode failed: ' + data.result, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else {
          this.flashMessage.show('Device set into boot mode', {
            cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        }
      }
    );
  }

  showPopup(request: string) {
    // this.popupRequest = request;
    this.bsModalRef = this.modalService.show(DeviceAdvanceCommandComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef.content.parentController = this;
    this.bsModalRef.content.popupRequest = request;
  }


  loadFilesList() {
    this.restService.getFilesList().subscribe(data => {

      console.log('data: ', data);
      console.log('>>> err code: ', data.errorCode);
      if(data.errorCode){

        this.flashMessage.show('Loading files list failed: ' + data.result, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
        this.router.navigate(['home']);
      } else {
        this.filesList = data.files;
        if (this.filesList !== undefined && this.filesList.length > 0) {
          this.selectedFile = this.filesList[0];
        }
      }
    });
  }

  onSwitchModeChange() {
    if (this.filesList === undefined) {
      this.loadFilesList();
    }

  }

  onSwitchControlChange() {

  }

  setAdvFunc(func: string) {
    this.selectedAdvFunc = func;
  }

  uploadFirmware() {
    console.log('>>> upload firmware');

    this.restService.uploadFirmware(this.selectedFile).subscribe(data => {

        console.log('data: ', data);
        console.log('>>> err code: ', data.errorCode);
        if(data.errorCode){

          this.flashMessage.show('Uploading firmware failed: ' + data.result, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else {
          this.flashMessage.show('Uploading firmware started', {
            cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        }
      }
    );
  }

  removeDevices() {
    console.log('>>>> remove devices');
    var catId = this.selectedCatAdv.id;
    console.log('>>>> cat id: ' + catId);

    this.restService.removeDeviceAdvance(catId, this.removeAdrBegin, this.removeAdrEnd).subscribe(data => {

        console.log('data: ', data);
        console.log('>>> err code: ', data.errorCode);
        if(data.errorCode){

          this.flashMessage.show('Removing devices failed: ' + data.result, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else {
          this.flashMessage.show('Devices removed', {
            cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        }
      }
    );
  }

  initDevices() {
    console.log('>>>> init devices');
    this.restService.initDevices().subscribe(data => {

        console.log('data: ', data);
        console.log('>>> err code: ', data.errorCode);
        if(data.errorCode){

          this.flashMessage.show('Initializing devices failed: ' + data.result, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else {
          this.flashMessage.show('Devices initialized successfully', {
            cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        }
      }
    );
  }

  showAdvanceControlSwitch() {
    return this.selectedCategory && this.advanceControlCategories.indexOf(this.selectedCategory['name']) >= 0;
  }

  checkCategorySelected(cat : string) {
    return this.selectedCategory['name'] == cat;
  }


}


@Component({
  selector: 'modal-content',
  styleUrls: ['./device-manager.component.css'],
  template: `
    <div class="modal-header">
      <h4 class="modal-title pull-left modal-header-custom">WARNING</h4>
    </div>
    <div class="modal-body modal-body-custom">
    <div class="modal-content-center">
        Do you really want to {{popupRequest}} device?
    </div>
    <div modal-input-section class="modal-content-center mt-3">
        <button type="button" class="btn btn-danger wide-button mr-2" onclick="this.blur();" (click)="onPopupCancel()">No</button>
        <button type="button" class="btn btn-success" onclick="this.blur();"  (click)="onPopupConfirm()">Yes</button>
    </div>
    </div>
  `
})

export class DeviceAdvanceCommandComponent implements OnInit {

  parentController : DeviceManagerComponent;
  popupRequest: string;

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit() {  }

  onPopupConfirm() : void {
    if (this.popupRequest == this.parentController.BOOT_REQUEST) {
      this.parentController.bootDevice();
    } else if (this.popupRequest == this.parentController.REMOVE_REQUEST) {
      this.parentController.removeDevice();
    }
    this.bsModalRef.hide();
  }

  onPopupCancel() : void {
    this.bsModalRef.hide();
  }
}
