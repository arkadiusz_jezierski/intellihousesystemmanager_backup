/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LivingRoomWallEffectComponent } from './living-room-wall-effect.component';

describe('LivingRoomWallEffectComponent', () => {
  let component: LivingRoomWallEffectComponent;
  let fixture: ComponentFixture<LivingRoomWallEffectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivingRoomWallEffectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivingRoomWallEffectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
