import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsWidgetsListComponent } from './actions-widgets-list.component';

describe('ActionsWidgetsListComponent', () => {
  let component: ActionsWidgetsListComponent;
  let fixture: ComponentFixture<ActionsWidgetsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionsWidgetsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsWidgetsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
