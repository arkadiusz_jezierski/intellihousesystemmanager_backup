import { Component, OnInit } from '@angular/core';
import { ActionWidget } from "../../models/action-widget";
import { WidgetAction } from "../../models/widget-action";
import { BackupService } from "../../services/backup.service";
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-backup-restore',
  templateUrl: './backup-restore.component.html',
  styleUrls: ['./backup-restore.component.css']
})
export class BackupRestoreComponent implements OnInit {

  downloadData;
 
  fileToUpload: File = null;
  uploadJson = null;
  selectedFileName;
  dataSets;

  constructor(
    private backupService : BackupService,
    private flashMessage:FlashMessagesService
  ) {  }

  ngOnInit() {
    this.selectedFileName = 'Choose file';
    this.dataSets = [
      'actionWidgetsGroups',
      'actionWidgets',
      'widgetActions',
      'devices',
      'basicConfigs',
      'deviceConfigs',
    ] 
  }

  onFileSelect(files: FileList) {
    this.fileToUpload = files.item(0);
    if (this.fileToUpload) {
      this.selectedFileName = this.fileToUpload.name;
    }
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      console.log(fileReader.result);
      this.uploadJson = fileReader.result;
     
    }
    fileReader.readAsText(this.fileToUpload);
  }

  loadData() {
    if (this.uploadJson) {
      var obj;
      try {
        obj = JSON.parse(this.uploadJson);
      } catch (ex) {
          this.flashMessage.show('Uploading data failed: ' + ex, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
      }
      var dataName = Object.keys(obj)[0];
      this.backupService.saveData(dataName, obj).subscribe((data) => {

        console.log('data: ', data);
        //console.log('>>> err code: ', data.errorCode);
        if (data['success'] === false){
          this.flashMessage.show('Uploading data failed: ' + data['result'], {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});

        } else if (data['success'] === true) {
          this.flashMessage.show('\'' + dataName + '\' data loaded successfully', {
            cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        }
      },(error) => {
        console.log('>>> error: ', error.message);
        this.flashMessage.show('Uploading data failed: ' + error.message, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
      })
    } else {
      this.flashMessage.show('No file selected', {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
    }
     
  }



  saveData(name) {
    this.backupService.getData(name).subscribe(data => {

      console.log('data: ', data);
      //console.log('>>> err code: ', data.errorCode);
      if (data['success'] === false){
        this.flashMessage.show('Downloading data failed: ' + data['result'], {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});


      } else if (data['success'] === true) {
        if (data[name]) {
          var backupData = {};
          backupData[name] = data[name];
          var stream = JSON.stringify(backupData);
          const blob = new Blob([stream], { type: 'application/octet-stream' });
          var url = window.URL.createObjectURL(blob);
          var a = document.createElement('a');
          document.body.appendChild(a);
          a.setAttribute('style', 'display: none');
          a.href = url;
          a.download = name + '.json';
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove(); 
        }

      }
    });
  }


}
