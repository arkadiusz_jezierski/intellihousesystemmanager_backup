import { Component, OnInit, OnChanges, SimpleChange } from '@angular/core';
import { Options } from 'ng5-slider';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DeviceManagerService } from '../../services/device-manager.service';
import { DeviceConsts } from '../../utils/deviceConsts';
import { RestService } from '../../services/rest.service';
import { Device } from '../../models/device';


@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.css']
})
export class ColorPickerComponent implements OnInit {

  retPage : string;
  devices :Device[];

  redValue : number;
  greenValue : number;
  blueValue : number;

  redSliderOptions: Options;
  greenSliderOptions: Options;
  blueSliderOptions: Options;
  speedSliderOptions: Options;

  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private deviceManager: DeviceManagerService,
    private restService:RestService
  ) { }

  ngOnInit() {
    this.devices = new Array();

    this.redSliderOptions = new Options();
    this.redSliderOptions.floor = 0;
    this.redSliderOptions.ceil = 0xfff;
    this.greenSliderOptions = new Options();
    this.greenSliderOptions.floor = 0;
    this.greenSliderOptions.ceil = 0xfff;
    this.blueSliderOptions = new Options();
    this.blueSliderOptions.floor = 0;
    this.blueSliderOptions.ceil = 0xfff;

    this.route.params.subscribe(params => {
      this.retPage = params['ret'];
      console.log('>>>> params: ', params);
      this.redValue = params['r'];
      this.greenValue = params['g'];
      this.blueValue = params['b'];

      var addresses = params['adr'].split(',');
      for (var adr of addresses) {
        this.devices.push(this.deviceManager.getDevice(params['cat'], adr));
      }
      console.log('>>> devs: ', this.devices);
      // this.device = this.deviceManager.getDevice(params['cat'], params['adr']);

    });

  }



  goBack() {
    this.router.navigate(['/' + this.retPage]);
  }

  setDeviceParameter(dev: Device, param : string, val: number) : void {
    this.restService.setDeviceParameter(dev, param, val).subscribe(data => {
      if(data.errorCode){
        console.log('Setting device parameter failed: ' + data.result);
      } else {
        console.log('Device parameter set successfully!');
      }
    });
  }

  onUserChange(param, event) {
    for (var dev of this.devices) {
      this.setDeviceParameter(dev, param, event.value);
    }
  
  }

  onColorPickerChange(event) {
    var color = JSON.parse(event);
    this.redValue = color.red * 0xf;
    this.greenValue = color.green * 0xf;
    this.blueValue = color.blue * 0xf;

    var rgb = (this.redValue * 0x1000000) + (this.greenValue * 0x1000) + this.blueValue;

    for (var dev of this.devices) {
      this.setDeviceParameter(dev, DeviceConsts.PARAM_RGB, rgb);
    }

  }

}
