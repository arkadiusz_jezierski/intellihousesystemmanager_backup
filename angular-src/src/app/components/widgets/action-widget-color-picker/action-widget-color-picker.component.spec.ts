import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionWidgetColorPickerComponent } from './action-widget-color-picker.component';

describe('ActionWidgetColorPickerComponent', () => {
  let component: ActionWidgetColorPickerComponent;
  let fixture: ComponentFixture<ActionWidgetColorPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionWidgetColorPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWidgetColorPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
