import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { ActionWidget } from "../../../models/action-widget";
import { WidgetAction } from "../../../models/widget-action";
import { WidgetActionSetup } from "../../../models/widget-action-setup";
import { RequestedFields } from "../../../models/widget-action-setup";
import { DeviceConsts } from '../../../utils/deviceConsts';
import { WIDGET_EVENT_RGB} from "../../../models/consts";
import { DeviceManagerService } from '../../../services/device-manager.service';
import { Options } from 'ng5-slider';
import { Subscription } from "rxjs";
import { TimerObservable } from "rxjs/observable/TimerObservable";

@Component({
  selector: 'app-action-widget-rgb-slider',
  templateUrl: './action-widget-rgb-slider.component.html',
  styleUrls: ['./action-widget-rgb-slider.component.css']
})
export class ActionWidgetRgbSliderComponent implements OnInit {

  @Output()
  nameClicked: EventEmitter<ActionWidget> = new EventEmitter();

  @Output()
  onAction: EventEmitter<WidgetAction> = new EventEmitter();

  @Output()
  onActionSetup: EventEmitter<WidgetActionSetup> = new EventEmitter();

  @Input() widget : ActionWidget;
  @Input() editMode : boolean;

  redValue : number;
  greenValue : number;
  blueValue : number;

  redSliderOptions: Options;
  greenSliderOptions: Options;
  blueSliderOptions: Options;

  actionMap : Object;

  setRgbMode : boolean = true;
  rgbModeSubscription: Subscription;

  constructor(
    private deviceManager:DeviceManagerService
  ) { }

  ngOnInit() {
    this.redSliderOptions = new Options();
    this.redSliderOptions.floor = 0;
    this.redSliderOptions.ceil = 0xfff;
    this.greenSliderOptions = new Options();
    this.greenSliderOptions.floor = 0;
    this.greenSliderOptions.ceil = 0xfff;
    this.blueSliderOptions = new Options();
    this.blueSliderOptions.floor = 0;
    this.blueSliderOptions.ceil = 0xfff;
    this.redValue = 0;
    this.greenValue = 0;
    this.blueValue = 0;

    if (this.widget && this.widget.actions) {
      this.actionMap = {};
      for (var action of this.widget.actions) {
        if (!this.actionMap[action.widgetEvent]) {
          this.actionMap[action.widgetEvent] = [];
        }
        this.actionMap[action.widgetEvent].push(action);
      }
    }
  }

  ngOnDestroy() {
    this.unsubscribeRgbModeSubscription();
  }

  unsubscribeRgbModeSubscription() {
    if (this.rgbModeSubscription) {
      this.rgbModeSubscription.unsubscribe();
    }
  }

  startRgbModeSubscription() {
    let timer = TimerObservable.create(1000,100000);
    this.rgbModeSubscription = timer.subscribe(t => {
      this.setRgbMode = true;
    });
  }

  onUserChange(param, event) {
    if (this.editMode) {
      var setup : WidgetActionSetup = new WidgetActionSetup(
        new RequestedFields(false, true, false, false),
        DeviceConsts.DEV_CATEGORY_RGB,
        DeviceConsts.PARAM_RGB,
        this.widget.key,
        WIDGET_EVENT_RGB
      );

      this.onActionSetup.emit(setup);
    } else {
      var actions = this.actionMap[WIDGET_EVENT_RGB];
      if (actions) {
        for (var action of actions) {
          if (this.setRgbMode) {
            var helpAction = new WidgetAction();
            helpAction.devCategory = action.devCategory;
            helpAction.devAddress = action.devAddress;
            helpAction.devCommand = DeviceConsts.PARAM_MODE;
            helpAction.devCommandValue = 4;
            this.onAction.emit(helpAction);
            this.setRgbMode = false;
          }

          var rgb = (this.redValue * 0x1000000) + (this.greenValue * 0x1000) + this.blueValue;
          action.devCommandValue = rgb;
          this.onAction.emit(action);
        }

        this.unsubscribeRgbModeSubscription();
        this.startRgbModeSubscription();

      }
    }

  }

  onNameClicked() {
    if (this.editMode) {
      this.nameClicked.emit(this.widget);
    }
  }



}
