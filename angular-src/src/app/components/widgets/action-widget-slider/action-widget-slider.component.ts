import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { ActionWidget } from "../../../models/action-widget";
import { WidgetAction } from "../../../models/widget-action";
import { WidgetActionSetup } from "../../../models/widget-action-setup";
import { RequestedFields } from "../../../models/widget-action-setup";
import { WIDGET_EVENT_SLIDER, WIDGET_DETAILS_SLIDER_MAX, WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC, WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR } from "../../../models/consts";
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-action-widget-slider',
  templateUrl: './action-widget-slider.component.html',
  styleUrls: ['./action-widget-slider.component.css']
})
export class ActionWidgetSliderComponent implements OnInit {

  @Output()
  nameClicked: EventEmitter<ActionWidget> = new EventEmitter();

  @Output()
  onAction: EventEmitter<WidgetAction> = new EventEmitter();

  @Output()
  onActionSetup: EventEmitter<WidgetActionSetup> = new EventEmitter();

  @Input() widget : ActionWidget;
  @Input() editMode : boolean;

  value : number;

  sliderOptions: Options;

  actionMap : Object;

  showSliderGraphic : boolean;
  graphicColor : string;
  maxSliderValue : number;


  constructor() { }

  ngOnInit() {
    var widgetDetails;
    var maxSlider;
    if (this.widget.details) {
      widgetDetails = JSON.parse(this.widget.details);
      this.showSliderGraphic = widgetDetails[WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC] ? widgetDetails[WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC] : false;
      this.maxSliderValue = widgetDetails[WIDGET_DETAILS_SLIDER_MAX] ? widgetDetails[WIDGET_DETAILS_SLIDER_MAX] : 255;
      this.graphicColor = widgetDetails[WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR] ? widgetDetails[WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR] : "ffffff";
    }

    this.sliderOptions = new Options();
    this.sliderOptions.floor = 0;
    this.sliderOptions.ceil = this.maxSliderValue;

    this.value = 0;

    if (this.widget && this.widget.actions) {
      this.actionMap = {};
      for (var action of this.widget.actions) {
        if (!this.actionMap[action.widgetEvent]) {
          this.actionMap[action.widgetEvent] = [];
        }
        this.actionMap[action.widgetEvent].push(action);
      }
    }
  }

  onUserChange(param, event) {
    if (this.editMode) {
      var setup : WidgetActionSetup = new WidgetActionSetup(
        new RequestedFields(true, true, true, false),
        null,
        null,
        this.widget.key,
        WIDGET_EVENT_SLIDER
      );

      this.onActionSetup.emit(setup);
    } else {
      var actions = this.actionMap[WIDGET_EVENT_SLIDER];
      if (actions) {
        for (var action of actions) {
          action.devCommandValue = this.value;
          this.onAction.emit(action);
        }

      }
    }

  }

  onNameClicked() {
    if (this.editMode) {
      this.nameClicked.emit(this.widget);
    }
  }

  getPaddingStyle() {
    if (this.showSliderGraphic) {
      return '';
    } else {
      return '50px';
    }
  }

  getIconStyle() : string {
    var red = 255;
    var green = 255;
    var blue = 255;
    if (this.graphicColor.length == 6) {
      red = parseInt('0x' + this.graphicColor.substr(0, 2));
      green = parseInt('0x' + this.graphicColor.substr(2, 2));
      blue = parseInt('0x' + this.graphicColor.substr(4, 2));
    }
    
    var red = (this.value * red) / this.maxSliderValue;
    var green = (this.value * green) / this.maxSliderValue;
    var blue = (this.value * blue) / this.maxSliderValue;

    return 'rgb(' + red +',' + green + ',' + blue + ')';
  }

}
