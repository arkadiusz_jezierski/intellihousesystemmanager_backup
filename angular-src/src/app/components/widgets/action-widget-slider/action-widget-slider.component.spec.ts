import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionWidgetSliderComponent } from './action-widget-slider.component';

describe('ActionWidgetSliderComponent', () => {
  let component: ActionWidgetSliderComponent;
  let fixture: ComponentFixture<ActionWidgetSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionWidgetSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWidgetSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
