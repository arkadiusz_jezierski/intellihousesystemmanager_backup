import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { ActionWidget } from "../../../models/action-widget";
import { WidgetAction } from "../../../models/widget-action";
import { WidgetActionSetup } from "../../../models/widget-action-setup";
import { RequestedFields } from "../../../models/widget-action-setup";
import {
  WIDGET_EVENT_BUTTON_1,
  WIDGET_EVENT_BUTTON_2,
  WIDGET_EVENT_BUTTON_3,
  WIDGET_EVENT_BUTTON_4,
  WIDGET_DETAILS_BUTTON_NAME,
  WIDGET_DETAILS_BUTTON_COLOR,
  WIDGET_DETAILS_BUTTON_SETTINGS
} from "../../../models/consts";
import { DeviceManagerService } from '../../../services/device-manager.service';
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-action-widget-buttons',
  templateUrl: './action-widget-buttons.component.html',
  styleUrls: ['./action-widget-buttons.component.css']
})
export class ActionWidgetButtonsComponent implements OnInit {

  @Output()
  nameClicked: EventEmitter<ActionWidget> = new EventEmitter();

  @Output()
  onAction: EventEmitter<WidgetAction> = new EventEmitter();

  @Output()
  onActionSetup: EventEmitter<WidgetActionSetup> = new EventEmitter();

  @Input() widget : ActionWidget;
  @Input() editMode : boolean;


  actionMap : Object;
  btnMap : {};
  buttonNames;
  buttonColors;


  constructor( ) { }

  ngOnInit() {
    

    this.btnMap = { 1 : WIDGET_EVENT_BUTTON_1, 2 : WIDGET_EVENT_BUTTON_2, 3 : WIDGET_EVENT_BUTTON_3, 4 : WIDGET_EVENT_BUTTON_4};
    this.buttonNames = ['1', '2', '3', '4'];
    this.buttonColors = [false, false, false, false];
    if (this.widget.details) {
      var widgetDetails = JSON.parse(this.widget.details);
     	var buttonSettings = widgetDetails[WIDGET_DETAILS_BUTTON_SETTINGS] ? widgetDetails[WIDGET_DETAILS_BUTTON_SETTINGS] : [];
     	for (var i = 0; i < buttonSettings.length; i++) {
     		this.buttonNames[i] = buttonSettings[i][WIDGET_DETAILS_BUTTON_NAME] ? buttonSettings[i][WIDGET_DETAILS_BUTTON_NAME] : '' + (i + 1);
     		this.buttonColors[i] = (buttonSettings[i][WIDGET_DETAILS_BUTTON_COLOR] != undefined) ? buttonSettings[i][WIDGET_DETAILS_BUTTON_COLOR] : false ;
     	}
    }

    if (this.widget && this.widget.actions) {
      this.actionMap = {};
      for (var action of this.widget.actions) {
        if (!this.actionMap[action.widgetEvent]) {
          this.actionMap[action.widgetEvent] = [];
        }
        this.actionMap[action.widgetEvent].push(action);
      }
    }
  }

 	onClick(btnId) {

    if (this.editMode) {
      var setup : WidgetActionSetup = new WidgetActionSetup(
        new RequestedFields(true, true, true, true),
        null,
        null,
        this.widget.key,
        this.btnMap[btnId]
      );

      this.onActionSetup.emit(setup);
    } else {
      var actions = this.actionMap[this.btnMap[btnId]];
      if (actions) {
        for (var action of actions) {
          this.onAction.emit(action);
        }

      }
    }
 	}

  onNameClicked() {
  	if (this.editMode) {
   	 	this.nameClicked.emit(this.widget);
  	}
  }

}
