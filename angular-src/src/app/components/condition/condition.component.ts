import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OperationManagerService } from '../../services/operation-manager.service';
import {Operation, Condition, Action, DelayedOperation, DeviceCondition} from '../../models/operation';
import { Device } from '../../models/device';
import { Category } from '../../models/category';
import {EParamConditionType, EStartStopConditionType, SRC_PAGE_COND_EDIT} from '../../models/consts';
import { DeviceManagerService } from '../../services/device-manager.service';
import { BuilderService } from '../../services/builder.service';
import { ValidateService } from '../../services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Utils } from '../../utils/utils';
import { Router } from '@angular/router';

@Component({
  selector: 'app-condition',
  templateUrl: './condition.component.html',
  styleUrls: ['./condition.component.css']
})
export class ConditionComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private operationManager: OperationManagerService,
    private router:Router,
    private deviceManager:DeviceManagerService,
    private builder:BuilderService,
    private validator: ValidateService,
    private flashMessage:FlashMessagesService,

  ) { }

  conditionId: number;
  operation: Operation;
  condition: Object;
  types: string[];
  logicalConditions: string[];
  internalCondType: EStartStopConditionType;
  private sub: any;

  ngOnInit() {

    this.loadConditionTypes();
    this.loadLogicalConditions();

    this.sub = this.route.params.subscribe(params => {
      this.conditionId = params['id'];
      this.operation = this.operationManager.tmpOperation;
      let cloneOption = params['clone'];
      if (this.conditionId > 10) {
        var tmpCond: Condition = this.operationManager.getOperationConditionById(this.operation, this.conditionId);
        this.internalCondType = tmpCond.internalType;
        if (cloneOption == 0) {
          this.condition = tmpCond.toJson();
        } else {
          this.conditionId = Number(this.internalCondType);
          this.condition = new Object();
          this.condition['type'] = tmpCond.type;
          this.condition['param'] = tmpCond['param'];
          this.condition['condition'] = tmpCond['condition'];
          this.condition['value'] = tmpCond.value;
          if (tmpCond['device']) {
            this.condition['category'] = (tmpCond as DeviceCondition).device.category;
            this.condition['address'] = (tmpCond as DeviceCondition).device.address;
          }

        }

      } else {
        this.condition = new Object();
      }


    });
  }


  getParamsForSelectedCategory() :string[] {
    if (this.condition['category']) {
      return this.deviceManager.getCategoryById(this.condition['category']).parameters;
    }

    return new Array<string>();

  }

  loadConditionTypes() {
    this.types = new Array<string>();
    this.types.push('device');
    this.types.push('time');
    this.types.push('alarm');
    //for (var type in EConditionType) {
    //  var isValueProperty = parseInt(type, 10) >= 0;
    //  if (isValueProperty) {
    //    this.types.push(EConditionType[type]);
    //  }
    //}
  }

  loadLogicalConditions() {
    this.logicalConditions = new Array<string>();
    this.logicalConditions.push('equal');
    this.logicalConditions.push('greater');
    this.logicalConditions.push('greater_equal');
    this.logicalConditions.push('less');
    this.logicalConditions.push('less_equal');
    this.logicalConditions.push('any');
    this.logicalConditions.push('rising');
    this.logicalConditions.push('falling');
    this.logicalConditions.push('not_equal');
  }

  getDevicesForSelectedCategory() :Device[] {
    if (this.condition['category']) {
      var devs :Device[] = this.deviceManager.devicesList.filter((item)=>item.category == this.condition['category']);
      console.log('>>>> devices: ', devs);
      return devs;
    }
    return new Array<Device>();
  }

  onConditionSave() {

    var res: Object = new Object();
    var condType : EStartStopConditionType;

    if (this.conditionId == 0) {
      condType = EStartStopConditionType.regular;
      this.condition['extId'] = this.generateExtendedConditionId(this.operation.conditions);
    } else if (this.conditionId == 1) {
      condType = EStartStopConditionType.start;
      this.condition['extId'] = this.generateExtendedConditionId((this.operation as DelayedOperation).startConditions());
    } else if (this.conditionId == 2) {
      condType = EStartStopConditionType.stop;
      this.condition['extId'] = this.generateExtendedConditionId((this.operation as DelayedOperation).stopConditions());
    }

    if (this.validator.validateCondition(this.condition, res)) {
      if (this.conditionId < 3) {
        this.operation.conditions.push(this.builder.createCondition(this.condition, this.deviceManager, condType));
      } else {
        this.operationManager.setOperationConditionById(
          this.operation,
          this.conditionId,
          this.builder.createCondition(
            this.condition,
            this.deviceManager,
            this.internalCondType
          )
        );
      }

      this.router.navigate(['/operation', this.operation.id, SRC_PAGE_COND_EDIT]);

    } else {
      this.flashMessage.show(res['error'], {
        cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
        timeout: 5000
      });
    }

  }

  onCancel() {
    this.router.navigate(['/operation', this.operation.id, SRC_PAGE_COND_EDIT]);
  }

  generateExtendedConditionId(conditions : Condition[]) : string{
    return new Utils().getLetterByIndex(conditions.length);
  }

}
