import { Component, Input, OnInit, OnChanges, SimpleChange } from '@angular/core';
import { Options } from 'ng5-slider';
import { Device } from '../../models/device';
import { DeviceConsts } from '../../utils/deviceConsts';
import { RestService } from '../../services/rest.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { RadialColorPickerComponent } from '../radial-color-picker/radial-color-picker.component'

@Component({
  selector: 'app-device-advance-control-rgb',
  templateUrl: './device-advance-control-rgb.component.html',
  styleUrls: ['./device-advance-control-rgb.component.css']
})
export class DeviceAdvanceControlRgbComponent implements OnInit {

  @Input()  device: Device;

  redValue : number;
  greenValue : number;
  blueValue : number;
  speedValue : number;
  mode : number;
  modesList : string[];

  redSliderOptions: Options;
  greenSliderOptions: Options;
  blueSliderOptions: Options;
  speedSliderOptions: Options;

  constructor (
    private restService:RestService,
    private flashMessage:FlashMessagesService
  ) {}

  ngOnInit() {
    this.redSliderOptions = new Options();
    this.redSliderOptions.floor = 0;
    this.redSliderOptions.ceil = 0xfff;
    this.greenSliderOptions = new Options();
    this.greenSliderOptions.floor = 0;
    this.greenSliderOptions.ceil = 0xfff;
    this.blueSliderOptions = new Options();
    this.blueSliderOptions.floor = 0;
    this.blueSliderOptions.ceil = 0xfff;
    this.speedSliderOptions = new Options();
    this.speedSliderOptions.showTicksValues = true;
    this.speedSliderOptions.stepsArray = [
      {value: 0},
      {value: 1},
      {value: 2},
      {value: 3}
    ];

    this.modesList = ['1','2','3','4','5','6'];

  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    console.log('>>> change: ', changes);
    if (changes.device && changes.device.currentValue && changes.device.currentValue.category != '7') {
      return;
    }
    this.loadDeviceParamsValue();
  }

  onUserChange(param, event) {
    this.restService.setDeviceParameter(
      this.device, param, event.value).subscribe(data => {
        //data = this.convertFromBufferToJson(data);
        console.log('data: ', data);
        console.log('>>> err code: ', data.errorCode);
        if(data.errorCode){

          this.flashMessage.show('Setting device parameter failed: ' + data.result, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else {
          console.log('>> success');
        }
      }
    );
  }


  loadDeviceParamsValue() :void {
    this.restService.getDeviceParameters(this.device).subscribe(data => {

      console.log('adv rgb data: ', data);
      console.log('>>> err code: ', data.errorCode);
      if(data.errorCode){
        this.flashMessage.show('Loading device parameters failed: ' + data.result, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else {
        var allParamsDefined = true;
        for (var param = 0; param < data.parameters.length; param++) {
          if (!data.parameters[param].defined) {
            allParamsDefined = false;
          } else {
            if (data.parameters[param]['parameter'] == DeviceConsts.PARAM_RED) {
              this.redValue = data.parameters[param]['value'];
            }
            if (data.parameters[param]['parameter'] == DeviceConsts.PARAM_GREEN) {
              this.greenValue = data.parameters[param]['value'];
            }
            if (data.parameters[param]['parameter'] == DeviceConsts.PARAM_BLUE) {
              this.blueValue = data.parameters[param]['value'];
            }
            if (data.parameters[param]['parameter'] == DeviceConsts.PARAM_SPEED) {
              this.speedValue = data.parameters[param]['value'];
            }
            if (data.parameters[param]['parameter'] == DeviceConsts.PARAM_MODE) {
              this.mode = data.parameters[param]['value'];
            }
          }
        }

        if (!allParamsDefined) {
          this.flashMessage.show('Some device params are undefined, component may not work properly', {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
        }
      }
    });
  }

  onColorPickerChange(event) {
    var color = JSON.parse(event);
    this.redValue = color.red * 0xf;
    this.greenValue = color.green * 0xf;
    this.blueValue = color.blue * 0xf;

    var rgb = (this.redValue * 0x1000000) + (this.greenValue * 0x1000) + this.blueValue;

    this.restService.setDeviceParameter(
      this.device, DeviceConsts.PARAM_RGB, rgb).subscribe(data => {
        //data = this.convertFromBufferToJson(data);
        console.log('>>> err code: ', data.errorCode);
        if(data.errorCode){

          this.flashMessage.show('Setting device parameter failed: ' + data.result, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else {
          console.log('>> success');
        }
      }
    );
  }

  setMode(mode) {
    this.restService.setDeviceParameter(
      this.device, DeviceConsts.PARAM_MODE, mode).subscribe(data => {
        //data = this.convertFromBufferToJson(data);
        console.log('data: ', data);
        console.log('>>> err code: ', data.errorCode);
        if(data.errorCode){

          this.flashMessage.show('Setting device parameter failed: ' + data.result, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else {
          console.log('>> success');
          this.mode = mode;
        }
      }
    );
  }

  isModeActive(mode) {
    return mode == this.mode;
  }

}
