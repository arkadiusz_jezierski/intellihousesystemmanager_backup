/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DeviceManagerService } from './device-manager.service';

describe('DeviceManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeviceManagerService]
    });
  });

  it('should ...', inject([DeviceManagerService], (service: DeviceManagerService) => {
    expect(service).toBeTruthy();
  }));
});
