import { Injectable } from '@angular/core';
import { ToolsService } from './tools.service';
import { Utils } from '../utils/utils';
import { ActionGroup } from '../models/action-group';
import { ActionWidget } from '../models/action-widget';
import { WidgetAction } from '../models/widget-action';
import { FlashMessagesService } from 'angular2-flash-messages';


@Injectable({
  providedIn: 'root'
})
export class ActionsWidgetsService {

  actionGroups : ActionGroup[];
  selectedActionGroup : ActionGroup;


  constructor(
  	private toolsService : ToolsService,
  	private flashMessage: FlashMessagesService
  ) { }

  createNewActionGroup(name : string) {
  	let key = new Utils().generateRandom();
  	this.toolsService.createUpdateActionWidgetsGroup(key, name).subscribe(data => {

      console.log('data: ', data);
      //console.log('>>> err code: ', data.errorCode);
      if (data['success'] === false){
        this.flashMessage.show('Creating action widgets group failed: ' + data['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else if (data['success'] === true) {
        this.flashMessage.show('New action widgets group created', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      }
    });

  }


  updateActionGroup(actionGroup : ActionGroup, callback) {
    this.toolsService.createUpdateActionWidgetsGroup(actionGroup.key, actionGroup.name).subscribe(data => {

      console.log('data: ', data);
      callback(data);
  
    });

  }

  loadActionGroups() {

    this.toolsService.getActionWidgetsGroups().subscribe(data => {

      console.log('data: ', data);
      //console.log('>>> err code: ', data.errorCode);
      if (data['success'] === false){

        this.flashMessage.show('Loading action widgets groups failed: ' + data['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else if (data['success'] === true) {
        this.actionGroups = new Array<ActionGroup>();

        for (var act of data['actionGroups']) {
          this.actionGroups.push(new ActionGroup(act.key, act.name));
        }

        console.log('>>>> loaded actions: ', this.actionGroups);

      }
    });

  }

  deleteActionWidgetsGroup(groupKey : number, callback) {

    this.toolsService.deleteActionWidgetsGroup(groupKey).subscribe(data => {

      console.log('data: ', data);
      //console.log('>>> err code: ', data.errorCode);
      if (data['success'] === false){
        this.flashMessage.show('Delete action widgets group failed: ' + data['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else if (data['success'] === true) {
        this.flashMessage.show('Action widgets group deleted', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      }
      callback();
    });
  }

  getActionWidgetsGroup(key : number, callback) {

    this.toolsService.getActionWidgetsGroup(key).subscribe(data => {

      console.log('data: ', data);
      //console.log('>>> err code: ', data.errorCode);
      if (data['success'] === false){

        this.flashMessage.show('Loading action widgets group failed: ' + data['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else if (data['success'] === true) {
        this.selectedActionGroup = data['group'];
        callback(this.selectedActionGroup);
      }
    });

  }

  createNewWidget(groupKey : number, type : string, active : boolean, name : string, details : string, callback) {
    if (groupKey == undefined) {
      this.flashMessage.show('Creating widget failed - group key cannot be empty', {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      return;
    }
    let key = new Utils().generateRandom();
    this.toolsService.createUpdateActionWidget(key, groupKey, type, active, name, details).subscribe(data => {

      console.log('data: ', data);
      //console.log('>>> err code: ', data.errorCode);
      callback(data);
    });

  }


  updateWidget(widget : ActionWidget, callback) {
    this.toolsService.createUpdateActionWidget(widget.key, widget.groupKey, widget.type, widget.active, widget.name, widget.details).subscribe(data => {

      console.log('data: ', data);
      callback(data);
  
    });

  }

  createNewWidgetAction(action : WidgetAction, callback) {
    if (action.widgetKey == undefined) {
      this.flashMessage.show('Creating widget action failed - widget key cannot be empty', {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      return;
    }
    action.key = new Utils().generateRandom();
    this.toolsService.createUpdateWidgetAction(action.key, action.widgetKey, action.widgetEvent, action.devCategory, action.devAddress, 
        action.devCommand, action.devCommandValue).subscribe(data => {

      console.log('data: ', data);
      //console.log('>>> err code: ', data.errorCode);
      callback(data);
    });

  }

  updateWidgetAction(action : WidgetAction, callback) {
    this.toolsService.createUpdateWidgetAction(action.key, action.widgetKey, action.widgetEvent, action.devCategory, action.devAddress, 
        action.devCommand, action.devCommandValue).subscribe(data => {

      console.log('data: ', data);
      callback(data);
  
    });

  }

  // loadWidgetsForGroup(groupKey : number) {
  //
  //   this.toolsService.getActionWidgetsForGroup(groupKey).subscribe(data => {
  //
  //     console.log('data: ', data);
  //     //console.log('>>> err code: ', data.errorCode);
  //     if (data['success'] === false){
  //
  //       this.flashMessage.show('Loading widgets failed: ' + data['msg'], {
  //         cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
  //         timeout: 5000});
  //     } else if (data['success'] === true) {
  //       this.actionWidgets = new Array<ActionWidget>();
  //
  //       for (var widget of data['widgets']) {
  //         this.actionWidgets.push(new ActionWidget(widget.key, widget.groupKey, widget.type, widget.active, widget.details));
  //       }
  //
  //       console.log('>>>> loaded widgets: ', this.actionWidgets);
  //
  //     }
  //   });
  //
  // }

  deleteActionsWidget(key : number, callback) {

    this.toolsService.deleteActionWidget(key).subscribe(data => {

      console.log('data: ', data);
      //console.log('>>> err code: ', data.errorCode);
      callback(data);
    });
  }

  getWidget(key : number, callback) {

    this.toolsService.getActionWidget(key).subscribe(data => {

        console.log('data: ', data);
        //console.log('>>> err code: ', data.errorCode);
        if (data['success'] === false){

          this.flashMessage.show('Loading widget failed: ' + data['msg'], {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else if (data['success'] === true) {
          var tmp = data['widget'];
          var widget = new ActionWidget(tmp.key, tmp.groupKey, tmp.type, tmp.active, tmp.name, tmp.details);
          widget.actions = new Array<WidgetAction>();
          for (var act of tmp.actions) {
            var action = new WidgetAction();
            action.key = act.key;
            action.widgetKey = act.widgetKey;
            action.widgetEvent = act.widgetEvent;
            action.devCategory = act.devCategory;
            action.devAddress = act.devAddress;
            action.devCommand = act.devCommand;
            action.devCommandValue = act.devCommandValue;
            widget.actions.push(action);
          }
          // callback(data['widget']);
          callback(widget);
        }
    });
  }

  deleteWidgetAction(key : number, callback) {

    this.toolsService.deleteWidgetAction(key).subscribe(data => {

      console.log('data: ', data);
      //console.log('>>> err code: ', data.errorCode);
      callback(data);
    });
  }

}
