import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Device } from '../models/device';
import { Category } from '../models/category';
import { RestService } from './rest.service';
import { FlashMessagesService } from 'angular2-flash-messages';

import 'rxjs/add/operator/map';

@Injectable()
export class DeviceManagerService {

  constructor(
    private http: Http,
    private restService: RestService,
    private flashMessage: FlashMessagesService
  ) { }

  devicesList: Device[];
  categoriesList: Category[];

  loadDevices(callback?: () => void) {

    this.loadCategories();

    if (this.devicesList) {
      if (callback) {
        callback();
      }
      return;
    }

    this.restService.getDevicesList().subscribe(data => {

      console.log('data: ', data);
      console.log('>>> err code: ', data.errorCode);
      if(data.errorCode){

        this.flashMessage.show('Loading devices failed: ' + data.result, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else {
        this.devicesList = new Array<Device>();

        for (var dev of data.devices) {
          this.devicesList.push(new Device(dev));
        }

        if (callback) {
          callback();
        }
      }
    });

  }

  getDevice(category: any, address: any): Device {
    if (this.devicesList) {
      for (var dev of this.devicesList) {
        if (dev.address == address && dev.category == category) {
          return dev;
        }
      }
    }

    return new Device({'category': category, 'address': address, 'name': 'notFound'});
  }

  getAllDevices() : Device[] {
    return this.devicesList;
  }

  getDevicesInCategory(cat : Category) : Device[] {
    let devList = new Array<Device>();

    if (cat) {
      for (let dev of this.devicesList) {
        if (dev.category == cat.id) {
          devList.push(dev);
        }
      }
    }

    return devList;
  }

  getCategoryById(categoryId: any): Category {
    if (typeof (categoryId) == "number") {
      categoryId = categoryId.toString();
    }
    for (var cat of this.categoriesList) {
      if (cat.id == categoryId) {
        return cat;
      }
    }


    return null;
  }

  loadCategories() {
    if (this.categoriesList) {
      return;
    }

    this.categoriesList = new Array<Category>();
    this.restService.getCategories().subscribe(data => {

      console.log('data: ', data);
      console.log('>>> err code: ', data.errorCode);
      if (data.errorCode) {

        this.flashMessage.show('Loading categories failed: ' + data.result, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000
        });
      } else {

        for (var i = 0; i < data.categories.length; i++) {
          this.categoriesList.push(new Category(
            data.categories[i]['id'].toString(),
            data.categories[i]['name'],
            data.categories[i]['commands'],
            data.categories[i]['parameters']
          ));
        }
      }
    });
  }

  getAllCategories() : Category[] {
    return this.categoriesList;
  }

  //getDevicesInCategory(category: number) :Device[]{
  //  var devsCat: Device[] = new Array<Device>();
  //
  //  console.log('<<<< devices list: ', this.devicesList);
  //  for (var dev of this.devicesList) {
  //    if (dev.category == category) {
  //      devsCat.push(dev);
  //    }
  //  }
  //
  //  return devsCat;
  //}

}
