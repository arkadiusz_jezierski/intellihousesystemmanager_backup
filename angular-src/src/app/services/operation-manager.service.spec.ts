/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { OperationManagerService } from './operation-manager.service';

describe('OperationManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OperationManagerService]
    });
  });

  it('should ...', inject([OperationManagerService], (service: OperationManagerService) => {
    expect(service).toBeTruthy();
  }));
});
