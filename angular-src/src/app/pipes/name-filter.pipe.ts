import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameFilter'
})
export class NameFilterPipe implements PipeTransform {

  transform(items: any[], filter: string): any {
    if (!items || !filter) {
      return items;
    }

    return items.filter(item => {
      var vals = filter.toLowerCase().split(' ');

      for (var val of vals) {
        if (item.name.toLowerCase().indexOf(val) === -1) {
          return false;
        }
      }
      return true;
    });
  }

}
