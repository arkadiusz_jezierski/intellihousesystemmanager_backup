import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hexTransform'
})
export class HexTransformPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (isNaN(value)) {
      return 'NaN';
    } else {
      return value.toString(16).toUpperCase();
    }
  }

}
