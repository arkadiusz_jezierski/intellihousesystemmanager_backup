export class Category {
  id: string;
  name: string;
  commands: string[];
  parameters: string[];

  constructor(id: string, name: string, commands: string[], params: string[]) {
    this.id = id;
    this.name = name;
    this.commands = commands;
    this.parameters = params;
  }

}
