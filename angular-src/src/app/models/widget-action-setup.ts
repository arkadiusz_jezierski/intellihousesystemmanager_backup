
export class WidgetActionSetup {
  fieldRequest : RequestedFields;
  defaultCategory : string;
  defaultCommand : string;
  widgetKey : number;
  widgetEvent : string;

  constructor(fieldRequest : RequestedFields,
              defaultCategory : string,
              defaultCommand : string,
              widgetKey : number,
              widgetEvent : string
  ) {
    this.fieldRequest = fieldRequest;
    this.defaultCategory = defaultCategory;
    this.defaultCommand = defaultCommand;
    this.widgetKey = widgetKey;
    this.widgetEvent = widgetEvent;
  }

}

export class RequestedFields {
  devCategory : boolean;
  devAddress : boolean;
  devCommand : boolean;
  devCommandValue : boolean;

  constructor(cat, adr, com, val) {
    this.devCategory = cat;
    this.devAddress = adr;
    this.devCommand = com;
    this.devCommandValue = val;
  }
}
