//client for intellihouse system REST server

const http = require('http');
const config = require('../config/rest');

module.exports.setDeviceParameter = function(devCat, devAddr, param, val, res) {
    if (!(isNumber(devCat) && isNumber(devAddr) && isNumber(val))) {
        res.send({
            "errorCode":100,
            "result":"Invalid type of parameters in setDeviceParameter method" +
            "{devCat:" + devCat + ", devAddr:" + devAddr + ", val:" + val + "}"
        });

        return;
    }

    args = {
        "requestName":"setDeviceParameter",
        "device":{
            "category":Number(devCat),
            "address":Number(devAddr)},
        "parameter":param,
        "value":Number(val)
    };

    this.makeRequest(args, res);
}

module.exports.getDeviceParameter = function(devCat, devAddr, param, res) {
    if (!(isNumber(devCat) && isNumber(devAddr))) {
        res.send({
            "errorCode":100,
            "result":"Invalid type of parameters in getDeviceParameter method" +
            "{devCat:" + devCat + ", devAddr:" + devAddr + "}"
        });

        return;
    }

    args = {
        "requestName":"getDeviceParameter",
        "device":{
            "category":Number(devCat),
            "address":Number(devAddr)},
        "parameter":param
    };

    this.makeRequest(args, res);
}

module.exports.getJson = function(jsonId, res) {

    args = {
        "requestName":"getJson",
        "id":jsonId
    };

    this.makeRequest(args, res);
}

module.exports.saveJson = function(jsonId, jsonBody, res) {
    args = {
        "requestName":"saveJson",
        "id":jsonId,
        "json":jsonBody
    };

    this.makeRequest(args, res);
}

module.exports.setDeviceName = function(devCat, devAddr, devName, res) {
    if (!(isNumber(devCat) && isNumber(devAddr))) {
        res.send({
            "errorCode":100,
            "result":"Invalid type of parameters in setDeviceName method" +
            "{devCat:" + devCat + ", devAddr:" + devAddr + "}"
        });

        return;
    }

    args = {
        "requestName":"setDeviceName",
        "device":{
            "category":Number(devCat),
            "address":Number(devAddr)},
        "name":devName
    };

    this.makeRequest(args, res);
}

module.exports.getDevicesList = function(res) {
    this.makeRequest({"requestName":"getDevicesList"}, res);
}

module.exports.getFilesList = function(res) {
    this.makeRequest({"requestName":"getFilesList"}, res);
}

module.exports.uploadFirmware = function(fileName, res) {
    args = {
        "requestName":"uploadFirmware",
        "fileName":fileName
    };

    this.makeRequest(args, res);
}

module.exports.bootDevice = function(uid, res) {
    if (!isNumber(uid)) {
        res.send({
            "errorCode":100,
            "result":"Invalid type of parameters in bootDevice method" +
            "{uid:" + uid + "}"
        });

        return;
    }

    args = {
        "requestName":"bootDevice",
        "uid":Number(uid)
    };

    this.makeRequest(args, res);
}

module.exports.initDevices = function(res) {
    this.makeRequest({"requestName":"initDevices"}, res);
}

module.exports.removeDevice = function(uid, res) {
    if (!isNumber(uid)) {
        res.send({
            "errorCode":100,
            "result":"Invalid type of parameters in removeDevice method" +
            "{uid:" + uid + "}"
        });

        return;
    }

    args = {
        "requestName":"removeDevice",
        "uid":Number(uid)
    };

    this.makeRequest(args, res);
}

module.exports.removeDeviceAdvance = function(cat, addrStart, addrStop, res) {
    if (!(isNumber(cat) && isNumber(addrStart) && isNumber(addrStop))) {
        res.send({
            "errorCode":100,
            "result":"Invalid type of parameters in removeDeviceAdvance method" +
            "{cat:" + cat + ", addrStart:" + addrStart + ", addrStop:" + addrStop + "}"
        });

        return;
    }

    args = {
        "requestName":"removeDeviceAdvance",
        "category":Number(cat),
        "adrStart":Number(addrStart),
        "adrStop":Number(addrStop)
    };

    this.makeRequest(args, res);
}

module.exports.getDeviceDetails = function(devCat, devAddr, res) {
    if (!(isNumber(devCat) && isNumber(devAddr))) {
        res.send({
            "errorCode":100,
            "result":"Invalid type of parameters in getDeviceDetails method" +
            "{devCat:" + devCat + ", devAddr:" + devAddr + "}"
        });

        return;
    }

    args = {
        "requestName":"getDeviceDetails",
        "device":{
            "category":Number(devCat),
            "address":Number(devAddr)}
    };

    this.makeRequest(args, res);
}

module.exports.getDeviceParameters = function(devCat, devAddr, res) {
    if (!(isNumber(devCat) && isNumber(devAddr))) {
        res.send({
            "errorCode":100,
            "result":"Invalid type of parameters in getDeviceParameters method" +
            "{devCat:" + devCat + ", devAddr:" + devAddr + "}"
        });

        return;
    }

    args = {
        "requestName":"getDeviceParameters",
        "device":{
            "category":Number(devCat),
            "address":Number(devAddr)}
    };

    this.makeRequest(args, res);
}

module.exports.getCategories = function(res) {
    this.makeRequest({"requestName":"getCategories"}, res);
}

module.exports.sendMessage = function(number, message, res) {
    args = {
        "requestName":"sendMessage",
        "number":number,
        "message":message
    };

    this.makeRequest(args, res);
}

module.exports.armSystem = function(armDisarm, hash, res) {
    if (!isNumber(armDisarm)) {
        res.send({
            "errorCode":100,
            "result":"Invalid type of parameters in armSystem method" +
            "{armDisarm:" + armDisarm + "}"
        });

        return;
    }

    args = {
        "requestName":"armSystem",
        "armDisarm":Number(armDisarm),
        "hash":hash
    };

    this.makeRequest(args, res);
}

module.exports.getArmSystemStatus = function(res) {
    this.makeRequest({"requestName":"getArmSystemStatus"}, res);
}



module.exports.makeRequest = function(args, res) {
    jsonObject = JSON.stringify(args);

    var postheaders = {
        'Content-Type' : 'application/json',
        'Content-Length' : Buffer.byteLength(jsonObject, 'utf8')
    };



    var optionspost = {
        host : config.serverHost,
        port : config.serverPort,
        path : '/',
        method : 'POST',
        headers : postheaders
    };

    console.info('Do the POST call: ', jsonObject);

    // do the POST call
    var reqPost = http.request(optionspost, function(resp) {
        // console.log(">>>>>>>>>>>>>>>>>>>>>> statusCode: ", resp.statusCode);
        // console.log('>>>>>>>>>>>>>>>>>>>>>>>> response: ' , resp);
        var str = '';
        resp.on('data', function (chunk) {
            str += chunk;
        });

        resp.on('end', function () {
            // console.log(str);
            res.send(str);
        });
    });

    // write the json data
    reqPost.write(jsonObject);
    reqPost.end();
    reqPost.on('error', function(e) {
        console.error('write request error: ', e);
    });
}

var isNumber = function(arg) {
    return /^-?[\d.]+(?:e-?\d+)?$/.test(arg);
    //return typeof arg == "number" || (typeof arg == "object" && arg.constructor === Number);
}
