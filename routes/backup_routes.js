const express = require('express');
const router = express.Router();
const ActionWidgetsGroup = require('../models/action_widgets_group');
const ActionWidget = require('../models/action_widget');
const WidgetAction = require('../models/widget_action');
const Device = require('../models/device');
const DeviceConfig = require('../models/device_config');
const BasicConfig = require('../models/basic_config');

//get Action Groups
router.get('/actionWidgetsGroups', (req, res, next) => {

    ActionWidgetsGroup.getActionWidgetsGroups((err, groups)=>{

        if(err) {
            res.json({success: false, msg: 'Get action widgets groups error: ' + err.message});
        }

        if(!groups){
            res.json({success: false, msg: 'Groups not found'});
        } else {

            res.json({
                success: true,
                actionWidgetsGroups: groups
            });
        }

    });

});


//get Action Widget
router.get('/actionWidgets', (req, res, next) => {

    let key = req.body.key;

    ActionWidget.getAllActionWidgets((err, widgets)=>{
        if (err) {
            res.json({success: false, msg: 'Get action widget error: '+ err.message});
        }

        if(!widgets){
            res.json({success: false, msg: 'Widget not found'});
        } else {
            res.json({
                success: true,
                actionWidgets: widgets
            });
        }

    });

});


//get all Widget Actions
router.get('/widgetActions', (req, res, next) => {

    WidgetAction.getAllWidgetActions((err, actions)=>{
        if (err) {
            res.json({success: false, msg: 'Get widget actions error: '+ err.message});
        }

        if(!actions){
            res.json({success: false, msg: 'Actions not found'});
        } else {
            res.json({
                success: true,
                widgetActions: actions
            });
        }

    });

});



// get all devices
router.get('/devices', (req, res, next) => {

    Device.getAllDevices((err, devices)=>{
        if (err) {
            res.json({success: false, msg: 'Get devices error: '+ err.message});
        }

        if(!devices){
            res.json({success: false, msg: 'Devices not found'});
        } else {
            res.json({
                success: true,
                devices: devices
            });
        }

    });

});


// get all basic config
router.get('/basicConfigs', (req, res, next) => {

    BasicConfig.getAllBasicConfigs((err, configs)=>{
        if (err) {
            res.json({success: false, msg: 'Get basic configs error: '+ err.message});
        }

        if(!configs){
            res.json({success: false, msg: 'Basic configs not found'});
        } else {
            res.json({
                success: true,
                basicConfigs: configs
            });
        }

    });

});


// get all device configs
router.get('/deviceConfigs', (req, res, next) => {

    DeviceConfig.getAllDeviceConfigs((err, configs)=>{
        if (err) {
            res.json({success: false, msg: 'Get device configs error: '+ err.message});
        }

        if(!configs){
            res.json({success: false, msg: 'Device configs not found'});
        } else {
            res.json({
                success: true,
                deviceConfigs: configs
            });
        }

    });

});


module.exports = router;