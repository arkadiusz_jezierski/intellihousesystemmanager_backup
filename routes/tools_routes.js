const express = require('express');
const router = express.Router();
const ClientLog = require('../models/client_logger');
const ActionWidgetsGroup = require('../models/action_widgets_group');
const ActionWidget = require('../models/action_widget');
const WidgetAction = require('../models/widget_action');
let counter = 0;
let randomItems = [];

//getImage
router.post('/getImage', (req, res, next) => {

    var fs = require('fs');
    var path = '/root/frame-photos';
    var imgPath = '';

    getNextValidImagePath = function(items) {
        var len = items.length;
        var fName = '';
        while(len-- > 0 && fName == '') {
            var index = this.getRandomIndex(items.length);
            if (this.checkValidName(items[index])) {
                fName = items[index];
            }

        }
        return fName;
    }

    checkValidName = function(fileName) {
        var dotPos = fileName.lastIndexOf('.');
        if (dotPos > 0) {
            var ext = fileName.substring(dotPos + 1);

            return (ext.toLowerCase() == 'jpg' || ext.toLowerCase() == 'jpeg');
        }
        return false;
    }

    getRandomIndex  = function(range) {
        if (randomItems.length >= range) {
            randomItems = [];
        }

        var index;
        do {
            index = Math.floor(Math.random()*range);
        } while(randomItems.indexOf(index) >= 0);
        randomItems.push(index);
        return index;
    }

    fs.readdir(path, function(err, items) {
        if (err) {
            res.json({success: false, msg: 'Error: ' + err.message});
            return;
        } else {
            if (items.length > 0) {
                //console.log('>> size: ' + items.length);

                var fName = this.getNextValidImagePath(items);
                imgPath = path + '/' + fName;

                var bitmap = fs.readFileSync(imgPath);
                res.json({success: true, img:Buffer(bitmap).toString('base64')});
            } else {
                res.json({success: false, msg: 'No images in path'});
            }
        }

    });


});


//postLog
router.post('/postLog', (req, res, next) => {

    if (req.body && req.body.log) {

        let logData = new ClientLog({
            log: req.body.log
        });

        ClientLog.insertLog(logData, (err) => {
            if(err){
                res.json({success: false, msg:'Failed to insert new log: ' + err.message});
                return;
            } else {
                // res.json({success: true, msg:'Config ' + config + ' added'});
                res.json({success: true, msg: 'Log received'});
                return;
            }
        });

        console.log('#postLog: key: ', req.body.log);
        return res.json({success: true, msg: 'Log received'});
    } else {
        return res.json({success: false, msg: 'Invalid data received'});
    }
});

//getLatestLogs
router.post('/getLatestLogs', (req, res, next) => {

    let lastLogId = req.body.logId;
    let filter = req.body.filter;

    ClientLog.getLatestLogs(lastLogId, filter, (err, clientLogs)=>{
        if(err) {
            res.json({success: false, msg: 'Latest logs not found: ' + err.message});
            return;
        }
        if(!clientLogs){
            res.json({success: false, msg: 'Logs not found'});
            return;
        } else {
            res.json({
                success: true,
                logs: clientLogs
            });
        }

    });


});

//getOlderLogs
router.post('/getOlderLogs', (req, res, next) => {

    let oldestLogId = req.body.logId;
    let filter = req.body.filter;

    ClientLog.getOlderLogs(oldestLogId, filter, (err, clientLogs)=>{

        if(err) {
            res.json({success: false, msg: 'Older logs not found: ' + err.message});
            return;
        }

        if(!clientLogs){

            res.json({success: false, msg: 'Logs not found'});
            return;
        } else {

            res.json({
                success: true,
                logs: clientLogs
            });
        }

    });
   

});

//getLogsQnty
router.post('/getLogsQnty', (req, res, next) => {

    ClientLog.getLogsQnty((err, clientLogs)=>{

        if(err) {
            res.json({success: false, msg: 'Logs not found: ' + err.message});
            return;
        }

        if(clientLogs == undefined){
            res.json({success: false, msg: 'Logs not found'});
            return;
        } else {
            res.json({
                success: true,
                logSize: clientLogs
            });
        }

    });
   
});

//addUpdateActionWidgetsGroup
router.post('/upsertActionWidgetsGroup', (req, res, next) => {
    if (req.body && req.body.key != undefined && req.body.name != undefined) {

        let group = new ActionWidgetsGroup({
            key: req.body.key,
            name: req.body.name
        });

        ActionWidgetsGroup.addUpdateActionWidgetsGroup(group, (err) => {
            if(err){
                res.json({success: false, msg:'Failed to upsert Action Widgets Group: ' + err.message});
            } else {
                // res.json({success: true, msg:'Config ' + config + ' added'});
                res.json({success: true, msg: 'Action Widgets Group upserted'});
            }
        });

        //return res.json({success: true, msg: 'Action Widgets Group upserted'});
    } else {
        res.json({success: false, msg:'Invalid parameters for upserting Action Widget Group'});
        //return res.json({success: false, msg: 'Invalid data received'});
    }
});


//get Action Groups
router.post('/getActionWidgetsGroups', (req, res, next) => {

    ActionWidgetsGroup.getActionWidgetsGroups((err, groups)=>{

        if(err) {
            res.json({success: false, msg: 'Get action widgets groups error: ' + err.message});
            return;
        }

        if(!groups){
            res.json({success: false, msg: 'Groups not found'});
            return;
        } else {

            res.json({
                success: true,
                actionGroups: groups
            });
        }

    });

});

//get Action Groups
router.post('/getActionWidgetsGroup', (req, res, next) => {

    let groupKey = req.body.key;

    ActionWidgetsGroup.getActionWidgetsGroup(groupKey, (err, groups)=>{

        if(err) {
            res.json({success: false, msg: 'Get action widgets group error: ' + err.message});
            return;
        }

        if(!groups){
            res.json({success: false, msg: 'Group not found'});
            return;
        } else {
            ActionWidget.getActionWidgets(groupKey, (err, widgets)=>{

                if(err) {
                    res.json({success: false, msg: 'Get action widgets error ' + err.message});
                    return;
                }

                if(!widgets) {
                    res.json({success: false, msg: 'Widgets not found'});
                    return;
                } else {
                    var widgetIds = [];
                    for (var widget of widgets) {
                        widgetIds.push(widget.key); 
                    }

                    WidgetAction.getWidgetsActions(widgetIds, (err, actions)=>{
                        if (err) {
                            res.json({success: false, msg: 'Get widget actions error: '+ err.message});
                            return;
                        }

                        if(!actions){
                            res.json({success: false, msg: 'Actions not found'});
                        } else {
                            
                            var response = {
                                "key" : groups[0].key,
                                "name" : groups[0].name,
                                "widgets" : []
                            }

                            for (var widget of widgets) {
                                var responseWidget = {
                                    "key" : widget.key,
                                    "active" : widget.active,
                                    "details" : widget.details, 
                                    "groupKey" : widget.groupKey,
                                    "type" : widget.type,
                                    "name" : widget.name,
                                    "actions" : []
                                };

                                for (var action of actions) {
                                    if (action.widgetKey == widget.key) {
                                        responseWidget.actions.push(action);
                                    }
                                }

                                response.widgets.push(responseWidget);
                            }
                 
                            res.json({
                                success: true,
                                group: response
                            });
                        }
                    });
                }

            });
        }

    });

});


//delete Action Widgets Group
router.post('/deleteActionWidgetsGroup', (req, res, next) => {

    let groupKey = req.body.key;

    ActionWidgetsGroup.deleteActionWidgetsGroup(groupKey, (err)=>{

        if(err){
            res.json({success: false, msg:'Failed to remove action group: ' + err.message});
        } else {
            res.json({success: true, msg:'Action widgets group removed successfully'});
        }

    });

});

//addUpdateActionWidget
router.post('/upsertActionWidget', (req, res, next) => {
    if (req.body && req.body.key != undefined && req.body.groupKey != undefined && req.body.type != undefined && req.body.active != undefined) {

        let widget = new ActionWidget({
            key: req.body.key,
            groupKey: req.body.groupKey,
            type: req.body.type,
            active: req.body.active
        });

        if (req.body.details != undefined) {
            widget.details = req.body.details;
        }
        if (req.body.name != undefined) {
            widget.name = req.body.name;
        }

        ActionWidget.addUpdateActionWidget(widget, (err) => {
            console.log('>>> err: ', err);
            if(err){
                res.json({success: false, msg:'Failed to upsert Action Widget: ' + err.message});
            } else {
                // res.json({success: true, msg:'Config ' + config + ' added'});
                res.json({success: true, msg: 'Action Widget upserted'});
            }
        });

        //return res.json({success: true, msg: 'Action Widgets Group upserted'});
    } else {
        res.json({success: false, msg:'Invalid parameters for upserting Action Widget'});
        //return res.json({success: false, msg: 'Invalid data received'});
    }
});


//get Action Widget
router.post('/getActionWidget', (req, res, next) => {

    let key = req.body.key;

    ActionWidget.getActionWidget(key, (err, widget)=>{
        if (err) {
            res.json({success: false, msg: 'Get action widget error: '+ err.message});
            return;
        }

        if(!widget){
            res.json({success: false, msg: 'Widget not found'});
        } else {

            WidgetAction.getWidgetActions(key, (err, actions)=>{
                if (err) {
                    res.json({success: false, msg: 'Get widget actions error: '+ err.message});
                    return;
                }

                if(!actions){
                    res.json({success: false, msg: 'Actions not found'});
                    return;
                } else {
    
                    var response = {
                        "key" : widget[0].key,
                        "active" : widget[0].active,
                        "details" : widget[0].details,
                        "groupKey" : widget[0].groupKey,
                        "type" : widget[0].type,
                        "name" : widget[0].name,
                        "actions" : actions
                    }

                    res.json({
                        success: true,
                        widget: response
                    });
                }
            });
        }

    });

});


//delete Action Widget
router.post('/deleteActionWidget', (req, res, next) => {

    let key = req.body.key;

    ActionWidget.deleteActionWidget(key, (err)=>{

        if(err){
            res.json({success: false, msg:'Failed to remove action widget: ' + err.message});
        } else {
            res.json({success: true, msg:'Action widget removed successfully'});
        }

    });

});

//upsertWidgetAction
router.post('/upsertWidgetAction', (req, res, next) => {
    if (req.body && req.body.key != undefined && req.body.widgetKey != undefined && req.body.widgetEvent != undefined && req.body.devCategory != undefined && req.body.devAddress != undefined) {

        let action = new WidgetAction({
            key: req.body.key,
            widgetKey: req.body.widgetKey,
            widgetEvent: req.body.widgetEvent,
            devCategory: req.body.devCategory,
            devAddress: req.body.devAddress
        });

        if (req.body.devCommand != undefined) {
            action.devCommand = req.body.devCommand;
        }
        if (req.body.devCommandValue != undefined) {
            action.devCommandValue = req.body.devCommandValue;
        }

        WidgetAction.addUpdateWidgetAction(action, (err) => {
            console.log('>>> err: ', err);
            if(err){
                res.json({success: false, msg:'Failed to upsert Widget Action: ' + err.message});
            } else {
                // res.json({success: true, msg:'Config ' + config + ' added'});
                res.json({success: true, msg: 'Widget Action upserted'});
            }
        });

        //return res.json({success: true, msg: 'Action Widgets Group upserted'});
    } else {
        res.json({success: false, msg:'Invalid parameters for upserting Widget Action'});
        //return res.json({success: false, msg: 'Invalid data received'});
    }
});

//delete Widget Action
router.post('/deleteWidgetAction', (req, res, next) => {

    let key = req.body.key;

    WidgetAction.deleteWidgetAction(key, (err)=>{

        if(err){
            res.json({success: false, msg:'Failed to remove widget action: ' + err.message});
        } else {
            res.json({success: true, msg:'Widget action removed successfully'});
        }

    });

});

module.exports = router;
